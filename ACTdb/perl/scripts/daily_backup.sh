#!/bin/sh

# This file will run a backup of your desired MySQL database and
# remove any backups older than 7 days.
#
# If you'd like to preserve backups for longer than a week, like say 
# 2 weeks, then set the '-mtime' value from '+7' to '+14'.

# Use configuration in ~/.my.cnf
/usr/bin/mysqldump --opt --skip-add-locks ACT |
   gzip > /home/webmaster/backups/act_webmaster_`date "+%Y-%m-%d"`.gz
cd /home/webmaster/backups/
/usr/bin/find *.gz -mtime +7 | xargs -n5 rm -f
