#! /usr/bin/perl -w
use strict;
# use CGI;
# use CGI::FormBuilder;
# use CGI::Carp;
# use Spreadsheet::WriteExcel;
#use lib "/home/webmaster/local/lib";
use CGI::NForm::ACT_Local;
use IO::Socket::SSL;
use Mozilla::CA;
use LWP::UserAgent;
use XML::LibXML;
use Encode;

my $bpt_dev_id = $CGI::NForm::ACT::Config{bpt_dev_id};
my $BPT_ID = '3414108'; # Rosencrantz & Guildenstern, 2018
my $BPT_account = 'act_tickets';
# See ../lib/CGI/NForm/ACT/Tickets.pm for an explanation
# of the SSL_verifycn_scheme => 'smtp' option.
IO::Socket::SSL::set_defaults(
  SSL_ca_file => Mozilla::CA::SSL_ca_file(),
  SSL_verify_mode => 'SSL_VERIFY_PEER', # should be SSL_VERIFY_PEER
  SSL_verifycn_scheme => 'smtp');
#my $ua = LWP::UserAgent->new(ssl_opts => { verify_hostname => 0 });
my $ua = LWP::UserAgent->new;
my $response = BPT_api($ua, 'orderlist', id => $bpt_dev_id,
    account => $BPT_account, event_id => $BPT_ID);
if ($response) {
  print "Success\n";
} else {
  print "Failure\n";
}


sub BPT_api {
  my ($ua, $feed, %opts) = @_;
  my $api = 'https://www.brownpapertickets.com/api2';
  my $url = "$api/$feed?" . join('&', map "$_=$opts{$_}", keys %opts);
  my $response = $ua->get($url);
  if (! ($response->is_success)) {
    warn("BPT_api($url) failed:\n  " . $response->message);
    return '';
  }
  return $response->content;
}

