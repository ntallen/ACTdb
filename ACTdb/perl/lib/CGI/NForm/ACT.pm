package CGI::NForm::ACT;

use 5.00503;
use strict;
use CGI::NForm;
use CGI::NForm::ACT_Local;

use AutoLoader qw(AUTOLOAD);
use vars qw($VERSION @ISA);
@ISA = qw(CGI::NForm);

$VERSION = '0.01';
our %Config;
# %Config defines local configuration variables
# It is initialized in CGI::NForm::ACT_Local
sub new {
  my $this = shift;
  my $testing = !( $ENV{SERVER_ADDR} &&
        $ENV{SERVER_ADDR} ne '127.0.0.1' );
  my $self = $this->CGI::NForm::new(
    testing => $testing,
    tmpldir => $Config{tmpldir},
    @_ );
  if ( ref($this) ) {
    for my $key ( qw(auth dbh confirm) ) {
      $self->{$key} = $this->{$key} if $this->{$key};
    }
  }
  return $self;
}

sub AppDef {
  return
    { '*' => 'CGI::NForm::ACT::',
      '' => 'CGI::NForm::ACT::Home'
    };
}

use DBI;

# Invocation:
#   $obj->Connect( @ops );
#   $obj = CGI::NForm::Safety->Connect( @ops );

#  On alberni:
#  dbhost => localhost
# dbname => act_webmaster
# dbuser => act_app
# dbpasswd => k4@7zvx*!

# On Nort's notebook:
# dbhost => 127.0.0.1
# dbname => act
# dbuser => act_app
# dbpasswd => k4@7zvx*!

sub Connect {
  my ( $self, @ops ) = @_;
  $self = $self->new unless ref($self);
  unless ( $self->{dbh} ) {
    $self->{dbh} = DBI->connect(
      "DBI:mysql:host=$Config{dbhost};database=$Config{dbname}",
      $Config{dbuser}, $Config{dbpasswd},
      { PrintError => 0, RaiseError => 1 } );
  }
  $self;
}

sub InitParams {}

1;
__END__

=head1 NAME

CGI::NForm::ACT - ACT Database CGI

=head1 SYNOPSIS

  use CGI::NForm::ACT;
  CGI::NForm::ACT->Process;

=head1 DESCRIPTION

  CGI::NForm::ACT provides a CGI interface to the ACT Database.

=head2 EXPORT

Nothing at all.

=head1 SEE ALSO

  CGI::NForm
  CGI::FormBuilder
  HTML::Template

=head1 AUTHOR

Norton Allen, E<lt>allen@huarp.harvard.eduE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2004 by Norton Allen

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.2 or,
at your option, any later version of Perl 5 you may have available.


=cut

sub create_ticket {
  my ( $self, $Part_ID, $AuthIP, $AuthTime ) = @_;
  my @base = ( 'A' .. 'Z', 'a' .. 'z', '0' .. '9' );
  my $baselen = @base;
  my $key = join '', map $base[int(rand($baselen))], (1..15);
  $self->{dbh}->do(
    'INSERT INTO ACT_AuthKey ( AuthKey, Part_ID, AuthIP, AuthTime )
     VALUES ( ?, ?, ?, ?);', undef,
    $key, $Part_ID, $AuthIP, $AuthTime );
  my $keyno = $self->{dbh}->{mysql_insertid};
  return "${keyno}_$key";
}

#---------------------------------------------------------
# check_key can be called as check_key(EU => 1) if context
# dictates that we should only be accepting existing users
# rather than advancing to the NewFamily page. This is
# handled in /Login.
#---------------------------------------------------------
sub check_key {
  my $self = shift;
  $self->fatal( "Assert failed: No form defined" )
     unless $self->{form};
  $self->fatal( "Assert failed: No dbh defined" )
     unless $self->{dbh};
  return $self if $self->{auth};
  my $form = $self->{form};
  my $Keys = $form->sessionid;
  if ( defined($Keys) && $Keys =~ m/^(\d+)_([a-zA-Z0-9]{15})$/ ) {
    my $Keyno = $1;
    my $Key = $2;
    my ( $AuthKey, $Part_ID, $AuthIP, $AuthTime, $Access ) =
      $self->{dbh}->selectrow_array(
      'SELECT T.AuthKey, T.Part_ID, T.AuthIP, T.AuthTime, U.AccessCode
       FROM ACT_AuthKey AS T LEFT OUTER JOIN ACT_Users AS U
       ON T.Part_ID = U.Part_ID
       WHERE T.Key_ID = ?;', {}, $Keyno );
    if ( $AuthKey ) {
      my $remaddr = $ENV{REMOTE_ADDR} || '';
      my $now = time();
      my $etime = $now - $AuthTime;
      if ( $now >= $AuthTime ) {
        $self->Log("Key $Keyno expired");
      } elsif ( $Key ne $AuthKey ) {
        $self->Log("Key $Keyno != AuthKey: $AuthKey");
      } else {
        if ( $AuthIP ne '' && $AuthIP ne $remaddr ) {
          $self->Log("Warn: Key $Keyno invoked from different IP: $remaddr");
          $AuthIP = '';
        }
        if ( $AuthIP eq '' || $now+1800 > $AuthTime ) {
          # reset inactivity
          $self->{dbh}->do(
            'UPDATE ACT_AuthKey SET AuthTime=?, AuthIP=? WHERE Key_ID=?;',
            {}, $now + 3600, $remaddr, $Keyno );
          $self->Log("Key $Keyno refreshed: $remaddr");
        }
        $self->{auth} = {
          Part_ID => $Part_ID,
          Access => $Access || ''
        };
        return 1;
      }
    } else {
      $self->Log("Key $Keyno Lookup failed");
    }
  } else {
    if ( defined($Keys) && $Keys ne '' ) {
      $self->Log("Keys did not match pattern: '$Keys'");
    }
  }
  return 0 if @_ && $_[0] eq 'NOFWD';
  $form->sessionid(''); # Clear old broken sessionid
  my @xtra;
  if ( $form->submitted ) {
    push @xtra, [
      _submit => $form->submitted,
      _submitted => 1 ];
  }
  $self->Call( '/Login', @xtra, @_ );
  exit(0);
}

# $self->check_passwd( $username, $password )
# Returns True on success (with new auth credentials)
sub check_passwd {
  use Digest::MD5;
  my ( $self, $username, $password ) = @_;
  $username = substr($username, 0, 15);
  my $udef = $self->{dbh}->selectrow_arrayref(
    'SELECT Password, Part_ID, AccessCode
     FROM ACT_Users WHERE Username = ?;',
     {}, $username );
  if ( $udef && @$udef ) {
    my ( $epw, $Part_ID, $Access ) = @$udef;
    my $espw = Digest::MD5::md5_hex($password);
    if ( $espw eq $epw ) {
      my $key = $self->create_ticket( $Part_ID, $ENV{REMOTE_ADDR},
                        time()+600 );
      $self->{auth} = { Part_ID => $Part_ID, Access => $Access };
      $self->{form}->sessionid($key);
      return 1;
    }
  }
  push @{$self->{errors}}, "Invalid Username or Password";
  return 0;
}

# $self->has_role( 1, 'Board' );
# $self->has_role( $Proj_ID, 'Pit' )
sub has_role {
  my ( $self, $Proj_ID, $Role_Name ) = @_;
  my $Part_ID = $self->{auth}->{Part_ID} || return 0;
  my $rv = $self->{dbh}->selectrow_array(
    'SELECT 1 FROM ACT_Role, ACT_Cast
     WHERE ACT_Role.Proj_ID = ? AND
           ACT_Role.Role_Name = ? AND
           ACT_Role.Role_ID = ACT_Cast.Role_ID AND
           ACT_Cast.Part_ID = ?', {},
     $Proj_ID, $Role_Name, $Part_ID );
  return $rv;
}

# $self->has_role2( $Role_ID, $Role_Name )
# Returns true if the authenticated user has the specified
# $Role_Name in the same project as the specified $Role_ID.
# Useful when we are looking at registrations, say, so we
# have the Role_ID for Registration for a project, and we
# want to allow access for the Registrar for that production.
# Also returns true if the user has the specified $Role_Name
# for the Board ( $Proj_ID == $Role_ID == 1 ). Use
# $self->has_role( 1, $Role_Name ) if you only want board
# access.
sub has_role2 {
  my ( $self, $Role_ID, $Role_Name ) = @_;
  my $Part_ID = $self->{auth}->{Part_ID} || return 0;
  my $rv = $self->{dbh}->selectrow_array(
    'SELECT 1 FROM ACT_Cast NATURAL JOIN ACT_Role AS R1, ACT_Role AS R2
     WHERE Part_ID = ? AND
          R1.Role_Name = ? AND
          R1.Proj_ID = R2.Proj_ID AND
          R2.Role_ID IN ( 1, ? )', {},
    $Part_ID, $Role_Name, $Role_ID );
  return $rv;
}

# $self->part_has_role( $Part_ID, $Role_ID );
sub part_has_role {
  my ( $self, $Part_ID, $Role_ID ) = @_;
  my $rv = $self->{dbh}->selectrow_array(
    'SELECT 1 FROM ACT_Cast
     WHERE Role_ID = ? AND Part_ID = ?',
     {}, $Role_ID, $Part_ID );
  return $rv;
}

sub Log {
  my ( $self, $descr, $linkto ) = @_;
  $self->Connect;
  my $P_ID = $self->{auth}->{Part_ID} || 0;
  my $op = $self->{op} || 'NONE';
  my $form = ref($self);
  $linkto ||= '';
  $form =~ s/^.*:://;
  $self->{dbh}->do(
    'INSERT INTO ACT_Log
     ( TStamp, Part_ID, Form, Op, LinkTo, Descr )
     VALUES ( NOW(), ?, ?, ?, ?, ? )',
    {}, $P_ID, $form, $op, $linkto, $descr );
}

sub GetProjText {
  my ($self, $Proj_ID, $Text_Name, $pretext) = @_;
  my $Text = $self->{dbh}->selectrow_array(
    "SELECT IF(ISNULL(T1.Text_Text),IF(ISNULL(T2.Text_Text),'',T2.Text_Text),T1.Text_Text)
      AS Text_Text
     FROM ACT_Text_Keys LEFT JOIN ACT_Text_Text AS T1
      ON ACT_Text_Keys.Text_ID = T1.Text_ID AND T1.Proj_ID = ?
     LEFT JOIN ACT_Text_Text AS T2
      ON ACT_Text_Keys.Text_ID = T2.Text_ID AND T2.Proj_ID = 0
     WHERE Text_Name = ?", {}, $Proj_ID, $Text_Name);
  $Text = "$pretext$Text" if $Text;
  return $Text;
}

sub RegConfirm {
  use Text::Wrap;
  $Text::Wrap::columns = 65;
  my ( $self, $Role_ID, $Part_ID, $Cast_ID, $Old_Status ) = @_;
  my %text;
  
  my $vals = $self->{dbh}->selectrow_hashref( 
    "SELECT Cast_Status, Role_Name,
     DATE_FORMAT(Cancel_100,'%M %D') AS Cancel_100,
     DATE_FORMAT(Cancel_75, '%M %D') AS Cancel_75, ACT_Role.Proj_ID,
     Invoice_ID, Proj_Type, Proj_Name, Role_Fee, Role_Due, Proj_URL,
     Role_BPT_ID
     FROM ACT_Cast, ACT_Role, ACT_Project
     WHERE ACT_Cast.Part_ID = ? AND ACT_Cast.Role_ID = ?
     AND ACT_Cast.Cast_ID = ? AND ACT_Cast.Role_ID = ACT_Role.Role_ID
     AND ACT_Role.Proj_ID = ACT_Project.Proj_ID",
     {}, $Part_ID, $Role_ID, $Cast_ID );
  $self->fatal("No such record!") unless $vals;
  $vals->{FullName} = $self->{dbh}->selectrow_array(
    "SELECT CONCAT(FirstName, ' ', LastName) FROM ACT_Participant WHERE Part_ID = ?",
    {}, $Part_ID );

  $Old_Status = '' unless $Old_Status;
  my $Proj_ID = $vals->{Proj_ID};
  
  $vals->{Event_Phrase} = "$vals->{Role_Name} for the ACT $vals->{Proj_Type} " .
    "'$vals->{Proj_Name}'";
  $text->{auditions} = '';
  $text->{offstage} = '';
  $text->{payment} = '';
  $text->{forms} = '';
  $text->{opening} = $self->GetProjText($Proj_ID,'Opening_Email', '');

  # Decide which verb phrase applies
  if ( $vals->{Cast_Status} eq 'Waitlisted' ) {
    $text->{Verb_Phrase} =
      "$vals->{FullName} has been placed on the waiting list for " .
      "$vals->{Event_Phrase}." . $self->GetProjText($Proj_ID, 'Waitlist_Email', ' ');
    if ( $vals->{Proj_Type} eq 'Show' ) {
      $text->{offstage} = $self->GetProjText($Proj_ID, 'Offstage_Email', '');
      $text->{offstage} .= "\n\n" if $text->{offstage};
    }
    $vals->{Invoice_ID} = 0;
  } elsif ( $vals->{Cast_Status} eq 'Applied' ) {
    $text->{Verb_Phrase} =
      "$vals->{FullName} has applied for $vals->{Event_Phrase}." .
      $self->GetProjText($Proj_ID, 'Moderated_Email', ' ');
  } elsif ( $vals->{Cast_Status} eq 'Withdrawn' ) {
    $text->{Verb_Phrase} =
      "$vals->{FullName} has withdrawn from $vals->{Event_Phrase}." .
      $self->GetProjText($Proj_ID, 'Withdrawn_Email', ' ');
    $vals->{Invoice_ID} = 0;
  } elsif ( $vals->{Cast_Status} eq 'Accepted' ) {
    if ( $Old_Status && $Old_Status eq 'Applied' ) {
      $text->{Verb_Phrase} =
        "We are pleased to inform you that $vals->{FullName} " .
        "has been accepted for $vals->{Event_Phrase}.";
    } elsif ( $Old_Status && $Old_Status eq 'Waitlisted' ) {
      $text->{Verb_Phrase} =
        "We are pleased to inform you that $vals->{FullName} " .
        "has been accepted off of the waiting list for " .
        "$vals->{Event_Phrase}.";
    } else {
      $text->{Verb_Phrase} =
        "$vals->{FullName}'s application for $vals->{Event_Phrase} " .
        "has been accepted.";
    }
    
    $vals->{Use_URL} = 1 if $vals->{Proj_URL};
    if ( $vals->{Role_Name} eq 'Registration' ) {
      # Now decide whether there are auditions
      # Old query seems to be flawed:
      # $vals->{auditions} = $self->{dbh}->selectall_arrayref(
        # "SELECT ACT_Role.Role_ID, Role_Status,
         # DATE_FORMAT(Event_Date, '%W, %b %e'),
         # TIME_FORMAT(Event_Start, '%l:%i %p'),
         # TIME_FORMAT(Event_End, '%l:%i %p'),
         # COUNT(ACT_Cast.Part_ID), Role_Limit
         # FROM ACT_Event NATURAL JOIN ACT_Role
           # LEFT JOIN ACT_Cast
         # ON ACT_Role.Role_ID = ACT_Cast.Role_ID AND
            # ACT_Cast.Cast_Status != ?
         # WHERE ACT_Role.Proj_ID = ? AND Role_Type = ?
         # AND ACT_Role.Role_ID = ACT_Event.GR_ID
         # AND ACT_Event.GR_Type = ?
         # GROUP BY ACT_Role.Role_ID
         # ORDER BY Event_Date, Event_Start",
          # {}, 'Withdrawn', $vals->{Proj_ID}, 'Audition', 'Role' );
      $text->{auditions} = $self->{dbh}->selectrow_array(
          "SELECT COUNT(ACT_Role.Role_ID)
           FROM ACT_Event NATURAL JOIN ACT_Role
           WHERE ACT_Role.Proj_ID = ? AND Role_Type = 'Audition'
           AND ACT_Role.Role_ID = ACT_Event.GR_ID
           AND ACT_Event.GR_Type = 'Role'", {}, $Proj_ID);

      $text->{auditions} = $text->{auditions} ?
        $self->GetProjText($Proj_ID, 'Audition_Text', '') : '';
    }
  }
  $text->{Verb_Phrase} .= "\n\n";

  if ( $vals->{Invoice_ID} ) {
    # need to pre-wrap all these thingies
    my $pyt = $self->GetProjText($Proj_ID, 'Custom_Payment_Text');
    unless ($pyt) {
      $vals->{Role_Due} =~ s/^\s*due by *//;
      $vals->{Role_Due} = ", due by $vals->{Role_Due}"
        if $vals->{Role_Due} ne '';
      $pyt = "The fee is \$$vals->{Role_Fee}$vals->{Role_Due}.";
    }
    $pyt .= "\n";
    # wrap pyt with ' -', '  '
    my $inst = $self->GetProjText($Proj_ID, 'Pay_Instructions', '');
    if ($vals->{Use_URL} ) {
      $inst .= "or on the Production Page at the link below.\n";
    }
    #wrap $inst with ' -', '  '
    my $withdraw = 
      "If you decide to withdraw, please let us know as soon as possible so we can make your space available to another child.\n";
    my $refund = $self->GetProjText($Proj_ID, 'Custom_Refund_Text', '');
    my $norefund = '';
    unless ($refund) {
      my $last;
      if ( defined $vals->{Cancel_100} && $vals->{Cancel_100} ne '' ) {
        $last = $vals->{Cancel_100};
        $refund = 
            "You may withdraw with a full refund before $vals->{'Cancel_100'}";
      }
      if ( defined $vals->{Cancel_75} && $vals->{Cancel_75} ne '' ) {
        $last = $vals->{Cancel_75};
        if ($refund) {
          $refund .= " or ";
        } else {
          $refund = "You may withdraw ";
        }
        $refund .= 
            "with a 75% refund before $vals->{Cancel_75}";
      }
      if ($refund) {
        $refund .= ".\n";
        $norefund = "No refunds are possible after $last.\n";
      }
    }
    $text->{payment} = join '', map( wrap(' -','  ',$_),
      $pyt, $inst, $withdraw, $refund, $norefund), "\n\n";
  }
  
  if ( $vals->{Use_URL} ) {
    $text->{forms} =
        "There are a number of forms and documents on our website " .
        "which you need to download, print and fill out. For more " .
        "information, please visit the Production Page for " .
        "'$vals->{Proj_Name}' at:\n\n" .
        "  $vals->{Proj_URL}\n\n";
  }
  
  $text->{annual_fund} =
    ( $vals->{Cast_Status} eq 'Accepted' ) ?
    $self->GetProjText($Proj_ID, 'Annual_Fund_Text', '') : '';

  $text->{closing} = $self->GetProjText($Proj_ID, 'Closing_Email', '');
  
  $vals->{Message} = wrap('', '', join '', map $text->{$_},
    qw(opening Verb_Phrase offstage payment forms auditions
       annual_fund closing));

  return $vals;
}

sub Grade {
  my ( $self, $ClassYear, $CurYear, $CurMonth ) = @_;
  my $Grade = '?';
  if ( $ClassYear > 0 ) {
    if ( ! $CurYear ) {
      my @dateinfo = localtime();
      $CurYear = $dateinfo[5] + 1900;
      $CurMonth = $dateinfo[4] + 1;
    }
    $Grade = 12 + $CurYear - $ClassYear;
    $Grade++ if $CurMonth > 6;
    if ( $Grade == 0 ) { $Grade = 'K'; }
    elsif ( $Grade < 0 ) { $Grade = 'P'; }
    elsif ( $Grade > 12 ) { $Grade = 'Grad'; }
  }
  return $Grade;
}

# $self->map_cols( \@labels, $arrayref );
# Produces an arrayref of hashrefs.
# Useful for mapping the results of selectall_arrayref
# to something useful for HTML::Template tmpl_loops.
sub map_cols {
  my ( $self, $labels, $aa ) = @_;
  my $ah = [ map {
      my $row = $_;
      my $ref = { map( ( $_ => shift @$row ), @$labels ) };
    } @$aa ];
}

# purge_fam eliminates the specified family record as
# part of a merge operation.
# Called from Fixup.pm and MrgPart.pm
sub purge_fam {
  my ( $self, $Fam_ID, $New_Fam_ID ) = @_;
  my $Part_ID = $self->{dbh}->selectrow_array(
    'SELECT Part_ID FROM ACT_Participant WHERE Fam_ID = ?',
    {}, $Fam_ID );
  if ( $Part_ID ) {
    # Still family members, but cleanup what's left:
    $self->Fixup_Parents( $Fam_ID );
    return;
  }
  if ( $New_Fam_ID && $New_Fam_ID != $Fam_ID ) {
    $self->{dbh}->do(
      'UPDATE ACT_Gift SET Fam_ID = ? WHERE Fam_ID = ?', {},
      $New_Fam_ID, $Fam_ID );
  }
  my $Addr_ID = $self->{dbh}->selectrow_array(
    'SELECT Addr_ID from ACT_Family WHERE Fam_ID = ?',
    {}, $Fam_ID );
  $self->{dbh}->do(
    'DELETE FROM ACT_Family WHERE Fam_ID = ?', {}, $Fam_ID );
  $self->Log( "Purged Family $Fam_ID" );
  
  # Are there any families or participants still using this addr?
  my $fam = $self->{dbh}->selectrow_array(
    'SELECT Fam_ID FROM ACT_Family WHERE Addr_ID = ?',
    {}, $Addr_ID );
  return if $fam;
  $Part_ID = $self->{dbh}->selectrow_array(
    'SELECT Part_ID FROM ACT_Participant WHERE Addr_ID = ?',
    {}, $Addr_ID );
  return if $Part_ID;
  $self->{dbh}->do(
    'DELETE FROM ACT_Address WHERE Addr_ID = ?', {}, $Addr_ID );
  $self->Log( "Purged Address $Addr_ID" );
}

# FLsearch() is the search invoked from AdminReg and Fixup.
sub FLsearch {
  my ( $self, $showall, %extra ) = @_;
  my $form = $self->{form};
  my $FirstName = $form->field('FirstName');
  my $LastName = $form->field('LastName');
  # Gather all the data that matches this search
  # and initialize template parameters
    
  # List all participants in all families where there is any participant
  # with the specified first or last name
  # Families: Fam_ID FamilyName HomePhone Address Members Emergency Contacts
  # Members: Part_ID FirstName LastName Adult Age Grade Address Roles
    
  my @cond_args;
  my $cond_phrase = '';
  if ( $FirstName ) {
    $cond_phrase .= " FirstName LIKE ? AND";
    push( @cond_args, $FirstName );
  }
  if ( $LastName ) {
    $cond_phrase .= " LastName LIKE ? AND";
    push( @cond_args, $LastName );
  }
  my $showallstmnt = $showall ? '' :
    " AND Proj_Status != 'Archive'";
  my $fams = $self->{dbh}->selectall_arrayref(
    "SELECT ACT_Family.Fam_ID, FamilyName, HomePhone, Address1, Address2,
      City, State, Zip, ACT_Family.Addr_ID, EmergencyContact1,
      EmergencyPhone1, EmergencyContact2, EmergencyPhone2
    FROM ACT_Participant, ACT_Family, ACT_Address
    WHERE$cond_phrase
     ACT_Participant.Fam_ID = ACT_Family.Fam_ID AND
     ACT_Family.Addr_ID = ACT_Address.Addr_ID
    GROUP BY Fam_ID
    ORDER By FamilyName;", {}, @cond_args );
  my @Fam_IDs = map $_->[0], @$fams;
  if ( @Fam_IDs ) {
    my $Fam_IDQ = join ", ", map "?", @Fam_IDs;
    my $stmnt =
      "SELECT ACT_Participant.Part_ID, ACT_Participant.Fam_ID, FirstName, LastName,
        STRCMP(Status,'Child') AS Adult,
        IF(Birthdate>0,YEAR(NOW())-YEAR(Birthdate)-(MID(NOW(),6,5)<MID(Birthdate,6,5)),
        '?') AS Age, ClassYear, YEAR(CURDATE()), MONTH(CURDATE()), Email,
        Address1, Address2, City, State, Zip, WorkPhone, CellPhone,
        ACT_Participant.Addr_ID, HomePhone, Username, Birthdate
      FROM ACT_Participant NATURAL LEFT JOIN ACT_Address
        LEFT JOIN ACT_Users ON ACT_Participant.Part_ID = ACT_Users.Part_ID
      WHERE ACT_Participant.Fam_ID IN ( $Fam_IDQ )
      ORDER BY ACT_Participant.Fam_ID, Adult, Age DESC";
    my $members = $self->{dbh}->selectall_arrayref( $stmnt, {}, @Fam_IDs );
    my $rstmnt =
      "SELECT Fam_ID, ACT_Cast.Part_ID, CONCAT(  CASE Cast_Status
                 WHEN 'Applied' THEN 'AP: '
                 WHEN 'Accepted' THEN ''
                 WHEN 'Waitlisted' THEN 'WL: '
                 WHEN 'Withdrawn' THEN 'WD: ' END,
                 Role_Name, ' for ', Proj_Name )
       FROM ACT_Cast, ACT_Role, ACT_Project, ACT_Participant
       WHERE ACT_Cast.Role_ID = ACT_Role.Role_ID AND
         ACT_Role.Proj_ID = ACT_Project.Proj_ID AND
         ACT_Cast.Part_ID = ACT_Participant.Part_ID AND
         Fam_ID IN ( $Fam_IDQ ) $showallstmnt
       ORDER BY ACT_Role.Proj_ID, ACT_Role.Role_ID, Fam_ID, ACT_Cast.Part_ID";
    my $roles = $self->{dbh}->selectall_arrayref( $rstmnt, {}, @Fam_IDs );
    $self->ContactParams( $fams, $members, $roles, 0, \%extra );
  } else {
    $self->error( "No matches found" );
  }
}

# Takes three arrayrefs of arrayrefs
# $self->ContactParams( $fams, $members, $roles );
# $fams => [ [ Fam_ID FamilyName HomePhone Address1 Address2 City State Zip Addr_ID
#    EmergencyContact1 EmergencyPhone1 EmergencyContact2 EmergencyPhone2 ] ];
# $members => [ [Part_ID Fam_ID FirstName LastName Adult Age ClassYear CurYear CurMonth
#    Email Address1 Address2 City State Zip WorkPhone CellPhone Addr_ID HomePhone
#    Username Birthdate ] ];
# $roles => [ [Fam_ID Part_ID Role] ];
sub ContactParams {
  my ( $self, $fams, $members, $roles, $exinfo, $flags ) = @_;
  my $form = $self->{form};
  $fams = $self->map_cols( [
    qw(Fam_ID FamilyName HomePhone Address1 Address2 City State Zip Addr_ID
    EmergencyContact1 EmergencyPhone1 EmergencyContact2 EmergencyPhone2) ], $fams );
  my %fams;
  for my $fam ( @$fams ) {
    $fams{$fam->{Fam_ID}} = $fam;
    $fam->{Members} = [];
    $fam->{Contact} = join "<br>", grep $_,
      $fam->{HomePhone} ? "h: $fam->{HomePhone}" : '',
      join( ": ", grep $_, $fam->{EmergencyContact1}, $fam->{EmergencyPhone1} ),
      join( ": ", grep $_, $fam->{EmergencyContact2}, $fam->{EmergencyPhone2} );
    $fam->{Address} = format_addr( $fam );
    if ( $flags ) {
      for my $key ( keys %$flags ) {
        $fam->{$key} = $flags->{$key};
      }
    }
  }
  $members = $self->map_cols(
    [ qw(Part_ID Fam_ID FirstName LastName Adult Age ClassYear CurYear CurMonth
         Email Address1 Address2 City State Zip WorkPhone CellPhone Addr_ID
         HomePhone Username Birthdate) ],
    $members );
  $roles = $self->map_cols( [qw(Fam_ID Part_ID Role)], $roles );
    
  # Now go through the members and add info to the Members element of $fams
  for my $part ( @$members ) {
    my $Fam_ID = $part->{Fam_ID};
    my $Part_ID = $part->{Part_ID};
    my $same_addr = $part->{Addr_ID} && $fams{$Fam_ID}->{Addr_ID} &&
                    $part->{Addr_ID} == $fams{$Fam_ID}->{Addr_ID};
    $part->{Address} = $same_addr ? 'same' : format_addr( $part );
    $part->{Grade} =
      $self->Grade( $part->{ClassYear}, $part->{CurYear}, $part->{CurMonth} );
    my @Roles = grep $_,
      map $_->{Part_ID} == $Part_ID ? $_->{Role} : '', @$roles;
    $part->{Roles} = join "<br>\n", @Roles;
    $part->{RoleLoop} = [ map( { Role => $_ }, @Roles ) ];
    $part->{Contact} = join "<br>\n", grep $_,
      ( $same_addr ? '' : ( $part->{HomePhone} ? "h: $part->{HomePhone}" : '' ) ),
      ( $part->{WorkPhone} ? "w: $part->{WorkPhone}" : '' ),
      ( $part->{CellPhone} ? "c: $part->{CellPhone}" : '' ),
      $part->{Email};
    if ( $exinfo ) {
      my $name = "$part->{FirstName} $part->{LastName}";
      if ( $exinfo->{$name} ) {
        for my $key ( qw(Special Excused Cast MayLeave MayLeaveTxt Photo Photo_w Photo_h) ) {
          if ( $exinfo->{$name}->{$key} ) {
            $part->{$key} = $exinfo->{$name}->{$key};
          }
        }
        $exinfo->{$name}->{matched} = 1;
      }
    }
    if ( $flags ) {
      for my $key ( keys %$flags ) {
        $part->{$key} = $flags->{$key};
      }
    }
    push( @{$fams{$Fam_ID}->{Members}}, $part );
  }
  $form->tmpl_param( Families => $fams );
  $form->tmpl_param( has_list => 1 );
}

# ProjSearch() invoked from Contact.pm and contact4show script
# Takes an argument optionally limiting search to:
# prod => Production Team
# kids => Registrants and Parents
# (Currently not using that option in contact4show. Mixing
#  everyone together.)
sub ProjSearch {
  my ( $self, $stype, $exinfo ) = @_;
  my $form = $self->{form};
  my $Proj_ID = $form->field('Proj_ID') || '';
  my ($Proj_Name, $n_subs) = $self->{dbh}->selectrow_array(
    'SELECT Proj_Name, COUNT(SP.Proj_ID) AS Nsubs
     FROM ACT_Project AS P
       LEFT JOIN ACT_Subproject AS SP ON P.Proj_ID = SP.Parent_Proj
     WHERE P.Proj_ID = ?
     GROUP BY P.Proj_ID', {}, $Proj_ID );
  $form->tmpl_param( Proj_Name => $Proj_Name );
    
  # List everyone associated with a Project
  # Families: Fam_ID FamilyName HomePhone Address Members Emergency Contacts
  # Members: Part_ID FirstName LastName Adult Age Grade Address Roles
  # Roles: Fam_ID Part_ID Role
  my $qualifier = '';
  if ($stype) {
    if ( $stype eq 'prod' ) {
      $qualifier = " AND Role_Name = 'Production Team'";
    } elsif ( $stype eq 'kids' ) {
      $qualifier = " AND Role_Name IN ( 'Registration', 'Parent' )";
    }
  }
  my $stmnt =
   "SELECT P.Part_ID, P.Fam_ID, P.FirstName, P.LastName,
      STRCMP(P.Status,'Child') AS Adult,
      IF(P.Birthdate>0,YEAR(Proj_Start)-YEAR(P.Birthdate)-
          (MID(Proj_Start,6,5)<MID(P.Birthdate,6,5)), '?') AS Age,
      P.ClassYear, YEAR(Proj_Start), MONTH(Proj_Start), P.Email,
      Address1, Address2, City, State, Zip, P.WorkPhone, P.CellPhone,
      P.Addr_ID, HomePhone, '', P.Birthdate
    FROM ACT_Project NATURAL JOIN ACT_Role AS R
      NATURAL JOIN ACT_Cast AS C
      NATURAL JOIN ACT_Participant AS P1
      JOIN ACT_Participant AS P
      NATURAL LEFT JOIN ACT_Address
    WHERE ACT_Project.Proj_ID = ?
      AND Cast_Status = ?$qualifier
      AND P.Fam_ID = P1.Fam_ID
      AND ( P.Part_ID = P1.Part_ID OR
          ( P1.Status = 'Child' AND P.Status = 'Adult' ))
    GROUP BY P.Part_ID
    ORDER BY P.Fam_ID, Adult, Age DESC, LastName, FirstName";
  my $members = $self->{dbh}->selectall_arrayref( $stmnt, {}, $Proj_ID, 'Accepted' );
  if ( @$members ) {
    my @Fam_IDs = map $_->[1], @$members;
    my $fstmnt =
      "SELECT F.Fam_ID, FamilyName, HomePhone, Address1, Address2,
        City, State, Zip, F.Addr_ID, EmergencyContact1,
        EmergencyPhone1, EmergencyContact2, EmergencyPhone2
      FROM ACT_Family AS F NATURAL JOIN ACT_Address
      WHERE F.Fam_ID IN ( " .
      join( ', ', map '?', @Fam_IDs ) .
      " ) ORDER By FamilyName";
    my $fams = $self->{dbh}->selectall_arrayref( $fstmnt, {}, @Fam_IDs );
    my $roles;
    if ( $n_subs ) {
      # For this query, I am going to ignore ACT_Is_Also entirely, requiring
      # that each actor actually be specifically cast for each role.
      # I should create a separate query to guarantee that the casting
      # and the Is_Also designations are in agreement.
      my $rstmnt =
       "SELECT Fam_ID, C.Part_ID,
          LTRIM(CONCAT(
            Proj_Abbr, ': ',
            IF(Role_Type = 'Character','Role:',''),
            GROUP_CONCAT(DISTINCT CONCAT(' ', Role_Name, IF(Cast_Note='','',CONCAT(': ', Cast_Note)))
                         ORDER BY Role_Name ASC),
            IF(C.Cast_ID>0,CONCAT(' (',Cast_Name2,')'),'')
          )) AS Role
        FROM 
          (SELECT ? AS Proj_ID UNION SELECT PP.Proj_ID FROM ACT_Subproject AS PP
           WHERE Parent_Proj = ?) AS PS
          NATURAL JOIN ACT_Project
          NATURAL JOIN ACT_Role AS R
          NATURAL JOIN ACT_Cast AS C
          NATURAL JOIN ACT_Participant AS P
          NATURAL JOIN ACT_Casts
        WHERE Role_Type IN ( 'Character', 'Committee' )
        GROUP BY P.Part_ID, PS.Proj_ID, Cast_Name2, Role_Type
        ORDER BY Fam_ID, P.Part_ID";
      $roles = $self->{dbh}->selectall_arrayref( $rstmnt, {}, $Proj_ID, $Proj_ID );
    } else {
      # This query uses ACT_Is_Also to make sure all roles are included by
      # doing a UNION in creating table URole. This gets around the
      # abysmal optimization of the previous complex query.
      my $rstmnt =
        "SELECT Fam_ID, Part_ID,
          LTRIM(CONCAT(
            IF(Role_Type = 'Character','Role:',''),
            GROUP_CONCAT(DISTINCT CONCAT(' ', Role_Name, IF(Cast_Note='','',CONCAT(': ', Cast_Note)))
                         ORDER BY Role_Name ASC),
            IF(Cast_ID>0,CONCAT(' (',Cast_Name2,')'),'')
          )) AS Role
        FROM (
          ( SELECT Fam_ID, Part_ID, Role_Name, C.Cast_ID, Role_Type, Cast_Note, Cast_Name2
            FROM ACT_Role AS R
              JOIN ACT_Is_Also AS I ON I.Is_Also_ID = R.Role_ID
              JOIN ACT_Cast AS C
                ON C.Role_ID = I.Role_ID
                AND (C.Cast_ID = I.Cast_ID OR I.Cast_ID = 0 OR C.Cast_ID = 0)
              NATURAL JOIN ACT_Participant AS P
              JOIN ACT_Casts ON C.Cast_ID = ACT_Casts.Cast_ID
            WHERE Proj_ID = ?
              AND Role_Type IN ( 'Character', 'Committee' )
          ) UNION (
            SELECT Fam_ID, Part_ID, Role_Name, C2.Cast_ID, Role_Type, Cast_Note, Cast_Name2
            FROM ACT_Role AS R2
              NATURAL JOIN ACT_Cast AS C2
              NATURAL JOIN ACT_Participant AS P2
              JOIN ACT_Casts AS CS2 ON C2.Cast_ID = CS2.Cast_ID
            WHERE Proj_ID = ?
              AND Role_Type IN ( 'Character', 'Committee' )
          )) AS URole
        GROUP BY Part_ID, Cast_Name2, Role_Type, Role_Name
        ORDER BY Fam_ID, Part_ID";
      $roles = $self->{dbh}->selectall_arrayref( $rstmnt, {}, $Proj_ID, $Proj_ID );
    }
    # for my $role ( @$roles ) {
      # $role->[2] =~ s/<[^>]*>//g;
    # }
    $self->ContactParams( $fams, $members, $roles, $exinfo );
  } else {
    $self->error( "No matches found" );
  }
}

# This search seems a little outdated. Given a Part_ID, it allows
# for all the Familys and Addresses that match that Part_ID, but
# Part_ID is the primary key for ACT_Participant, and Fam_ID is
# the primary key for ACT_Family, so there can be only one. Of
# course it's possible that a more complex arrangement will
# appear later.
# This search is referenced from /Home
sub FamSearch {
  my ( $self, $Part_ID, $showall ) = @_;
  my $form = $self->{form};
    
  my $fams = $self->{dbh}->selectall_arrayref(
    "SELECT ACT_Family.Fam_ID, FamilyName, HomePhone, Address1, Address2,
      City, State, Zip, ACT_Family.Addr_ID, EmergencyContact1,
      EmergencyPhone1, EmergencyContact2, EmergencyPhone2
    FROM ACT_Participant NATURAL JOIN ACT_Family
      NATURAL JOIN ACT_Address
    WHERE Part_ID = ?", {}, $Part_ID );
  my @Fam_IDs = map $_->[0], @$fams;
  if ( @Fam_IDs ) {
    my $Fam_IDQ = join ", ", map "?", @Fam_IDs;
    my $stmnt =
      "SELECT Part_ID, ACT_Participant.Fam_ID, FirstName, LastName,
        STRCMP(Status,'Child') AS Adult,
        IF(Birthdate>0,YEAR(NOW())-YEAR(Birthdate)-(MID(NOW(),6,5)<MID(Birthdate,6,5)),
        '?') AS Age, ClassYear, YEAR(CURDATE()), MONTH(CURDATE()), Email,
        Address1, Address2, City, State, Zip, WorkPhone, CellPhone,
        ACT_Participant.Addr_ID, HomePhone, '', Birthdate
      FROM ACT_Participant NATURAL LEFT JOIN ACT_Address
      WHERE ACT_Participant.Fam_ID IN ( $Fam_IDQ )
      ORDER BY ACT_Participant.Fam_ID, Adult, Age DESC";
    my $members = $self->{dbh}->selectall_arrayref( $stmnt, {}, @Fam_IDs );
    my $showallstmnt = $showall ? '' :
      " AND Proj_Status != 'Archive'";
    my $rstmnt =
      "SELECT Fam_ID, ACT_Cast.Part_ID,
       CONCAT( CASE Cast_Status
                 WHEN 'Applied' THEN 'AP: '
                 WHEN 'Accepted' THEN ''
                 WHEN 'Waitlisted' THEN 'WL: '
                 WHEN 'Withdrawn' THEN 'WD: ' END,
        Role_Name, ' for ', Proj_Name )
       FROM ACT_Participant NATURAL JOIN ACT_Cast
         NATURAL JOIN ACT_Role
         NATURAL JOIN ACT_Project
       WHERE Fam_ID IN ( $Fam_IDQ )$showallstmnt
       ORDER BY ACT_Role.Proj_ID, ACT_Role.Role_ID, Fam_ID, ACT_Cast.Part_ID";
    my $roles = $self->{dbh}->selectall_arrayref( $rstmnt, {}, @Fam_IDs );
    $self->ContactParams( $fams, $members, $roles );
  } else {
    $self->error( "No matches found" );
  }
}

sub format_addr {
  my $part = shift;
  my $addr = 
    join "<br>", grep $_, $part->{Address1}, $part->{Address2},
      join( " ", grep $_, join( ", ", grep $_, $part->{City}, $part->{State} ),
            $part->{Zip} );
}

sub check_invoice {
  my ( $self, $Cast_Status, $Role_ID, $Part_ID, $Role_Fee, $Desc ) = @_; 
  my $Invoice_ID = 0;
  if ( $Cast_Status eq 'Accepted' && $Role_Fee > 0 &&
       $self->{dbh}->do(
          'INSERT IGNORE INTO ACT_Invoice
           ( Part_ID, Role_ID, Inv_Desc, Inv_Amt, Inv_Paid,
             Scholarship, Inv_Date )
           VALUES ( ?, ?, ?, ?, ?, ?, NOW() )', {},
          $Part_ID, $Role_ID, $Desc, $Role_Fee, 'Unpaid', 0 ) ) {
    $Invoice_ID = $self->{dbh}->{mysql_insertid};
    $self->{dbh}->do(
      'UPDATE ACT_Cast SET Invoice_ID = ?
      WHERE Role_ID = ? AND Part_ID = ?', {},
      $Invoice_ID, $Role_ID, $Part_ID );
  }
}

# $self->Part_ID gets the form Part_ID or $self->{auth}->{Part_ID}
# The form Part_ID is bypassed if it is not numeric. This method
# assumes the
sub Part_ID {
  my $self = shift;
  $self->fatal( "Assert failed: No auth in Part_ID method" )
     unless $self->{auth};
  my $form = $self->{form};
  my $Part_ID = $form->field('Part_ID') || '';
  $Part_ID = $self->{auth}->{Part_ID} unless $Part_ID =~ m/^\d+$/;
  return $Part_ID;
}

# $self->Fixup_Parents( $Fam_ID ) checks the family record to
# make sure the parents are listed on all the appropriate
# projects. Should be called from everywhere any changes to
# the family record occur, including:
#   Adding adults to a family
#   Merging Families or Family members
#   Registering or withdrawing from a project
# If file locking is involved, this routine requires locks on
#    act_cast(R/W), act_project(RO), act_role(RO),
#    act_participant(RO), act_participant as P2(RO),
#    and act_role as R2(RO)
# The fact that file locking is probably involved means you can't
# rewrite the query to rename the tables.
sub Fixup_Parents {
  my ( $self, $Fam_ID ) = @_;
  $self->fatal( "No Fam_ID specified to Fixup_Parents" )
    unless $Fam_ID && $Fam_ID =~ m/^\d+$/;
  my $should_be = $self->{dbh}->selectall_arrayref(
    "SELECT STRAIGHT_JOIN R2.Role_ID, 0, P2.Part_ID
    FROM ACT_Project NATURAL JOIN ACT_Role NATURAL JOIN ACT_Cast
        NATURAL JOIN ACT_Participant,
      ACT_Participant as P2, ACT_Role AS R2
    WHERE Proj_Status = 'Active'
      AND ACT_Role.Role_Type = 'Category'
      AND ACT_Role.Role_Name = 'Registration'
      AND Cast_Status = 'Accepted'
      AND ACT_Participant.Fam_ID = ?
      AND ACT_Participant.Status = 'Child'
      AND P2.Fam_ID = ACT_Participant.Fam_ID
      AND P2.Status = 'Adult'
      AND R2.Proj_ID = ACT_Role.Proj_ID
      AND R2.Role_Name = 'Parent'
    GROUP BY R2.Role_ID, P2.Part_ID",
    {}, $Fam_ID );
  my $is = $self->{dbh}->selectall_arrayref(
    "SELECT ACT_Role.Role_ID, Cast_ID, ACT_Cast.Part_ID as Par
    FROM ACT_Project NATURAL JOIN ACT_Role NATURAL JOIN ACT_Cast
      NATURAL JOIN ACT_Participant
    WHERE Role_Name = 'Parent' AND Proj_Status = 'Active' AND Fam_ID = ?
    GROUP BY ACT_Role.Role_ID, Cast_ID, Par", {}, $Fam_ID );
  my %sb;
  for ( map join( ':', @$_ ), @$should_be ) {
    $sb{$_} = 'add';
  }
  for ( map join( ':', @$_ ), @$is ) {
    if ( not defined $sb{$_} ) {
      $sb{$_} = 'rm';
    } else {
      $sb{$_} = 'ok';
    }
  }

  my @rm = grep $sb{$_} eq 'rm', keys %sb;
  my @add = grep $sb{$_} eq 'add', keys %sb;
  if ( @rm ) {
    my $sth = $self->{dbh}->prepare(
      "DELETE FROM ACT_Cast WHERE Role_ID = ? AND Cast_ID = ? AND Part_ID = ?" );
    for ( @rm ) {
      unless( $sth->execute( split ':', $_ ) ) {
        warn( "Error attempting to remove parent $_\n" );
      }
      $self->Log( "Fixup_Parent: Removed $_" );
    }
  }
  if ( @add ) {
    my $sth = $self->{dbh}->prepare(
      "INSERT IGNORE INTO ACT_Cast ( Role_ID, Cast_ID, Part_ID, Cast_Status )
       VALUES ( ?, ?, ?, 'Accepted' )" );
    for ( @add ) {
      unless( $sth->execute( split ':', $_ ) ) {
        warn( "Error inserting parents: ", join( ', ', @add ), "\n" );
      }
      $self->Log( "Fixup_Parent: Added $_" );
    }
  }
}

# $self->forgot_password( $LastName, $FirstName, $Email );
# Does not handle basic form validation, but handles
# all of the database logic and the e-mailing of the
# Key. Returns 1 if a match is found and the key is
# mailed. Returns 0 if no match is found, leaving it
# to the calling procedure to decide what to do.
# In the special case that a key would be mailed but
# $CGI::NForm::ACT::Config{smtp_server} is not defined, the value 1 is returned, and
# the key is stored in $self->{tmp}->{key} for use by
# the calling procedure
sub forgot_password {
  my ( $self, $LastName, $FirstName, $Email ) = @_;
  my $form = $self->{form};
  my $Part_ID = 0;
  my $res = $self->{dbh}->selectall_hashref(
    'SELECT P.Email, P.Part_ID, P.Fam_ID, P.Status, U.Username
     FROM ACT_Participant AS P
     LEFT OUTER JOIN ACT_Users as U
     ON P.Part_ID = U.Part_ID
     WHERE P.LastName = ? AND P.FirstName = ?', 'Part_ID',
     {}, $LastName, $FirstName );
  die "Expected hashref" unless $res && ref($res);
  my @ematch = grep lc($res->{$_}->{Email}) eq lc($Email), keys %$res;
  if ( @ematch ) {
    warn "Duplicate Last/First/Email on $LastName/$FirstName/$Email\n"
      if @ematch > 1;
    $Part_ID = shift @ematch;
  } elsif ( $Email ) {
    PICK_PART:
    for my $PID ( keys %$res ) {
      if ( ! $res->{$PID}->{Username} ) {
        my $fam = $self->{dbh}->selectall_hashref(
          "SELECT P.Part_ID, U.Username FROM ACT_Participant AS P
           LEFT OUTER JOIN ACT_Users as U
           ON P.Part_ID = U.Part_ID
           WHERE P.Fam_ID = ? AND P.Email = ?
            AND P.Status = 'Adult';",
           'Part_ID', {}, $res->{$PID}->{Fam_ID}, $Email );
        die "Expected arrayref" unless $fam && ref($fam);
        for my $fPID ( keys %$fam ) {
          # The e-mail address matches someone in this family
          if ( $res->{$PID}->{Status} eq 'Adult' ) {
            if ( ! $res->{$PID}->{Email} ) {
              # I'm not currently a user, my record does not
              # include an e-mail address, but the e-mail address
              # submitted matches an adult in my family, so I'll
              # update my record's e-mail to match what I
              # submitted.
              $self->{dbh}->do(
                'UPDATE ACT_Participant
                 SET Email = ? WHERE Part_ID = ?;',
                {}, $Email, $PID );
              $Part_ID = $PID;
              $self->Log("E-mail copied from spouse: Part_ID=$Part_ID" );
              last PICK_PART;
            } else {
              # We clearly have data on this family, but
              # something isn't quite right.
            }
          } else {
            push @{$self->{errors}},
              "For the purposes of sign on, please use the name
               of an adult in the family. Adults can then
               update the records of their children.";
            return 0;
          }
        }
      }
    }
  }
  if ( $Part_ID && $Email ) {
    my $key = $self->create_ticket( $Part_ID, '', time()+(72*3600) );
    
    if ( $CGI::NForm::ACT::Config{smtp_server} ) {
      $self->mail_key( $key, $res->{$Part_ID}->{Username} );
    } else {
      $self->{tmp}->{key} = $key;
    }
    return 1;
  }
  return 0;
}

sub mail_key {
  use Mail::Sendmail;
  my ( $self, $key, $username ) = @_;
  my $form = $self->{form};
  my ( $first, $last, $email, $nxtPI, $nxtPA ) =
    map $form->field($_),
    qw(FirstName LastName Email nxtPI nxtPA);
  my $query = $self->build_query( nxtPI => $nxtPI, nxtPA => $nxtPA,
                  _sessionid => $key );
  my $script = $self->script_uri . "/User/" .
    ( $username ? "edit" : "new" ) . "?$query";
  my @smtp;
  if ( $CGI::NForm::ACT::Config{smtp_server} ) {
    push @smtp, smtp => $CGI::NForm::ACT::Config{smtp_server};
  }
  if ( sendmail(
    To => $email,
    From => 'webmaster@act.arlington.ma.us',
    Subject => 'ACT Email Verification',
    @smtp,
    Message => <<EOF
Your ACT Database Key is $key
You can copy and paste it directly onto the Login screen or you can
follow this url:

$script

If the URL breaks, you may need to cut and paste it into
your browser as well. This key is only valid for a limited time.
EOF
  ) ) {
    $self->Log("Key $key mailed");
  } else {
    $self->Log("Sendmail failed");
    $self->fatal("An error occurred when attempting to send mail");
  }
}

sub FixPhoneEntry {
  my $self = shift;
  my $form = $self->{form};
  for my $field ( @_ ) {
    my $val = $form->field($field);
    if ( $val =~ m/^(\d\d\d) [-.]? (\d\d\d)
                    [-.]? (\d\d\d\d) ((?:x\d+)?)$/x ) {
      $val = "$1-$2-$3$4";
      $form->field( name => $field, value => $val, force => 1 );
    }
  }
}

# If $Proj_ID < 0, the condition applies to any active project
# Update 090423 to support subprojects
sub Get_Privilege_Value {
  my ( $self, $Proj_ID, $Privilege ) = @_;
  my $Part_ID = $self->{auth}->{Part_ID};
  return 0 unless $Part_ID;
  return 0 unless $Proj_ID =~ m/^-?\d+$/;
  my $Proj_cond;
  if ( $Proj_ID < 0 ) {
    $Proj_cond = '';
  } elsif ( $Proj_ID == 0 ) {
    $Proj_cond = 'AND R.Proj_ID = 0';
  } else {
    # $Proj_cond = "AND ( R.Proj_ID = 0 OR R.Proj_ID = $Proj_ID )";
    $Proj_cond = "AND R.Proj_ID IN (
      SELECT $Proj_ID UNION SELECT 0
      UNION SELECT Parent_Proj FROM ACT_Subproject
      WHERE Proj_ID = $Proj_ID)";
  }
  my $PV_Value = $self->{dbh}->selectrow_array(
    "SELECT MAX(PV_VALUE)
      FROM ACT_Cast
        NATURAL JOIN ACT_Role AS R
        NATURAL JOIN ACT_Project
        JOIN ACT_Priv_Roles AS PR
          ON R.Role_Name = PR.Role_Name AND
             ( PR.Role_ID = 0 OR PR.Role_ID = R.Role_ID )
        NATURAL JOIN ACT_PRVals
        NATURAL JOIN ACT_Privileges
    WHERE Part_ID = ? AND Cast_Status = 'Accepted'
      $Proj_cond
      AND Proj_Status != 'Archived'
      AND Priv_Name = ?", {},
    $Part_ID, $Privilege );
  $PV_Value = 0 unless $PV_Value;
  return $PV_Value;
}

# If $Proj_ID < 0, the condition applies to any active project
sub Has_Privilege {
  my ( $self, $Proj_ID, $Privilege, $min_val ) = @_;
  my $PV_Value = $self->Get_Privilege_Value( $Proj_ID, $Privilege );
  # warn( "Has_Privilege( $Proj_ID, $Privilege, $min_val ) got $PV_Value\n" );
  return $PV_Value >= $min_val;
}

# resolve_template() internal function called by parse_exinfo()
sub resolve_template {
  my ( $template, $flds ) = @_;
  while ( $template =~ m/\${(\w+)}/ ) {
    my $var = $1;
    my $val = $flds->{$var} || '';
    $template =~ s/\${$var}/$val/g;
  }
  return $template;
}

# $self->parse_exinfo( $fh [, $full])
# Parses input for:
#   Name
#   MayLeave
#   MayLeaveTxt
# If $full is defined and non-zero, also pulls out:
#   Special
#   Excused
#   Photo
#   Photo_w
#   Photo_h
sub parse_exinfo {
  my ( $self, $fh, $full ) = @_;
  my $exinfo = {};

  # filevars can include:
  #   Name: template for making the name, defaults to '${First} ${Last}'
  #   Photo: template for making photo filename
  my %filevars = ( Name => '${First} ${Last}' );
  my %hdrs;
  my $nflds = 0;
  my $dpi = 300;
  my $min_height = 1; # 1 inch

  while ( <$fh> ) {
    my @flds = map { s/\s*$//;
                     s/^\s*//;
                     s/^"(.*)"$/$1/;
                     s/(^| )""/$1\`\`/g;
                     s/""( |$)/\'\'$1/g;
                     s/""/"/g; $_; } split /\t/, $_;
    if ( $flds[0] =~ m/^(\w+):$/ ) {
      $filevars{$1} = $flds[1];
    } elsif ( !$nflds ) {
      while ( @flds ) {
        my $fld = shift @flds;
        $hdrs{$fld} = $nflds++;
      }
    } else {
      my %flds;
      for my $hdr ( keys %hdrs ) {
        $flds{$hdr} = $flds[$hdrs{$hdr}];
      }
      my $name = resolve_template($filevars{Name}, \%flds);
      $exinfo->{$name} ||= {};
      my $rec = $exinfo->{$name};
      if ( $flds{MayLeave} ) {
        $rec->{MayLeave} = 1;
        if ( $flds{MayLeave} ne '1' ) {
          $rec->{MayLeaveTxt} = " $flds{MayLeave}";
        }
      }
      if ( $full ) {
        $rec->{Special} = $flds{Special} if $flds{Special};
        $rec->{Excused} = $flds{Excused} if $flds{Excused};
        if ( $filevars{Photo} ) {
          my $photo = resolve_template($filevars{Photo}, \%flds);
          if ( -f $photo ) {
            my ($w, $h) = imgsize($photo);
            if ( defined $w ) {
              my $h_in = $h/$dpi;
              my $w_in = $w/$dpi;
              if ( $h_in < $min_height ) {
                my $ndpi = $h/$min_height;
                $h_in = $h/$ndpi; # $min_height, right?
                $w_in = $w/$ndpi;
              }
              $rec->{Photo} = $photo;
              $rec->{Photo_w} = sprintf "%.2fin", $w_in;
              $rec->{Photo_h} = sprintf "%.2fin", $h_in;
            }
          }
        }
      }
    }
  }
  close $fh || warn "Error closing filehandle in parse_exinfo\n";
  return $exinfo;
}

sub sendmail_wrap {
  use Mail::Sendmail;
  my ($self, %msg) = @_;
  if ($msg{smtp}) {
    return sendmail(%msg);
  } else {
    my $tmp = $CGI::NForm::ACT::Config{tmpdir};
    my $fname;
    for (my $cnt = 1; ; ++$cnt) {
      $fname = "$tmp/sendmail_$cnt.txt";
      -e $fname || last;
    }
    if ( open(my $fh, '>', $fname)) {
      print $fh
        "To: $msg{To}\n",
        "CC: $msg{CC}\n",
        "From: $msg{From}\n",
        "Subject: $msg{Subject}\n\n",
        $msg{Message};
      close($fh);
      warn "Confirmation written to $fname\n";
      return 1;
    } else {
      warn "Unable to write to $fname\n";
      return 0;
    }
  }
}
