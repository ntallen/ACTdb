package CGI::NForm::ACT::Venue;
use strict;
use CGI;
use CGI::FormBuilder;

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    { rm  =>  'DELETE',
      edit => 'UPDATE',
      new =>  'INSERT',
      '' =>   'SELECT'
    };
}

# Support for the $params argument is optional, but necessary
# if you want to allow this form to be redirected.
# With FormBuilder 3.0, this functionality can be moved back
# to NForm and get_form() will be responsible only for specifying
# the fields and the template.
sub get_form {
  my ( $self, $op, $params )  = @_;
  $self->Connect();
  my $q = $self->{q} = ref($params) eq 'CGI' ? $params : new CGI($params);
  my %lbldef = (
    SELECT => '',
    INSERT => 'Create',
    UPDATE => 'Submit Changes',
    DELETE => 'Delete'
  );
  my $form = CGI::FormBuilder->new(
    action => "$ENV{SCRIPT_NAME}$self->{path_info}",
    method => 'POST',
    params => $q,
    template => "$self->{tmpldir}/Venue.html",
    title => "ACT Event Venue",
    submit => [ $lbldef{$op} || (), "Back" ]
  );
  my $static = $op eq 'SELECT' || $op eq 'DELETE';
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'Venue_ID', type => 'hidden', required => $op ne 'INSERT' );
  $form->field( name => 'Venue_Short', size => 40, maxlength => 40, required => 1, static => $static );
  $form->field( name => 'Venue_Long', type => 'textarea', cols => 50,
                rows => 4, wrap => 'physical', static => $static );
  $form->field( name => 'Address1', size => 50, maxlength => 50, static => $static );
  $form->field( name => 'Address2', size => 50, maxlength => 50, static => $static );
  $form->field( name => 'City', size => 50, maxlength => 50, value => 'Arlington', static => $static );
  $form->field( name => 'State', size => 2, maxlength => 2, value => 'MA', static => $static );
  $form->field( name => 'ZIP', size => 10, static => $static );
  $form->field( name => 'URL', size => 60, maxlength => 200, static => $static );
  return $form;
}

sub authorize {
  my ( $self, $op ) = @_;
  return 1 if $op eq 'SELECT';
  return 1 if $self->check_key(EU=>1) &&
    $self->Has_Privilege(0, 'Schedule', 1);
  $self->fatal("This action requires global Schedule write privileges");
  # if ( $self->check_key(EU=>1) ) {
  #   return 1 if $self->has_role( 1, 'Board' );
  # }
  # return 0;
}

sub INSERT {
  my $self = shift;
  my $form = $self->{form};
  $self->Return if $form->submitted eq 'Back';
  
  if ( $form->validate ) {
    my @flds = qw(Venue_Short Venue_Long Address1 Address2
	  City State ZIP URL);
    my %vals;
    for (@flds) {
      $vals{$_} = $form->field($_);
    }
    $self->{dbh}->do(
      'INSERT INTO ACT_Venue ( ' . join(', ', @flds ) .
      ' ) VALUES ( ' . join( ', ', map '?', @flds ) . ' )',
      {}, map $vals{$_}, @flds );
    my $Venue_ID = $self->{dbh}->{mysql_insertid};
    $self->Return( Venue_ID => $Venue_ID );
  }
  $self->error('Validation Error');
  return 0;
}

sub UPDATE {
  my $self = shift;
  my $form = $self->{form};
  $self->Return if $form->submitted eq 'Back';
  
  if ( $form->validate ) {
    my @flds = qw(Venue_Short Venue_Long Address1 Address2
	  City State ZIP URL);
    my %vals;
    for ( 'Venue_ID', @flds) {
      $vals{$_} = $form->field($_);
    }
    $self->{dbh}->do(
      'UPDATE ACT_Venue SET ' .
      join( ', ', map "$_ = ?", @flds ) .
      ' WHERE Venue_ID = ?',
      {}, map $vals{$_}, @flds, 'Venue_ID' );
    $self->Return;
  }
  return 0;
}

sub DELETE {
  my $self = shift;
  my $form = $self->{form};
  $self->Return if $form->submitted eq 'Back';
  my $Venue_ID = $form->field('Venue_ID') ||
    $self->fatal("No Venue_ID Specified");
  $self->{dbh}->do(
    'UPDATE ACT_Event SET Venue_ID = ?
     WHERE Venue_ID = ?', {}, 0, $Venue_ID );
  $self->{dbh}->do(
    'DELETE FROM ACT_Venue WHERE Venue_ID = ?',
    {}, $Venue_ID );
  $self->Return;
}

sub SELECT {
  return 1;
}

sub InitParams {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  if ( $op eq 'SELECT' ) {
    $form->tmpl_param( SELECT => 1 );
    my @flds = qw(Address1 Address2 City State ZIP URL);
    my %vals;
    for my $fld ( @flds ) {
      $vals{$fld} = $form->field($fld) || '';
    }
    my $CSZ = join( ' ', grep $_,
      join( ', ', grep $_, $vals{City}, $vals{State} ),
      $vals{ZIP} );
    my $Addr = join( "<br>\n", grep $_, $vals{Address1}, $vals{Address2}, $CSZ );
    $form->tmpl_param( Address => $Addr );
    $form->tmpl_param( URL => $vals{URL} );
  }
}

sub InitFields {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  my $Venue_ID = $form->field('Venue_ID');
  if ( $Venue_ID ) {
    my @flds = qw(Venue_Short Venue_Long Address1 Address2
          City State ZIP URL);
    my $vals = $self->{dbh}->selectrow_hashref(
      'SELECT ' . join( ', ', @flds ) . ' FROM ACT_Venue
      WHERE Venue_ID = ?', {}, $Venue_ID );
    for ( keys %$vals ) {
      $self->fatal("Misunderstood hashref") if ref $vals->{$_};
      $form->field( name => $_, value => $vals->{$_}, force => 1 );
    }
  } elsif ( $op ne 'INSERT' ) {
    $self->fatal( "No Venue_ID Specified" );
  }
}

1;
