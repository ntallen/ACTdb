package CGI::NForm::ACT::Schedule;
use strict;
use CGI;
use CGI::FormBuilder;

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    {
      '' => 'SELECT'
    };
}

# Support for the $params argument is optional, but necessary
# if you want to allow this form to be redirected.
# With FormBuilder 3.0, this functionality can be moved back
# to NForm and get_form() will be responsible only for specifying
# the fields and the template.
sub get_form {
  my ( $self, $op, $params )  = @_;
  $self->Connect();
  my $q = $self->{q} = ref($params) eq 'CGI' ? $params : new CGI($params);
  my $form = CGI::FormBuilder->new(
    action => "$ENV{SCRIPT_NAME}$self->{path_info}",
    method => 'POST',
    fields => [ qw(nxtPI nxtPA Proj_ID) ],
    params => $q,
    template => "$self->{tmpldir}/Schedule.html",
    title => "Schedule for an ACT Project",
  );
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'Proj_ID', type => 'hidden' );
  $form->field( name => 'FullSched', options => [ 'Show Past Rehearsals' ] );
  my $Proj_ID = $form->field('Proj_ID') ||
    $self->fatal("No Proj_ID specified");
  my ( $Proj_Name, $Proj_Status ) = $self->{dbh}->selectrow_array(
    'SELECT Proj_Name, Proj_Status FROM ACT_Project WHERE Proj_ID = ?',
    {}, $Proj_ID );
  $form->title( "Full Schedule for $Proj_Name" );
  if ( $Proj_Status ne 'Archive' ) {
    $form->tmpl_param( Proj_Current => 1 );
    $self->{tmp}->{Proj_Current} = 1;
  }
  return $form;
}

sub authorize {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  $self->{tmp}->{is_admin} = 0;
  
  # The idea here is allow unauthenticated access,
  # but if we're already authenticated, give us more
  # choices
  my $rolepriv = 0;
  my $Proj_ID = $form->field('Proj_ID');
  if ( $form->sessionid && $self->check_key ) {
    $rolepriv =
      $self->Has_Privilege( $Proj_ID, 'Schedule', 1 );
  }
  if ( $rolepriv ) {
    $self->{tmp}->{is_admin} = 1;
    $form->submit( [ "Scene Grid", "Edit Scene Grid", "Scene List",
       "Character List", "New Rehearsal", "Back" ] );
    $form->field( name => 'Proj_Schedule', type => 'textarea',
                  rows => 4, cols => 50 );
  } else {
    $form->field( name => 'Proj_Schedule', static => 1 );
    my @submits = ( "Scene List", "Scene Grid", "Character List" );
    push @submits, "Back" if $form->field("nxtPI");
    $form->submit( \@submits );
  }
  return 1;
}

sub INSERT {}

sub UPDATE {}

sub DELETE {}

sub SELECT {
  my $self = shift;
  my $form = $self->{form};
  my $Proj_ID = $form->field('Proj_ID');
  my $sbmt = $form->submitted;
  if ( $sbmt eq 'Update Text' ) {
    $self->fatal("Not authorized to Update Text")
      unless $self->{tmp}->{is_admin};
    my $Proj_Schedule = $form->field('Proj_Schedule');
    $self->{dbh}->do(
      'UPDATE ACT_Project SET Proj_Schedule = ?
       WHERE Proj_ID = ?', {}, $Proj_Schedule, $Proj_ID );
    # $self->Return;
    return 0;
  } elsif ( $sbmt eq 'Go' ) {
    return 0;
  } elsif ( $sbmt eq 'Scene Grid' ) {
    $self->Call( '/Scenes', Proj_ID => $Proj_ID );
  } elsif ( $sbmt eq 'Edit Scene Grid' ) {
    $self->Call( '/Scenes/edit', Proj_ID => $Proj_ID );
  } elsif ( $sbmt eq 'Scene List' ) {
    $self->Call( $self->{tmp}->{is_admin} ? '/Groups/edit' :'/Groups', Proj_ID => $Proj_ID );
  } elsif ( $sbmt eq 'Character List' ) {
    $self->Call( $self->{tmp}->{is_admin} ? '/Chars/edit' :'/Chars', Proj_ID => $Proj_ID );
  } elsif ( $sbmt eq 'New Rehearsal' ) {
    $self->Call( '/Rehearsal/new', Proj_ID => $Proj_ID );
  } elsif ( $sbmt eq 'Back' ) {
    $self->Return;
  } else {
    my ( $submit ) = grep m/^[A-Z][A-Z]\d+$/, $form->cgi_param();
    my $cmd = 'XX';
    my $id = 0;
    if ( $submit && $submit =~ m/^([A-Z][A-Z])(\d+)$/ ) {
      $cmd= $1;
      $id = $2;
    }
    if ( $cmd eq 'VW' ) {
      $self->Call( '/Rehearsal', Proj_ID => $Proj_ID, Event_ID => $id );
    } elsif ( $cmd eq 'UP' ) {
      $self->Call( '/Rehearsal/edit', Proj_ID => $Proj_ID, Event_ID => $id );
    } elsif ( $cmd eq 'IN' ) {
      $self->Call( '/Rehearsal/new', Proj_ID => $Proj_ID, Event_ID => $id );
    } elsif ( $cmd eq 'DE' ) {
      $self->Call( '/Rehearsal/delete', Proj_ID => $Proj_ID, Event_ID => $id );
    } else {
      $self->fatal("Unknown option: sbmt = '$sbmt' cmd = '$cmd' id =  '$id'");
    }
  }
}

sub InitFields {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
}

sub InitParams {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  ### For starters, just list Rehearsals ('Group' type Events)
  ### Want to add Auditions, Meetings and Special Events
  ### (Auditions are like special events in that they have
  ### specially created roles. Meetings use pre-existing roles
  ###  (or could use specially created roles with ad-hoc lists of participants))
  
  my $Proj_ID = $form->field('Proj_ID');
  my ( $Proj_Schedule, $N_Casts, $Subs ) = $self->{dbh}->selectrow_array(
    'SELECT Proj_Schedule, N_Casts,
      COUNT(ACT_Subproject.Proj_ID) AS Subs
    FROM ACT_Project
      LEFT JOIN ACT_Subproject ON ACT_Project.Proj_ID = Parent_Proj
    WHERE ACT_Project.Proj_ID = ?
    GROUP BY ACT_Project.Proj_ID',
    {}, $Proj_ID );
  $form->field( name => 'Proj_Schedule',
    value => $Proj_Schedule, force => 1 );
  my $doublecast = $N_Casts == 2 ? 1 : 0;
  $form->tmpl_param( doublecast => $doublecast );
  my $params;
  if ( $Subs > 0 ) {
    my $query =
      "SELECT Events.Event_ID, Cast_Name,
        DATE_FORMAT(Event_Date,'%a %b %e, %Y'),
        CONCAT(TIME_FORMAT(Event_Start, '%l:%i %p&ndash;'),
          TIME_FORMAT(Event_End, '%l:%i %p')),
        Event_Desc, $self->{tmp}->{is_admin},
        Venue_Short, ACT_Event.Venue_ID, Who,
        Proj_Abbr
      FROM
        ( SELECT ACT_Event.Event_ID, GROUP_CONCAT(Grp_Name ORDER BY Grp_Order, Grp_Name) AS Who
          FROM ACT_Event
            NATURAL LEFT JOIN ACT_Evt_Grps
            NATURAL LEFT JOIN ACT_Group
          WHERE GR_Type = 'Group'
            AND ACT_Event.Proj_ID IN (
               SELECT ?
             UNION
               SELECT Proj_ID
               FROM ACT_Subproject
               WHERE Parent_Proj = ?
            )
          GROUP BY ACT_Event.Event_ID
              UNION
          SELECT ACT_Event.Event_ID, Role_Name AS Who
          FROM ACT_Event JOIN ACT_Role ON GR_Type = 'Role' AND GR_ID = Role_ID
          WHERE ACT_Event.Proj_ID IN (
               SELECT ?
             UNION
               SELECT Proj_ID
               FROM ACT_Subproject
               WHERE Parent_Proj = ?
          ) ) AS Events
        JOIN ACT_Event ON Events.Event_ID = ACT_Event.Event_ID
        JOIN ACT_Project ON ACT_Event.Proj_ID = ACT_Project.Proj_ID
        LEFT JOIN ACT_Venue ON ACT_Event.Venue_ID = ACT_Venue.Venue_ID
        JOIN ACT_Casts ON ACT_Event.Cast_ID = ACT_Casts.Cast_ID\n";
    $query .= "AND Event_Date >= CURDATE()\n"
      if $self->{tmp}->{Proj_Current} && ! $form->field('FullSched');
    $query .=
      "ORDER BY Event_Date, Event_Start, Event_End, ACT_Event.Event_ID";
    my $evts = $self->{dbh}->selectall_arrayref( $query, {},
      $Proj_ID, $Proj_ID, $Proj_ID, $Proj_ID );
    $params = $self->map_cols(
      [ 'Event_ID', 'cast', 'date', 'time', 'desc', 'is_admin', 'venue',
        'venue_id', 'scenes', 'abbr' ], $evts );
  } else {
    my $query =
      "SELECT Events.Event_ID, Cast_Name,
        DATE_FORMAT(Event_Date,'%a %b %e, %Y'),
        CONCAT(TIME_FORMAT(Event_Start, '%l:%i %p&ndash;'),
          TIME_FORMAT(Event_End, '%l:%i %p')),
        Event_Desc, $self->{tmp}->{is_admin},
        Venue_Short, ACT_Event.Venue_ID, Who
      FROM
        ( SELECT ACT_Event.Event_ID, GROUP_CONCAT(Grp_Name ORDER BY Grp_Order, Grp_Name) AS Who
          FROM ACT_Event
            NATURAL LEFT JOIN ACT_Evt_Grps
            NATURAL LEFT JOIN ACT_Group
          WHERE ACT_Event.Proj_ID = ? AND GR_Type = 'Group'
          GROUP BY ACT_Event.Event_ID
              UNION
          SELECT ACT_Event.Event_ID, Role_Name AS Who
          FROM ACT_Event JOIN ACT_Role ON GR_Type = 'Role' AND GR_ID = Role_ID
          WHERE ACT_Event.Proj_ID = ? ) AS Events
        JOIN ACT_Event ON Events.Event_ID = ACT_Event.Event_ID
        LEFT JOIN ACT_Venue ON ACT_Event.Venue_ID = ACT_Venue.Venue_ID
        JOIN ACT_Casts ON ACT_Event.Cast_ID = ACT_Casts.Cast_ID\n";
    $query .= "AND Event_Date >= CURDATE()\n"
      if $self->{tmp}->{Proj_Current} && ! $form->field('FullSched');
    $query .=
      "ORDER BY Event_Date, Event_Start, Event_End, ACT_Event.Event_ID";
    my $evts = $self->{dbh}->selectall_arrayref( $query, {}, $Proj_ID, $Proj_ID );
    $params = $self->map_cols(
      [ 'Event_ID', 'cast', 'date', 'time', 'desc', 'is_admin', 'venue', 'venue_id', 'scenes' ],
      $evts );
  }
  my $oparams = [];
  my $cur_event;
  my $cur_date;
  for ( @$params ) {
    if ( defined($cur_event) &&
	 $cur_event->{Event_ID} == $_->{Event_ID} ) {
      # This should no longer happen.
      $cur_event->{scenes} .= ", $_->{scenes}";
    } else {
      $cur_event = $_;
      $_->{date_rows} = 0;
      $cur_date = $_ unless defined $cur_date &&
	  $cur_date->{date} eq $_->{date};
      $cur_date->{date_rows}++;
      $_->{time} =~ s/12:00 PM/Noon/g;
      $_->{time} =~ s/12:00 AM/Midnight/g;
      $_->{time} =~ s/:00//g;
      $_->{time} =~ s/( [AP]M)(\&ndash;.*\1)/$2/;
      $_->{doublecast} = $doublecast;
      push @$oparams, $_;
    }
  }
  $form->tmpl_param( Events => $oparams );
  $form->tmpl_param( is_admin => $self->{tmp}->{is_admin} );
  $form->tmpl_param( Proj_Schedule => $form->field('Proj_Schedule') );
  # loop:Events [
  #    date
  #    desc
  #    venue
  #    Event_ID
  #    is_admin ]
	  
	my $Proj_URL = $self->{dbh}->selectrow_array(
		'SELECT Proj_URL FROM ACT_Project WHERE Proj_ID = ?',
		{}, $Proj_ID);
	$form->tmpl_param( Proj_URL => $Proj_URL) if $Proj_URL;

}

1;
