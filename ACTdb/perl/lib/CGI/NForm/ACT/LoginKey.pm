package CGI::NForm::ACT::LoginKey;
use strict;
use CGI;
use CGI::FormBuilder;
use Digest::MD5;

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    { # rm  =>  'DELETE',
      # edit => 'UPDATE',
      '' =>   'SELECT'
    };
}

# Support for the $params argument is optional, but necessary
# if you want to allow this form to be redirected.
# With FormBuilder 3.0, this functionality can be moved back
# to NForm and get_form() will be responsible only for specifying
# the fields and the template.
sub get_form {
  my ( $self, $op, $params )  = @_;
  $self->Connect();
  my $q = $self->{q} = ref($params) eq 'CGI' ? $params : new CGI($params);
  my $form = CGI::FormBuilder->new(
    action => "$ENV{SCRIPT_NAME}$self->{path_info}",
    method => 'POST',
    fields => [ 'Key', 'nxtPI', 'nxtPA' ],
    params => $q,
    template => "$self->{tmpldir}/LoginKey.html",
    title => "ACT Login"
  );
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'Key', required => 1 );
  return $form;
}

sub authorize {
  return 1;
}

sub INSERT {
  return 0;
}

sub UPDATE {
}

sub DELETE {
}

sub SELECT {
  my $self = shift;
  my $form = $self->{form};
  if ( $form->validate ) {
    my $key = $form->field('Key');
    $key =~ s/^(?:.*\D)?(\d+_[a-zA-Z0-9]{15}).*$/$1/;
    $form->sessionid($key);
    if ( $self->check_key ) {
      my $Part_ID = $self->{auth}->{Part_ID};
      my $Username = $self->{dbh}->selectrow_array(
        'SELECT Username from ACT_Users
	 WHERE Part_ID = ?;', {}, $Part_ID );
      my $PI = $Username ? "/User/edit" : "/User/new";
      $self->GoTo( $PI );
    } else {
      push @{$self->{errors}}, "Invalid Key";
      my $rawkey = $form->field('Key');
      $self->Log("Invalid Key: '$rawkey'");
    }
  }
  return 0;
}

sub InitFields {}

1;
