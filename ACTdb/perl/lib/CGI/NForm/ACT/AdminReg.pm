package CGI::NForm::ACT::AdminReg;
use strict;
use CGI;
use CGI::FormBuilder;

# Merge participant Part_ID2 into participant Part_ID1

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    { 
      '' =>   'SELECT'
    };
}

# Support for the $params argument is optional, but necessary
# if you want to allow this form to be redirected.
# With FormBuilder 3.0, this functionality can be moved back
# to NForm and get_form() will be responsible only for specifying
# the fields and the template.
sub get_form {
  my ( $self, $op, $params )  = @_;
  $self->Connect();
  my $q = $self->{q} = ref($params) eq 'CGI' ? $params : new CGI($params);
  my $form = CGI::FormBuilder->new(
    action => "$ENV{SCRIPT_NAME}$self->{path_info}",
    method => 'POST',
    fields => [ qw(nxtPI nxtPA FirstName LastName) ],
    params => $q,
    template => "$self->{tmpldir}/AdminReg.html",
    title => "Administrative Registration",
    submit => [ "Create a New Family", "Back" ]
  );
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'showall', options => 'Show Past Roles' );
  return $form;
}

sub authorize {
  my ( $self, $op ) = @_;
  if ( $self->check_key ) {
    my $contact_priv =
      $self->Get_Privilege_Value( -1, "ContactInfo" );
    return $self->error("Not authorized to view member data")
      unless $contact_priv >= 1;
    my $form = $self->{form};
    $form->tmpl_param( isdbadmin =>
      $self->Has_Privilege( 0, "DBAdmin", 1 ) );
    $self->{tmp}->{canedit} = $contact_priv >= 2;
    $self->{tmp}->{canreg} =
      $self->Has_Privilege( -1, "RegisterOthers", 1 );
    $self->{tmp}->{cangift} =
      $self->Has_Privilege( -1, "Gifts", 1 );
    $form->tmpl_param( canedit => $self->{tmp}->{canedit} );
    $form->tmpl_param( canreg => $self->{tmp}->{canreg} );
    return 1;
  }
  return 0;
}

sub INSERT {
}

sub UPDATE {
  return 0;
}

sub DELETE {
}

sub SELECT {
  my $self = shift;
  my $form = $self->{form};
  my $st = $form->submitted;
  if ( $form->submitted eq 'Back' ) {
    $self->Return;
  } elsif ( $form->submitted eq 'Create a New Family' ) {
    $self->Call( "/NewFamily" );
  } elsif ( $form->submitted eq 'DB Admin' ) {
    my $ln = $form->field('LastName');
    my $fn = $form->field('FirstName');
    my $sh = $form->field('showall');
    $self->GoTo( "/Fixup", LastName => $ln, FirstName => $fn,
      showall => $sh, _submitted => 1, _submit => 'Search' );
  }
  my ( $submit ) = grep m/^[A-Z][A-Z]\d+$/, $form->cgi_param();
  if ( $submit && $submit =~ m/^([A-Z][A-Z])(\d+)$/ ) {
    my $cmd= $1;
    my $id = $2;
    if ( $cmd eq 'EF' ) {
      $self->Call( "/Family/edit", [ _submitted => 1, _submit => 'Search' ], Fam_ID => $id );
    } elsif ( $cmd eq 'EP' ) {
      $self->Call( "/Person/edit", [ _submitted => 1, _submit => 'Search' ], Part_ID => $id );
    } elsif ( $cmd eq 'RP' ) {
      $self->Call( "/RegList", [ _submitted => 1, _submit => 'Search' ], Part_ID => $id );
    } elsif ( $cmd eq 'GH' ) {
      $self->Call( "/Gifts", [ _submitted => 1, _submit => 'Search' ], Fam_ID => $id );
    }
  }
  my $showall = $form->field('showall');
  if ( $form->submitted eq 'Search' || $form->submitted eq 'Submit' ) {
    $self->FLsearch( $showall,
      canedit => $self->{tmp}->{canedit},
      canreg => $self->{tmp}->{canreg},
      cangift => $self->{tmp}->{cangift} );
  } else {
    my $st = $form->submitted;
    $self->error( "Unrecognized submit: $st" );
  }
  return 0;
}

sub InitFields {
}

1;
