package CGI::NForm::ACT::Person;
use strict;
use CGI;
use CGI::FormBuilder;

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    { # rm  =>  'DELETE',
      edit => 'UPDATE',
      new => 'INSERT',
      '' =>   'SELECT'
    };
}

# Support for the $params argument is optional, but necessary
# if you want to allow this form to be redirected.
# With FormBuilder 3.0, this functionality can be moved back
# to NForm and get_form() will be responsible only for specifying
# the fields and the template.
sub get_form {
  my ( $self, $op, $params )  = @_;
  $self->Connect();
  my $q = $self->{q} = ref($params) eq 'CGI' ? $params : new CGI($params);
  my $form = CGI::FormBuilder->new(
    action => "$ENV{SCRIPT_NAME}$self->{path_info}#CurStep",
    method => 'POST',
    fields => [ qw(nxtPI nxtPA Part_ID Fam_ID
		  FirstName LastName FullName
		  WorkPhone CellPhone Status
		  BirthDate
		  Grade Email) ],
    required => [ qw(FirstName LastName Status) ],
    params => $q,
    template => "$self->{tmpldir}/Person.html",
    title => "Family Member Information",
    submit => [ "Submit Changes", "Back" ],
    stylesheet => 1,
    jsfunc => <<'EOJS'
      if (form._submit.value == 'Back') {
        return true;
      }
EOJS
  );
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'Part_ID', type => 'hidden' );
  $form->field( name => 'Fam_ID', type => 'hidden' );
  $form->field( name => 'FirstName', jsmessage => 'FirstName is required', message => 'FirstName is required' );
  $form->field( name => 'LastName', jsmessage => 'LastName is required', message => 'LastName is required' );
  $form->field( name => 'Status', options => [ 'Adult', 'Child' ], required => 1, label => 'Adult/Child' );
  my @date = localtime();
  my $month = $date[4] + 1;
  my $year = $date[5] + 1900;
  if ( $month < 7 ) { $year--; }
  my $nextyear = $year+1;
  $form->field( name => 'Grade', options => [ 'P', 'K', 1 .. 12, 'Grad' ],
      comment => "For the $year-$nextyear school year" );
  $form->field( name => 'WorkPhone', validate => '/^\d\d\d[ -.]?\d\d\d[ -.]?\d\d\d\d(x\d{1,4})?$/',
    comment => '###-###-####', required => 0 );
  $form->field( name => 'CellPhone', validate => '/^\d\d\d[ -.]?\d\d\d[ -.]?\d\d\d\d$/',
    comment => '###-###-####', required => 0 );
  $form->field( name => 'BirthDate', validate => 'DATE', required => 0 );
  return $form;
}

sub authorize {
  my ( $self, $op ) = @_;
  if ( $self->check_key(EU=>1) ) {
    my $form = $self->{form};
    my $My_Part_ID = $self->{auth}->{Part_ID};
    my ( $My_Fam_ID, $My_Status, $Age ) = $self->{dbh}->selectrow_array(
      'SELECT Fam_ID, Status,
       IF(Birthdate>0,
        YEAR(CURDATE())-YEAR(Birthdate)-(MID(CURDATE(),6,5)<MID(Birthdate,6,5)),
        -1) AS Age
       FROM ACT_Participant where Part_ID = ?',
      {}, $My_Part_ID );
    unless ( defined $My_Fam_ID && defined $My_Status ) {
      $self->fatal("No Fam_ID or Status defined for user Part_ID = $My_Part_ID");
    }
    my $Fam_ID = $form->field('Fam_ID');
    if ( $op eq 'INSERT' ) {
      if ( ! defined $Fam_ID ) {
        $form->field( name => 'Fam_ID', value => $My_Fam_ID, force => 1 );
        $Fam_ID = $My_Fam_ID;
      }
    } else {
      my $Part_ID = $form->field('Part_ID');
      my $Part_Fam_ID = $self->{dbh}->selectrow_array(
        'SELECT Fam_ID FROM ACT_Participant where Part_ID = ?',
        {}, $Part_ID );
      unless ( defined $Part_Fam_ID ) {
	$self->fatal("No Fam_ID defined for user Part_ID = $Part_ID");
      }
      if ( $Fam_ID ) {
	if ( $Fam_ID != $Part_Fam_ID ) {
	  $self->fatal("Specified Fam_ID does not match Participant's Fam_ID" );
	}
      } else {
	$form->field( name => 'Fam_ID', value => $Part_Fam_ID, force => 1 );
	$Fam_ID = $Part_Fam_ID;
      }
    }
    return 1 if ($Fam_ID == $My_Fam_ID && ($My_Status eq 'Adult' || $Age >= 18))
      || $self->Has_Privilege( -1, 'ContactInfo', 2 );
    $self->fatal( "Access to this page limited to adult family members" );
  }
  return 0;
}

sub get_submission {
  my $self = shift;
  my $form = $self->{form};
  my %vals;
  $self->FixPhoneEntry( qw(WorkPhone CellPhone) );
  for my $fld ( qw(Part_ID Fam_ID FirstName LastName FullName
		WorkPhone CellPhone Status BirthDate
		Grade Email ) ) {
    $vals{$fld} = $form->field($fld) || '';
  }
  if ( $vals{BirthDate} =~ m|^(\d+)/(\d+)/(\d\d\d\d)$| ) {
    $vals{BirthDate} = sprintf( "%04d-%02d-%02d", $3, $1, $2 );
  } elsif ( $vals{BirthDate} ) {
    $self->error( "Invalid BirthDate" );
  } elsif ( $vals{Status} eq 'Child' ) {
    $self->error( "BirthDate is required for children" );
  }
  my @date = localtime();
  my $acadyr = $date[5]+1900+($date[4]>=6?1:0);
  $vals{Grade} = 0 if $vals{Grade} eq 'K';
  $vals{Grade} = -1 if $vals{Grade} eq 'P';
  if ( $vals{Grade} =~ m/^(-?\d+)$/ ) {
    $vals{ClassYear} = 12 + $acadyr - $1;
  } elsif ( $vals{Grade} eq 'Grad' ) {
    $vals{ClassYear} = $acadyr - 1;
  } else {
    $vals{ClassYear} = '';
  }
  if ( ! $vals{FullName} ) {
    $vals{FullName} = "$vals{FirstName} $vals{LastName}";
  }
  return %vals;
}

sub INSERT {
  my $self = shift;
  my $form = $self->{form};
  $self->Return if $form->submitted eq 'Back';
  if ( $self->{form}->validate ) {
    my %vals = $self->get_submission;
    return 0 if @{$self->{errors}};
    my @KWs = qw(Fam_ID Addr_ID FirstName LastName FullName
		WorkPhone CellPhone Status BirthDate
		ClassYear Email);
    $vals{Addr_ID} = $self->{dbh}->selectrow_array(
      'SELECT Addr_ID FROM ACT_Family WHERE Fam_ID = ?',
      {}, $vals{Fam_ID} );
    my $cmd = join ' ',
      'INSERT INTO ACT_Participant (',
      join( ', ', @KWs ),
      ') VALUES (',
      join( ', ', map '?', @KWs ),
      ");";
    $self->{dbh}->do( $cmd, {}, map $vals{$_}, @KWs );
    my $Part_ID = $self->{dbh}->{mysql_insertid};
    $self->Log("New Part: $Part_ID:$vals{Fam_ID}");
    $self->Fixup_Parents( $vals{Fam_ID} );
    $self->Return( Part_ID => $Part_ID );
  }
  return 0;
}

sub UPDATE {
  my $self = shift;
  my $form = $self->{form};
  $self->Return if $form->submitted eq 'Back';
  if ( $form->validate ) {
    my %vals = $self->get_submission;
    return 0 if @{$self->{errors}};
    my @KWs = qw(FirstName LastName FullName
      WorkPhone CellPhone Status BirthDate
      Email);
    if ( $vals{Grade} eq 'Grad' ) {
      # Only update if current ClassYear is greater
      my $old_ClassYear = $self->{dbh}->selectrow_array(
        'SELECT ClassYear from ACT_Participant WHERE Part_ID = ?',
        {}, $vals{Part_ID} );
      push( @KWs, 'ClassYear' ) if $old_ClassYear == 0 || $old_ClassYear > $vals{ClassYear};
    } else {
      push( @KWs, 'ClassYear' );
    }
    my $cmd = join ' ',
      'UPDATE ACT_Participant SET',
      join( ', ', map "$_ = ?", @KWs ),
      'WHERE Part_ID = ?';
    $self->{dbh}->do( $cmd, {}, map $vals{$_}, @KWs, 'Part_ID' );
    $self->Fixup_Parents( $vals{Fam_ID} );
    $self->Return;
  }
  return 0;
}

sub DELETE {
}

sub SELECT {
  return 1;
}

sub InitFields {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  if ( $op eq 'UPDATE' ) {
    my $Part_ID = $form->field('Part_ID');
    my ( $ClassYear, @vals ) =
      $self->{dbh}->selectrow_array(
        "SELECT ClassYear, LastName, FirstName, FullName, WorkPhone, CellPhone,
	 Status, IF(BirthDate>0,DATE_FORMAT(BirthDate,'%m/%d/%Y'),''),
	 Email
	 FROM ACT_Participant WHERE Part_ID = ?", {}, $Part_ID );
    my $Grade = '';
    if ($ClassYear > 0) {
      my @date = localtime();
      my $acadyr = $date[5]+1900+($date[4]>=6?1:0);
      $Grade = 12 + $acadyr - $ClassYear;
      if ( $Grade > 12 ) {
        $Grade = 'grad';
      } elsif ( $Grade == 0 ) {
	$Grade = 'K';
      } elsif ( $Grade < 0 ) {
	$Grade = 'P';
      }
    }
    push( @vals, $Grade );
    for my $fname ( qw(LastName FirstName FullName WorkPhone CellPhone
                       Status BirthDate Email Grade) ) {
      $form->field( name => $fname, value => shift(@vals), force => 1 );
    }
  } else {
    my $LastName = $self->{dbh}->selectrow_array(
      'SELECT FamilyName FROM ACT_Family WHERE Fam_ID = ?', {},
       $form->field('Fam_ID') );
    if ( $LastName ) {
      $form->field( name => 'LastName', value => $LastName, force => 1 );
    }
  }
}

1;
