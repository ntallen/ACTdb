package CGI::NForm::ACT::Groups;
use strict;
use CGI;
use CGI::FormBuilder;

# Groups.pm is 'Scene List'
# provides a different view of scenes than Scenes.pm
# This is more traditional with just a list of groups/scenes
# with options to edit/delete, etc.

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    {
      '' => 'SELECT',
      'edit' => 'UPDATE'
    };
}

# Support for the $params argument is optional, but necessary
# if you want to allow this form to be redirected.
# With FormBuilder 3.0, this functionality can be moved back
# to NForm and get_form() will be responsible only for specifying
# the fields and the template.
sub get_form {
  my ( $self, $op, $params )  = @_;
  $self->Connect();
  my $q = $self->{q} = ref($params) eq 'CGI' ? $params : new CGI($params);
  my $form = CGI::FormBuilder->new(
    action => "$ENV{SCRIPT_NAME}$self->{path_info}",
    method => 'POST',
    fields => [ qw(nxtPI nxtPA Proj_ID) ],
    params => $q,
    template => "$self->{tmpldir}/Groups.html",
    title => "Scene Definitions",
    submit => [  "New Scene", "Back"  ],
    reset => 0
  );
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'Proj_ID', type => 'hidden' );
  
  my $Proj_ID = $form->field('Proj_ID') ||
    $self->fatal("No Proj_ID Specified");
  my $Proj_Name = $self->{dbh}->selectrow_array(
    'SELECT Proj_Name FROM ACT_Project WHERE Proj_ID = ?', {}, $Proj_ID );
  $form->title( "Scene/Group Definitions for $Proj_Name" );
  if ( $op eq 'UPDATE' ) {
    $form->submit( [ "New Scene", "Back" ] );
  } elsif ( $form->field('nxtPI') ) {
    $form->submit( [ "Back" ] );
  } else {
    $form->tmpl_param( nobutton => 1 );
  }
  return $form;
}

# Since we don't do anything on this form, we can allow
# access to all.
sub authorize {
  my ( $self, $op ) = @_;
  return 1 if $op eq 'SELECT';
  my $form = $self->{form};
  my $Proj_ID = $form->field('Proj_ID');
  if ( $self->check_key(EU=>1) &&
       $self->Has_Privilege( $Proj_ID, 'Schedule', 1 ) ) {
    $self->{tmp}->{is_admin} = 1;
    return 1;
  }
  $self->GoTo( "/Groups", Proj_ID => $Proj_ID );
}

sub INSERT {}

sub UPDATE {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  my $Proj_ID = $form->field('Proj_ID');
  if ( $form->submitted eq 'Back') {
    $self->Return;
  } elsif ( $form->submitted eq 'New Scene' ) {
    $self->Call( "/Scene/new", Proj_ID => $Proj_ID );
  } else {
    my ( $submit ) = grep m/^[A-Z][A-Z]\d+$/, $form->cgi_param();
    if ( $submit && $submit =~ m/^([A-Z][A-Z])(\d+)$/ ) {
      my $cmd = $1;
      my $id = $2;
      
      if ( $cmd eq 'ED' ) {
	$self->Call( "/Scene/edit", Proj_ID => $Proj_ID, Grp_ID => $id );
      } elsif ( $cmd eq 'CP' ) {
	$self->Call( "/Scene/new", Proj_ID => $Proj_ID, Grp_ID => $id );
      } elsif ( $cmd eq 'DE' ) {
	$self->Call( "/Scene/delete", Proj_ID => $Proj_ID, Grp_ID => $id );
      }
    }
  }
  $self->error("Unknown Option");
  return 0;
}

sub DELETE {}

sub SELECT {
  my ( $self ) = @_;
  $self->Return;
}

sub InitFields {}

sub InitParams {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  
  # scenes = [ name id ]
  my $is_admin = $self->{tmp}->{is_admin} || 0;
  my $Proj_ID = $form->field('Proj_ID');
  
  my $scene_ids = $self->{dbh}->selectall_arrayref(
    'SELECT ACT_Group.Grp_ID, Grp_Name, Grp_Desc, Role_Name
     FROM ACT_Group NATURAL LEFT JOIN ACT_Grp_Mem NATURAL LEFT JOIN ACT_Role
     WHERE ACT_Group.Proj_ID = ? AND Grp_Type = ?
     ORDER BY Grp_Order, Grp_Name, Role_Name', {},
     $Proj_ID, 'Normal' );

  my @scenes;
  my $curscene;
  for ( @$scene_ids ) {
    my ( $id, $name, $desc, $role ) = @$_;
    $role = '&nbsp;' unless $role;
    if ( $curscene && $curscene->{id} == $id ) {
      $curscene->{roles} .= "<br>\n$role";
    } else {
      $curscene = { id => $id, name => $name, desc => $desc, roles => $role, is_admin => $is_admin };
      push @scenes, $curscene;
    }
  }
  $form->tmpl_param( scenes => \@scenes );
  $form->tmpl_param( ShowRoles => 1 );
  $form->tmpl_param( is_admin => 1 ) if $is_admin;
}

1;
