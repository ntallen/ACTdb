package CGI::NForm::ACT::UnWait;
use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI::FormBuilder;
use Mail::Sendmail;
use Text::Wrap;

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    { 
      '' =>   'UPDATE'
    };
}

# Support for the $params argument is optional, but necessary
# if you want to allow this form to be redirected.
# With FormBuilder 3.0, this functionality can be moved back
# to NForm and get_form() will be responsible only for specifying
# the fields and the template.
sub get_form {
  my ( $self, $op, $params )  = @_;
  $self->Connect();
  my $q = $self->{q} = ref($params) eq 'CGI' ? $params : new CGI($params);
  my $form = CGI::FormBuilder->new(
    action => "$ENV{SCRIPT_NAME}$self->{path_info}",
    method => 'POST',
    fields => [ qw(nxtPI nxtPA Part_ID Role_ID Mail_ID Cast_Status) ],
    params => $q,
    template => "$self->{tmpldir}/UnWait.html",
    title => "Accept Participant from the Waiting List",
    submit => [ "Submit", "Back" ]
  );
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'Role_ID', type => 'hidden' );
  $form->field( name => 'Part_ID', type => 'hidden' );
  $form->field( name => 'Cast_Status', type => 'hidden' );
  my $Role_ID = $form->field('Role_ID');
  $self->fatal("Missing or invalid Role_ID")
    unless ( defined($Role_ID) && $Role_ID =~ m/^\d+$/ );
  my $Part_ID = $form->field('Part_ID');
  $self->fatal("Missing or invalid Part_ID")
    unless ( defined($Part_ID) && $Part_ID =~ m/^\d+$/ );
  my $Crnt_Status = $self->{dbh}->selectrow_array(
    'SELECT Cast_Status FROM ACT_Cast WHERE Role_ID = ? AND Part_ID = ?',
    {}, $Role_ID, $Part_ID );
  unless ( $Crnt_Status ) {
    $self->fatal( "Specified Part_ID not found in specified Role_ID" );
  }
  $self->{Crnt_Status} = $Crnt_Status;
  
  return $form;
}

sub authorize {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  my $Role_ID = $form->field('Role_ID');
  if ( $self->check_key(EU => 1) ) {
    my $Proj_ID = $self->{dbh}->selectrow_array(
      "SELECT Proj_ID FROM ACT_Role WHERE Role_ID = ?",
      {}, $Role_ID );
    return self->error( "Invalid Role_ID" )
      unless defined($Proj_ID);
    return 1 if $self->Has_Privilege( $Proj_ID, 'Registration', 2 );
    # return 1 if $self->has_role2( $Role_ID, 'Registrar' );
    $self->error( "Not authorized to modify registration" );
  }
  return 0;
}

sub INSERT {
}

sub SELECT {
  return 0;
}

sub DELETE {
}

sub UPDATE {
  my $self = shift;
  my $form = $self->{form};
  $self->Return if $form->submitted eq 'Back';
  my $Part_ID = $form->field('Part_ID');
  my $Role_ID = $form->field('Role_ID');
  my $Cast_Status = $form->field('Cast_Status');
  my $Crnt_Status = $self->{Crnt_Status};
  $self->fatal( "Role_ID and Part_ID must be defined" )
    unless $Part_ID && $Role_ID;

  if ( $Crnt_Status eq $Cast_Status ) {
    $self->error( "Participant is already $Cast_Status" );
    return 0;
  }
  my @MIDs = $form->field('Mail_ID');
  my %msg;
  if ( @MIDs ) {
    my $cmd = join ' ',
      'SELECT Email from ACT_Participant WHERE Part_ID IN (',
      join( ', ', @MIDs ), ')';
    my $emails = $self->{dbh}->selectcol_arrayref( $cmd );
    $msg{To} = join(', ', @$emails);
    $msg{CC} = 'webmaster@act.arlington.ma.us';
  } else {
    $msg{To} = 'webmaster@act.arlington.ma.us';
  }
  my ( $Role_Name, $Proj_ID, $Proj_Name, $Role_Fee ) =
    $self->{dbh}->selectrow_array(
      'SELECT Role_Name, ACT_Role.Proj_ID, Proj_Name, Role_Fee FROM
       ACT_Role NATURAL JOIN ACT_Project
       WHERE Role_ID = ?', {}, $Role_ID );
  $self->fatal( "Unknown Role/Project" ) unless $Role_Name && $Proj_Name;
  # warn("$Crnt_Status/$Proj_Name/$Role_Name/$Proj_ID/$Role_ID/\$$Role_Fee\n");
  
  $self->{dbh}->do(
   'LOCK TABLES ACT_Cast WRITE, ACT_Cast AS C READ, ACT_Invoice WRITE,
    ACT_Participant READ, ACT_Project WRITE, ACT_Role READ,
    ACT_Participant AS P2 READ, ACT_Role AS R1 WRITE,
    ACT_Role AS R2 WRITE, ACT_Log WRITE, ACT_Subproject WRITE' );

  $self->{dbh}->do(
    "UPDATE ACT_Cast SET Cast_Status = ?
     WHERE Role_ID = ? AND Part_ID = ? ",
    {}, $Cast_Status, $Role_ID, $Part_ID );
    
  # Also if transition is to withdraw, should also withdraw
  # participant from any other roles in this project (specifically
  # auditions and character roles) Actually, I think I'll delete
  # entries for Audition and Character roles and then leave
  # any other Category, Committee or Job roles untouched
  # (That allows for the case where an intern is on the
  # Production Team category and registered, but withdraws
  # as an ACTor, staying on as intern)
  # The downside is that we lose a bit of history. The
  # upside is I can be a little sloppier with queries
  # regarding the cast.
  # To handle subprojects, I will delete roles in the
  # same project as $Role_ID as well as roles in projects
  # that list this project as $Parent_Proj
  if ( $Cast_Status eq 'Withdrawn' ) {
    $self->{dbh}->do(
      "DELETE FROM ACT_Cast
      WHERE Part_ID = ? AND Role_ID IN
      ( SELECT R1.Role_ID
        FROM ACT_Role AS R1
        WHERE R1.Proj_ID IN
          (   SELECT ? AS Proj_ID
            UNION
              SELECT Proj_ID
              FROM ACT_Subproject
              WHERE Parent_Proj = ?
          )
          AND R1.Role_Type IN ( 'Audition', 'Character' ))",
      {}, $Part_ID, $Proj_ID, $Proj_ID );
  }

  my $Invoice_ID = $self->check_invoice( $Cast_Status, $Role_ID, $Part_ID,
		    $Role_Fee, "$Role_Name for $Proj_Name" );

  my $Fam_ID = $self->{dbh}->selectrow_array(
    'SELECT Fam_ID FROM ACT_Participant WHERE Part_ID = ?', {},
    $Part_ID );
  $self->Fixup_Parents( $Fam_ID );

  $self->{dbh}->do('UNLOCK TABLES');

  $self->Log( $Cast_Status, "Part_ID=$Part_ID&Role_ID=$Role_ID" );
    
  # Now send an e-mail confirmation
  if ( 1 ) { # $CGI::NForm::ACT::Config{smtp_server} ) {
    my @MIDs = $form->field('Mail_ID');
    my %msg;
    if ( @MIDs ) {
      my $cmd = join ' ',
      	'SELECT Email from ACT_Participant WHERE Part_ID IN (',
      	join( ', ', @MIDs ), ") AND Email <> ''";
      my $emails = $self->{dbh}->selectcol_arrayref( $cmd );
      $msg{To} = join(', ', @$emails);
      $msg{CC} = 'webmaster@act.arlington.ma.us';
    } else {
      $msg{To} = 'webmaster@act.arlington.ma.us';
    }
    $msg{From} = 'webmaster@act.arlington.ma.us';
    my $vals = $self->RegConfirm( $Role_ID, $Part_ID, 0, $Crnt_Status );
    $msg{Subject} = "ACT Registration Update: $vals->{Proj_Name}";
    $msg{smtp} = $CGI::NForm::ACT::Config{smtp_server};
    $msg{Message} = $vals->{Message};
    if ( $self->sendmail_wrap( %msg ) ) {
      $self->Log( "Email Sent Role_ID=$Role_ID Part_ID=$Part_ID");
    }
  }

  return 0 if @{$self->{errors}};
  $self->Return;
}

sub InitFields {}

sub InitParams {
  # Need to initialize the list of Mail_ID selections
  # loop Mail_IDS ( Part_ID FullName Email )
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  my $Part_ID = $form->field('Part_ID');
  $self->fatal("Part_ID required") unless $Part_ID;
  my $Role_ID = $form->field('Role_ID');
  $self->fatal("Role_ID required") unless $Role_ID;
  my ( $Role_Name, $Proj_Name ) = $self->{dbh}->selectrow_array(
    'SELECT Role_Name, Proj_Name FROM ACT_Role NATURAL JOIN ACT_Project
     WHERE Role_ID = ?', {}, $Role_ID );
  $self->fatal("Invalid Role_ID") unless $Role_Name && $Proj_Name;
  $form->tmpl_param( Role_Name => $Role_Name );
  $form->tmpl_param( Proj_Name => $Proj_Name );
  my ( $FirstName, $LastName ) = $self->{dbh}->selectrow_array(
    'SELECT FirstName, LastName FROM ACT_Participant
     WHERE Part_ID = ?', {}, $Part_ID );
  $self->fatal("Invalid Part_ID") unless $FirstName && $LastName;
  $form->tmpl_param( FirstName => $FirstName );
  $form->tmpl_param( LastName => $LastName );
  $form->tmpl_param( Crnt_Status => $self->{Crnt_Status} );
  $form->tmpl_param( Cast_Status => $form->field('Cast_Status') );
  my $MIDs = $self->{dbh}->selectall_arrayref(
    "SELECT P2.Part_ID, P2.FullName, P2.Email
     FROM ACT_Participant AS P1, ACT_Participant AS P2
     WHERE P1.Part_ID = ? AND
        ( ( P1.Fam_ID = P2.Fam_ID AND
            ( P2.Status = 'Adult' OR P2.Part_ID = P1.Part_ID ) ) OR
	  P2.Part_ID = ? ) AND
	P2.Email != ''",
     {}, $Part_ID, $self->{auth}->{Part_ID} );
  $MIDs = $self->map_cols( [ 'Part_ID', 'FullName', 'Email' ], $MIDs );
  $form->tmpl_param( Mail_IDS => $MIDs );
}

1;
