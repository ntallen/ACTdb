package CGI::NForm::ACT::MySchedule;
use strict;
use CGI;
use CGI::FormBuilder;

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    { # rm  =>  'DELETE',
      # edit => 'UPDATE',
      '' =>   'SELECT'
    };
}

# Support for the $params argument is optional, but necessary
# if you want to allow this form to be redirected.
# With FormBuilder 3.0, this functionality can be moved back
# to NForm and get_form() will be responsible only for specifying
# the fields and the template.
sub get_form {
  my ( $self, $op, $params )  = @_;
  $self->Connect();
  my $q = $self->{q} = ref($params) eq 'CGI' ? $params : new CGI($params);
  my $form = CGI::FormBuilder->new(
    action => "$ENV{SCRIPT_NAME}$self->{path_info}",
    method => 'GET',
    params => $q,
    template => "$self->{tmpldir}/MySchedule.html",
    title => "My Schedule",
    submit => [ "My Schedule" ]
  );
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'Proj_ID', type => 'hidden' );
  $form->field( name => 'FullSched', options => [ 'Show Past Rehearsals' ] );
  my $Proj_ID = $form->field('Proj_ID') ||
    $self->fatal("No Proj_ID Specified" );
  my $chars = $self->{dbh}->selectall_arrayref(
    'SELECT Role_ID, Role_Name FROM ACT_Role WHERE
     Proj_ID = ? AND Role_Type = ?
     ORDER BY Role_Name', {},
     $Proj_ID, 'Character' );
  $form->field( name => 'Chars', type => 'checkbox',
     options => $chars, linebreaks => 1 );
  $self->{tmp}->{chars} = $chars;
  my ( $Proj_Name, $Proj_Schedule, $N_Casts, $Proj_Status ) =
    $self->{dbh}->selectrow_array(
    'SELECT Proj_Name, Proj_Schedule, N_Casts, Proj_Status
     FROM ACT_Project WHERE Proj_ID = ?', {}, $Proj_ID );
  $form->tmpl_param( Proj_Name => $Proj_Name );
  $form->tmpl_param( Proj_Schedule => $Proj_Schedule );
  if ( $Proj_Status ne 'Archive' ) {
    $form->tmpl_param( Proj_Current => 1 );
    $self->{tmp}->{Proj_Current} = 1;
  }
  if ( $N_Casts == 2 ) {
    $self->{tmp}->{doublecast} = 1;
    $form->tmpl_param( doublecast => 1 );
    my $casts = $self->{dbh}->selectall_arrayref(
      'SELECT Cast_ID, Cast_Name FROM ACT_Casts ORDER BY Cast_ID' );
    $form->field( name => 'Cast_ID', options => $casts,
      value => 0, cleanopts => 0 );
    $self->{tmp}->{casts} = { map @$_, @$casts };
  }
  return $form;
}

sub authorize {
  return 1;
  ### Later, I could add authentication if Part_ID or Fam_ID
  ### is specified
}

sub INSERT {
  return 0;
}

sub UPDATE {
}

sub DELETE {
}

#      LEFT JOIN ACT_Role AS R2 ON ((I.Role_ID IS NULL AND R1.Role_ID = R2.Role_ID) OR
#       (I.Role_ID IS NOT NULL AND I.Role_ID = R2.Role_ID
#        AND (I.Cast_ID = 0 OR I.Cast_ID = ACT_Event.Cast_ID)))
sub SELECT {
  my $self = shift;
  my $form = $self->{form};
  return 0 if $form->submitted eq 'Back';
  my $Proj_ID = $form->field('Proj_ID');
  my @chars = $form->field('Chars');
  my $cast = $self->{tmp}->{doublecast} ? $form->field('Cast_ID') : 0;
  my $ForWhom = 'All Roles';
  my @args;
  my $query =
    "SELECT
      E.Event_ID,
      DATE_FORMAT(Event_Date,'%a %b %e'),
      CONCAT(TIME_FORMAT(Event_Start, '%l:%i %p&ndash;'),
      TIME_FORMAT(Event_End, '%l:%i %p')),
      Event_Desc, Venue_Short, E.Venue_ID, R1.Role_Name,
      Cast_Name
     FROM ACT_Event AS E USE INDEX (Proj_ID)
      NATURAL JOIN ACT_Evt_Grps
      NATURAL JOIN ACT_Group
      NATURAL JOIN ACT_Grp_Mem AS G
      NATURAL JOIN ACT_Role AS R1\n";
  if ( @chars ) {
    my $where = "WHERE Role_ID IN (" . join( ', ', map '?', @chars ) . ")";
    $query .= "JOIN ((SELECT Role_ID, 0 AS Cast_ID FROM ACT_Role "
      . $where . ") UNION (SELECT Is_Also_ID, Cast_ID FROM ACT_Is_Also AS I "
      . $where . ")) AS R2 ON R1.Role_ID = R2.Role_ID ";
    push @args, @chars, @chars;
  }
  $query .=
     "LEFT JOIN ACT_Venue ON E.Venue_ID = ACT_Venue.Venue_ID
      JOIN ACT_Casts
     WHERE E.Proj_ID = ? AND GR_Type = 'Group'
     AND E.Cast_ID = ACT_Casts.Cast_ID\n";
  $query .= "AND Event_Date >= CURDATE()\n"
    if $self->{tmp}->{Proj_Current} && ! $form->field('FullSched');
  push @args, $Proj_ID;
  if ( @chars ) {
    $query .= "AND (R2.Cast_ID = 0 OR E.Cast_ID = 0 OR R2.Cast_ID = E.Cast_ID)";
    my %charnames = ( map @$_, @{$self->{tmp}->{chars}} );
    my @charnames = map $charnames{$_}, @chars;
    $ForWhom = pop(@charnames);
    if ( @charnames ) {
      $ForWhom = join( ', ', @charnames ) . " and $ForWhom";
    }
  }
  if ( $self->{tmp}->{doublecast} ) {
    if ( $cast ) {
      $query .= " AND ( E.Cast_ID = 0 OR E.Cast_ID = ? )";
      push @args, $cast;
      $ForWhom .= " in the " . $self->{tmp}->{casts}->{$cast} . " Cast";
    } else {
      $ForWhom .= " in Both Casts";
    }
  }
  $query .= " GROUP BY E.Event_ID, R1.Role_ID
              ORDER BY Event_Date, Event_Start,
	      Event_End, E.Event_ID, R1.Role_ID";
  my $reh = $self->{dbh}->selectall_arrayref( $query, {}, @args );
  my $params = $self->map_cols(
    [ 'Event_ID', 'date', 'time', 'desc', 'venue', 'venue_id', 'who', 'cast' ],
    $reh );
  my $cur_event;
  my $cur_date;
  my $oparams = [];
  for ( @$params ) {
    if ( defined($cur_event) &&
	 $cur_event->{Event_ID} == $_->{Event_ID} ) {
      $cur_event->{who} .= ", $_->{who}";
    } else {
      $cur_event = $_;
      $_->{date_rows} = 0;
      $cur_date = $_ unless defined $cur_date &&
	  $cur_date->{date} eq $_->{date};
      $cur_date->{date_rows}++;
      $_->{time} =~ s/12:00 PM/Noon/g;
      $_->{time} =~ s/12:00 AM/Midnight/g;
      $_->{time} =~ s/:00//g;
      $_->{time} =~ s/( [AP]M)(\&ndash;.*\1)/$2/;
      $_->{doublecast} = 1 if $self->{tmp}->{doublecast};
      push @$oparams, $_;
    }
  }
  my $Proj_URL = $self->{dbh}->selectrow_array(
    "SELECT Proj_URL FROM ACT_Project WHERE Proj_ID = ?",
	{}, $Proj_ID );
  $form->tmpl_param( Proj_URL => $Proj_URL ) if ( $Proj_URL );
  $form->tmpl_param( Proj_CurURL => 1 )
    if ( $Proj_URL || $self->{tmp}->{Proj_Current} );
  $form->tmpl_param( ForWhom => $ForWhom );
  $form->tmpl_param( Events => $oparams );
  $form->tmpl_param( has_list => 1 );
  $form->field(name => 'Chars', type => 'hidden', linebreaks => 0 );
  $form->field(name => 'Cast_ID', type => 'hidden' );
  return 0;
}

sub InitFields {
}

sub InitParams {
  my $self = shift;
  my $form = $self->{form};
  my $Proj_ID = $form->field('Proj_ID');
}

1;
