package CGI::NForm::ACT::Task_Admin;
use strict;
use CGI;
use CGI::FormBuilder;

# Created 2/19/2009

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    {
      '' => 'UPDATE'
    };
}

# 
sub InitForm {
  my ( $self, $form, $op )  = @_;
  $self->Connect();
  # Define the form fields
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'Proj_ID', type => 'hidden' );
  $form->field( name => 'ClearOpts', label => 'Clear',
    options => [ qw(Tasks Notes Audit) ], multiple => 1, value => 'Tasks');
  $form->field( name => 'Upload', type => 'textarea', cols => 80, rows => 3  );
  my $Proj_ID = $form->field('Proj_ID');
  $self->fatal("Invalid or missing Proj_ID") if $Proj_ID !~ /^\d+$/;
  my $Proj_Name = $self->{dbh}->selectrow_array(
    "SELECT Proj_Name FROM ACT_Project WHERE Proj_ID = ?", {}, $Proj_ID );
  $self->fatal("Invalid Proj_ID") unless $Proj_Name;
  $form->title("Task Administration for $Proj_Name"); # This should be qualified by project
  $form->template( "$self->{tmpldir}/Task_Admin.html" );
}

# Return non-zero if authorized
sub authorize {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  if ( $self->check_key( EU => 1 )) {
    my $Proj_ID = $form->field('Proj_ID');
    $self->fatal("Not authorized for administrative access to tasks for this project")
      unless $self->Has_Privilege( $Proj_ID, 'TaskAdmin', 4 );
    return 1;
  }
  return 0;
}

sub INSERT {}

sub UPDATE {
  my $self = shift;
  my $form = $self->{form};
  $self->Return if $form->submitted eq 'Back';
  my $Proj_ID = $form->field('Proj_ID');
  if ( $form->validate ) {
    my $Upload = $form->field('Upload');
    my @Upload = grep /^[^\t]/, split( /[\r\n]+/, $Upload );
    if ( @Upload ) {
      my $cmd = shift @Upload;
      my @cmd = split(/\t/, $cmd);
      if ( $cmd[0] ne 'Project' ) {
        return $self->error("First line of upload must be 'Project'");
      }  elsif ( $cmd[1] ne $Proj_ID ) {
        return $self->error("Wrong Project ID in Upload" );
      }
    }
    my @ClearOpts = $form->field('ClearOpts');
    for my $clear ( @ClearOpts ) {
      if ( $clear eq 'Tasks' ) {
        $self->{dbh}->do(
          "DELETE FROM ACT_Task WHERE Proj_ID = ?", {}, $Proj_ID );
        $self->{dbh}->do(
          "DELETE FROM ACT_Task_Abbr WHERE Proj_ID = ?", {}, $Proj_ID );
        $self->{dbh}->do(
          "DELETE FROM ACT_Task_Milestone WHERE Proj_ID = ?", {}, $Proj_ID );
        $self->{dbh}->do(
          "DELETE FROM ACT_Task_Delegate WHERE Proj_ID = ?", {}, $Proj_ID );
      } elsif ( $clear eq 'Notes' ) {
        $self->{dbh}->do(
          "DELETE FROM ACT_Task_Note WHERE Proj_ID = ?", {}, $Proj_ID );
      } elsif ( $clear eq 'Audit' ) {
        $self->{dbh}->do(
          "DELETE FROM ACT_Task_Audit WHERE Proj_ID = ?", {}, $Proj_ID );
      } else {
        $self->error("Invalid Clear option: '$clear'");
        return 0;
      }
    }
    my %Abbr_ID;
    my %Milestone_ID;
    $Milestone_ID{Ongoing} = 0;
    my %Task_ID;
    for my $cmd ( @Upload ) {
      my @cmd = split(/\t/, $cmd);
      return $self->error("Duplicate 'Project' specification" )
        if $cmd[0] eq 'Project';
      if ( $cmd[0] eq 'Abbr' ) {
        return $self->error("Abbr: Not enough arguments")
          unless @cmd >= 4;
        my $Role_Abbr = $cmd[1];
        my $Role_Type = $cmd[2];
        my $Role_Name = $cmd[3];
        my $Cast_Note = $cmd[4] || '';
        return $self->error("Invalid abbreviation: '$Role_Abbr'")
          unless $Role_Abbr =~ m/^\w+$/;
        return $self->error("Attempt to redefine abbreviation: '$Role_Abbr'")
          if $Abbr_ID{$Role_Abbr};
        return $self->error("Invalid Role_Name: '$Role_Name'")
          unless $Role_Name =~ m/^\w(?:[\w\s]*\w)$/;
        my $Role_Proj_ID = $Proj_ID;
        if ( $Role_Type eq 'Global' ) {
          $Role_Proj_ID = 0;
        } elsif ( $Role_Type eq 'Board' ) {
          $Role_Proj_ID = 1;
        } elsif ( $Role_Type ne 'Current' ) {
          return $self->error("Invalid Role_Type: '$Role_Type'" );
        }
        my $Role_ID = $self->{dbh}->selectrow_array(
          "SELECT Role_ID FROM ACT_Role WHERE Proj_ID = ? AND Role_Name = ?",
          {}, $Role_Proj_ID, $Role_Name );
        if ( ! defined($Role_ID) ) {
          if ( $Role_Type eq 'Current' ) {
            $self->{dbh}->do(
              "INSERT INTO ACT_Role ( Proj_ID, Role_Name, Role_Type, Role_Status )
               VALUES ( ?, ?, ?, ? )", {},
               $Proj_ID, $Role_Name, "Committee", "Closed" );
            $Role_ID = $self->{dbh}->{mysql_insertid};
            $self->error("Defined role '$Role_Name' for current project");
          }
          return $self->error("Role '$Role_Name' undefined for $Role_Type project")
            unless $Role_ID;
        }
        $self->{dbh}->do(
          "INSERT INTO ACT_Task_Abbr ( Proj_ID, Role_Abbr, Role_ID, Cast_Note )
           VALUES (?, ?, ?, ?)", {},
           $Proj_ID, $Role_Abbr, $Role_ID, $Cast_Note );
        $Abbr_ID{$Role_Abbr} = $self->{dbh}->{mysql_insertid};
      } elsif ( $cmd[0] eq 'Milestone' ) {
        # ID	Date	Description	Constraints
        my $Milestone = $cmd[1];
        my $Milestone_Date = $cmd[2];
        my $Milestone_Desc = $cmd[3];
        return $self->error("Must define Milestone, Date and Description")
          unless $Milestone && $Milestone_Date && $Milestone_Desc;
        return $self->error("Invalid Milestone: '$Milestone'")
          unless $Milestone =~ m/^\w+$/;
        return $self->error("Attempt to redefine milestone '$Milestone'")
          if $Milestone_ID{$Milestone};
        if ( $Milestone_Date =~ m|^(\d+)/(\d+)/(\d+)$| ) {
          my $y = $3 > 2000 ? $3 : ($3+2000);
          $Milestone_Date = sprintf("%d-%02d-%02d", $y, $1, $2);
        } elsif ( $Milestone_Date !~ m|^(\d{4})-(\d{2})-(\d{2})$| ) {
          return $self->error("Unrecognized date format: '$Milestone_Date'");
        }
        $self->{dbh}->do(
          "INSERT INTO ACT_Task_Milestone ( Proj_ID, Milestone, Milestone_Date, Milestone_Desc )
           VALUES ( ?, ?, ?, ? )", {},
           $Proj_ID, $Milestone, $Milestone_Date, $Milestone_Desc );
        $Milestone_ID{$Milestone} = $self->{dbh}->{mysql_insertid};
      } elsif ( $cmd[0] eq 'Task' ) {
        # ID	When	Action	Notes	Category	Do	Consult	Inform	Approve
        my $Task_ID = $cmd[1];
        my $When = $cmd[2];
        my $Task_Action = $cmd[3];
        my $Task_Desc = $cmd[4];
        my $Task_Category = $cmd[5];
        my %Delegate;
        $Delegate{Do} = $cmd[6];
        $Delegate{Consult} = $cmd[7];
        $Delegate{Inform} = $cmd[8];
        $Delegate{Approve} = $cmd[9];
        next unless $Task_Action;
        return $self->error("Invalid Task_ID: '$Task_ID'")
          unless $Task_ID =~ m/^\d+$/;
        return $self->error("Duplicate Task_ID: $Task_ID")
          if $Task_ID{$Task_ID};
        return $self->error("Invalid Milestone reference: '$When'")
          unless $When =~ m|^(\w+)((?:[+-]\d+)?)$|;
        my $Milestone = $1;
        my $Milestone_Delta = $2 || 0;
        return $self->error("Task $Task_ID: Undefined Milestone: '$Milestone'" )
          unless defined $Milestone_ID{$Milestone};
        return $self->error("Task $Task_ID: Undefined or invalid category")
          unless $Task_Category =~ m|^\w+$|;
        my ( $Task_Status, $Date_Completed ) =
          $self->{dbh}->selectrow_array(
            "SELECT Task_Status, TA_Date FROM ACT_Task_Audit
             WHERE Proj_ID = ? AND Task_ID = ?
             ORDER BY TA_Date Desc LIMIT 1",
             {}, $Proj_ID, $Task_ID
          );
        if ( ! defined $Task_Status ) {
          $Task_Status = $Milestone eq 'Ongoing' ? 'Ongoing' : '';
        }
        $Date_Completed = '0000-00-00' unless $Task_Status eq 'Done';
        $self->{dbh}->do(
          "INSERT INTO ACT_Task ( Proj_ID, Task_ID, Milestone_ID, Milestone_Delta,
            Task_Action, Task_Desc, Task_Category, Task_Status, Task_Date_Completed )
           VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ? )", {},
           $Proj_ID, $Task_ID, $Milestone_ID{$Milestone}, $Milestone_Delta,
           $Task_Action, $Task_Desc, $Task_Category, $Task_Status, $Date_Completed );
        # Now we have to process the delegations
        for my $Task_Del_Role ( qw(Do Consult Inform Approve) ) {
          my @list = split /[, ]+/, $Delegate{$Task_Del_Role};
          my @unknown = grep ! $Abbr_ID{$_}, @list;
          return $self->error( "Task_ID $Task_ID: Unknown Abbreviation(s): " .
                    join(", ", map("'$_'", @unknown)))
            if @unknown;
          for my $Abbr ( @list ) {
            $self->{dbh}->do(
              "INSERT INTO ACT_Task_Delegate (Proj_ID, Task_ID, Abbr_ID, Task_Del_Role)
               VALUES (?,?,?,?)", {},
              $Proj_ID, $Task_ID, $Abbr_ID{$Abbr}, $Task_Del_Role );
          }
        }
      } elsif ( $cmd[0] ) {
        return $self->error("Invalid command: '$cmd[0]'");
      }
    }
  }
  return 0;
}

sub SELECT {}

sub DELETE {}

# Called before form has been submitted
sub InitFields {
  # my ( $self, $op ) = @_;
  # my $form = $self->{form};
}

# Called after INSERT, UPDATE, SELECT, DELETE or InitFields
sub InitParams {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
}

1;
