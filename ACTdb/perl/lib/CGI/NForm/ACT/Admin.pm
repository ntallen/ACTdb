package CGI::NForm::ACT::Admin;
use strict;
use CGI;
use CGI::FormBuilder;

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    { # rm  =>  'DELETE',
      # edit => 'UPDATE',
      '' =>   'SELECT'
    };
}

# Support for the $params argument is optional, but necessary
# if you want to allow this form to be redirected.
# With FormBuilder 3.0, this functionality can be moved back
# to NForm and get_form() will be responsible only for specifying
# the fields and the template.
sub get_form {
  my ( $self, $op, $params )  = @_;
  $self->Connect();
  my $q = $self->{q} = ref($params) eq 'CGI' ? $params : new CGI($params);
  my $form = CGI::FormBuilder->new(
    action => "$ENV{SCRIPT_NAME}$self->{path_info}",
    method => 'POST',
    fields => [ 'nxtPI', 'nxtPA', 'FirstName', 'LastName' ],
    params => $q,
    template => "$self->{tmpldir}/Admin.html",
    title => "ACT Administrative Interface",
    submit => "Back"
  );
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'showall', options => 'Show Past Roles' );
  my $shows = $self->map_cols( [ 'Proj_ID', 'Proj_Name', 'Proj_Preview' ],
    $self->{dbh}->selectall_arrayref(
      "SELECT Proj_ID, Proj_Name,
         IF(Proj_Status = 'Preview' OR Proj_Status = 'Proof', Proj_Status, '')
       FROM ACT_Project WHERE Proj_Type = 'Show' AND
       ( Proj_Status != 'Archive' )
       ORDER BY Proj_ID" ) );
  my $workshops = $self->map_cols( [ 'Proj_ID', 'Proj_Name', 'Proj_Preview' ],
    $self->{dbh}->selectall_arrayref(
      "SELECT Proj_ID, Proj_Name,
         IF(Proj_Status = 'Preview' OR Proj_Status = 'Proof', Proj_Status, '')
       FROM ACT_Project WHERE Proj_Type IN ( 'Workshop', 'FamilyEvent' ) AND
       ( Proj_Status != 'Archive' )
       ORDER BY Proj_ID" ) );
  my $archived = $self->map_cols( [ 'Proj_ID', 'Proj_Name', 'Proj_Type', 'Proj_End' ],
    $self->{dbh}->selectall_arrayref(
      "SELECT Proj_ID, Proj_Name, Proj_Type, DATE_FORMAT(Proj_End,'%m/%y') AS Proj_End
       FROM ACT_Project WHERE Proj_Status = 'Archive'
       ORDER BY Proj_ID DESC" ) );
  $form->tmpl_param( Shows => $shows );
  $form->tmpl_param( Workshops => $workshops );
  $form->tmpl_param( Archived => $archived );
  return $form;
}

sub authorize {
  my ( $self, $op ) = @_;
  if ( $self->check_key ) {
    $self->fatal( "Assert failed: No auth element" )
      unless $self->{auth};
    return 1 if $self->Has_Privilege( -1, 'ProjectAdmin', 1)
  } else {
    $self->fatal("Authentication Failed");
  }
  return 0;
}

sub INSERT {
  return 0;
}

sub UPDATE {
}

sub DELETE {
}

sub SELECT {
  my $self = shift;
  my $form = $self->{form};
  my $sbmt = $form->submitted;
  if ( $sbmt =~ m/Families/ ) {
    $self->Call( '/MailList' );
  } elsif ( $sbmt eq 'Back' ) {
    $self->Return;
  } elsif ( $sbmt eq "Create a new Project" ||
            $sbmt eq "New Project" ) {
    $self->Call( "/Project/new" );
  } elsif ( $sbmt eq "Administrative Registration" ||
            $sbmt eq "Search" ) {
    my $ln = $form->field('LastName');
    my $fn = $form->field('FirstName');
    my $sh = $form->field('showall');
    $self->Call( "/AdminReg", _submitted => 1, _submit => 'Search',
       LastName => $ln, FirstName => $fn, showall => $sh );
  } elsif ( $sbmt eq "Database Administration" ) {
    $self->Call( "/Fixup" );
  } elsif ( $sbmt eq "Board Schedule" ) {
    $self->Call( "/Schedule/edit", Proj_ID => 1 );
  } elsif ( $sbmt eq "Global Roles" ) {
    $self->Call( "/Roles", Proj_ID => 0 );
  } elsif ( $sbmt eq "Workshops Page" ) {
    $self->Call( "/Workshops" );
  } else {
    my ( $submit ) = grep m/^[A-Z][A-Z]\d+$/, $form->cgi_param();
    if ( $submit && $submit =~ m/^([A-Z][A-Z])(\d+)$/ ) {
      my $cmd= $1;
      my $id = $2;
      
      # Several of these options have been removed from the
      # Admin page and moved to the Production Page.
      if ( $cmd eq 'CL' ) {
        $self->Call( "/Contact", Proj_ID => $id );
      } elsif ( $cmd eq 'PD' ) {
        $self->Call( "/Production", Proj_ID => $id );
      } elsif ( $cmd eq 'PJ' ) {
        $self->Call( "/Project/edit", Proj_ID => $id );
      } elsif ( $cmd eq 'CP' ) {
        $self->Call( "/Project/new", Proj_ID => $id );
      } elsif ( $cmd eq 'AU' ) {
        $self->Call( "/Audition", Proj_ID => $id );
      } elsif ( $cmd eq 'RE' ) {
        $self->Call( "/Registration", Proj_ID => $id );
      } elsif ( $cmd eq 'RS' ) {
        my $Proj_ID = $id;
        if ( $self->Has_Privilege( $Proj_ID, 'DBAdmin', 1 ) ) {
          my $Proj_Status = $self->{dbh}->selectrow_array(
            'SELECT Proj_Status FROM ACT_Project WHERE Proj_ID = ?',
            {}, $Proj_ID );
          if ( $Proj_Status ) {
            if ( $Proj_Status =~ m/^Preview|Proof$/ ) {
            } else {
              $self->error( "Cannot reset project at status '$Proj_Status'" );
            }
          } else {
            $self->error( "Proj_ID not found" );
          }
          my $Roles = $self->{dbh}->selectall_arrayref(
            'SELECT Role_ID FROM ACT_Role WHERE Proj_ID = ?', {}, $Proj_ID );

          if ( @$Roles ) {
            my $cmd = "DELETE FROM ACT_Invoice WHERE Role_ID IN ( " .
              join( ", ", map $_->[0], @$Roles ) . " )";
            my $ninv = $self->{dbh}->do( $cmd );
            $cmd =  "DELETE FROM ACT_Cast WHERE Role_ID IN ( " .
              join( ", ", map $_->[0], @$Roles ) . " )";
            my $nreg = $self->{dbh}->do( $cmd );
            $self->error( "$ninv Invoices and $nreg Registrations have been deleted" );
          } else {
            $self->error( "No matching records found" );
          }
        } else {
          $self->error( "Must have project DBAdmin privilege to reset a project" );
        }
        return 0;
      }
    }
  }
  $self->error("Unknown Option");
  return 0;
}

sub InitFields {}

1;
