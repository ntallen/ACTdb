package CGI::NForm::ACT::SignIn;
use strict;
use CGI;
use CGI::FormBuilder;
use POSIX qw(strftime);
use Spreadsheet::WriteExcel;
use Digest::MD5;

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    {
      '' => 'SELECT'
    };
}

sub InitForm {
  my ( $self, $form, $op ) = @_;
  $self->Connect();
  $form->field( name => 'Proj_ID', type => 'hidden' );
  $form->field( name => 'MLInfo', type => 'file' );
  $form->field( name => 'StartDate', type => 'text' );
  $form->field( name => 'EndDate', type => 'text' );
  
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->template("$self->{tmpldir}/SignIn.html");
  $form->reset(0);
  $form->submit( ['Download', 'Back'] );

  my $Proj_ID = $form->field('Proj_ID');
  $self->fatal("No Proj_ID Specified")
    unless defined($Proj_ID) && $Proj_ID =~ m/^\d+$/;
  my $Proj_Name = $self->{dbh}->selectrow_array(
    'SELECT Proj_Name FROM ACT_Project WHERE Proj_ID = ?', {}, $Proj_ID );
  $form->title( "Sign In Sheets for '$Proj_Name'" );
  return $form;
}

sub authorize {
  my ( $self, $op ) = @_;
  if ( $self->check_key ) {
    my $form = $self->{form};
    my $Proj_ID = $form->field('Proj_ID');
    my $rolepriv = $self->Get_Privilege_Value( $Proj_ID, 'SignIn' );
    $self->{tmp}->{rolepriv} = $rolepriv;
    return 1 if $rolepriv > 0;
  }
  return 0;
}

# Called when form is first displayed (prior to submission)
# Fill in starting date (today)
sub InitFields {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  my $today = strftime('%Y-%m-%d', localtime);
  $form->field( name => 'StartDate', value => $today, force => 1 );
}

sub INSERT {}

sub UPDATE {}

sub DELETE {}

sub SELECT {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  my $Proj_ID = $form->field('Proj_ID');
  if ( $form->submitted eq 'Back' ) {
    $self->Return;
  } elsif ( $form->validate ) {
    if ( $form->submitted eq 'Save' ) {
      return $self->error("Not authorized to modify May Leave data")
        if $self->{tmp}->{rolepriv} < 2;
      my $fh = $form->field('MLInfo');
      if ( defined($fh) && ref($fh) ) {
        my $exinfo = $self->parse_exinfo( $fh );
        my %dbnames;
        $self->{dbh}->do(
          'DELETE FROM ACT_MayLeave WHERE Proj_ID = ?', {}, $Proj_ID );
        $self->confirm( "Reset MayLeave in database");
        my $info = $self->{dbh}->selectall_arrayref(
          "SELECT CONCAT(FirstName, ' ', LastName) AS Name, Part_ID FROM
          ACT_Role NATURAL JOIN ACT_Cast NATURAL JOIN ACT_Participant
          WHERE ACT_Role.Proj_ID = ?
            AND Role_Name = 'Registration'
            AND Cast_Status = 'Accepted'", {}, $Proj_ID );
        for my $row ( @$info ) {
          if ( defined $dbnames{ $row->[0] } ) {
            $self->error("Name '$row->[0]' is ambiguous");
          } else {
            $dbnames{ $row->[0] } = $row->[1];
          }
        }
        for my $name ( sort keys %dbnames ) {
          $self->error("Name '$name' not found in uploaded file") unless defined $exinfo->{$name};
        }
        for my $name ( sort keys %$exinfo ) {
          if ( defined $dbnames{$name} ) {
            if ( $exinfo->{$name}->{MayLeave} ) {
              $self->{dbh}->do(
                "INSERT INTO ACT_MayLeave ( Part_ID, Proj_ID ) VALUES ( ?, ? )", {},
                $dbnames{$name}, $Proj_ID );
            }
          } else {
            $self->error("Name '$name' not found in database");
          }
        }
        $self->confirm( "Info updated");
      } else {
        return $self->error("Must specify an upload file");
      }
      return 0;
    } elsif ( $form->submitted eq 'Download' ) {
      return $self->error("Invalid Proj_ID") unless $Proj_ID =~ m/^(\d+)$/;
      $Proj_ID = $1; # Detaint
      my ( $Proj_Name, $Proj_mnc, $double_cast, $Par_ID ) =
       $self->{dbh}->selectrow_array(
        "SELECT P.Proj_Name, P.Proj_Abbr, IF(P.N_Casts>1,1,0),
           Parent_Proj, PP.Proj_Abbr AS Parent_Abbr
         FROM ACT_Project AS P NATURAL LEFT JOIN ACT_Subproject LEFT JOIN
           ACT_Project AS PP ON Parent_Proj = PP.Proj_ID WHERE P.Proj_ID = ?",
        {}, $Proj_ID );
      my $dbinfo = $Par_ID ?
        $self->{dbh}->selectcol_arrayref(
          "SELECT CONCAT(FirstName, ' ', LastName) AS Name
          FROM ACT_MayLeave NATURAL JOIN ACT_Participant
          WHERE Proj_ID IN (?, ?)
          GROUP BY ACT_MayLeave.Part_ID", {}, $Proj_ID, $Par_ID ) :
        $self->{dbh}->selectcol_arrayref(
          "SELECT CONCAT(FirstName, ' ', LastName) AS Name
          FROM ACT_MayLeave NATURAL JOIN ACT_Participant
          WHERE Proj_ID = ?", {}, $Proj_ID );
      my $exinfo;
      for my $name ( @$dbinfo ) {
        $exinfo->{$name}->{MayLeave} = 1;
      }
      my $Start = $form->field('StartDate');
      my $End = $form->field('EndDate');
      $self->generate_signin( $Proj_ID, $Proj_Name, $Proj_mnc, $double_cast, $exinfo, $Start, $End );
      return 0; # We only get here on failure
    }
  } else {
    return $self->error( "Validation failed" );
  }
  return $self->error("Invalid submission code");
}

sub InitParams {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  $form->tmpl_param( can_write => 1 ) if $self->{tmp}->{rolepriv} > 1;
}

sub generate_signin {
  my ( $self, $Proj_ID, $Proj_Name, $Proj_mnc, $double_cast, $exinfo, $Start, $End ) = @_;
  my ( $Start_Date, $Start_Time, $End_Date, $End_Time );
  my $dbh = $self->{dbh};
  $Proj_mnc = "P$Proj_ID" unless $Proj_mnc;

  my %worksheets;

  if ( defined $Start && $Start ne '' ) {
    if ( $Start =~ m/^(\d\d\d\d-\d\d-\d\d)(?:\@(\d\d?:\d\d(?::\d\d)?))?$/ ) {
      $Start_Date = $1;
      $Start_Time = $2;
    } else {
      return $self->error("Invalid start date/time specifier: '$Start'");
    }
  }
  if ( defined $End && $End ne '' ) {
    if ( $End =~ m/^(\d\d\d\d-\d\d-\d\d)(?:\@(\d\d?:\d\d(?::\d\d)?))?$/ ) {
      $End_Date = $1;
      $End_Time = $2;
    } else {
      return $self->error("Invalid end date/time specifier: '$End'" );
    }
  }
  
  # Now find all the event dates within that range
  my @query_args;
  my $time_qual = '';
  my $time_range = '';
  if ( $Start_Date ) {
    $time_range = "$Start_Date";
    if ( $Start_Time ) {
      $time_qual .= " AND CONCAT(Event_Date, ' ', Event_Start) >= ?";
      push @query_args, "$Start_Date $Start_Time";
      $time_range .= "\@$Start_Time";
    } else {
      $time_qual .= " AND Event_Date >= ?";
      push @query_args, "$Start_Date";
    }
    $time_range .= "-";
  }
  if ( $End_Date ) {
    $time_range .= "$End_Date";
    if ( $End_Time ) {
      $time_qual .= " AND CONCAT(Event_Date, ' ', Event_End) <= ?";
      push @query_args, "$End_Date $End_Time";
      $time_range .= "\@$End_Time";
    } else {
      $time_qual .= " AND Event_Date <= ?";
      push @query_args, "$End_Date";
    }
  }
  $time_range = 'All Rehearsals' unless $time_range;
    
  my $dates = $dbh->selectcol_arrayref(
    "SELECT Event_Date FROM ACT_Event
     WHERE Proj_ID = ?
     AND GR_Type = 'Group'
     $time_qual
     GROUP BY Event_Date", {},
    $Proj_ID, @query_args );
  
  return $self->error( "No rehearsals found in time range $time_range" )
    unless @$dates;

  my $spreadsheet = "$CGI::NForm::ACT::Config{tmpdir}/actwebmaster_" . Digest::MD5::md5_hex("${Proj_ID}_signin.xls");
  my $wb = Spreadsheet::WriteExcel->new($spreadsheet);
  # warn "Writing $Proj_Name Sign-in Sheets to $spreadsheet\n";
  my $bold_right = $wb->add_format( bold => 1, align => 'right' );
  my $bold_center = $wb->add_format( bold => 1, align => 'center' );
  my $grey = $wb->add_format( bg_color => 'silver' );
  my $white = $wb->add_format();
  my $grey_center = $wb->add_format( bg_color => 'silver', align => 'center', border => 1 );
  my $white_center = $wb->add_format( align => 'center', border => 1 );
  my $outline = $wb->add_format( border => 2 );
  
  for my $Event_Date ( @$dates ) {
    my ( $Event_Date_Text, $Short_Date ) = $dbh->selectrow_array(
      "SELECT DATE_FORMAT( ?, '%W %M %D' ), DATE_FORMAT( ?, '%a %b %e' )",
      {}, $Event_Date, $Event_Date );
    my $events = $dbh->selectall_arrayref(
      "SELECT Event_ID, Event_Desc,
        TIME_FORMAT( Event_Start, '%l:%i %p' ),
        TIME_FORMAT( Event_End, '%l:%i %p' ),
        Cast_Name
      FROM ACT_Event NATURAL JOIN ACT_Casts
      WHERE Proj_ID = ? AND GR_Type = 'Group'
        AND Event_Date = ?$time_qual
      ORDER BY Event_Start, Event_End, Event_ID", {},
      $Proj_ID, $Event_Date, @query_args );

    # warn "Expected events for $Proj_Name on $Event_Date\n" unless @$events;
    my $wsname = $Short_Date;
    if ( $worksheets{$wsname} ) {
      my $n = 2;
      while ( $worksheets{"${Short_Date}($n)"} ) {
        ++$n;
      }
      $wsname = "${Short_Date}($n)";
    }
    $worksheets{$wsname} = 1;
    my $ws = $wb->add_worksheet($wsname);

    my $row = 0;
    
    # warn "  Writing sign-in sheet $wsname\n";

    #print "$Proj_Name Sign-in Sheet\n",
    #  "$Event_Date_Text\t$Short_Date\n\n";

    my $col = 'A';
    for my $event ( @$events ) {
      my ( $id, $desc, $from, $to, $cast ) = @$event;
      if ( $double_cast) {
        $cast =~ s/<[^>]*>//g;
        $desc = "$cast $desc";
      }
      $ws->write_string( $row, 0, $col, $bold_right );
      $ws->write_string( $row, 2, "$from to $to: $desc" );
      # print "$col\t\t$from to $to: $desc\n";
      ++$col;
      ++$row;
    }
    $ws->write_string( $row, 0, '*', $bold_right );
    $ws->write_string( $row, 2, 'May not leave without parent');
    $row += 2;
    $ws->repeat_rows($row); # The upcoming row with the column headers

    my @ids = map $_->[0], @$events;
    # warn "ids are: ", join(", ", @ids), "\n";

    # This depends on there being an ACT_Is_Also record of 0,0,0
    # The statement could use a GROUP BY P.Part_ID, E.Event_ID,
    # but I end up handling that implicitly when I sort the
    # contents.
    my $parts = $dbh->selectall_arrayref(
      "SELECT CONCAT(FirstName,' ',LastName), Event_ID
      FROM (
        SELECT E.Event_ID,
          IF(I.Is_Also_ID = 0,G.Role_ID,I.Role_ID) AS Role_ID,
          IF(E.Cast_ID>I.Cast_ID,E.Cast_ID,I.Cast_ID) AS Cast_ID
        FROM ACT_Event AS E NATURAL JOIN ACT_Evt_Grps
          NATURAL JOIN ACT_Grp_Mem AS G
          LEFT JOIN ACT_Is_Also AS I
            ON (I.Is_Also_ID = 0 OR I.Is_Also_ID = G.Role_ID)
              AND (E.Cast_ID = 0 OR I.Cast_ID = 0 OR E.Cast_ID = I.Cast_ID )
        WHERE E.Event_ID IN ( " .
        join( ", ", @ids ) . " )
      ) AS Roles
        JOIN ACT_Cast AS C
          ON Roles.Role_ID = C.Role_ID AND
            (Roles.Cast_ID = 0 OR C.Cast_ID = 0 OR Roles.Cast_ID = C.Cast_ID)
        NATURAL JOIN ACT_Participant AS P
        WHERE C.Cast_Status = 'Accepted'
      GROUP BY LastName, FirstName, Event_ID" );

    my %kids;
    my @kids;
    for my $part ( @$parts ) {
      push @kids, $part->[0] unless $kids{$part->[0]};
      $kids{$part->[0]}->{$part->[1]} = 1;
    }


    $col = 'A';
    my $ncols = @ids;
    $ws->write_string( $row, 2, "In", $bold_center );
    for my $n (1 .. $ncols) {
      $ws->write_string( $row, 3+$n, $col++, $bold_center );
    }
    $ws->write_string( $row, 5+$ncols, "Out", $bold_center );

    my $max_len = 0;
    my $odd = 1;
    for my $kid ( @kids ) {
      ++$row;
      my $format = $odd ? $grey : $white;
      my $len = length($kid);
      $max_len = $len if $len > $max_len;
      $ws->write_string($row, 0, $kid, $format);
      $ws->write_blank($row, 1, $format);
      $ws->write_blank($row, 3, $format);
      $ws->write_blank($row, 2+$ncols, $format);
      $format = $odd ? $grey_center : $white_center;
      for my $n (1 .. $ncols) {
        my $txt = $kids{$kid}->{$ids[$n-1]} ? 'X' : '';
        $ws->write_string( $row, 3+$n, $txt, $format );
      }
      $ws->write_blank( $row, 2, $outline );
      $ws->write_string( $row, 5+$ncols, $exinfo->{$kid}->{MayLeave} ? '' : '*', $outline );
      $odd = ! $odd;
    }

    # Now let's get the column widths right:
    $ws->set_column('A:A', $max_len);
    $ws->set_column('B:B', 1.25);
    $ws->set_column('D:D', 1.25);
    $ws->set_column( 4, 3+$ncols, 4 );
    $ws->set_column(4+$ncols,4+$ncols,1.25);
    $ws->hide_gridlines(1);

    # Set up page header
    # Title big bold centered across entire sheet
    # Subtitle (long date) big bold centered across entire sheet
    # Spacing before scenes
    # Page number on the right, starting with page 1
    $ws->center_horizontally();
    $ws->set_header("&C&\"Arial,Bold\"&16$Proj_Name Sign-in Sheet\n$Event_Date_Text&R&14Page &P");
    $ws->set_margin_top(1.25);
  }

  $wb->close();
  my $filesize = -s $spreadsheet;
  open( XLS, "<$spreadsheet" ) ||
    return $self->error("Unable to read spreadsheet");
  my $contents;
  my $rb = read XLS, $contents, $filesize;
  return $self->error("Short read: size: $filesize read: $rb")
    if $rb != $filesize;
  close XLS;
  unlink($spreadsheet);
  my $q = $self->{q};
  print
    $q->header( -type => 'application/vnd.ms-excel', -Content_length => $filesize,
        -attachment => "${Proj_ID}_${Proj_mnc}_signin.xls" ),
    $contents;
  exit(0);
}

1;
