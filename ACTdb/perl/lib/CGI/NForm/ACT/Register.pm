package CGI::NForm::ACT::Register;
use strict;
use CGI;
use CGI::FormBuilder;
use Mail::Sendmail;
use Text::Wrap;

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    {
      '' => 'INSERT'
    };
}

# Support for the $params argument is optional, but necessary
# if you want to allow this form to be redirected.
# With FormBuilder 3.0, this functionality can be moved back
# to NForm and get_form() will be responsible only for specifying
# the fields and the template.
sub get_form {
  my ( $self, $op, $params )  = @_;
  $self->Connect();
  my $q = $self->{q} = ref($params) eq 'CGI' ? $params : new CGI($params);
  my $form = CGI::FormBuilder->new(
    action => "$ENV{SCRIPT_NAME}$self->{path_info}",
    method => 'POST',
    fields => [ qw(nxtPI nxtPA Part_ID Role_ID Fam_ID Mail_ID) ],
    params => $q,
    template => "$self->{tmpldir}/Register.html",
    title => "ACT Registration",
    submit => [ "Register" ]
  );
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'Role_ID', type => 'hidden' );
  $form->field( name => 'Fam_ID', type => 'hidden' );
  $form->field( name => 'Mail_ID', type => 'hidden' );
  return $form;
}

# If Fam_ID is specified, it had better be your family unless you're
# on the board. If it isn't specified, set it to your family.
# If Part_ID is specified, it must be in Fam_ID.
sub authorize {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  my $Role_ID = $form->field('Role_ID');
  $self->fatal("No Project Specified for Registration") unless $Role_ID;
  return 1 if $form->submitted eq 'Cancel';
  my ( $Role_Status, $Job_Committee, $Role_Name, $Proj_Name, $Proj_Type ) =
    $self->{dbh}->selectrow_array(
    'SELECT Role_Status, Job_Committee, Role_Name, Proj_Name, Proj_Type
     FROM ACT_Role, ACT_Project
     WHERE Role_ID = ? AND ACT_Role.Proj_ID = ACT_Project.Proj_ID',
    {}, $Role_ID );
  $self->fatal("Project undefined") unless $Role_Status;
  if ( $self->check_key ) {
    my $My_Part_ID = $self->{auth}->{Part_ID};
    my ( $My_Fam_ID, $My_Status ) = $self->{dbh}->selectrow_array(
      'SELECT Fam_ID, Status FROM ACT_Participant where Part_ID = ?',
      {}, $My_Part_ID );
    unless ( defined $My_Fam_ID && defined $My_Status ) {
      $self->fatal("No Fam_ID or Status defined for user Part_ID = $My_Part_ID");
    }
    my $Fam_ID = $form->field('Fam_ID');
    my $Part_ID = $form->field('Part_ID');
    if ( defined($Part_ID) && $Part_ID ne '' ) {
      unless ( $Part_ID =~ m/^\d+$/ && $Part_ID ) {
	$self->fatal("Invalid Part_ID specified");
      }
      my $Part_Fam_ID = $self->{dbh}->selectrow_array(
	'SELECT Fam_ID FROM ACT_Participant where Part_ID = ?',
	{}, $Part_ID );
      unless ( defined $Part_Fam_ID ) {
	$self->fatal("No Fam_ID defined for user Part_ID = $Part_ID");
      }
      $Fam_ID = $Part_Fam_ID unless $Fam_ID;
      if ( $Part_Fam_ID != $Fam_ID ) {
	$self->fatal("Form Fam_ID ($Fam_ID) does not match Participant's Fam_ID ($Part_Fam_ID)" );
      }
      # Check to make sure Part_ID is in the specified Role
      if ( $Job_Committee && ! $self->part_has_role( $Part_ID, $Job_Committee ) ) {
	$self->GoTo( "/Register", Part_ID => $Part_ID, Role_ID => $Job_Committee );
      }
    } elsif ( $Job_Committee ) {
      $self->GoTo( "/Register", Role_ID => $Job_Committee );
    }

    my $is_admin = $self->has_role2( $Role_ID, 'Registrar' );
    if ( $Role_Status eq 'Closed' && ( ! $is_admin || ! $form->submitted ) ) {
      my $qual = $Role_Name eq 'Registration' ? '' : 'Registration for ';
      $self->error("$qual$Role_Name for $Proj_Name is closed");
    }
    $self->{tmp} = {};
    $self->{tmp}->{reg_ok} = 1 if $Role_Status ne 'Closed' || $is_admin;
    
    $Fam_ID = $My_Fam_ID unless $Fam_ID;
    if ( $Fam_ID != $My_Fam_ID && ! $is_admin ) {
      $self->fatal("You are not authorized to register for another family");
    }
    $form->field( name => 'Fam_ID', value => $Fam_ID, force => 1 );
    if ( $Fam_ID == $My_Fam_ID ) {
      $form->field( name => 'Mail_ID', value => $My_Part_ID, force => 1 );
    }
    $self->{tmp}->{My_Fam_ID} = $My_Fam_ID;
    $self->{tmp}->{My_Part_ID} = $My_Part_ID;

    ### This should key off of Role_Type, not Proj_Type
    ### But actually, Event registration should probably take place
    ### in an entirely separate module
    if ( $Proj_Type eq 'FamilyEvent' ) {
      $self->fatal("FamilyEvents should be handled separately!");
    }
    my $kids = $self->{dbh}->selectall_arrayref(
      'SELECT Part_ID, FirstName FROM ACT_Participant
       WHERE Fam_ID = ? AND Status = ?', {}, $Fam_ID, 'Child' );
    $form->tmpl_param( child => 'child' ); # lingering hook from Event reg.
    $form->field( name => 'Part_ID', options => $kids, type => 'select' );
    return 1;
  }
  return 0;
}

sub InitParams {
  my $self = shift;
  my $My_Fam_ID = $self->{tmp}->{My_Fam_ID};
  my $My_Part_ID = $self->{tmp}->{My_Part_ID};
  my $form = $self->{form};
  my $Part_ID = $form->field('Part_ID');
  my $Role_ID = $form->field('Role_ID');
  my $Fam_ID = $form->field('Fam_ID');
  
  my @TKWs = qw( Role_Name Proj_Type Proj_Name Role_Fee Cancel_100
      Cancel_75 Proj_Ages Proj_Desc Proj_Meetings Proj_Rehearsals
      Proj_Performances Role_Due);
  my $cmd = join ' ', 'SELECT',
    join( ', ', @TKWs ), 'FROM ACT_Role, ACT_Project',
    'WHERE ACT_Project.Proj_ID = ACT_Role.Proj_ID AND',
    'Role_ID = ?';
  my @vals = $self->{dbh}->selectrow_array( $cmd, {},
			  $Role_ID );
  for ( @TKWs ) {
    my $val = shift(@vals);
    if ( defined $val ) {
      $form->tmpl_param( $_ => $val );
    }
  }

  if ( $Fam_ID != $My_Fam_ID ) {
    my $MIDs = $self->{dbh}->selectall_arrayref(
      "SELECT Part_ID, FullName, Email
       FROM ACT_Participant
       WHERE ( Fam_ID = ? AND Email != '' AND
	      ( Status = 'Adult' OR Part_ID = ? ) )
	 OR Part_ID = ?",
       {}, $Fam_ID, $Part_ID, $My_Part_ID );
    $MIDs = $self->map_cols( [ 'Part_ID', 'FullName', 'Email' ], $MIDs );
    $form->tmpl_param( Mail_IDS => $MIDs );
    $form->tmpl_param( adminreg => 1 );
  }

  $form->tmpl_param( reg_ok => 1 ) if $self->{tmp}->{reg_ok};
}

# Issues:
#   I want to eliminate auto-closing of Limited roles. This will
#   simplify reopening of auditions if people change their choices.
#   Instead, a count will be made at registration time to determine
#   if all the positions have been taken or not. One alternative
#   implementation I've considered is to switch from 'Limited' to
#   'Filled'.
#
#   Without a Filled state, the following sequence:
#     All spots and wait list fills
#     applicant gets auto-rejected due to being filled
#     Someone withdraws
#     another applicant gets added to the waiting list
#   Or worse:
#     All spots and wait list fills
#     applicant gets auto-rejected due to being filled
#     Enough people withdraw so total drops below Role_limit
#     another applicant gets accepted before those on the waiting list
#
#   In reality, neither scenario is significant. The second can be
#   avoided by accepting waitlistees before withdrawing an applicant.
#   In either case, the size of the waiting list should be selected
#   to be larger than is every likely to be needed.
#
# With a Filled state, we would go to Filled whenever all the
# spots and waitlist are filled and no subsequent applications
# would be allowed. The only reasonable case for automatically
# removing the Filled state would be if a) the total drops
# below Role_limit and b) there is no one on the waitlist.
#
# Audition enrollment is the main place where this is likely to
# be useful. There we have no waitlist, so any withdrawal would
# reopen the slot, but this is true for the implementation
# above as well.
#
# Since we aren't using Limited in really competetive cases,
# this is probably not a high priority to change.

sub INSERT {
  my $self = shift;
  my $form = $self->{form};
  if ( $form->submitted =~ m/^Add a (?:child|Family Member)/ ) {
    $self->Call( "/Person/new", Fam_ID => $form->field('Fam_ID'), Status => 'Child' );
  } elsif ( $form->submitted eq 'Update my Family Information' ) {
    $self->Call( "/Family/edit", Fam_ID => $form->field('Fam_ID') );
  } elsif ( $form->submitted eq 'Back' || $form->submitted eq 'Cancel' ) {
    $self->Return;
  } elsif ( $self->{form}->validate ) {
    return 0 if @{$self->{errors}};
    my $Part_ID = $form->field('Part_ID');
    unless ( $Part_ID ) {
      $self->error( "You must select a child" );
      return 0;
    }
    my $Role_ID = $form->field('Role_ID');
    my $Fam_ID = $form->field('Fam_ID');
    my ( $Age, $ClassYear, $Status ) = $self->{dbh}->selectrow_array(
      "SELECT
	   IF(Birthdate>0,
	      YEAR(NOW())-YEAR(Birthdate)-(MID(NOW(),6,5)<MID(Birthdate,6,5)),
	      -1) AS Age, ClassYear, Status
       FROM ACT_Participant WHERE Part_ID = ?", {}, $Part_ID );
    if ( $Status eq 'Child' && ( $Age < 3 || $ClassYear < 2004 ) ) {
      $self->error( "You must specify the child's age and grade.",
        "Please select 'Update my Family Information' below" );
      return 0;
    }

    my $Brd_Fam = $self->{dbh}->selectrow_array(
      'SELECT 1 FROM ACT_Role, ACT_Cast, ACT_Participant
       WHERE ACT_Role.Proj_ID = ? AND
	     ACT_Role.Role_ID = ACT_Cast.Role_ID AND
	     ACT_Cast.Part_ID = ACT_Participant.Part_ID AND
	     ACT_Role.Role_Name = ? AND Fam_ID = ?', {},
      1, 'Board', $Fam_ID );
    $self->{dbh}->do(
     'LOCK TABLES ACT_Project READ, ACT_Cast WRITE,
                  ACT_Role WRITE, ACT_Invoice WRITE, ACT_Log WRITE,
		  ACT_Participant READ, ACT_Participant AS P2 READ,
		  ACT_Role AS R2 READ' );
    my ( $Role_Status, $Board_Reserve, $Role_Limit, $Wait_Limit, $Role_Fee,
	 $Role_Name, $Proj_Name ) =
      $self->{dbh}->selectrow_array(
	'SELECT Role_Status, Board_Reserve, Role_Limit, Wait_Limit, Role_Fee,
	 Role_Name, Proj_Name
	 FROM ACT_Role, ACT_Project
	 WHERE Role_ID = ? AND ACT_Role.Proj_ID = ACT_Project.Proj_ID',
	{}, $Role_ID );
    $self->fatal("Error accessing Project registrations") unless defined $Role_Limit;
    my $Cast_Status = 'Applied';
    my $Used_reserve = 0;
    my $Count;
    if ( $Role_Status eq 'Closed' && ! $self->{tmp}->{reg_ok} ) {
      $self->Log("Closed: $Role_Name for $Proj_Name", "Part_ID=$Part_ID");
      $self->{dbh}->do( 'UNLOCK TABLES');
      $self->fatal( "Registration for this offering is closed." );
    } elsif ( $Role_Status eq 'Limited' ) {
      $Count = $self->{dbh}->selectrow_array(
	"SELECT COUNT(Part_ID) FROM ACT_Cast
	 WHERE Role_ID = ? AND Cast_Status != 'Withdrawn'
	 GROUP BY Role_ID", {}, $Role_ID );
      $Wait_Limit = $Board_Reserve if $Wait_Limit < $Board_Reserve;
      if ( $Brd_Fam && $Board_Reserve ) {
	$Used_reserve = 1;
	$Board_Reserve--;
	$Cast_Status = 'Accepted';
      } elsif ( $Count + $Board_Reserve < $Role_Limit ) {
	# add as accepted
	$Cast_Status = 'Accepted';
      } elsif ( $Count + $Board_Reserve < $Role_Limit + $Wait_Limit ) {
	$Cast_Status = 'Waitlisted';
      } else {
	# $self->{dbh}->do(
	#   'UPDATE ACT_Role SET Role_Status = ? WHERE Role_ID = ?',
	#   {}, 'Closed', $Role_ID );
	$self->Log("Filled: $Role_Name for $Proj_Name", "Part_ID=$Part_ID");
	$self->{dbh}->do( 'UNLOCK TABLES');
	$self->fatal(
	  $Wait_Limit ?
	  "All available spots for this offering and on the waiting list
	   have been filled, so we cannot accept any more registrations
	   at this time." :
	  "All available spots for this offering have been filled,
	   so we cannot accept any more registrations at this time." );
      }
    } elsif ( $Role_Status eq 'Open' ) {
      $Cast_Status = 'Accepted';
    } # else for Closed and Moderated, they're added as Applied
    
    # Probably want to add Cast_ID, Cast_Note, Cast_Order when generalizing
    eval {
      $self->{dbh}->do(
	'INSERT INTO ACT_Cast ( Role_ID, Part_ID, Cast_Status, Invoice_ID, Cast_Time )
	 VALUES ( ?, ?, ?, ?, IF(?,0,NOW()) )', {},
	 $Role_ID, $Part_ID, $Cast_Status, 0, $Brd_Fam );
    };
    if ( $@ ) {
      if ( $@ =~ m/Duplicate entry/ ) {
	# $self->error("Participant has already registered for this project");
	$self->{dbh}->do('UNLOCK TABLES');
	$self->GoTo( "/RegConfirm", Role_ID => $Role_ID, Part_ID =>
		      $Part_ID, Cast_ID => 0,
		      Reg_errmsg => "Participant has already registered for this project" );
	return 0;
      }
    }
    $self->Fixup_Parents( $Fam_ID );
    $self->Log("$Role_Name for $Proj_Name", "Part_ID=$Part_ID");
    my $Invoice_ID = $self->check_invoice( $Cast_Status, $Role_ID, $Part_ID,
		      $Role_Fee, "$Role_Name for $Proj_Name" );
    if ( $Role_Status eq 'Limited' ) {
      if ( $Used_reserve ) {
	$self->{dbh}->do(
	  'UPDATE ACT_Role SET Board_Reserve = ? WHERE Role_ID = ?',
	  {}, $Board_Reserve, $Role_ID );
      }
    }
    $self->{dbh}->do('UNLOCK TABLES');
    my $EConfirm = 0;
    
    # Now send an e-mail confirmation
    if ( $CGI::NForm::ACT::Config{smtp_server} ) {
      my @MIDs = $form->field('Mail_ID');
      my %msg;
      if ( @MIDs ) {
      	my $cmd = join ' ',
      	  'SELECT Email from ACT_Participant WHERE Part_ID IN (',
      	  join( ', ', @MIDs ), ") AND Email <> ''";
      	my $emails = $self->{dbh}->selectcol_arrayref( $cmd );
      	$msg{To} = join(', ', @$emails);
      	$msg{CC} = 'webmaster@act.arlington.ma.us';
      } else {
        $msg{To} = 'webmaster@act.arlington.ma.us';
      }
      $msg{From} = 'webmaster@act.arlington.ma.us';
      my $vals = $self->RegConfirm( $Role_ID, $Part_ID, 0 );
      $msg{Subject} = "ACT Registration Confirmation: $vals->{Proj_Name}";
      $msg{smtp} = $CGI::NForm::ACT::Config{smtp_server};
      $msg{Message} = $vals->{Message};
      if ( sendmail( %msg ) ) {
      	$EConfirm = 1;
      	$self->Log( "Email Sent Role_ID=$Role_ID Part_ID=$Part_ID");
      }
    }


    $self->GoTo( "/RegConfirm", Role_ID => $Role_ID, Part_ID =>
		  $Part_ID, Cast_ID => 0, EConfirm => $EConfirm );
  }
  return 0;
}

sub UPDATE {}

sub DELETE {}

sub SELECT {}

sub InitFields {}

1;
