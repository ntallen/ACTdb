package CGI::NForm::ACT::Members;
use strict;
use CGI;
use CGI::FormBuilder;

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    { '' =>   'SELECT'
    };
}

# Support for the $params argument is optional, but necessary
# if you want to allow this form to be redirected.
# With FormBuilder 3.0, this functionality can be moved back
# to NForm and get_form() will be responsible only for specifying
# the fields and the template.
sub get_form {
  my ( $self, $op, $params )  = @_;
  $self->Connect();
  my $q = $self->{q} = ref($params) eq 'CGI' ? $params : new CGI($params);
  my $form = CGI::FormBuilder->new(
    action => "$ENV{SCRIPT_NAME}$self->{path_info}",
    method => 'POST',
    fields => [ qw(nxtPI nxtPA) ],
    params => $q,
    template => "$self->{tmpldir}/Members.html",
    title => "Gift History",
    submit => [ "Go", "Back" ],
    stylesheet => 1
  );
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'Who', type => 'select',
     options => [ 'Current', 'Expired', 'All' ], value => 'Current' );
  $form->field( name => 'SmallGifts', options => [ 'Small Gifts' ] );
  $form->field( name => 'How', type => 'select',
     options => [ 'Reference', 'Program', 'Detail', 'Email', 'Address' ],
     value => 'Reference' );
  $form->title( "Family Gift History" );
  return $form;
}

sub authorize {
  my ( $self, $op ) = @_;
  if ( $self->check_key(EU=>1) ) {
    if ( $self->has_role( 1, 'Treasurer' ) ||
	 $self->has_role( 1, 'DBadmin' ) ) {
      $self->{tmp}->{is_admin} = 1;
      return 1;
    } elsif ( $self->has_role( 1, 'Board' ) ) {
      return 1;
    }
  }
  $self->fatal( "Access restricted to ACT Board" );
}

sub INSERT {}

sub UPDATE {}

sub DELETE {}

sub SELECT {
  my $self = shift;
  my $form = $self->{form};
  $self->Return if $form->submitted eq 'Back';
  return 0 if $form->submitted eq 'Go';
  my ( $submit ) = grep m/^[A-Z][A-Z]\d+$/, $form->cgi_param();
  my $cmd = 'XX';
  my $id = 0;
  if ( $submit && $submit =~ m/^([A-Z][A-Z])(\d+)$/ ) {
    $cmd= $1;
    $id = $2;
  }
  if ( $cmd eq 'ED' ) {
    $self->Call( "/Gifts", Fam_ID => $id );
  }
  $self->fatal( "Unknown command: '$cmd'" );
}

sub InitFields {
}

sub InitParams {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  my $Who = $form->field('Who');
  my $SmallGifts = $form->field('SmallGifts') ? 1 : 0;
  my $How = $form->field('How');
  if ( $form->submitted ) {
    $form->tmpl_param( reporting => 1 );
    if ( $How eq 'Reference' ) {
      # reference => [ family, expiration, is_admin, Fam_ID ]
    } elsif ( $How eq 'Detail' ) {
      # detail => [ Gift_Attribution, Gift_Amount, Gift_Date,
      # Gift_ExpDate ]
      my @flds = qw(Gift_Attribution Gift_Amount Gift_Type
		    Gift_Date Gift_ExpDate);
      my $query = "SELECT " . join( ', ', @flds ) . " FROM ACT_Gift ";
      if ( $Who eq 'Current' ) {
	$query .= "WHERE Gift_ExpDate > CURDATE()";
      } elsif ( $Who eq 'Expired' ) {
	$query .= "WHERE Gift_ExpDate <= CURDATE()";
      }
      if ( ! $SmallGifts ) {
	$query .= " AND Gift_Amount >= 25";
      }
      $query .= " ORDER BY Gift_Attribution, Gift_Date";
      my $flds = $self->{dbh}->selectall_arrayref( $query );
      $form->tmpl_param( detail => $self->map_cols( \@flds, $flds ) );
    } elsif ( $How eq 'Program' ) {
      # program => [ category, members => [ attribution ]]
    } elsif ( $How eq 'Email' ) {
    } elsif ( $How eq 'Address' ) {
    }
  }
  # $form->tmpl_param( Gifts => $self->map_cols( \@kws, $vals ) );
}
1;
