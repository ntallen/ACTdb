package CGI::NForm::ACT::Home;
use strict;
use CGI;
use CGI::FormBuilder;

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    { # rm  =>  'DELETE',
      '' =>   'SELECT'
    };
}

# Support for the $params argument is optional, but necessary
# if you want to allow this form to be redirected.
# With FormBuilder 3.0, this functionality can be moved back
# to NForm and get_form() will be responsible only for specifying
# the fields and the template.
sub get_form {
  my ( $self, $op, $params )  = @_;
  $self->Connect();
  my $q = $self->{q} = ref($params) eq 'CGI' ? $params : new CGI($params);
  my $form = CGI::FormBuilder->new(
    action => "$ENV{SCRIPT_NAME}$self->{path_info}",
    method => 'POST',
    fields => [ qw(nxtPI nxtPA FamilyName Part_ID) ],
    params => $q,
    template => "$self->{tmpldir}/Home.html",
    title => "Your Home Page",
    submit => "Update my Family's Records"
  );
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'Part_ID', type => 'hidden' );
  $form->field( name => 'showall', type => 'hidden', value => 0 );
  return $form;
}

sub authorize {
  my ( $self, $op ) = @_;
  if ( $self->check_key ) {
    my $form = $self->{form};
    # my $is_admin = $self->has_role( 1, 'Board');
    my $can_view = $self->Has_Privilege( -1, 'ContactInfo', 1 );
    my $Part_ID = $form->field( 'Part_ID' ) ||
      $self->{auth}->{Part_ID};
    my ( $FamilyName, $Fam_ID ) = $self->{dbh}->selectrow_array(
      'SELECT FamilyName, P.Fam_ID
       FROM ACT_Participant AS P NATURAL JOIN ACT_Family
       WHERE Part_ID = ?',
       {}, $Part_ID );
    my $My_Fam_ID;
    if ( $Part_ID == $self->{auth}->{Part_ID} ) {
      $My_Fam_ID = $Fam_ID;
    } else {
      $My_Fam_ID = $self->{dbh}->selectrow_array(
        'SELECT Fam_ID FROM ACT_Participant WHERE Part_ID = ?',
        {}, $self->{auth}->{Part_ID} );
      $self->fatal("Not authorized to view contact information")
        unless $My_Fam_ID == $Fam_ID || $can_view;
    }
    $form->field( name => 'Part_ID', value => $Part_ID, force => 1 );
    $form->tmpl_param( FamilyName => $FamilyName );
    $self->{tmp}->{Fam_ID} = $Fam_ID;
    $self->{tmp}->{My_Fam_ID} = $My_Fam_ID;
    return 1;
  }
  return 0;
}

sub INSERT {
}

sub UPDATE {
}

sub DELETE {
}

sub SELECT {
  my $self = shift;
  my $form = $self->{form};
  if ( $form->submitted =~ m/Family/ ) {
    my $Part_ID = $form->field('Part_ID');
    my ( $Fam_ID ) = $self->{dbh}->selectrow_array(
      'SELECT Fam_ID FROM ACT_Participant WHERE Part_ID = ?', {},
      $Part_ID );
    $self->Call( '/Family/edit', Fam_ID => $Fam_ID );
  } elsif ( $form->submitted =~ m/Administrative/ ) {
    $self->Call( '/Admin' );
  } elsif ( $form->submitted eq 'Show All Roles' ) {
    $form->field( name => 'showall', value => 1, force => 1 );
  } elsif ( $form->submitted eq 'Show Current Roles' ) {
    $form->field( name => 'showall', value => 0, force => 1 );
  } elsif ( $form->submitted eq 'Back' ) {
    $self->Return;
  } else {
    return $self->error('Unknown function');
  }
  return 0;
}

sub InitFields {
}

sub InitParams {
  my $self = shift;
  my $form = $self->{form};
  my $Part_ID = $form->field('Part_ID');
  my $Fam_ID = $self->{tmp}->{Fam_ID};
  my $My_Fam_ID = $self->{tmp}->{My_Fam_ID};

  my $is_admin = $self->Has_Privilege( -1, 'ProjectAdmin', 1 );
  my $can_write = $Fam_ID == $My_Fam_ID ||
    $self->Has_Privilege( -1, 'ContactInfo', 2 );
  my $showall = $form->field('showall') || 0;
  my @submits;
  push @submits, 'Administrative Links' if $is_admin;
  push( @submits,
    $Fam_ID == $My_Fam_ID ?
      "Update my Family's Records" :
      "Update this Family's Records" ) if $can_write;
  push @submits,
    $showall ?
      'Show Current Roles' :
      'Show All Roles';
  push @submits, 'Back' if $form->field( 'nxtPI' );
  $form->submit( \@submits );
  $self->FamSearch( $Part_ID, $showall );
}

1;
