package CGI::NForm::ACT::RolePrivs;
use strict;
use CGI;
use CGI::FormBuilder;

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    {
      '' => 'SELECT',
      'edit' => 'UPDATE',
      'new' => 'INSERT'
    };
}

# One way to look at select:
# List each Role and template and give options
# [] Create Role
# [] Edit Role
# [] Create Template
# [] Edit Template

sub InitForm {
  my ( $self, $form, $op )  = @_;
  $self->Connect();
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'Proj_ID', type => 'hidden' );
  my $Proj_ID = $form->field('Proj_ID');
  $self->fatal("No Proj_ID Specified") unless $Proj_ID =~ /^\d+$/;

  my $Proj_Name = $self->{dbh}->selectrow_array(
    "SELECT Proj_Name FROM ACT_Project WHERE Proj_ID = ?",
    {}, $Proj_ID );
  $form->tmpl_param( Proj_Name => $Proj_Name );

  # SELECT Fields:
  my $Roles = $self->{dbh}->selectall_arrayref(
    "(SELECT CONCAT(R.Role_ID, '_', IFNULL(P.RP_ID,0)),
             CONCAT( R.Role_Name, IF(ISNULL(P.RP_ID),' [New]',''),
                IF(ISNULL(P2.RP_ID),'',' [Template]') )
        FROM ACT_Role AS R LEFT JOIN ACT_Priv_Roles AS P
            ON R.Role_ID = P.Role_ID
          LEFT JOIN ACT_Priv_Roles AS P2
            ON R.Role_Name = P2.Role_Name AND P2.Role_ID = 0
        WHERE Proj_ID = ? AND
          R.Role_Type IN ( 'Category', 'Committee', 'Job' ) )
     UNION
     (SELECT CONCAT(Role_ID, '_', RP_ID), CONCAT(Role_Name, ': Template')
        FROM ACT_Priv_Roles WHERE Role_ID = 0 )",
    {}, $Proj_ID );
  $form->field( name => 'Pick_Role_Name', type => 'select',
    options => $Roles );
  $form->field( name => 'New_Role_Name', type => 'text' );
  $form->field( name => 'New_Role_Type', type => 'select',
    options => [ qw(Category Committee Job) ] );
  $form->field( name => 'New_TempRole',
     options => [ 'Template', "Role with privileges for Project '$Proj_Name'",
       "Role without privileges" ] );
  $form->template( "$self->{tmpldir}/RolePrivs.html" );

  my $Ref_RP_ID = 0;
  my ( $Role_Name, $Role_Type, $RPType );
  if ( $op eq 'INSERT' ) {
    $form->field( name => 'Role_Name', type => 'hidden' );
    $form->field( name => 'Role_ID', type => 'hidden' );
    $form->field( name => 'Role_Type', type => 'hidden' );
    $form->field( name => 'AsTemplate', type => 'hidden' );

    ### We are planning to create a new role or template
    ### In the template case, Role_Name and Role_Type must
    ### be specified and must not conflict with an existing
    ### template. In the Role case, Role_ID must be defined,
    ### and there shouldn't be an existing ACT_Priv_Roles entry.
    my $AsTemplate = $form->field('AsTemplate');
    if ( $AsTemplate ) {
      $Role_Name = $form->field('Role_Name');
      $Role_Type = $form->field('Role_Type');
      $self->error("Role_Name not defined") unless $Role_Name;
      $self->error("Role_Type not defined") unless $Role_Type;
      $RPType = "Template for $Role_Type '$Role_Name'";
    } else {
      my $Role_ID = $form->field('Role_ID');
      if ( $Role_ID ) {
        # Check to see if there is a template
        $Ref_RP_ID = $self->{dbh}->selectrow_array(
          "SELECT RP_ID FROM ACT_Role AS R, ACT_Priv_Roles AS P
          WHERE R.Role_ID = ? AND R.Role_Name = P.Role_Name
            AND P.Role_ID = 0", {}, $Role_ID );
        ( $Role_Name, $Role_Type ) = $self->{dbh}->selectrow_array(
          "SELECT Role_Name, Role_Type FROM ACT_Role WHERE Role_ID = ?",
          {}, $Role_ID );
        $Ref_RP_ID = 0 unless $Ref_RP_ID;
        return $self->error("Invalid Role_ID") unless $Role_Name;
      } else {
        $Role_Name = $form->field('Role_Name');
        $Role_Type = $form->field('Role_Type');
        $Ref_RP_ID = $self->{dbh}->selectrow_array(
          "SELECT RP_ID FROM ACT_Priv_Roles WHERE
           Role_Name = ? AND Role_Type = ? AND Role_ID = 0",
           {}, $Role_Name, $Role_Type );
        $Ref_RP_ID = 0 unless $Ref_RP_ID;
      }
      $RPType = "$Role_Type '$Role_Name' for $Proj_Name";
    }
  }
  
  if ( $op eq 'UPDATE' ) {
    $form->field( name => 'RP_ID', type => 'hidden' );
    my $RP_ID = $form->field('RP_ID');
    $self->error("RP_ID not defined") unless $RP_ID;
    $Ref_RP_ID = $RP_ID;
    my $Role_ID;
    ( $Role_Name, $Role_Type, $Role_ID ) = $self->{dbh}->selectrow_array(
      "SELECT Role_Name, Role_Type, Role_ID
       FROM ACT_Priv_Roles WHERE RP_ID = ?",
      {}, $RP_ID );
    $self->error("Invalid RP_ID") unless $Role_Name;
    $RPType = $Role_ID ? "$Role_Type '$Role_Name' for $Proj_Name" :
      "Template for $Role_Type '$Role_Name'";
  }

  if ( $op eq 'INSERT' || $op eq 'UPDATE' ) {
    # Need to create a select field for each privilege
    $self->fatal("Role_Name should have been defined") unless $Role_Name;
    $form->tmpl_param(Role_Name => $Role_Name );
    $form->tmpl_param(Role_Type => $Role_Type );
    $form->tmpl_param(RPType => $RPType );
    $form->title( ($op eq 'INSERT' ? "Create" : "Update" ) .
      " $RPType" );
    my $privs = $self->{dbh}->selectall_arrayref(
      'SELECT ACT_Privileges.Priv_ID, Priv_Name,
       IFNULL(PV_Value,0) AS PV_Value,
       Priv_IsGlobal
       FROM ACT_Privileges LEFT JOIN ACT_PRVals
       ON ACT_PRVals.RP_ID = ?
         AND ACT_Privileges.Priv_ID = ACT_PRVals.Priv_ID
       ORDER BY Priv_Order', {}, $Ref_RP_ID
    );
    my $pvals = $self->{dbh}->selectall_arrayref(
      "SELECT Priv_ID, PV_Value, CONCAT(PV_String, ': ', Priv_Desc)
       FROM ACT_Priv_Values ORDER BY Priv_ID, PV_Value"
    );
    for my $row ( @$privs ) {
      my ( $Priv_ID, $Priv_Name, $PV_Value, $Priv_IsGlobal ) = @$row;
      my $fieldname = "P$Priv_ID";
      my @vals = map { $_->[0] == $Priv_ID ? [ $_->[1], $_->[2] ] : () }
        @$pvals;
      $form->field( name => $fieldname, type => 'select', options => \@vals,
	value => $PV_Value, validate => '/^[0-9]+$/', required => 1 );
    }
    $self->{tmp}->{privs} = $privs; # We'll need these again to generate the loop
    $form->submit( [ 'Submit', 'Back' ] );
    my $jsfunc = <<'EOJS';
      if (form._submit.value == 'Back') {
	    return true;
      }
EOJS
    $form->jsfunc( $jsfunc );
  } else {
    $form->title("Select Role for $Proj_Name");
  }
}

sub authorize {
  my ( $self, $op ) = @_;
  if ( $self->check_key ) {
    my $form = $self->{form};
    my $Proj_ID = $form->field('Proj_ID');
    return $self->Has_Privilege( 0, 'Privileges', 2 );
    # return 1 if $self->has_role(1, 'DBadmin');
  }
  return 0;
}

sub SELECT {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  $self->Return if $form->submitted eq 'Back';
  my $Proj_ID = $form->field('Proj_ID');
  if ( $form->submitted eq 'Edit Privileges' ) {
    my $RoleSel = $form->field('Pick_Role_Name');
    return( $self->error('Must select a role name to edit') )
      unless $RoleSel =~ m/^(\d+)_(\d+)$/;
    my ( $Role_ID, $RP_ID ) = ( $1, $2 );
    $self->Call( "/RolePrivs/edit", RP_ID => $RP_ID, Proj_ID => $Proj_ID ) if $RP_ID;
    $self->Call( "/RolePrivs/new", Role_ID => $Role_ID, Proj_ID => $Proj_ID ) if $Role_ID;
    return $self->error( "Invalid Role_ID/RP_ID" );
  }
  if ( $form->submitted eq 'Create Role' ) {
    my $Role_Name = $form->field('New_Role_Name');
    my $Role_Type = $form->field('New_Role_Type');
    $self->error('Must specify a role name and type' )
      unless $Role_Name && $Role_Type;
    my $TempRole = $form->field('New_TempRole');
    $self->error('Must select Template or Role')
      unless $TempRole =~ m/^(?:Template|Role)/;
    return 0 if @{$self->{errors}};
    if ( $TempRole =~ m/^Role without priv/ ) {
      $self->{dbh}->do( "INSERT INTO ACT_Role
        ( Proj_ID, Role_Name, Role_Type, Role_Status ) VALUES
        ( ?, ?, ?, ? )", {},
        $Proj_ID, $Role_Name, $Role_Type, 'closed' );
      $self->confirm("$Role_Type Role $Role_Name created");
      return 0;
    }
    my $AsTemplate = $TempRole =~ m/^Template/;

    ### Perform sanity checks on new role
    ### $self->New_Role_Check( $Role_Name, $Role_Type, $TempRole );
    ### return 0 if @{@self->{errors}};

    $self->Call( '/RolePrivs/new', Role_Name => $Role_Name,
      Role_Type => $Role_Type, AsTemplate => $AsTemplate,
      Proj_ID => $Proj_ID );
  }
  return $self->error("Unknown selection");
}

sub INSERT {
  my $self = shift;
  my $form = $self->{form};
  $self->SELECT if $form->submitted eq 'Edit Privileges' ||
		   $form->submitted eq 'Create Role';
  if ( $form->validate ) {
    my $Role_Name = $form->field('Role_Name');
    my $Role_Type = $form->field('Role_Type');
    my $Role_ID = $form->field('Role_ID');
    my $AsTemplate = $form->field('AsTemplate');
    my $Proj_ID = $form->field('Proj_ID');
    $self->fatal('AsTemplate is inconsistent with Role_ID')
      if $AsTemplate && $Role_ID;
    if ($AsTemplate) {
      $Role_ID = 0;
    } else {
      if ( $Role_ID ) {
        my $chk_Proj_ID;
        ( $Role_Name, $Role_Type, $chk_Proj_ID ) =
          $self->{dbh}->selectrow_array(
            "SELECT Role_Name, Role_Type, Proj_ID FROM ACT_Role
             WHERE Role_ID = ?", {}, $Role_ID );
        $self->fatal( "Role_ID $Role_ID does not match Proj_ID $Proj_ID" )
          if $chk_Proj_ID != $Proj_ID;
      } else {
        $self->{dbh}->do(
          "INSERT IGNORE INTO ACT_Role
           ( Proj_ID, Role_Name, Role_Type, Role_Status )
           VALUES ( ?, ?, ?, 'closed' )", {},
           $Proj_ID, $Role_Name, $Role_Type );
        $Role_ID = $self->{dbh}->selectrow_array(
          "SELECT Role_ID FROM ACT_Role WHERE
           Proj_ID = ? AND Role_Name = ?", {},
           $Proj_ID, $Role_Name );
        $self->fatal( "Create Role failed" ) unless $Role_ID;
      }
    }
    $self->{dbh}->do(
      "INSERT INTO ACT_Priv_Roles ( Role_Name, Role_Type, Role_ID )
       VALUES ( ?, ?, ? )", {}, $Role_Name, $Role_Type, $Role_ID );
    my $RP_ID = $self->{dbh}->{mysql_insertid};
    $self->fatal( "RP_ID not defined after insert" ) unless $RP_ID;
    $self->update_privs( $RP_ID );
    $self->Return;
  }
  return $self->error("Not implemented");
}

sub UPDATE {
  my $self = shift;
  my $form = $self->{form};
  $self->Return if $form->submitted eq 'Back';
  $self->SELECT if $form->submitted eq 'Edit Privileges' ||
		   $form->submitted eq 'Create Role';

  if ( $form->validate ) {
    my $RP_ID = $form->field('RP_ID');
    $self->fatal( "RP_ID not defined" ) unless $RP_ID;
    $self->update_privs( $RP_ID );
    $self->Return;
  }
  return $self->error("Form did not validate");
}

sub DELETE {}

sub InitFields {}

sub InitParams {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  my $Proj_ID = $form->field('Proj_ID');
  $self->fatal('No Proj_ID defined') unless
    $Proj_ID =~ m/^\d+$/;
  if ( $op eq 'INSERT' ) {
    $form->tmpl_param( IsInsert => 1 );
  } elsif ( $op eq 'UPDATE' ) {
    $form->tmpl_param( IsUpdate => 1 );
  }

  if ( $op eq 'INSERT' || $op eq 'UPDATE' ) {    
    my $loop = [];
    my $privs = $self->{tmp}->{privs};
    my %flds = map { $_ => $_ } $form->field;
    for my $row ( @$privs ) {
      my ( $Priv_ID, $Priv_Name, $PV_Value, $Priv_IsGlobal ) = @$row;
      my $fieldname = "P$Priv_ID";
      my $fieldtxt = $flds{$fieldname}->tag;
      push @$loop, { privilege => $Priv_Name, is_global => $Priv_IsGlobal,
	setting => $fieldtxt };
    }
    $form->tmpl_param(Privs => $loop);
  }
}

sub update_privs {
  my ( $self, $RP_ID ) = @_;
  my $form = $self->{form};
  my $privs = $self->{tmp}->{privs};
  my @vals;
  for my $row ( @$privs ) {
    my ( $Priv_ID, $Priv_Name, $PV_Value ) = @$row;
    my $fieldname = "P$Priv_ID";
    $PV_Value = $form->field($fieldname);
    $self->fatal( "PV_Value for $fieldname not numeric" )
      unless $PV_Value =~ m/^\d+$/;
    push @vals, "($RP_ID, $Priv_ID, $PV_Value)";
  }
  $self->{dbh}->do(
    "INSERT INTO ACT_PRVals ( RP_ID, Priv_ID, PV_Value ) VALUES " .
    join( ", ", @vals ) .
    " ON DUPLICATE KEY UPDATE PV_Value = VALUES(PV_Value)" );
}

1;
