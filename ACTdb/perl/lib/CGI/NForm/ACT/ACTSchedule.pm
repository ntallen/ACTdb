package CGI::NForm::ACT::ACTSchedule;
use strict;
use CGI;
use CGI::FormBuilder;

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    {
      '' => 'SELECT'
    };
}

# Support for the $params argument is optional, but necessary
# if you want to allow this form to be redirected.
# With FormBuilder 3.0, this functionality can be moved back
# to NForm and get_form() will be responsible only for specifying
# the fields and the template.
sub get_form {
  my ( $self, $op, $params )  = @_;
  $self->Connect();
  my $q = $self->{q} = ref($params) eq 'CGI' ? $params : new CGI($params);
  my $form = CGI::FormBuilder->new(
    action => "$ENV{SCRIPT_NAME}$self->{path_info}",
    method => 'POST',
    fields => [ qw(Venue_ID) ],
    params => $q,
    template => "$self->{tmpldir}/ACTSchedule.html",
    title => "ACT Schedule"
  );
  $form->title( "Full Schedule for ACT" );
  return $form;
}

sub authorize {
  my ( $self, $op ) = @_;
  return 1;
}

sub INSERT {}

sub UPDATE {}

sub DELETE {}

sub SELECT {}

sub InitFields{}

sub InitParams {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  my $Venue_ID = $form->field('Venue_ID');
  my $Venue_Qual = '';
  if ( $Venue_ID =~ m/^\d+$/ ) {
    my @flds = qw(Venue_Short Venue_Long Address1 Address2
          City State ZIP URL);
    my $vals = $self->{dbh}->selectrow_hashref(
      'SELECT ' . join( ', ', @flds ) . ' FROM ACT_Venue
      WHERE Venue_ID = ?', {}, $Venue_ID );
    my $CSZ = join( ' ', grep $_,
      join( ', ', grep $_, $vals->{City}, $vals->{State} ),
      $vals->{ZIP} );
    my $Addr = join( "<br>\n", grep $_, $vals->{Address1}, $vals->{Address2}, $CSZ );
	$form->tmpl_param( Venue_Long => $vals->{Venue_Long} );
    $form->tmpl_param( Address => $Addr );
    $form->tmpl_param( URL => $vals->{URL} );
	$form->title("ACT Schedule at $vals->{Venue_Long}");
	$Venue_Qual = " AND ACT_Event.Venue_ID = '$Venue_ID'";
  }

  ### List Rehearsals ('Group' type Events),
  ### Auditions, Meetings and Special Events
  ### (Auditions are like special events in that they have
  ### specially created roles. Meetings use pre-existing roles)
  
  my $evts = $self->{dbh}->selectall_arrayref(
   "SELECT
      DATE_FORMAT(Event_Date,'%a %b %e, %Y'),
      CONCAT(TIME_FORMAT(Event_Start, '%l:%i %p&ndash;'),
        TIME_FORMAT(Event_End, '%l:%i %p')),
      Proj_Name, ACT_Event.Proj_ID, Event_Desc,
      Venue_Short, ACT_Event.Venue_ID
    FROM
      ACT_Event NATURAL JOIN ACT_Project
      LEFT JOIN ACT_Venue
       ON ( ACT_Event.Venue_ID = ACT_Venue.Venue_ID )
    WHERE Proj_Status = 'Active' AND Event_Date >= CURDATE()$Venue_Qual
    ORDER BY Event_Date, Event_Start, Proj_Name" );
  my $params = $self->map_cols(
    [ 'date', 'time', 'Proj_Name', 'Proj_ID', 'desc', 'venue', 'venue_id' ],
    $evts );
  my $oparams = [];
  my $cur_date;
  for ( @$params ) {
    $_->{date_rows} = 0;
    $cur_date = $_ unless defined $cur_date &&
	$cur_date->{date} eq $_->{date};
    $cur_date->{date_rows}++;
    $_->{time} =~ s/12:00 PM/Noon/g;
    $_->{time} =~ s/12:00 AM/Midnight/g;
    $_->{time} =~ s/:00//g;
    $_->{time} =~ s/( [AP]M)(\&ndash;.*\1)/$2/;
    push @$oparams, $_;
  }
  $form->tmpl_param( Events => $oparams );
  # $form->tmpl_param( Proj_Schedule => $form->field('Proj_Schedule') );
}

1;
