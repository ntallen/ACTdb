package CGI::NForm::ACT::MySched;
use strict;
use CGI;
use CGI::FormBuilder;

# List all contact information for Participants associated with
# a specific Role.

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    { 
      '' =>   'SELECT'
    };
}

my %allchars = (
  "All Characters" => '.',
  Becky => 'Becky|Girls',
  Bellamy => 'Bellamy',
  Ben => 'Ben|Boys',
  "Boys' Ensemble" => 'Boys',
  Dobbins => 'Dobbins|Men',
  Doc => 'Dobbins',
  Drunk => 'Drunk|Men',
  "Girls' Ensemble" => 'Girls',
  Hartley => 'Hartley|Men',
  Huck => 'Huck|Boys',
  Injun => 'Injun',
  Joe => 'Joe|Boys',
  "Men's Ensemble" => 'Men',
  Muff => 'Muff',
  Polly => 'Polly|Women',
  "Rev Sprague" => 'Rev Sprague|Men',
  Sid => 'Sid|Boys',
  Thatcher => 'Thatcher|Men',
  Tom => 'Tom|Boys',
  Wain => 'Wain|Women',
  Widow => 'Widow|Women',
  "Women's Ensemble" => 'Women'
);

# Support for the $params argument is optional, but necessary
# if you want to allow this form to be redirected.
# With FormBuilder 3.0, this functionality can be moved back
# to NForm and get_form() will be responsible only for specifying
# the fields and the template.
sub get_form {
  my ( $self, $op, $params )  = @_;
  my $q = $self->{q} = ref($params) eq 'CGI' ? $params : new CGI($params);
  my $form = CGI::FormBuilder->new(
    action => "$ENV{SCRIPT_NAME}$self->{path_info}#Current",
    method => 'GET',
    fields => [ qw(Cast Chars) ],
    params => $q,
    template => "$self->{tmpldir}/MySched.html",
    title => "My Schedule"
  );
  $form->field( name => 'Cast', type => 'checkbox', options => [ 'Red', 'Blue' ], linebreaks => 1 );
  $form->field( name => 'Chars', type => 'checkbox', multiple => 1,
    options => [ sort keys %allchars ], linebreaks => 1 );
  $form->tmpl_param( 'Proj_Name' => 'Tom Sawyer' );
  return $form;
}

sub authorize {
  return 1;
}

sub INSERT {
}

sub UPDATE {
}

sub DELETE {
}

sub SELECT {
  my $self = shift;
  my $form = $self->{form};
  my @cast = $form->field('Cast');
  push( @cast, 'Red', 'Blue' ) unless @cast;
  @cast = grep m/^Red|Blue$/, @cast;
  my ( $casttext, $castpat );
  if ( @cast == 1 ) {
    $casttext = "the " . $cast[0] . " Cast";
    $castpat = ">$cast[0]<";
    if ( $cast[0] eq 'Red' ) {
      $allchars{Bellamy} = 'Bellamy|Men';
    } else {
      $allchars{Bellamy} = 'Bellamy|Women';
    }
  } else {
    $casttext = 'both Casts';
    $castpat = '.';
    $allchars{Bellamy} = 'Bellamy|Men|Women';
  }
  my @chars = $form->field('Chars');
  push @chars, 'All Characters' unless @chars;
  my $charpat = '\b' . join( '|',
    map( $allchars{$_}, grep $allchars{$_}, @chars ), 'ALL' ) . '\b';
  $form->tmpl_param( ForWhom =>
    join( ' and ', grep $allchars{$_}, @chars ) . " in $casttext" );

  my $curdate;
  my %chars;
  my @Days;
  my $act_events;
  my $current = 0;
  my $act_date = '';
  open( SCHED, "<$self->{tmpldir}/MySched.txt" ) ||
    $self->fatal( "Unable to read MySched.txt" );
  while (my $line = <SCHED>) {
    if ( $line =~ m/^CURRENT/ ) {
      $current = 1;
      next;
    }
    next if $line =~ m/^\s*$/;
    while ( $line =~ s|\\\r?\n|| ) {
      my $nextline = <SCHED>;
      last unless $nextline;
      $line .= $nextline;
    }
    my ( $date, $times, $cast, $scene, $chars, $room, $leader, $comment ) =
      split /\t/, $line;
    if ( $date ) {
      $curdate = $date;
    } else {
      $date = $curdate;
    }
    next unless $cast =~ m/$castpat/ && $chars =~ m/$charpat/;
    $cast = "$cast$scene" if $scene;
    $chars .= " $comment" if $comment;
    if ( $curdate ne $act_date ) {
      $act_events = [];
      $act_date = $curdate;
      my $label = $current ? '<a name="Current"></a>' : '';
      push @Days, { Day => "$label$act_date", Events => $act_events };
      $current = 0;
    }
    push @$act_events,
      { Time => $times, Cast => $cast, Chars => $chars, Room => $room,
        Staff => $leader };
  }
  $form->tmpl_param( Days => \@Days );
  $form->tmpl_param( has_list => 1 );
  return 0;
}

sub InitFields {
}

1;
