package CGI::NForm::ACT::Rehearsal;
use strict;
use CGI;
use CGI::FormBuilder;

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    {
      '' => 'SELECT',
      'new' => 'INSERT',
      'edit' => 'UPDATE',
      'delete' => 'DELETE'
    };
}

# Support for the $params argument is optional, but necessary
# if you want to allow this form to be redirected.
# With FormBuilder 3.0, this functionality can be moved back
# to NForm and get_form() will be responsible only for specifying
# the fields and the template.
sub get_form {
  my ( $self, $op, $params )  = @_;
  $self->Connect();
  my $q = $self->{q} = ref($params) eq 'CGI' ? $params : new CGI($params);
  my %submit_lbl = (
    SELECT => '',
    INSERT => 'Create',
    UPDATE => 'Update',
    DELETE => 'Delete'
  );
  
  ### Cannot use static because that hides the submit buttons
  ### Could set each field static individually
  my $static = $op eq 'SELECT' || $op eq 'DELETE';
  my $form = CGI::FormBuilder->new(
    action => "$ENV{SCRIPT_NAME}$self->{path_info}",
    method => 'POST',
    params => $q,
    template => "$self->{tmpldir}/Rehearsal.html",
    title => "Rehearsal",
    submit => [ $submit_lbl{$op} || (), "Back"  ],
    ### static => $op eq 'SELECT' || $op eq 'DELETE',
    reset => $op eq 'UPDATE' ? 'Reset' : 0
  );
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'confirm', type => 'hidden' );
  $form->field( name => 'Proj_ID', type => 'hidden' );
  $form->field( name => 'Event_ID', type => 'hidden' );
  $form->field( name => 'Event_Desc', static => $static );
  $form->field( name => 'Venue_ID', type => 'select', static => $static );
  $form->field( name => 'Leader_ID', static => $static );
  $form->field( name => 'Event_Date',
     validate => '/^\d\d\d\d[-\/]\d\d?[-\/]\d\d?$/',
     static => $static );
  $form->field( name => 'Event_Start',
     validate => '/^\d\d?(:\d\d)?( *[AaPp][Mm])?$/',
     static => $static );
  $form->field( name => 'Event_End',
     validate => '/^\d\d?(:\d\d)?( *[AaPp][Mm])?$/',
     static => $static );
  
  my $Proj_ID = $form->field('Proj_ID') ||
    $self->fatal("No Proj_ID Specified");
  my ( $Proj_Name, $N_Casts ) = $self->{dbh}->selectrow_array(
    'SELECT Proj_Name, N_Casts FROM ACT_Project WHERE Proj_ID = ?', {}, $Proj_ID );
  $form->title( "$submit_lbl{$op} Rehearsal for $Proj_Name" );
  if ($N_Casts == 2 ) {
    $self->{tmp}->{doublecast} = 1;
    my $casts = $self->{dbh}->selectall_arrayref(
      'SELECT Cast_ID, Cast_Name FROM ACT_Casts ORDER BY Cast_ID' );
    $form->field( name => 'Cast_ID', options => $casts,
      static => $static, cleanopts => 0 );
  }

  my $Scenes = $self->{dbh}->selectall_arrayref(
    "SELECT Grp_ID, CONCAT( Grp_Name, ': ', Grp_Desc ) FROM ACT_Group
     WHERE Proj_ID = ? and Grp_Type != 'JobClone'
     ORDER by Grp_Order, Grp_Name", {}, $Proj_ID );
  $form->field( name => 'Scenes', options => $Scenes, type => 'checkbox',
    linebreaks => 1, static => $static );
  
  my $Venues = $self->{dbh}->selectall_arrayref(
    "SELECT Venue_ID, Venue_Short FROM ACT_Venue
     ORDER BY Venue_Short", {} );
  $form->field( name => 'Venue_ID', options => $Venues, static => $static );
  
  if ( $op eq 'INSERT' ) {
    $form->field( name => 'RepeatCt', value => 1, size => 2,
      validate => '/[0-9]+/' );
    $form->field( name => 'RepeatTime', value => 1, size => 2,
      validate => '/[0-9]+/' );
    $form->field( name => 'RepeatUnit', value => 'Weeks',
      options => [ qw(Minutes Hours Days Weeks) ],
      type => 'select' );
  }
  return $form;
}

# Modification authorized for Project or Board Registrar
sub authorize {
  my ( $self, $op ) = @_;
  return 1 if $op eq 'SELECT';
  if ( $self->check_key ) {
    my $form = $self->{form};
    my $Proj_ID = $form->field('Proj_ID');
    return 1 if $self->Has_Privilege( $Proj_ID, 'Schedule', 1 );
    # return 1 if $self->has_role( $Proj_ID, 'Registrar' ) ||
    #   $self->has_role( 1, 'Registrar' );
  }
  return 0;
}

# Called when form is first displayed (prior to submission)
sub InitFields {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  my $Proj_ID = $form->field('Proj_ID');
  my $Event_ID = $form->field('Event_ID');
  if ( $Event_ID ) {
    my @flds = ( qw(Event_Desc Event_Date Event_Start Event_End Venue_ID Cast_ID) );
    my @vals = $self->{dbh}->selectrow_array(
      "SELECT Event_Desc, Event_Date,
       TIME_FORMAT(EVENT_Start,'%l:%i %p'),
       TIME_FORMAT(EVENT_End,'%l:%i %p'),
       Venue_ID, Cast_ID
       FROM ACT_Event WHERE Event_ID = ?", {}, $Event_ID );
    for ( @flds ) {
      my $val = shift(@vals);
      $form->field( name => $_, value => $val, force => 1 )
	if ! defined $form->field($_);
    }
    if ( ! defined $form->field('Scenes') ) {
      my $scenes = $self->{dbh}->selectcol_arrayref(
	'SELECT Grp_ID FROM ACT_Evt_Grps WHERE Event_ID = ?',
	{}, $Event_ID );
      $form->field( name => 'Scenes', value => $scenes, force => 1 );
    }
  } elsif ( $op ne 'INSERT' ) {
    $self->fatal("No Event_ID Specified");
  }
}

sub addminutes {
  use integer;
  my ( $t1, $m2 ) = @_;
  my ( $h, $m, $s ) = $t1 =~ m/^(\d+):(\d+):(\d+)$/;
  return undef unless defined($s);
  $m += $m2;
  if ( $s >= 60 ) {
    $m += $s/60;
    $s = $s % 60;
  }
  if ( $m >= 60 ) {
    $h += $m/60;
    $m = $m % 60;
  }
  return undef if $h >= 24;
  return sprintf( "%02d:%02d:%02d", $h, $m, $s );
}

sub INSERT {
  my $self = shift;
  my $form = $self->{form};
  $self->Return if $form->submitted eq 'Back';
  if ( $form->submitted eq 'New Venue' ) {
    $self->Call( '/Venue/new' );
  } elsif ( $form->submitted eq 'Edit Venue' ) {
    my $Venue_ID = $form->field('Venue_ID');
    return $self->error('Must select a venue to edit') unless $Venue_ID;
    $self->Call( '/Venue/edit', Venue_ID => $Venue_ID );
  }
  unless ( $form->validate ) {
    $self->error('Validation Error');
    return 0;
  }
  my @flds = qw(Proj_ID Event_Desc Venue_ID Cast_ID);
  my @dflds = qw(Event_Date);
  my @tflds = qw(Event_Start Event_End);
  my @rflds = qw(RepeatCt RepeatTime RepeatUnit);
  ### Add Leader_ID
  my %vals;
  for my $fld ( @flds, @dflds, @tflds, @rflds ) {
    $vals{$fld} = $form->field($fld);
  }
  for ( @tflds ) {
    $vals{$_} = fix_time($vals{$_});
  }
  if ( $self->{tmp}->{doublecast} ) {
    if ( ! defined($vals{Cast_ID}) ) {
      return $self->error('Must select a Cast');
    }
  } else {
    $vals{Cast_ID} = 0;
  }
  my @grps = $form->field('Scenes');
  $vals{RepeatUnit} =~ s/[sS]$//;
  if ( uc($vals{RepeatUnit}) eq 'WEEK' ) {
    $vals{RepeatTime} *= 7;
    $vals{RepeatUnit} = 'DAY';
  } elsif ( uc($vals{RepeatUnit}) eq 'HOUR' ) {
    $vals{RepeatTime} *= 60;
    $vals{RepeatUnit} = 'MINUTE';
  }
  my $RepeatCt = $vals{RepeatCt};
  my $RepeatType = 'NONE';
  if ( $RepeatCt > 1 ) {
    if ( uc($vals{RepeatUnit}) eq 'DAY' ) {
      $RepeatType = 'DATE';
    } else {
      $RepeatType = 'TIME';
    }
  }
  my $RepeatSum = 0;
  my $RepeatUnit = uc($vals{RepeatUnit});
  while ( $RepeatCt-- > 0 ) {
    if ( $RepeatType eq 'NONE' ) {
      $self->{dbh}->do(
	'INSERT INTO ACT_Event
	(Proj_ID, Event_Desc, Venue_ID, Cast_ID, Event_Date, Event_Start, Event_End, GR_Type)
	VALUES ( ?, ?, ?, ?, ?, ?, ?, ? )', {},
	map( $vals{$_}, @flds, @dflds, @tflds ), 'Group' );
    } elsif ( $RepeatType eq 'DATE' ) {
      $self->{dbh}->do(
	'INSERT INTO ACT_Event
	(Proj_ID, Event_Desc, Venue_ID, Cast_ID, Event_Start, Event_End, Event_Date, GR_Type)
	VALUES ( ?, ?, ?, ?, ?, ?, ? + INTERVAL ? DAY, ? )', {},
	map( $vals{$_}, @flds, @tflds, @dflds ), $RepeatSum, 'Group' );
    } else {
      warn("Event Start: $vals{Event_Start} End: $vals{Event_End}\n");
      my $stime = addminutes($vals{Event_Start}, $RepeatSum);
      my $etime = addminutes($vals{Event_End}, $RepeatSum);
      warn("Event times: $stime - $etime\n");
      last unless defined($stime) && defined($etime);
      $self->{dbh}->do(
	"INSERT INTO ACT_Event
	(Proj_ID, Event_Desc, Venue_ID, Cast_ID, Event_Date, Event_Start, Event_End, GR_Type)
	VALUES ( ?, ?, ?, ?, ?, ?, ?, ? )", {},
	map( $vals{$_}, @flds, @dflds ), $stime, $etime, 'Group' );
    }
    $RepeatSum += $vals{RepeatTime};
  
    my $Event_ID = $self->{dbh}->{mysql_insertid};

    $self->add_groups( $Event_ID, @grps );
  }
  $self->GoTo( '/Rehearsal/new', Proj_ID =>
      $form->field('Proj_ID'), Venue_ID => $vals{Venue_ID},
      Cast_ID => $vals{Cast_ID}, Event_Date => $vals{Event_Date},
      Event_Start => $form->field('Event_Start'),
      Event_End => $form->field('Event_End'),
      confirm => 1 );
}

sub UPDATE {
  my $self = shift;
  my $form = $self->{form};
  $self->Return if $form->submitted eq 'Back';
  if ( $form->submitted eq 'New Venue' ) {
    $self->Call( '/Venue/new' );
  } elsif ( $form->submitted eq 'Edit Venue' ) {
    my $Venue_ID = $form->field('Venue_ID');
    return $self->error('Must select a venue to edit') unless $Venue_ID;
    $self->Call( '/Venue/edit', Venue_ID => $Venue_ID );
  }
  unless ( $form->validate ) {
    $self->error('Validation Error');
    return 0;
  }
  my $Event_ID = $form->field('Event_ID');

  my @flds = qw(Event_Desc Event_Date Event_Start Event_End Venue_ID Cast_ID);
  ### Add Leader_ID
  my %vals;
  for my $fld ( @flds ) {
    $vals{$fld} = $form->field($fld);
  }
  for ( qw(Event_Start Event_End) ) {
    $vals{$_} = fix_time($vals{$_});
  }
  $vals{Cast_ID} = 0 unless $self->{tmp}->{doublecast};
  
  $self->{dbh}->do(
    'UPDATE ACT_Event
     SET ' . join(', ', map "$_ = ?", @flds ) .
    ' WHERE Event_ID = ?', {},
    map( $vals{$_}, @flds ), $Event_ID );

  my $curgrps = $self->{dbh}->selectcol_arrayref(
    'SELECT Grp_ID FROM ACT_Evt_Grps WHERE Event_ID = ?', {},
    $Event_ID );
  my %curgrps;
  for ( @$curgrps ) {
    $curgrps{$_} = 1;
  }
  my @newgrps = $form->field('Scenes');
  my @addgrps = grep ! $curgrps{$_}, @newgrps;
  for ( @newgrps ) {
    $curgrps{$_} = 0;
  }
  my @delgrps = grep $curgrps{$_}, keys %curgrps;
  $self->add_groups( $Event_ID, @addgrps );
  if ( @delgrps ) {
    my $st = $self->{dbh}->prepare(
      'DELETE FROM ACT_Evt_Grps WHERE Event_ID = ? AND Grp_ID = ?' );
    for ( @delgrps ) {
      $st->execute( $Event_ID, $_ );
    }
  }
  
  $self->Return;
}

sub DELETE {
  my $self = shift;
  my $form = $self->{form};
  $self->Return if $form->submitted eq 'Back';
  my $Event_ID = $form->field('Event_ID');
  $self->{dbh}->do(
    'DELETE FROM ACT_Evt_Grps WHERE Event_ID = ?',
    {}, $Event_ID );
  $self->{dbh}->do(
    'DELETE FROM ACT_Event WHERE Event_ID = ?',
    {}, $Event_ID );
  $self->Return;
}

sub SELECT {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  $self->Return; # The only option on submit
}

sub InitParams {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  if ( $op eq 'INSERT' || $op eq 'UPDATE' ) {
    $form->tmpl_param( pick_venue =>
      '<input type="submit" name="_submit" value="Edit Venue">
       <input type="submit" name="_submit" value="New Venue">' );
  }
  $form->tmpl_param( doublecast => 1 ) if $self->{tmp}->{doublecast};
  $form->tmpl_param( confirm => 1 ) if $form->field('confirm');
}

sub fix_time {
  my $input = shift;
  if ( $input =~ m,^(\d\d?)(?::(\d\d))?(?: *([AaPp][Mm]))?$, ) {
    my ( $h, $m, $suff ) = ( $1, $2, $3 );
    $m = '00' unless defined $m;
    if ( $suff && $suff =~ m,pm,i && $h < 12 ) {
      $h += 12;
    }
    $input = "$h:$m:00";
  }
  return $input;
}

sub add_groups {
  my ( $self, $Event_ID, @grps ) = @_;
  if ( @grps ) {
    my $st = $self->{dbh}->prepare(
      'INSERT INTO ACT_Evt_Grps (Event_ID, Grp_ID) VALUES ( ?, ? )' );
    for my $Grp_ID ( @grps ) {
      $st->execute( $Event_ID, $Grp_ID );
    }
  }
}

1;
