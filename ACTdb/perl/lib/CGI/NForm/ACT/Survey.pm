package CGI::NForm::ACT::Survey;
use strict;
use CGI;
use CGI::FormBuilder;

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    { # rm  =>  'DELETE',
      # edit => 'UPDATE',
      review => 'SELECT',
      '' =>   'INSERT'
    };
}

my @checks = qw(OptA OptB OptC OptD OptE
		M4 T4 W4 Th4 F4
	        M7 T7 W7 Th7 F7 );

# Support for the $params argument is optional, but necessary
# if you want to allow this form to be redirected.
# With FormBuilder 3.0, this functionality can be moved back
# to NForm and get_form() will be responsible only for specifying
# the fields and the template.
sub get_form {
  my ( $self, $op, $params )  = @_;
  # $self->Connect();
  my $q = $self->{q} = ref($params) eq 'CGI' ? $params : new CGI($params);
  my $form = CGI::FormBuilder->new(
    action => "$ENV{SCRIPT_NAME}$self->{path_info}",
    method => 'POST',
    fields => [ qw(Age Gender), @checks ],
    params => $q,
    template => "$self->{tmpldir}/Survey.html",
    title => "ACT Administrative Interface",
    submit => "Submit Survey Responses"
  );
  $form->field( name => 'Age', type => 'select',
		options => [ 7..17 ] );
  $form->field( name => 'Gender', options => [ qw(M F) ] );
  for my $check ( @checks ) {
    $form->field( name => $check, options => [1],
        labels => { 1 => ' ' } );
  }
  return $form;
}

sub authorize {
  my ( $self, $op ) = @_;
  return 1;
}

sub INSERT {
  # This is where we record the survey results
  my $self = shift;
  $self->Connect;
  my %vals = (
    Age => $self->{form}->field('Age') || 0,
    Gender => $self->{form}->field('Gender') || '',
    map { ( $_ => $self->{form}->field($_) || 0 ) } @checks );
  my $st = join ' ',
    'INSERT INTO ACT_Survey ( TStamp, Age, Gender,',
    join( ', ', @checks ),
    ') VALUES ( NOW(), ?, ?,',
    join( ', ', map "?", @checks ),
    ');';
  $self->{dbh}->do( $st, {}, $vals{Age}, $vals{Gender},
		map $vals{$_}, @checks );
  $self->Process( '/Survey/review', '' );
  exit(0);
}

sub UPDATE {
}

sub DELETE {
}

sub SELECT {
  use HTML::Template;
  # This is where we list the survey results
  my $self = shift;
  $self->Connect;
  my $tmpl =
    HTML::Template->new_file( "$self->{tmpldir}/Survey.html" );
  my $vals = $self->{dbh}->selectall_arrayref(
    'SELECT Age, Gender FROM ACT_Survey' );
  my $nresp = @$vals;
  $tmpl->param('responses' => $nresp );
  my %Avals;
  my %Gvals;
  for my $val ( @$vals ) {
    $Avals{$val->[0]}++;
    $Gvals{$val->[1]}++;
  }
  $tmpl->param("field-Age" =>
      join( ' ', map "$_:$Avals{$_}",
	    sort { $a <=> $b } keys %Avals ) );
  $tmpl->param("field-Gender" =>
      join( ' ', map "$_:$Gvals{$_}",
	    sort keys %Gvals ) );
  my $st = join ' ',
    'SELECT',
    join( ', ', map( "SUM($_)", @checks )),
    'FROM ACT_Survey;';
  my $cvals = $self->{dbh}->selectall_arrayref( $st );
  $cvals = $cvals->[0];
  for my $check( @checks ) {
    $tmpl->param("field-$check" => shift @$cvals);
  }
  print "Content-type: text/html\cM\cJ\cM\cJ", $tmpl->output;
  exit(0);
}

sub InitFields {
  my ( $self, $op ) = @_;
  $self->SELECT if $op eq 'SELECT';
}

sub map_cols {
  my ( $labels, $aa ) = @_;
  my $ah = [ map {
      my $row = $_;
      my $ref = { map( ( $_ => shift @$row ), @$labels ) };
    } @$aa ];
}

1;
