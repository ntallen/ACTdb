package CGI::NForm::ACT::Contact;
use strict;
use CGI;
use CGI::FormBuilder;

# List all contact information for Participants associated with
# a specific Role.

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    { 
      '' =>   'SELECT'
    };
}

# Support for the $params argument is optional, but necessary
# if you want to allow this form to be redirected.
# With FormBuilder 3.0, this functionality can be moved back
# to NForm and get_form() will be responsible only for specifying
# the fields and the template.
sub get_form {
  my ( $self, $op, $params )  = @_;
  $self->Connect();
  my $q = $self->{q} = ref($params) eq 'CGI' ? $params : new CGI($params);
  my $form = CGI::FormBuilder->new(
    action => "$ENV{SCRIPT_NAME}$self->{path_info}",
    method => 'POST',
    fields => [ qw(nxtPI nxtPA Proj_ID Role_ID) ],
    params => $q,
    template => "$self->{tmpldir}/Contact.html",
    title => "Contact Information List",
    submit => [ "Back" ]
  );
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'Proj_ID', type => 'hidden' );
  $form->field( name => 'Role_ID', type => 'hidden' );
  return $form;
}

sub authorize {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  if ( $self->check_key ) {
    my $Proj_ID = $form->field('Proj_ID');
    $self->fatal( "No Project Specified" ) unless $Proj_ID;
    return 1 if $self->Has_Privilege( $Proj_ID, 'ContactInfo', 1 );
    # return 1 if $self->has_role( 1, 'Board' ) ||
    #   $self->has_role( $Proj_ID, 'Production Team');
    # $self->error( "Access restricted to members of the Board or the Production Team" );
  }
  return 0;
}

sub INSERT {
}

sub UPDATE {
}

sub DELETE {
}

sub SELECT {
  my $self = shift;
  $self->Return;
}

sub InitFields {
  my $self = shift;
  $self->ProjSearch;
}

1;
