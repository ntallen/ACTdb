package CGI::NForm::ACT::Fixup;
use strict;
use CGI;
use CGI::FormBuilder;

# Merge participant Part_ID2 into participant Part_ID1

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    { 
      '' =>   'SELECT'
    };
}

# Support for the $params argument is optional, but necessary
# if you want to allow this form to be redirected.
# With FormBuilder 3.0, this functionality can be moved back
# to NForm and get_form() will be responsible only for specifying
# the fields and the template.
sub get_form {
  my ( $self, $op, $params )  = @_;
  $self->Connect();
  my $q = $self->{q} = ref($params) eq 'CGI' ? $params : new CGI($params);
  my $form = CGI::FormBuilder->new(
    action => "$ENV{SCRIPT_NAME}$self->{path_info}",
    method => 'POST',
    fields => [ qw(nxtPI nxtPA FirstName LastName Keeper Merge) ],
    params => $q,
    template => "$self->{tmpldir}/Fixup.html",
    title => "Database Administration",
    submit => [ "Submit", "Back" ]
  );
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'showall', options => 'Show Past Roles' );
  return $form;
}

sub authorize {
  my ( $self, $op ) = @_;
  return 1 if $self->check_key( EU => 1 ) &&
    $self->Has_Privilege( 0, 'DBAdmin', 1 );
  return 0;
}

sub INSERT {
}

sub UPDATE {
  return 0;
}

sub DELETE {
}

sub SELECT {
  my $self = shift;
  my $form = $self->{form};
  my $ln = $form->field('LastName');
  my $fn = $form->field('FirstName');
  my $st = $form->submitted;
  if ( $form->submitted eq 'Back' ) {
    $self->Return;
  } elsif ( $st eq 'Admin Reg' ) {
    $self->GoTo( "/AdminReg", LastName => $ln, FirstName => $fn,
      _submitted => 1, _submit => 'Search');
  } elsif ( $st eq 'Submit' ) {
    my $Keep = $form->field( 'Keeper' ) || '';
    my $Merge = $form->field( 'Merge' ) || '';
    my $Part_ID1 = 0;
    my $Part_ID2 = 0;
    if ( $Keep && $Merge ) {
      if ( $Merge =~ m/^PID(\d+)$/ ) {
        $Part_ID2 = $1;
        if ( $Keep =~ m/^PID(\d+)$/ ) {
          $Part_ID1 = $1;
          $self->Call( "/MrgPart", Part_ID1 => $Part_ID1, Part_ID2 => $Part_ID2 );
        } elsif ( $Keep =~ m/^FID(\d+)$/ ) {
          $self->{dbh}->do(
            'LOCK TABLES ACT_Participant WRITE, ACT_Family WRITE, ACT_Address WRITE,
             ACT_Log WRITE, ACT_Gift WRITE, ACT_Cast WRITE, ACT_Project READ,
             ACT_Role READ, ACT_Participant AS P2 READ, ACT_Role AS R2 READ' );
          my $Fam_ID = $1;
          my $Old_Fam_ID = $self->{dbh}->selectrow_array(
            'SELECT Fam_ID FROM ACT_Participant WHERE Part_ID = ?', {}, $Part_ID2 );
          my $New_Addr_ID = $self->{dbh}->selectrow_array(
            'SELECT Addr_ID FROM ACT_Family WHERE Fam_ID = ?', {}, $Fam_ID );
          if ( $Old_Fam_ID && $New_Addr_ID ) {
            $self->{dbh}->do(
              'UPDATE ACT_Participant SET Fam_ID = ?, Addr_ID = ? WHERE Part_ID = ?',
              {}, $Fam_ID, $New_Addr_ID, $Part_ID2 );
            $self->Log( "Moved Part_ID $Part_ID2 from Fam $Old_Fam_ID to $Fam_ID" );
            $self->purge_fam( $Old_Fam_ID, $Fam_ID );
            $self->Fixup_Parents( $Fam_ID );
            $self->{dbh}->do('UNLOCK TABLES');
            $self->GoTo( "/Fixup", LastName => $ln, FirstName => $fn,
              _submitted => 1, _submit => "Search" );
          } else {
            $self->{dbh}->do('UNLOCK TABLES');
            $self->error( "Participant or new addr did not exist" );
          }
        } else {
          $self->error( "Invalid Keep Selection: '$Keep'" );
        }
      } else {
        $self->error( "Invalid Merge Selection" );
      }
    } else {
      $self->error( "Must select an entry from both the Keep and Merge columns" );
    }
  }
  if ( $form->submitted eq 'Search' || $form->submitted eq 'Submit' ) {
    my $showall = $form->field('showall');
    $self->FLsearch($showall);
  } else {
    my $st = $form->submitted;
    $self->error( "Unrecognized submit: $st" );
  }
  return 0;
}

sub InitFields {
}

1;
