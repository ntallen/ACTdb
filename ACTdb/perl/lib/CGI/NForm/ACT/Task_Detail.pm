package CGI::NForm::ACT::Task_Detail;
use strict;
use CGI;
use CGI::FormBuilder;

# Created 2/19/2009
# What we want to display for a task:
# Task_ID RO
# Milestone RO
# Milestone_Delta RO
# Task_Category RO
# Task_Action RO
# Task_Description RO
##   I list these all as read only because I expect the tasks to be editted offline and uploaded
## I'll need a task list export before I allow folks to add new tasks online
## So what *can* they edit:
# Task_Status TA_Desc
# Task Audit History
# Task Notes History

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    {
      '' => 'SELECT',
      'edit' => 'UPDATE'
    };
}

# 
sub InitForm {
  my ( $self, $form, $op )  = @_;
  $self->Connect();
  my $static = '';
  my $submit = [ 'Back' ];
  if ( $op eq 'SELECT' ) {
    $static = 'static';
  } else {
    push(@$submit, 'Update');
    $form->tmpl_param( update => 1 );
  }
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'Proj_ID', type => 'hidden' );
  $form->field( name => 'Task_ID', type => 'text', static => 'static' );
  $form->field( name => 'TA_Desc', type => 'text', static => $static );
  $form->field( name => 'Task_Status', options => [ 'To Do', 'Ongoing', 'N/A', 'Done' ],
                type => 'select', static => $static );
  $form->field( name => 'Task_Category', type => 'text', static => $static );
  $form->field( name => 'Task_Action', type => 'text', size => 80, static => $static );
  $form->field( name => 'Task_Desc', type => 'text', size => 80, static => $static );
  $form->field( name => 'Milestone', type => 'text', static => 'static' );
  $form->field( name => 'Milestone_Delta', type => 'text', static => 'static' );
  $form->field( name => 'Task_Date', type => 'text', static => 'static' );
  $form->submit( $submit );
  my $Proj_ID = $form->field('Proj_ID');
  $self->fatal("Invalid or missing Proj_ID") if $Proj_ID !~ /^\d+$/;
  my $Proj_Name = $self->{dbh}->selectrow_array(
    "SELECT Proj_Name FROM ACT_Project WHERE Proj_ID = ?", {}, $Proj_ID );
  $self->fatal("Invalid Proj_ID") unless $Proj_Name;
  $form->title("Task Detail for $Proj_Name");
  my $Task_ID = $form->field('Task_ID');
  $self->fatal("Invalid or missing Task_ID") if $Task_ID !~ /^\d+$/;
  $form->template( "$self->{tmpldir}/Task_Detail.html" );
}

sub authorize {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  if ( $self->check_key( EU => 1 ) ) {
    my $Proj_ID = $form->field('Proj_ID');
    my $TaskPriv = $self->Get_Privilege_Value( $Proj_ID, 'TaskAdmin' );
    return $self->error("Not authorized to access tasks for this project")
      unless $TaskPriv;
    my $can_update = $TaskPriv >= 3;
    if ($TaskPriv == 2 && $op eq 'UPDATE') {
      my $Task_ID = $form->field('Task_ID');
      my ( $update ) = $self->{dbh}->selectrow_array(
        "SELECT 1
        FROM ACT_Task_Delegate AS D
          JOIN ACT_Task_Abbr AS A ON D.Abbr_ID = A.Abbr_ID
          JOIN ACT_Cast AS C ON A.Role_ID = C.Role_ID
        WHERE D.Proj_ID = ? AND D.Task_ID = ? AND C.Part_ID = ?
          AND ( A.Cast_Note = '' OR A.Cast_Note = C.Cast_Note)",
        {}, $Proj_ID, $Task_ID, $self->{auth}->{Part_ID} );
      $can_update = 1 if $update;
    }
    if ($op eq 'UPDATE' && !$can_update) {
      $form->field( name => 'TA_Desc', static => 'static' );
      $form->field( name => 'Task_Status', static => 'static' );
      $form->field( name => 'Task_Category', static => 'static' );
      $form->field( name => 'Task_Action', static => 'static' );
      $form->field( name => 'Task_Desc', static => 'static' );
      $form->submit(['Back']);
    }
    $self->{tmp}->{can_update} = $can_update;
    return 1;
  }
  return 0;
}

sub INSERT {}

sub UPDATE {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  $self->Return if $form->submitted eq 'Back';
  return $self->error('Not authorized to modify this task')
    unless $self->{tmp}->{can_update};
  if ( $form->submitted eq 'Update' ) {
    my $Proj_ID = $form->field('Proj_ID');
    my $Task_ID = $form->field('Task_ID');
    my $Task_Action = $form->field('Task_Action');
    my $Task_Desc = $form->field('Task_Desc');
    my $TA_Desc = $form->field('TA_Desc');
    my $Task_Category = $form->field('Task_Category');
    my $Task_Status = $form->field('Task_Status');
    $Task_Status = '' if $Task_Status eq 'To Do';
    $self->{dbh}->do(
      "UPDATE ACT_Task SET Task_Action = ?, Task_Desc = ?,
       Task_Status = ?, Task_Category = ?
       WHERE Proj_ID = ? AND Task_ID = ?", {},
      $Task_Action, $Task_Desc, $Task_Status, $Task_Category,
      $Proj_ID, $Task_ID );
    $self->{dbh}->do(
      "INSERT INTO ACT_Task_Audit ( Proj_ID, Task_ID, Task_Status, TA_Date, Part_ID, TA_Desc )
       VALUES ( ?, ?, ?, CURRENT_DATE, ?, ? )", {},
      $Proj_ID, $Task_ID, $Task_Status, $self->{auth}->{Part_ID}, $TA_Desc );
    $self->Return;
  }
  return $self->error("Unknown action");
}

sub SELECT {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  $self->Return if $form->submitted eq 'Back';
}

sub DELETE {}

# Called before form has ever been submitted to retrieve values from the database
sub InitFields {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  my $Proj_ID = $form->field('Proj_ID');
  my $Task_ID = $form->field('Task_ID');
  my ( $Task_Status, $Task_Category, $Task_Action, $Task_Desc,
    $Milestone, $Milestone_Delta, $Task_Date_Completed, $Task_Date ) =
    $self->{dbh}->selectrow_array(
      "SELECT Task_Status, Task_Category, Task_Action, Task_Desc,
        Milestone, Milestone_Delta, Task_Date_Completed,
        DATE_ADD(Milestone_Date, INTERVAL Milestone_Delta DAY) AS Task_Date
      FROM ACT_Task LEFT JOIN ACT_Task_Milestone
        ON ACT_Task.Milestone_ID = ACT_Task_Milestone.Milestone_ID
        AND ACT_Task.Proj_ID = ACT_Task_Milestone.Proj_ID
      WHERE ACT_Task.Proj_ID = ? AND Task_ID = ?",
      {}, $Proj_ID, $Task_ID );
  $Task_Status ||= 'To Do';
  if ($Task_Status eq 'Ongoing') {
    $Task_Date = 'Ongoing';
    $Milestone = '';
    $Milestone_Delta = '';
  } else {
    $Milestone_Delta = "+$Milestone_Delta" if $Milestone_Delta >= 0;
  }
  $form->field( name => 'Task_Status', value => $Task_Status, force => 1 );
  $form->field( name => 'Task_Category', value => $Task_Category, force => 1 );
  $form->field( name => 'Task_Action', value => $Task_Action, force => 1 );
  $form->field( name => 'Task_Desc', value => $Task_Desc, force => 1 );
  $form->field( name => 'Milestone', value => $Milestone, force => 1, static => 'static' );
  $form->field( name => 'Milestone_Delta', value => $Milestone_Delta, force => 1, static => 'static' );
  $form->field( name => 'Task_Date', value => $Task_Date, force => 1, static => 'static' );
  $form->field( name => 'Task_Date_Completed', value => $Task_Date_Completed, force => 1 );
}

# Called after INSERT, UPDATE, SELECT, DELETE or InitFields
sub InitParams {
  my ( $self ) = @_;
  my $form = $self->{form};
  my $Proj_ID = $form->field('Proj_ID');
  my $Task_ID = $form->field('Task_ID');
  my ( $Who ) = $self->{dbh}->selectrow_array(
    "SELECT GROUP_CONCAT(Delegate SEPARATOR ' ') FROM (
    SELECT CONCAT('<b>', Task_Del_Role, ':</b>', GROUP_CONCAT(Role_Abbr) ) AS Delegate
    FROM ACT_Task_Delegate JOIN ACT_Task_Abbr ON ACT_Task_Delegate.Abbr_ID = ACT_Task_Abbr.Abbr_ID
    WHERE ACT_Task_Delegate.Proj_ID = ? AND ACT_Task_Delegate.Task_ID = ?
    GROUP BY Task_Del_Role ) AS WhoRole", {}, $Proj_ID, $Task_ID );
  $form->tmpl_param( Who => $Who );
  my $Audit_Trail = $self->map_cols(
    [ qw(Date Who Status Desc) ],
    $self->{dbh}->selectall_arrayref(
      "SELECT TA_Date, CONCAT(FirstName, ' ', LastName) AS Name, Task_Status, TA_Desc
       FROM ACT_Task_Audit NATURAL JOIN ACT_Participant
       WHERE Proj_ID = ? AND Task_ID = ?
       ORDER BY TA_Date, TA_ID", {}, $Proj_ID, $Task_ID ));
  $form->tmpl_param( Audit_Trail => $Audit_Trail );
}
1;
