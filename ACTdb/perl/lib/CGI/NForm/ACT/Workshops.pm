package CGI::NForm::ACT::Workshops;
use CGI;
use CGI::FormBuilder;

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    { '' =>   'SELECT'
    };
}

# TODO:
#  Generate the age guide
#  Do this on the web. Add links to update workshop def
#  Add Role_Minimum to the database, Project edit
#  Add Production Team to the mix

sub get_form {
  my ( $self, $op, $params )  = @_;
  $self->Connect();
  my $q = $self->{q} = ref($params) eq 'CGI' ? $params : new CGI($params);
  my $form = CGI::FormBuilder->new(
    action => "$ENV{SCRIPT_NAME}$self->{path_info}",
    method => 'POST',
    params => $q,
    template => "$self->{tmpldir}/Workshops.html",
    title => "ACT Workshops",
    submit => [ "Submit", "Back" ]
  );
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'Show_Register', options =>
  'Show Register' );
  $form->field( name => 'Active_Only', type => 'checkbox', options =>
  'Active Only' );
  return $form;
}

sub authorize {
  my ( $self, $op ) = @_;
  return 1 if $self->check_key(EU=>1) &&
    $self->Has_Privilege(0, 'ProjectAdmin', 1);
  $self->fatal( "Access to workshops page requires ProjectAdmin read privileges");
  #  $self->has_role( 1, 'Board' );
  # $self->fatal( "Access restricted to ACT Board" );
}

sub INSERT {}

sub UPDATE {}

sub DELETE {}

sub SELECT {
  my $self = shift;
  my $form = $self->{form};
  $self->Return if $form->submitted eq 'Back';
  unless ( $form->submitted eq 'Submit' ) {
    my ( $submit ) = grep m/^[A-Z][A-Z]\d+$/, $form->cgi_param();
    my $cmd = 'XX';
    my $id = 0;
    if ( $submit && $submit =~ m/^([A-Z][A-Z])(\d+)$/ ) {
      $cmd= $1;
      $id = $2;
    }
    my $Proj_ID = $self->{dbh}->selectrow_array(
      'SELECT Proj_ID FROM ACT_Role WHERE Role_ID = ?', {}, $id );
    if ( $cmd eq 'ED' ) {
      $self->Call( '/Project/edit', Proj_ID => $Proj_ID );
    } elsif ( $cmd eq 'SC' ) {
      $self->Call( '/Schedule', Proj_ID => $Proj_ID );
    }
  }
  return 0;
}


sub InitFields {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  ## This is an example of how to initialize a checkbox as checked
  ## *not* easy:
  # $form->field( name => 'Show_Register', checked => 'checked', force
  # => 1, value => 'Show Register' );
}

sub InitParams {
  my ( $self, $op ) = @_;
  my $dbh = $self->{dbh};
  my $form = $self->{form};
  my $Show_Register = $form->field('Show_Register');
  my $Show_Edit = $Show_Register ? 0 : 1;
  my $Active_Only = $form->field('Active_Only');

  # Select active or preview workshops
  # Collect
  #  Proj_Name
  #  Role_ID (of Registration)
  #  Proj_Ages Bracket
  #  Proj_Meetings
  #  Proj_Rehearsals
  #  Proj_Performances
  #  Role_Fee (of Registration)

  my $ws = $dbh->selectall_arrayref(
    "SELECT Proj_Name, Role_ID, Proj_Ages, Proj_Desc, Proj_Meetings, Proj_Rehearsals,
     Proj_Performances, Role_Fee, Proj_Leader, Proj_LdrTitle, Role_Limit, Role_Min,
     ACT_Venue.Venue_ID, Venue_Long
    FROM ACT_Project NATURAL JOIN ACT_Role
      LEFT JOIN ACT_Event ON ACT_Project.Proj_ID = ACT_Event.Proj_ID
        AND ACT_Event.Venue_ID != 0
      NATURAL LEFT JOIN ACT_Venue
    WHERE Proj_Type = 'Workshop' AND Role_Name = 'Registration'" .
    ( $Active_Only ? " AND Proj_Status = 'Active'" :
     " AND Proj_Status != 'Archive'" ) .
     " GROUP BY ACT_Project.Proj_ID" );

  $ws = $self->map_cols(
    [ qw(Proj_Name Role_ID Proj_Ages Proj_Desc Proj_Meetings Proj_Rehearsals
	 Proj_Performances Role_Fee Proj_Leader Proj_LdrTitle Role_Limit Role_Min
	 Venue_ID Venue_Long) ],
    $ws );
  $form->tmpl_param( 'Workshops' => $ws );

  my %Age_Brackets;

  for my $ww ( @$ws ) {
    my $ages = $ww->{Proj_Ages};
    $ages =~ s/^\s+//;
    $ages =~ s/\s+$//;
    $ages =~ s/\s+to\s+/-/;
    $ages =~ s/\s*-\s*/&ndash;/;
    $ww->{Proj_Ages} = $ages;
    if ( $ages =~ s/^(\d+&ndash;\d+)(?:.*)?$/$1/ ) {
      $Age_Brackets{$ages} ||= [];
      push @{$Age_Brackets{$ages}},
	{ Role_ID => $ww->{Role_ID}, Proj_Name => $ww->{Proj_Name} };
    }
    # Eliminate Bio links with move to new website 8/3/19
    # $ww->{Proj_Leader} =~
    #   s|<([^>]*) #([^>]*)>|<a href="http://act.arlington.ma.us/workshops/leaders.html#$2">$1 $2</a>|g;
    $ww->{Proj_Leader} =~
      s|<([^>]*) #([^>]*)>|$1 $2|g;
    $ww->{Show_Edit} = $Show_Edit;
  }
  
  my @Brackets =
    map { { Proj_Ages => $_->[0], Links => $Age_Brackets{$_->[0]} } }
    sort { $a->[1] <=> $b->[1] || $a->[2] <=> $b->[2] || $a cmp $b }
    map { $_ =~ m/^(\d+)&ndash;(\d+)/; [ $_, $1 || 0, $2 || 0 ] }
    keys %Age_Brackets;
  $form->tmpl_param( Age_Brackets => \@Brackets );
  $form->tmpl_param( Show_Edit => $Show_Edit );
}
1;
