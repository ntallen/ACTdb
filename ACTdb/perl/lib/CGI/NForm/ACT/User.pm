package CGI::NForm::ACT::User;
use strict;
use CGI;
use CGI::FormBuilder;
use Digest::MD5;

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    { # rm  =>  'DELETE',
      new  => 'INSERT',
      edit => 'UPDATE',
      '' =>   'INSERT'
    };
}

# Support for the $params argument is optional, but necessary
# if you want to allow this form to be redirected.
# With FormBuilder 3.0, this functionality can be moved back
# to NForm and get_form() will be responsible only for specifying
# the fields and the template.
sub get_form {
  my ( $self, $op, $params )  = @_;
  $self->Connect();
  my $q = $self->{q} = ref($params) eq 'CGI' ? $params : new CGI($params);
  my $form = CGI::FormBuilder->new(
    action => "$ENV{SCRIPT_NAME}$self->{path_info}",
    method => 'POST',
    fields => [ 'Username', 'Password', 'Confirm',
		'Key', 'nxtPI', 'nxtPA', 'Part_ID' ],
    params => $q,
    template => "$self->{tmpldir}/User.html",
    title => "ACT User Update"
  );
  $form->field( name => 'Password', type => 'password' );
  $form->field( name => 'Confirm', type => 'password' );
  $form->field( name => 'Key', type => 'hidden' );
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'Part_ID', type => 'hidden' );
  return $form;
}

sub authorize {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  return 0 unless $self->check_key( EU => 1 );
  my $Part_ID = $self->Part_ID;
  if ( $Part_ID != $self->{auth}->{Part_ID} && ! $self->Has_Privilege( -1, "ContactInfo", 2 ) ) {
    return $self->error('Not authorized to make changes to another user');
  }
  return 1;
}

sub INSERT {
  shift->do_user( 1 );
}

sub UPDATE {
  shift->do_user( 0 );
}

sub do_user {
  my ( $self, $new ) = @_;
  my $form = $self->{form};
  my $Part_ID = $self->Part_ID;
  if ( $form->submitted eq 'Submit' ) {
    my $Username = $form->field('Username');
    my $Password = $form->field('Password');
    my $Confirm = $form->field('Confirm');
    # check length and format of username, password
    # check that Password and Confirm are both set and equal
    eval {
      if ( $new ) {
	$self->{dbh}->do(
	  'INSERT INTO ACT_Users
	   ( Username, Password, Part_ID )
	   VALUES ( ?, MD5(?), ? );', {},
	  $Username, $Password, $Part_ID );
      } else {
	$self->{dbh}->do(
	  'UPDATE ACT_Users
	   SET Username = ?, Password = MD5(?)
	   WHERE Part_ID = ?;', {},
	  $Username, $Password, $Part_ID );
      }
    };
    if ( $@ ) {
      if ( $@ =~ /Duplicate entry/ ) {
	push @{$self->{errors}},
	  "That Username is already in use. Please select another";
	return 0;
      } else {
	warn "Error from mysql during User/",
	      $new ? "INSERT" : "UPDATE",
	      ": $@\n";
	$self->fatal(
	  "An error occurred while ",
	  $new ? "creating the new user.\n" :
		 "updating the user record\n",
	  "Please contact the webmaster.\n" );
      }
    } else {
      if ( $new ) {
	$self->Log( "User $Username Created" );
      } else {
	$self->Log( "User $Username Updated" );
      }
    }
  }
  my $Fam_ID = $self->{dbh}->selectrow_array(
    'SELECT Fam_ID FROM ACT_Participant WHERE Part_ID = ?', {}, $Part_ID );
  # $self->GoTo( "/Family/edit", Fam_ID => $Fam_ID );
  $self->Return;
}

sub DELETE {
}

sub SELECT {
}

sub InitFields {
  my ( $self, $op ) = @_;
  if ( $op eq 'UPDATE' ) {
    my $Username =
      $self->{dbh}->selectrow_array(
	'SELECT Username FROM ACT_Users
	 WHERE Part_ID = ?;', {}, $self->{auth}->{Part_ID} );
    if ( $Username ) {
      $self->{form}->field( name => 'Username',
	value => $Username );
    } else {
      $self->fatal( "Invalid Request: No such user" );
    }
  }
}

1;
