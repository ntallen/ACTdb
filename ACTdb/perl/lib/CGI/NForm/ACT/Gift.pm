package CGI::NForm::ACT::Gift;
use strict;
use CGI;
use CGI::FormBuilder;

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    { rm  =>  'DELETE',
      edit => 'UPDATE',
      new => 'INSERT',
      '' =>   'SELECT'
    };
}

# Support for the $params argument is optional, but necessary
# if you want to allow this form to be redirected.
# With FormBuilder 3.0, this functionality can be moved back
# to NForm and get_form() will be responsible only for specifying
# the fields and the template.
sub get_form {
  my ( $self, $op, $params )  = @_;
  $self->Connect();
  my $q = $self->{q} = ref($params) eq 'CGI' ? $params : new CGI($params);
  my %submit_lbl = (
    SELECT => '',
    INSERT => 'Submit',
    UPDATE => 'Update',
    DELETE => 'Delete'
  );
  my %title_lbl = (
    SELECT => '',
    INSERT => 'New',
    UPDATE => 'Update',
    DELETE => 'Delete'
  );
  my $static = $op eq 'DELETE' ? 1 : 0;
  my $form = CGI::FormBuilder->new(
    action => "$ENV{SCRIPT_NAME}$self->{path_info}",
    method => 'POST',
    fields => [ qw(nxtPI nxtPA Part_ID Fam_ID
      Gift_ID Gift_Amount Gift_Attribution
      Gift_Date Gift_ExpDate Gift_Type Gift_Memo) ],
    required => [ qw(Gift_Amount Gift_Attribution Gift_Date
		  Gift_ExpDate Gift_Type) ],
    params => $q,
    template => "$self->{tmpldir}/Gift.html",
    title => "$title_lbl{$op} Gift to ACT",
    submit => [ grep $_, $submit_lbl{$op}, "Back" ],
    stylesheet => 1,
	jsfunc => <<'EOJS'
	  if (form._submit.value == 'Back') {
		return true;
	  }
EOJS
  );
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'Fam_ID', type => 'hidden' );
  $form->field( name => 'Gift_ID', type => 'hidden' );
  $form->field( name => 'Gift_Amount', validate =>
    '/^\$?\d+(\.\d\d)?$/', default => 0, static => $static );
  $form->field( name => 'Gift_Date', validate =>
    '/^\d\d\d\d[-\/]\d\d?[-\/]\d\d?$/', static => $static );
  $form->field( name => 'Gift_ExpDate', validate =>
    '/^\d\d\d\d[-\/]\d\d?[-\/]\d\d?$/', static => $static );
  $form->field( name => 'Gift_Attribution', static => $static );
  $form->field( name => 'Gift_Type', type => 'select',
    options => [ qw(Cash Board) ], static => $static );
  $self->fatal( "No Fam_ID specified" ) unless $form->field('Fam_ID');
  return $form;
}

sub authorize {
  my ( $self, $op ) = @_;
  if ( $self->check_key(EU=>1) ) {
    return 1 if $self->has_role( 1,'Treasurer') ||
		$self->has_role( 1,'DBadmin');
    $self->fatal( "Access restricted to ACT Treasurers" );
  }
  return 0;
}

sub INSERT {
  my $self = shift;
  my $form = $self->{form};
  if ( $form->submitted eq 'Back' ) {
    $self->Return;
  } elsif ( $self->{form}->validate ) {
    my @kws = qw(Fam_ID Gift_Amount Gift_Attribution
      Gift_Date Gift_ExpDate Gift_Type Gift_Memo);
    my %vals = map { ( $_ => $form->field($_) ) } @kws;
	$vals{Gift_Amount} =~ s/^\$//;
    $self->{dbh}->do(
      "INSERT INTO ACT_Gift ( " .
      join( ", ", @kws ) . " ) VALUES ( " .
      join( ", ", map '?', @kws ) . " )", {},
      map $vals{$_}, @kws );
    $self->Return;
  } else {
    $self->error('Validation error');
  }
  return 0;
}

sub UPDATE {
  my $self = shift;
  my $form = $self->{form};
  if ( $form->submitted eq 'Back' ) {
    $self->Return;
  } elsif ( $self->{form}->validate ) {
    my $Gift_ID = $form->field('Gift_ID');
    my @kws = qw(Gift_Amount Gift_Attribution
      Gift_Date Gift_ExpDate Gift_Type Gift_Memo);
    my %vals = map { ( $_ => $form->field($_) ) } @kws;
	$vals{Gift_Amount} =~ s/^\$//;
    $self->{dbh}->do(
      "UPDATE ACT_Gift SET " .
      join(', ', map "$_ = ?", @kws ) .
      " WHERE Gift_ID = ?", {},
	  map( $vals{$_}, @kws ), $Gift_ID );
    $self->Return;
  } else {
    $self->error('Validation error');
  }
  return 0;
}

sub DELETE {
  my $self = shift;
  my $form = $self->{form};
  if ( $form->submitted eq 'Back' ) {
    $self->Return;
  } elsif ( $self->{form}->validate ) {
    my $Gift_ID = $form->field('Gift_ID');
    $self->{dbh}->do(
      "DELETE FROM ACT_Gift WHERE Gift_ID = ?", {}, $Gift_ID );
    $self->Return;
  }
  return 0;
}

sub SELECT {
  return 1;
}

sub InitFields {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  my $Fam_ID = $form->field('Fam_ID');
  my $Gift_ID = $form->field('Gift_ID');
  if ( $op eq 'INSERT' ) {
    my $attr;
    if ( $Gift_ID ) {
      ( $attr ) = $self->{dbh}->selectrow_array(
        "SELECT Gift_Attribution FROM ACT_Gift WHERE Gift_ID = ?",
	{}, $Gift_ID );
    } else {
      my @attr;
      my $names = $self->{dbh}->selectall_arrayref(
	"SELECT FirstName, LastName FROM ACT_Participant
	WHERE Fam_ID = ? AND Status = ?
	ORDER BY LastName, FirstName", {}, $Fam_ID, 'Adult' );
      my %names;
      for ( @$names ) {
	$names{$_->[1]} ||= [];
	push( @{$names{$_->[1]}}, $_->[0] );
      }
      for my $lname ( sort keys %names ) {
	push @attr, join(" and ", @{$names{$lname}} ) . ' ' . $lname;
      }
      $attr = join " and ", @attr;
    }
    $form->field( name => "Gift_Attribution", value => $attr,
      force => 1 );
    my @dates = $self->{dbh}->selectrow_array(
      "SELECT CURDATE(),
      CONCAT(YEAR(CURDATE())+IF(MONTH(CURDATE())<6,0,1), '-08-15')" );
    $form->field( name => "Gift_Date", value => $dates[0],
	    force => 1 );
    $form->field( name => "Gift_ExpDate", value => $dates[1],
	    force => 1 );
  } elsif ( $Gift_ID ) {
    my @kws = qw(Gift_Amount Gift_Attribution
      Gift_Date Gift_ExpDate Gift_Type Gift_Memo);
    my %vals;
    my $query = "SELECT " .
      join( ", ", @kws ) .
      " FROM ACT_Gift WHERE Gift_ID = ?";
    my @vals = $self->{dbh}->selectrow_array(
      $query, {}, $Gift_ID );
    for my $kw ( @kws ) {
      $form->field( name => $kw, value => shift @vals, force => 1 );
    }
  } else {
    $self->fatal('No Gift_ID specified');
  }
}

1;
