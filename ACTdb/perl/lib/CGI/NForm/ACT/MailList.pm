package CGI::NForm::ACT::MailList;
use strict;
use CGI;
use CGI::FormBuilder;

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    { # rm  =>  'DELETE',
      '' =>   'SELECT'
    };
}

# Support for the $params argument is optional, but necessary
# if you want to allow this form to be redirected.
# With FormBuilder 3.0, this functionality can be moved back
# to NForm and get_form() will be responsible only for specifying
# the fields and the template.
sub get_form {
  my ( $self, $op, $params )  = @_;
  $self->Connect();
  my $q = $self->{q} = ref($params) eq 'CGI' ? $params : new CGI($params);
  my $form = CGI::FormBuilder->new(
    action => "$ENV{SCRIPT_NAME}$self->{path_info}",
    method => 'POST',
    fields => [ 'nxtPI', 'nxtPA' ],
    params => $q,
    template => "$self->{tmpldir}/MailList.html",
    title => "ACT Mailing List",
    submit => "Back"
  );
  $form->field( name=>'nxtPI', type=>'hidden');
  $form->field( name=>'nxtPA', type=>'hidden');
  return $form;
}

sub authorize {
  my ( $self, $op ) = @_;
  if ( $self->check_key ) {
    return 1 if $self->has_role( 1, 'Board');
    $self->fatal(
      "Access to mailing list is restricted to members\n" .
      "of the board." );
  }
  return 0;
}

sub INSERT {
}

sub UPDATE {
}

sub DELETE {
}

sub SELECT {
  my $self = shift;
  $self->Return;
  return 1;
}

sub InitFields {
  my ( $self, $op ) = @_;
  my $list = $self->{dbh}->selectall_arrayref(
    'SELECT FamilyName, Address1, City, State, Zip
     FROM ACT_Address AS A, ACT_Family AS F
     WHERE A.Fam_ID = F.Fam_ID
     ORDER BY FamilyName;' );
  $self->{form}->tmpl_param( maillist =>
    map_cols( [ qw(Name Street City State Zip) ], $list ) );
}

sub map_cols {
  my ( $labels, $aa ) = @_;
  my $ah = [ map {
      my $row = $_;
      my $ref = { map( ( $_ => shift @$row ), @$labels ) };
    } @$aa ];
}

1;
