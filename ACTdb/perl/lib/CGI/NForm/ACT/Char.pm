package CGI::NForm::ACT::Char;
use strict;
use CGI;
use CGI::FormBuilder;

# Char.pm provides Create/Edit/Delete for individual Char.
# Linked from Production.pm

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    {
      'edit' => 'UPDATE',
      'delete' => 'DELETE'
    };
}

my %submit = (
  INSERT => 'Create',
  UPDATE => '',
  DELETE => 'Delete'
);
my %static = ( SELECT => 1, INSERT => 0, UPDATE => 0, DELETE => 1 );

# Support for the $params argument is optional, but necessary
# if you want to allow this form to be redirected.
# With FormBuilder 3.0, this functionality can be moved back
# to NForm and get_form() will be responsible only for specifying
# the fields and the template.
sub get_form {
  my ( $self, $op, $params )  = @_;
  $self->Connect();
  my $q = $self->{q} = ref($params) eq 'CGI' ? $params : new CGI($params);
  my $form = CGI::FormBuilder->new(
    action => "$ENV{SCRIPT_NAME}$self->{path_info}",
    method => 'POST',
    fields => [ qw(nxtPI nxtPA Proj_ID Role_ID) ],
    params => $q,
    template => "$self->{tmpldir}/Char.html",
    title => "Character Definitions",
    reset => 0
  );
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'Proj_ID', type => 'hidden' );
  $form->field( name => 'Role_ID', type => 'hidden' );
  $form->field( name => 'Role_Name', static => $static{$op} );
  $form->submit( $submit{$op} ? [ $submit{$op}, "Back" ] : [ "Back" ] );  

  my $Proj_ID = $form->field('Proj_ID') ||
    $self->fatal("No Proj_ID Specified");
  my $Role_ID = $form->field('Role_ID') ||
    $self->fatal("No Role_ID Specified");
  my $Proj_Name = $self->{dbh}->selectrow_array(
    'SELECT Proj_Name FROM ACT_Project WHERE Proj_ID = ?', {}, $Proj_ID );
  $form->title( "Character Definition for $Proj_Name" );

  my $n_casts = $self->{dbh}->selectrow_array(
    'SELECT N_Casts FROM ACT_Project WHERE Proj_ID = ?', {}, $Proj_ID );
  my $doublecast = $n_casts > 1;
  $self->{tmp}->{doublecast} = $doublecast;
  
  if ( $doublecast ) {
    my $casts = $self->{dbh}->selectall_arrayref(
      'SELECT Cast_ID, Cast_Name FROM ACT_Casts ORDER BY Cast_ID' );
    for ( @$casts ) {
      $_->[1] = $_->[0] ? "the $_->[1] cast" : "both casts";
    }
    $form->field( name => 'Cast_ID', options => $casts, type => 'select', value => 0 );
  }
  
  my $Is_Also_IDs = $self->{dbh}->selectall_arrayref(
    'SELECT Role_ID, Role_Name
     FROM ACT_Role
     WHERE Proj_ID = ? AND Role_ID != ? AND Role_Type = ?', {},
     $Proj_ID, $Role_ID, 'Character' );
  $form->field( name => 'Is_Also_ID', options => $Is_Also_IDs, type => 'select' );

  return $form;
}

# Must be a Project Registrar
sub authorize {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  my $Proj_ID = $form->field('Proj_ID');
  return 1 if $self->check_key(EU=>1) &&
    $self->Has_Privilege( $Proj_ID, 'Cast', 2 );
  return 0;
}

sub INSERT {}

sub UPDATE {
  my $self = shift @_;
  my $form = $self->{form};
  $self->Return if $form->submitted eq 'Back';
  my $Role_ID = $form->field('Role_ID');
  my $Proj_ID = $form->field('Proj_ID');
  if ( $form->submitted eq 'Update Name' ) {
    # Update Role_Name
    my $Role_Name = $form->field('Role_Name');
    return $self->error( "Invalid name" ) unless $Role_Name;
    # Check to make sure new name isn't already in use
    $self->{dbh}->do( 'LOCK TABLES ACT_Role WRITE' );
    my ( $new_ID ) = $self->{dbh}->selectrow_array(
      'SELECT Role_ID FROM ACT_Role
       WHERE Proj_ID = ? AND Role_Name = ?
       AND Role_Type = ?', {}, $Proj_ID, $Role_Name, 'Character' );
    if ( $new_ID ) {
      $self->{dbh}->do( 'UNLOCK TABLES' );
      $self->Return if $new_ID == $Role_ID;
      return $self->error( 'Name already in use' );
    } else {
      $self->{dbh}->do(
        'UPDATE ACT_Role SET Role_Name = ? WHERE Role_ID = ?', {},
	$Role_Name, $Role_ID );
      $self->{dbh}->do( 'UNLOCK TABLES' );
      $self->Return;
    }
  } elsif ( $form->submitted eq "Add 'Is Also'" ) {
    my $Cast_ID = $self->{tmp}->{doublecast} ? $form->field('Cast_ID') : 0;
    my $Is_Also_ID = $form->field('Is_Also_ID');
    return $self->error("Please select a role") unless $Is_Also_ID;
    $self->{dbh}->do(
      'INSERT INTO ACT_Is_Also ( Role_ID, Cast_ID, Is_Also_ID )
       VALUES ( ?, ?, ? )', {}, $Role_ID, $Cast_ID, $Is_Also_ID );
    $self->GoTo( '/Char/edit', Proj_ID => $Proj_ID, Role_ID => $Role_ID );
  } else {
    my ( $submit ) = grep m/^D\d+_\d+$/, $form->cgi_param();
    if ( $submit && $submit =~ m/^D(\d+)_(\d+)$/ ) {
      my $Cast_ID = $1;
      my $Is_Also_ID = $2;
      $self->{dbh}->do(
        'DELETE FROM ACT_Is_Also
	 WHERE Role_ID = ? AND Cast_ID = ? AND Is_Also_ID = ?', {},
	 $Role_ID, $Cast_ID, $Is_Also_ID );
      $self->GoTo( '/Char/edit', Proj_ID => $Proj_ID, Role_ID => $Role_ID );
    }
  }
  return $self->error('Not implemented');
}

sub DELETE {
  my $self = shift @_;
  my $form = $self->{form};
  $self->Return if $form->submitted eq 'Back';
  my $Role_ID = $form->field('Role_ID');
  $self->{dbh}->do(
    'DELETE FROM ACT_Grp_Mem WHERE Role_ID = ?', {}, $Role_ID );
  $self->{dbh}->do(
    'DELETE FROM ACT_Cast WHERE Role_ID = ?', {}, $Role_ID );
  $self->{dbh}->do(
    'DELETE FROM ACT_Is_Also WHERE Role_ID = ? OR Is_Also_ID = ?', {}, $Role_ID, $Role_ID );
  $self->{dbh}->do(
    'DELETE FROM ACT_Role WHERE Role_ID = ?', {}, $Role_ID );
  $self->Return;
}

# For each listed scene/group, I'll offer Edit/Copy/Delete
sub SELECT {}

sub InitFields {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  my $Proj_ID = $form->field('Proj_ID');
  my $Role_ID = $form->field('Role_ID');
  my $Role_Name = $self->{dbh}->selectrow_array(
    'SELECT Role_Name FROM ACT_Role WHERE Role_ID = ?', {}, $Role_ID );
  $form->field( name => 'Role_Name', value => $Role_Name, force => 1 );
}

sub InitParams {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  my $Role_ID = $form->field('Role_ID');
  $form->tmpl_param( doublecast => $self->{tmp}->{doublecast} );
  my $also = $self->{dbh}->selectall_arrayref(
    'SELECT ACT_Is_Also.Cast_ID, Cast_Name, Role_Name, Is_Also_ID
     FROM ACT_Is_Also NATURAL JOIN ACT_Casts, ACT_Role
     WHERE ACT_Is_Also.Role_ID = ?
     AND Is_Also_ID = ACT_Role.Role_ID', {}, $Role_ID );
  my $Also = [];
  my $hdr = 'Is Also';
  for ( @$also ) {
    push @$Also, { hdr => $hdr, name => $_->[2], cast => $_->[0] ? "the $_->[1] cast" : "both casts",
	    doublecast => $self->{tmp}->{doublecast}, Cast_ID => $_->[0], Is_Also_ID => $_->[3], Edit => ! $static{$op} };
  }
  $form->tmpl_param( Is_Also => $Also );
  $form->tmpl_param( Edit => ! $static{$op} );
}

1;
