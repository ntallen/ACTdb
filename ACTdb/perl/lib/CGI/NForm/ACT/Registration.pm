package CGI::NForm::ACT::Registration;
use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use CGI::FormBuilder;

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    { 
      '' =>   'SELECT'
    };
}

# Support for the $params argument is optional, but necessary
# if you want to allow this form to be redirected.
# With FormBuilder 3.0, this functionality can be moved back
# to NForm and get_form() will be responsible only for specifying
# the fields and the template.
sub get_form {
  my ( $self, $op, $params )  = @_;
  $self->Connect();
  my $q = $self->{q} = ref($params) eq 'CGI' ? $params : new CGI($params);
  my $form = CGI::FormBuilder->new(
    action => "$ENV{SCRIPT_NAME}$self->{path_info}",
    method => 'POST',
    fields => [ qw(nxtPI nxtPA Proj_ID) ],
    params => $q,
    template => {
      type => 'HTML',
      filename => "$self->{tmpldir}/Registration.html",
      global_vars => 1 },
    title => "Registration List",
    submit => [ "Back" ]
  );
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'Proj_ID', type => 'hidden' );
  return $form;
}

sub authorize {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  my $Proj_ID = $form->field('Proj_ID') ||
    $self->fatal("No Proj_ID Specified");
  my $Role_ID = $self->{dbh}->selectrow_array(
    'SELECT Role_ID FROM ACT_Role WHERE
     Proj_ID = ? AND Role_Name = ?', {},
     $Proj_ID, 'Registration' );
  $self->fatal("No Registration Role found for specified project")
    unless $Role_ID;
  $self->{tmp}->{Role_ID} = $Role_ID;
  if ( $self->check_key(EU => 1) ) {
    my $role_priv = $self->Get_Privilege_Value( $Proj_ID, 'Registration' );
    my $is_admin = $role_priv >= 2;
    $form->tmpl_param( is_admin => $is_admin );
    $self->{reg_is_admin} = $is_admin;
    return 1 if $role_priv > 0;
    $self->error( "Not authorized to view project registration" );
  }
  return 0;
}

sub INSERT {
}

sub UPDATE {
  return 0;
}

sub DELETE {
}

sub SELECT {
  my $self = shift;
  my $form = $self->{form};
  if ( $form->submitted eq 'Back' ) {
    $self->Return;
  }
  my ( $submit ) = grep m/^[A-Z][A-Z]\d+$/, $form->cgi_param();
  if ( $submit && $submit =~ m/^([A-Z][A-Z])(\d+)$/ ) {
    my $cmd= $1;
    my $id = $2;
    my $Role_ID = $self->{tmp}->{Role_ID};
    if ( $cmd eq 'AC' ) {
      $self->Call( '/UnWait', Part_ID => $id,
        Cast_Status => 'Accepted', Role_ID => $Role_ID );
    } elsif ( $cmd eq 'WL' ) {
      $self->Call( '/UnWait', Part_ID => $id,
        Cast_Status => 'Waitlisted', Role_ID => $Role_ID );
    } elsif ( $cmd eq 'WD' ) {
      $self->Call( '/UnWait', Part_ID => $id,
        Cast_Status => 'Withdrawn', Role_ID => $Role_ID );
    } elsif ( $cmd eq 'CT' ) {
      $self->Call( '/Home', Part_ID => $id );
    } else {
      $self->error( "Unrecognized command: '$cmd'" );
    }
  } else {
    $self->error( "No match" );
  }
}

sub InitFields {
}

sub InitParams {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  
  my $Role_ID = $self->{tmp}->{Role_ID} ||
    $self->fatal("No Role_ID Specified");

  # initialize titles
  my ( $Proj_Type, $Proj_Name, $Proj_ID, $Proj_Start ) = $self->{dbh}->selectrow_array(
    'SELECT Proj_Type, Proj_Name, ACT_Role.Proj_ID, Proj_Start
     FROM ACT_Role NATURAL JOIN ACT_Project
     WHERE Role_ID = ?', {}, $Role_ID );
  $form->tmpl_param( Proj_Type => $Proj_Type );
  $form->tmpl_param( Proj_Name => $Proj_Name );
    
  # initialize registration table
  my $ages = $self->{dbh}->selectall_arrayref(
    'SELECT IF(Birthdate>0,
        YEAR(Proj_Start)-YEAR(Birthdate)-(MID(Proj_Start,6,5)<MID(Birthdate,6,5)),
        -1) AS Age,
      Cast_Status, COUNT(FirstName)
    FROM ACT_Project NATURAL JOIN ACT_Role NATURAL JOIN ACT_Cast
      NATURAL JOIN ACT_Participant
    WHERE ACT_Project.Proj_ID = ? AND ACT_Role.Role_ID = ?
    GROUP BY Age, Cast_Status', {}, $Proj_ID, $Role_ID );
  my %ages;
  my %statuses;
  my %counts;
  for my $row ( @$ages ) {
    my ( $age, $status, $count ) = @$row;
    $ages{$age} = 1;
    $statuses{$status} = 1;
    $counts{"$age.$status"} = $count;
  }
  my @ages = sort { $a <=> $b } keys %ages;
  my @statuses = sort keys %statuses;
  my $table =
    '<table class="form">' . "\n" .
    "<tr><th>Age as of<br>$Proj_Start:</th>" .
    join( '', map "<th>$_</th>", @ages, 'Total' ) .
    "</tr>\n";
  for my $status ( @statuses ) {
    my $total = 0;
    $table .= "<tr><th>$status:</th>";
    for my $age ( @ages ) {
      my $count = $counts{"$age.$status"} || '0';
      $total += $count;
      $table .= "<td>" . ( $count ) . "</td>";
    }
    $table .= "<td>$total</td></tr>\n";
  }
  $table .= "</table>\n";
  $form->tmpl_param( age_table => $table );

  # initialize registration list
  my $cast = $self->{dbh}->selectall_arrayref(
    "SELECT Cast_Status, C1.Part_ID, FirstName, LastName,
         IF( Cast_Time>0, DATE_FORMAT(Cast_Time,'%b %e, %k:%i:%s'), 'Board' ),
         IF(Birthdate>0,
        YEAR(Proj_Start)-YEAR(Birthdate)-(MID(Proj_Start,6,5)<MID(Birthdate,6,5)),
        -1) AS Age, ClassYear
    FROM ACT_Project NATURAL JOIN ACT_Role NATURAL JOIN
      ACT_Cast AS C1 NATURAL JOIN ACT_Participant
    WHERE ACT_Project.Proj_ID = ? AND ACT_Role.Role_ID = ?
    ORDER BY Cast_Status, Cast_Time", {}, $Proj_ID, $Role_ID );
  my %sts;
  my $loop = [];
  for my $row ( @$cast ) {
    my $status = shift @$row;
    unless ( $sts{$status} ) {
      $sts{$status} = [];
      push @$loop, { Cast_Status => $status, SLoop => $sts{$status} };
    }
    push @{$sts{$status}},
     { Part_ID => $row->[0],
       FirstName => $row->[1],
       LastName => $row->[2],
       Cast_Time => $row->[3],
       Age => $row->[4],
       Grade => $self->Grade($row->[5], substr($Proj_Start,0,4), substr($Proj_Start,5,2)) };
  }
  $form->tmpl_param( CLoop => $loop );
}

1;
