package CGI::NForm::ACT::Roles;
use strict;
use CGI;
use CGI::FormBuilder;

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    {
      '' => 'UPDATE'
    };
}

# Support at least 3 categories of Roles
# Committees for this project
#   Anyone with read permission 1 can list these
#   Anyone with write permission 2 can add or delete
# Categories for this project
#   Anyone with write permission 2 can list these
#   Anyone with write permission 3 can add or delete
# Global Committees: Board, 'Usual Suspects'
#   Anyone with write perms 2 can list these
#   These should not be modified at all from here.

# Can view this as 2 categories, Modifiable and Unmodifiable
# Whether the Categories go in one or the other depends on
# the level of write permission

sub InitForm {
  my ( $self, $form, $op )  = @_;
  $self->Connect();
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'Proj_ID', type => 'hidden' );
  $form->field( name => 'Part_ID1', multiple => 1 );
  $form->field( name => 'Part_ID2', multiple => 1 );
  $form->field( name => 'delete_users', type => 'checkbox',
    options => [ 'Delete selected users' ] );
  $form->field( name => 'FirstName', type => 'text' );
  $form->field( name => 'LastName', type => 'text' );

  # Role_ID defined to select target for additions
  # Only needs to be defined if we have $rolepriv >= 2

  # RoleIDs: checkbox-selectable list of roles
  # Needs to be defined unless print_friendly

  $form->template( "$self->{tmpldir}/Roles.html" );
}

sub authorize {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  my $Proj_ID = $form->field('Proj_ID');
  $self->fatal("Invalid or missing Proj_ID") if $Proj_ID !~ /^\d+$/;
  if ( $self->check_key ) {
    my $rolepriv = $self->Get_Privilege_Value( $Proj_ID, 'Roles' );
    my $isdbadmin = $self->Has_Privilege( 0, 'DBAdmin', 1 );
    # my $isdbadmin = 0;
    # my $rolepriv = 0;
    # $rolepriv = 1 if $self->has_role(1, 'Board');
    # if ( $self->has_role(1, 'DBadmin') ) {
    #   $rolepriv = 3;
    #   $isdbadmin = 1;
    # }

    $self->fatal("Not authorized for Role Operations") unless $rolepriv;
    $self->{tmp}->{rolepriv} = $rolepriv;

    $self->{tmp}->{isdbadmin} = $isdbadmin;
    $form->tmpl_param( isdbadmin => $isdbadmin );

    $self->define_RoleIDs;
    return 1;
  } else {
    $self->fatal("Authentication failed");
  }
}

sub INSERT {}

sub UPDATE {
  my $self = shift;
  my $form = $self->{form};
  $self->Return if $form->submitted eq 'Back';
  if ( $form->submitted eq 'DBAdmin' ) {
    my $FirstName = $form->field('FirstName');
    my $LastName = $form->field('LastName');
    $self->Call( "/Fixup", FirstName => $FirstName, LastName => $LastName,
      _submit => 'Search', _submitted => 1 );
  }
  if ( $form->submitted eq 'Edit Privileges' ) {
    my $Proj_ID = $form->field('Proj_ID');
    $self->Call( "/RolePrivs", Proj_ID => $Proj_ID );
  }
  if ( $form->submitted eq 'Printer-Friendly Version' ) {
    $form->tmpl_param( print_friendly => 1 );
    $self->{tmp}->{print_friendly} = 1;
    $form->field( name => 'delete_users', value => '0', force => 1 );
    return 0;
  }
  if ( $form->validate ) {
    if ( $form->submitted eq 'Confirm Delete' ) {
      my $delete_count = 0;
      my @Part_ID2 = $form->field('Part_ID2');
      return $self->error("Not authorized to delete users from other categories")
        if @Part_ID2;
      my @Part_ID1 = $form->field('Part_ID1');
      return $self->error("No users selected for deletion")
        unless @Part_ID1;
      return $self->error("Delete not confirmed")
        unless $form->field('delete_users');
      for my $RolePart ( @Part_ID1 ) {
        if ( $RolePart =~ m/^(\d+)_(\d+)$/ ) {
          $self->{dbh}->do(
            "DELETE FROM ACT_Cast WHERE Role_ID = ? AND Part_ID = ?",
            {}, $1, $2 );
          $delete_count++;
        } else {
          $self->error("Invalid Role_Part '$RolePart'");
        }
      }
      $self->confirm( "Deleted $delete_count entries" ) if $delete_count;
      $form->field( name => 'delete_users', value => '0', force => 1 );
      return 0;
    }
    $form->field( name => 'delete_users', value => '0', force => 1 );
    return 0 if $form->submitted eq 'Refresh';
    if ( $form->submitted eq 'Go' ) {
      my @Part_ID1 = $form->field('Part_ID1');
      my @Part_ID2 = $form->field('Part_ID2');
      my @Part_IDs = ( @Part_ID1, @Part_ID2 );
      return $self->error("No users selected") unless @Part_IDs;

      my $Role_ID = $form->field('Role_ID');
      return $self->error("Must select a committee to add users")
        unless $Role_ID;
      if ( $Role_ID !~ m/^\d+$/ ) {
        my $Proj_ID = $form->field('Proj_ID');
        $self->fatal("Invalid or missing Proj_ID") if $Proj_ID !~ /^\d+$/;
        $self->{dbh}->do(
          "INSERT IGNORE INTO ACT_Role ( Role_Name, Proj_ID, Role_Type, Role_Status )
           SELECT Role_Name, ?, Role_Type, 'Closed'
           FROM ACT_Priv_Roles WHERE Role_Name = ?", {},
           $Proj_ID, $Role_ID );
        my $newRole_ID = $self->{dbh}->selectrow_array(
          "SELECT Role_ID FROM ACT_Role WHERE Proj_ID = ? AND Role_Name = ?",
          {}, $Proj_ID, $Role_ID );
        return $self->error("Create role '$Role_ID' failed") unless $newRole_ID;
        $self->define_RoleIDs( $newRole_ID );
        $Role_ID = $newRole_ID;
      }
      for my $RolePart ( @Part_IDs ) {
        if ( $RolePart =~ m/^(?:\d+_)?(\d+)$/ ) {
          $self->{dbh}->do(
            "INSERT IGNORE INTO ACT_Cast ( Role_ID, Part_ID, Cast_Status )
             VALUES ( ?, ?, 'Accepted' )", {},
            $Role_ID, $1
          );
        } else {
          $self->error("Invalid Role_Part '$RolePart'");
        }
      }
      return 0;
    }
    if ( $form->submitted eq 'Search' ) {
      my $FirstName = $form->field('FirstName');
      my $LastName = $form->field('LastName');
      my @args;
      my $where = '';
      if ( $FirstName ) {
        $where .= "FirstName = ?";
        push @args, $FirstName;
      }
      if ( $LastName ) {
        $where .= " AND " if $where;
        $where .= "LastName = ?";
        push @args, $LastName;
      }
      return $self->error( "No pattern selected for search" ) unless $where;
      $form->tmpl_param( Searches => 1 );
      my $peeps = $self->{dbh}->selectall_arrayref(
        "SELECT CONCAT(FirstName, ' ', LastName) AS Name, Part_ID
         FROM ACT_Participant WHERE $where
         ORDER BY LastName, FirstName, Part_ID", {}, @args );
      if ( @$peeps ) {
        $form->tmpl_param( HasResults => 1 );
        # Results => Name, Age, Part_ID
        my @Results = map { { Name => $_->[0], Part_ID => $_->[1] } }
          @$peeps;
        $form->tmpl_param( Results => \@Results );
      }
      return 0;
    }
    if ( $form->submitted eq 'Submit' ) { # EditRoles response
      $self->EditRole_Fields;
      my $RolePart = $form->field('RolePart');
      my $Cast_Note = $form->field('Cast_Note');
      my $Cast_Order = $form->field('Cast_Order');
      my $Part_ID;
      my $Role_ID;
      if ( $RolePart =~ m/^(\d+)_(\d+)$/ ) {
        $Role_ID = $1;
        $Part_ID = $2;
      } else {
        $self->fatal("Invalid Role/Part");
      }
      if ( $Cast_Order =~ m/^(\d+)$/ ) {
        $Cast_Order = $1;
      } else {
        $self->error("Cast_Order must be numeric");
      }
      ### How do we check that user has permission to modify
      ### This Role?
      $self->{dbh}->do(
        "UPDATE ACT_Cast SET Cast_Order = ?, Cast_Note = ?
         WHERE Role_ID = ? AND Part_ID = ?", {},
         $Cast_Order, $Cast_Note, $Role_ID, $Part_ID );
      return 0;
      ### Should figure out a way to stay in this form on error
    }
    my ( $submit ) = grep m/^ID\d+_\d+$/, $form->cgi_param();
    if ( $submit && $submit =~ m/^ID(\d+)_(\d+)$/ ) {
      my $Role_ID= $1;
      my $Part_ID = $2;
      my $Role_Name = $self->{dbh}->selectrow_array(
        "SELECT Role_Name FROM ACT_Role WHERE Role_ID = ?", {},
        $Role_ID );
      my $Name = $self->{dbh}->selectrow_array(
        "SELECT CONCAT(FirstName, ' ', LastName) FROM ACT_Participant
         WHERE Part_ID = ?", {}, $Part_ID );
      my ( $Cast_Order, $Cast_Note ) = $self->{dbh}->selectrow_array(
        "SELECT Cast_Order, Cast_Note FROM ACT_Cast WHERE
         Role_ID = ? AND Part_ID = ? LIMIT 1", {},
         $Role_ID, $Part_ID );
      $self->fatal( "Role/Part not found in ACT_Cast" )
        unless defined($Cast_Order) && defined($Cast_Note);
      $form->tmpl_param( EditRole => 1 );
      $form->tmpl_param( Role_Name => $Role_Name );
      $form->tmpl_param( Name => $Name );
      $self->EditRole_Fields( $Role_ID, $Part_ID, $Cast_Order, $Cast_Note );
      return 0;
    }
    $self->error('Unknown Response');
  } else {
    $self->error('Form did not validate' );
  }
  return 0;
}

sub DELETE {}

sub SELECT {}

# Only called if form has not been submitted
sub InitFields {
  # my ( $self, $op ) = @_;
  # my $form = $self->{form};
}

# Template Parameters
#  Proj_Name
#  Proj_Type
#  modify - true if we have write permission
#  isdbadmin - true if we are DBAdmin
#  print_friendly - true to hide form stuff
#  Committees:loop {
#    Role_Name
#    People:loop {
#      Name
#      Cast_Note
#      Cast_Order
#      Part_ID
#      modify
#    }
#  }
#  Searches - true if search was requested
#  HasResults - true if search found something
#  Results:loop {
#    Name
#    Age/Adult
#    Part_ID
#  }
#  
sub InitParams {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  my $Proj_ID = $form->field('Proj_ID');

  my $rolepriv = $self->{tmp}->{rolepriv};
  my $modify = $rolepriv >= 2 ? 1 : 0;
  $modify = 0 if $self->{tmp}->{print_friendly};
  $form->tmpl_param( modify => 1 ) if $modify;
  
  ### New Privileges:
  $form->tmpl_param( privileges =>
    $self->Has_Privilege( 0, 'Privileges', 1 ) );
  # $form->tmpl_param( privileges => ($rolepriv >= 3) );
  
  my @Roles = $form->field('RoleIDs');

  $self->list_people( 'Committees', $modify, @Roles );
  @Roles = $form->field('OtherIDs');
  $self->list_people( 'Others', $modify, @Roles );
  
  my ( $Proj_Name, $Proj_Type ) = $self->{dbh}->selectrow_array(
    "SELECT Proj_Name, Proj_Type FROM ACT_Project WHERE Proj_ID = ?",
    {}, $Proj_ID );
  $form->tmpl_param( Proj_Name => $Proj_Name );
  $form->tmpl_param( Proj_Type => $Proj_Type );
  $form->title("Roles for $Proj_Type '$Proj_Name'");
}

sub list_people {
  my ( $self, $fieldname, $modify, @Roles ) = @_;
  return unless @Roles;
  my $form = $self->{form};
  my $Comms = [];
  my $people = $self->{dbh}->selectall_arrayref(
    "SELECT Role_Name, CONCAT(FirstName, ' ', LastName) AS Name, Cast_Note,
     Cast_Order, ACT_Cast.Part_ID, ACT_Role.Role_ID
     FROM ACT_Role NATURAL JOIN ACT_Cast NATURAL JOIN ACT_Participant
     WHERE ACT_Role.Role_ID IN ( " .
    join( ", ", @Roles ) .
     " ) AND Cast_Status = 'Accepted'
     GROUP BY Role_Name, ACT_Cast.Part_ID
     ORDER BY Role_Type DESC, Role_Name, Cast_Order DESC, LastName, FirstName"
  );
  my $curcomm = '';
  my $curppl;
  for my $row ( @$people ) {
    my ( $Role_Name, $Name, $Cast_Note, $Cast_Order, $Part_ID, $Role_ID ) = @$row;
    if ( $Role_Name ne $curcomm ) {
      $curcomm = $Role_Name;
      $curppl = [];
      push( @$Comms, { Role_Name => $Role_Name, People => $curppl } );
    }
    push( @$curppl, { Name => $Name, Cast_Note => $Cast_Note,
      Cast_Order => $Cast_Order, Part_ID => $Part_ID,
      Role_ID => $Role_ID, modify => $modify } );
  }
  $form->tmpl_param( $fieldname => $Comms );
}

sub define_RoleIDs {
  my ( $self, $newrole ) = @_;
  my $form = $self->{form};
  my $rolepriv = $self->{tmp}->{rolepriv};
  my $Proj_ID = $form->field('Proj_ID');
  $self->fatal("Invalid or missing Proj_ID in define_RoleIDs")
    if $Proj_ID !~ /^\d+$/;

  # RoleIDs: checkbox-selectable list of roles
  # to be displayed. RoleIDs are roles that we
  # can modify, so if
  #   rolepriv <  2 RoleIDs is empty
  #   rolepriv == 2 RoleIDs contains Committees
  #   rolepriv >  2 RoleIDs contains Committees and Categories
  # Needs to be defined unless print_friendly

  # OtherIDs is for Roles that we can't modify
  #   rolepriv == 1 Committees
  #   rolepriv == 2 Categories and specials
  #   rolepriv == 3 specials

  # Role_ID defined to select target for additions
  # Only needs to be defined if we have $rolepriv >= 2
  # Only include Categories if rolepriv == 3

  my $roles = $self->{dbh}->selectall_arrayref(
    "SELECT Role_ID, Role_Name, Role_Type
    FROM ACT_Role WHERE Proj_ID = ? AND
    Role_Type IN ( 'Category', 'Committee' )
    ORDER BY Role_Type DESC, Role_Name", {}, $Proj_ID );

  if ( $rolepriv >= 2 ) {
    my @roleopts = map [ $_->[0], "$_->[1]: $_->[2]" ],
      grep $rolepriv > 2 || $_->[2] eq 'Committee', @$roles;
    my $newroles = $self->{dbh}->selectall_arrayref(
      "SELECT P.Role_Name, P.Role_Type FROM ACT_Priv_Roles AS P
      LEFT JOIN ACT_Role AS R ON P.Role_Name = R.Role_Name
      AND R.Proj_ID = ?
      WHERE ISNULL(R.Role_ID)
      GROUP BY P.Role_Name, P.Role_Type
      ORDER BY P.Role_Type DESC, P.Role_Name", {}, $Proj_ID );
    my @newopts = map [ $_->[0], "$_->[0]: $_->[1]" ],
      grep $rolepriv > 2 || $_->[1] eq 'Committee', @$newroles;
    push( @roleopts, [ 0, '------' ], @newopts );
    $form->field( name => 'Role_ID', options => \@roleopts,
      type => 'select' );
    
    my @IDSopts = map [ $_->[0], "$_->[1]: $_->[2]" ],
      grep $rolepriv > 2 || $_->[2] eq 'Committee',
      @$roles;
    if ( @IDSopts ) {
      my @IDSvals = map $_->[0], grep $_->[2] eq 'Committee', @$roles;
      $form->field( name => 'RoleIDs', multiple => 1, type => 'checkbox',
        linebreaks => 1, options => \@IDSopts, value => \@IDSvals );
      if ( $newrole ) {
        @IDSvals = $form->field('RoleIDs');
        push(@IDSvals, $newrole );
        $form->field( name => 'RoleIDs', value => \@IDSvals, force => 1 );
      }
    }
  }

  unless ( $newrole ) {
    # The theory is that OtherIDs won't change when a new role is created,
    # so it doesn't need to be redefined.
    my @OTHopts;
    my @OTHvals;
    if ( $rolepriv < 3 ) {
      my $othrole = $rolepriv == 1 ? 'Committee' : 'Category';
      @OTHopts = map [ $_->[0], "$_->[1]: $_->[2]" ],
        grep $_->[2] eq $othrole,
        @$roles;
      if ( $rolepriv == 1 ) {
        @OTHvals = map $_->[0], @OTHopts;
      }
    }
    if ( $rolepriv >= 2 && $Proj_ID != 1 ) {
      push( @OTHopts, [ 1, 'Board: Committee' ] );
    }
    if ( @OTHopts ) {
      $form->field( name => 'OtherIDs', multiple => 1, type => 'checkbox',
        linebreaks => 1, options => \@OTHopts, value => \@OTHvals );
    }
  }
}

sub EditRole_Fields {
  my ( $self, $Role_ID, $Part_ID, $Cast_Order, $Cast_Note ) = @_;
  my $form = $self->{form};
  my $RolePart = ( $Role_ID && $Part_ID ) ? "${Role_ID}_$Part_ID" : 0;
  $form->field( name => 'RolePart', type => 'hidden', value => $RolePart );
  $form->field( name => 'Cast_Note', type => 'text', size => 40, value => $Cast_Note || '' );
  $form->field( name => 'Cast_Order', type => 'text', size => 5, value => $Cast_Order || '');
}
