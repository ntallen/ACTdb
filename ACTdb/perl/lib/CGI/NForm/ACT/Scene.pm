package CGI::NForm::ACT::Scene;
use strict;
use CGI;
use CGI::FormBuilder;

# Scene.pm provides Create/Edit/Delete for individual scenes.
# Linked from Groups.pm

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    {
      '' => 'SELECT',
      'edit' => 'UPDATE',
      'new' => 'INSERT',
      'delete' => 'DELETE'
    };
}

my %submit = (
  INSERT => 'Create',
  UPDATE => 'Update',
  DELETE => 'Delete'
);
my %static = ( SELECT => 1, INSERT => 0, UPDATE => 0, DELETE => 1 );

# Support for the $params argument is optional, but necessary
# if you want to allow this form to be redirected.
# With FormBuilder 3.0, this functionality can be moved back
# to NForm and get_form() will be responsible only for specifying
# the fields and the template.
sub get_form {
  my ( $self, $op, $params )  = @_;
  $self->Connect();
  my $q = $self->{q} = ref($params) eq 'CGI' ? $params : new CGI($params);
  my $form = CGI::FormBuilder->new(
    action => "$ENV{SCRIPT_NAME}$self->{path_info}",
    method => 'POST',
    fields => [ qw(nxtPI nxtPA Proj_ID Grp_ID) ],
    params => $q,
    template => "$self->{tmpldir}/Scene.html",
    title => "Scene Definition",
    reset => 0
  );
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'Proj_ID', type => 'hidden' );
  $form->field( name => 'Grp_ID', type => 'hidden' );
  $form->field( name => 'Grp_Name', required => 1, static => $static{$op} );
  $form->field( name => 'Grp_Desc', static => $static{$op} );
  $form->submit( $submit{$op} ? [ $submit{$op}, "Back" ] : [ "Back" ] );  

  my $Proj_ID = $form->field('Proj_ID') ||
    $self->fatal("No Proj_ID Specified");
  my $Proj_Name = $self->{dbh}->selectrow_array(
    'SELECT Proj_Name FROM ACT_Project WHERE Proj_ID = ?', {}, $Proj_ID );
  $form->title( "Scene/Group Definition for $Proj_Name" );
  my $Roles = $self->{dbh}->selectall_arrayref(
    'SELECT Role_ID, Role_Name FROM ACT_Role
     WHERE Proj_ID = ? AND Role_Type = ?
     ORDER BY Role_Name', {}, $Proj_ID, 'Character' );
  $form->field( name => 'Role_ID', options => $Roles, multiple => 1,
    type => 'checkbox', linebreaks => 1, static => $static{$op} );
  $self->{tmp}->{Roles} = $Roles;

  return $form;
}

# Must be a Project Registrar
sub authorize {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  my $Proj_ID = $form->field('Proj_ID');
  return 0 if $op eq 'SELECT';
  if ( $self->check_key(EU=>1) ) {
    return 1 if $self->Has_Privilege( $Proj_ID, 'Schedule', 1 );
    # if ( $self->has_role( $Proj_ID, 'Registrar' ) ||
    #      $self->has_role( 1, 'Registrar' ) ) {
    #   return 1;
    # }
  }
  return 0;
}

sub INSERT {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  if ( $form->submitted eq 'Back' ) {
    $self->Return;
  } elsif ( $form->validate ) {
    my @Roles = $form->field('Role_ID');
    my $Grp_Name = $form->field( 'Grp_Name' );
    my $Grp_Desc = $form->field( 'Grp_Desc' );
    my $Proj_ID = $form->field('Proj_ID');
    $self->{dbh}->do( 'LOCK TABLES ACT_Group WRITE, ACT_Grp_Mem WRITE;' );
    my $oldids = $self->{dbh}->selectall_arrayref(
      'SELECT Grp_ID FROM ACT_Group WHERE Grp_Name = ? AND Proj_ID = ?',
       {}, $Grp_Name, $Proj_ID );
    if ( $oldids && @$oldids ) {
      $self->{dbh}->do( 'UNLOCK TABLES;' );
      return $self->error( "That scene name already exists" );
    }
    my $rv = $self->{dbh}->do(
      'INSERT INTO ACT_Group ( Proj_ID, Grp_Name, Grp_Desc, Grp_Type )
       Values ( ?, ?, ?, ? )', {}, $Proj_ID, $Grp_Name, $Grp_Desc, 'Normal' );
    if ( ! defined $rv ) {
      return $self->error( "DBH error on insert: '" . $self->{dbh}->errstr . "'" );
    }
    my $Grp_ID = $self->{dbh}->{mysql_insertid};
    my $sth = $self->{dbh}->prepare(
      'INSERT INTO ACT_Grp_Mem ( Grp_ID, Role_ID )
       VALUES ( ?, ? )' );
    my $failure = 0;
    for my $Role_ID ( @Roles ) {
      unless ( $sth->execute( $Grp_ID, $Role_ID )) {
	$self->error( "Insert role $Role_ID failed" );
	$failure = 1;
      }
    }
    $self->{dbh}->do( 'UNLOCK TABLES;' );
    $self->Return unless $failure;
    return 0;
  }
  return $self->error('Validation error');
}

sub UPDATE {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  $self->Return if $form->submitted eq 'Back';

  my $Grp_ID = $form->field('Grp_ID');
  my $Grp_Name = $form->field('Grp_Name');
  my $Grp_Desc = $form->field('Grp_Desc');
  my $Proj_ID = $form->field('Proj_ID');
  $self->{dbh}->do(
    'LOCK TABLES ACT_Group WRITE, ACT_Grp_Mem WRITE;' );
  my $Old_IDs = $self->{dbh}->selectcol_arrayref(
    'SELECT Grp_ID FROM ACT_Group WHERE Grp_Name = ? AND Proj_ID = ?',
     {}, $Grp_Name, $Proj_ID );
  if ( $Old_IDs && @$Old_IDs ) {
    # Name matches an existing group
    unless ( @$Old_IDs == 1 && $Old_IDs->[0] == $Grp_ID ) {
      $self->{dbh}->do( 'UNLOCK TABLES' );
      return $self->error( 'New name matches an existing group name' );
    }
  }
  $self->{dbh}->do( 'UPDATE ACT_Group SET Grp_Name = ?, Grp_Desc = ? WHERE Grp_ID = ?',
    {}, $Grp_Name, $Grp_Desc, $Grp_ID );

  # Now check the roles to see which are old and which are new
  my $oldroles = $self->{dbh}->selectcol_arrayref(
      'SELECT Role_ID FROM ACT_Grp_Mem WHERE Grp_ID = ?',
      {}, $Grp_ID );
  my @oldroles = sort { $a <=> $b } @$oldroles;
  my @newroles = sort { $a <=> $b } $form->field('Role_ID');
  # warn "oldroles = " . join(', ', @oldroles) . "\n";
  # warn "newroles = " . join(', ', @newroles) . "\n";
  my @delroles;
  my @addroles;
  while ( @oldroles && @newroles ) {
    if ( $oldroles[0] == $newroles[0] ) {
      shift @oldroles;
      shift @newroles;
    } elsif ( $oldroles[0] < $newroles[0] ) {
      push @delroles, shift @oldroles;
    } else {
      push @addroles, shift @newroles;
    }
  }
  push @delroles, @oldroles;
  push @addroles, @newroles;
  # warn "delroles = " . join(', ', @delroles) . "\n";
  # warn "addroles = " . join(', ', @addroles) . "\n";
  my $sth = $self->{dbh}->prepare(
    'INSERT INTO ACT_Grp_Mem ( Grp_ID, Role_ID )
     VALUES ( ?, ? )' );
  my $failure = 0;
  for my $Role_ID ( @addroles ) {
    unless ( $sth->execute( $Grp_ID, $Role_ID )) {
      $self->error( "Insert role $Role_ID failed" );
      $failure = 1;
    }
  }
  $sth = $self->{dbh}->prepare(
    'DELETE FROM ACT_Grp_Mem WHERE Grp_ID = ? AND Role_ID = ?' );
  for my $Role_ID ( @delroles ) {
    unless ( $sth->execute( $Grp_ID, $Role_ID )) {
      $self->error( "Delete role $Role_ID failed" );
      $failure = 1;
    }
  }

  $self->{dbh}->do( 'UNLOCK TABLES' );
  $self->Return unless $failure;
  return 0;
}

sub DELETE {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  $self->Return if $form->submitted eq 'Back';
  my $Grp_ID = $form->field('Grp_ID');
  $self->{dbh}->do(
    'LOCK TABLES ACT_Group WRITE, ACT_Grp_Mem WRITE, ACT_Evt_Grps WRITE;' );
  $self->{dbh}->do(
    'DELETE FROM ACT_Grp_Mem WHERE Grp_ID = ?', {}, $Grp_ID );
  $self->{dbh}->do(
    'DELETE FROM ACT_Evt_Grps WHERE Grp_ID = ?', {}, $Grp_ID );
  $self->{dbh}->do(
    'DELETE FROM ACT_Group WHERE Grp_ID = ?', {}, $Grp_ID );
  $self->{dbh}->do( 'UNLOCK TABLES' );
  $self->Return;
}

# For each listed scene/group, I'll offer Edit/Copy/Delete
sub SELECT {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  $self->Return if $form->submitted eq 'Back';
  return $self->error("Unknown Option");
}

sub InitFields {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  my $Grp_ID = $form->field('Grp_ID');
  if ( $Grp_ID ) {
    my ( $Grp_Name, $Grp_Desc ) = $self->{dbh}->selectrow_array(
      'SELECT Grp_Name, Grp_Desc FROM ACT_Group WHERE
       Grp_ID = ?', {}, $Grp_ID );
    $form->field( name => 'Grp_Name', value => $Grp_Name, force => 1 );
    $form->field( name => 'Grp_Desc', value => $Grp_Desc, force => 1 );
    my $Roles = $self->{dbh}->selectcol_arrayref(
      'SELECT Role_ID FROM ACT_Grp_Mem WHERE Grp_ID = ?',
      {}, $Grp_ID );
    $form->field( name => 'Role_ID', value => $Roles );
  }
}

sub InitParams {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
}

1;
