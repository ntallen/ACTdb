package CGI::NForm::ACT::MrgPart;
use strict;
use CGI;
use CGI::FormBuilder;
use Digest::MD5;

# Merge participant Part_ID2 into participant Part_ID1

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    { 
      '' =>   'SELECT'
    };
}

# Support for the $params argument is optional, but necessary
# if you want to allow this form to be redirected.
# With FormBuilder 3.0, this functionality can be moved back
# to NForm and get_form() will be responsible only for specifying
# the fields and the template.
sub get_form {
  my ( $self, $op, $params )  = @_;
  $self->Connect();
  my $q = $self->{q} = ref($params) eq 'CGI' ? $params : new CGI($params);
  my $form = CGI::FormBuilder->new(
    action => "$ENV{SCRIPT_NAME}$self->{path_info}",
    method => 'POST',
    fields => [ qw(nxtPI nxtPA Part_ID1 Part_ID2 ) ],
    params => $q,
    template => "$self->{tmpldir}/MrgPart.html",
    title => "Merge Participants",
    submit => [ "Submit", "Back" ]
  );
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'Part_ID1', type => 'hidden' );
  $form->field( name => 'Part_ID2', type => 'hidden' );
  return $form;
}

sub authorize {
  my ( $self, $op ) = @_;
  if ( $self->check_key &&
       $self->Has_Privilege( 0, 'DBAdmin', 1 ) ) {
    my $form = $self->{form};
    my $Part_ID1 = $form->field('Part_ID1');
    my $Part_ID2 = $form->field('Part_ID2');
    if ( $Part_ID1 && $Part_ID2 ) {
      my $members = $self->{dbh}->selectall_arrayref(
        "SELECT Part_ID, STRCMP(Status,'Child') AS Adult, FullName, WorkPhone, CellPhone,
         IF(Birthdate>0,YEAR(NOW())-YEAR(Birthdate)-(MID(NOW(),6,5)<MID(Birthdate,6,5)),
         '?') AS Age, ClassYear, YEAR(CURDATE()), MONTH(CURDATE()), Email,
         IF(Part_ID=?,0,1) AS O,
         Address1, Address2, City, State, Zip, HomePhone
         FROM ACT_Participant AS P LEFT OUTER JOIN ACT_Address AS A
         ON P.Addr_ID = A.Addr_ID
         WHERE ( Part_ID = ? OR Part_ID = ? )
         ORDER BY O", {}, $Part_ID1, $Part_ID2, $Part_ID1 );
      $members =
        $self->map_cols( [ qw(Part_ID Adult Name Work Cell Age
           ClassYear CurYear CurMonth Email Second
           Address1 Address2 City State Zip HomePhone) ], $members );
      for my $m ( @$members ) {
        $m->{Grade} = $self->Grade( $m->{ClassYear}, $m->{CurYear}, $m->{CurMonth} );
        my $roles = $self->{dbh}->selectall_arrayref(
          'SELECT Role_Name, Proj_Name
           FROM ACT_Cast, ACT_Role, ACT_Project
           WHERE Part_ID = ? AND ACT_Cast.Role_ID = ACT_Role.Role_ID
           AND ACT_Role.Proj_ID = ACT_Project.Proj_ID
           ORDER BY ACT_Role.Proj_ID, ACT_Role.Role_ID;',
          {}, $m->{Part_ID} );
        $roles = $self->map_cols( [ 'Role_Name', 'Proj_Name' ], $roles );
        $m->{Roles} = join( "<br>\n          ",
           map "$_->{Role_Name} for $_->{Proj_Name}", @$roles );
        $m->{Addr} = join( "<br>", grep $_, map( $m->{$_}, 'Address1', 'Address2' ),
          join( ' ', grep $_, map( $m->{$_}, 'City', 'State', 'Zip' ) ),
          $m->{HomePhone} );
      }
      $form->tmpl_param( Members => $members );
      return 1;
    } else {
      $self->error("Must specify Part_ID1 and Part_ID2");
      return 0;
    }
  }
  return 0;
}

sub INSERT {
}

sub UPDATE {
  return 0;
}

sub DELETE {
}

sub SELECT {
  my $self = shift;
  my $form = $self->{form};
  if ( $form->submitted eq 'Back' ) {
    $self->Return( _submitted => 1, _submit => 'Search' );
  }
  my $Part_ID1 = $form->field('Part_ID1');
  my $Part_ID2 = $form->field('Part_ID2');
  my %stat_priority = (
    Withdrawn => 0,
    Applied => 1,
    Waitlisted => 2,
    Accepted => 3
  );
  $self->{dbh}->do(
   'LOCK TABLES ACT_Users WRITE, ACT_AuthKey WRITE, ACT_Cast WRITE,
    ACT_Invoice WRITE, ACT_Log WRITE, ACT_Family WRITE, ACT_Address WRITE,
    ACT_Cast AS A READ, ACT_Cast AS B READ, ACT_Role READ, ACT_Project READ,
    ACT_Participant WRITE, ACT_Users AS UA READ, ACT_Role AS R2 READ,
    ACT_Participant AS P2 READ, ACT_Gift WRITE,  ACT_Task_Audit WRITE,
    ACT_Task_Note WRITE' );
  my $conflicts = $self->{dbh}->selectall_arrayref(
    "SELECT A.Role_ID, A.Invoice_ID, A.Cast_Status, A.Cast_Time,
      B.Invoice_ID, B.Cast_Status, B.Cast_Time
     FROM ACT_Cast AS A, ACT_Cast AS B
     WHERE A.Part_ID = ? AND B.Part_ID = ?
     AND A.Role_ID = B.Role_ID", {},
    $Part_ID1, $Part_ID2 );
  for my $conflict ( @$conflicts ) {
    # we'll delete roles and invoices associated with Part_ID2
    # if 2 has an invoice and 1 doesn't, move the invoice
    # if 2's cast status is higher than 1's, update 1's status
    my ( $Role_ID, $Inv_ID1, $CS1, $CT1, $Inv_ID2, $CS2, $CT2 ) = @$conflict;
    if ( $Inv_ID2 ) {
      if ( $Inv_ID1 ) {
        # delete Inv_ID2
        $self->{dbh}->do(
          'DELETE FROM ACT_Invoice WHERE Invoice_ID = ?', {},
          $Inv_ID2 );
      } else {
        # move $Inv_ID2 to Part_ID1
        $self->{dbh}->do(
          'UPDATE ACT_Cast SET Invoice_ID = ?
           WHERE Role_ID = ? AND Part_ID = ?', {},
           $Inv_ID2, $Role_ID, $Part_ID1 );
      }
    }
    if ( $stat_priority{$CS2} > $stat_priority{$CS1} ) {
      $self->{dbh}->do(
        'UPDATE ACT_Cast SET Cast_Status = ?
         WHERE Role_ID = ? AND Part_ID = ?', {},
         $CS2, $Role_ID, $Part_ID1 );
    }
    if ( $CT2 lt $CT1 ) {
      $self->{dbh}->do(
        'UPDATE ACT_Cast SET Cast_Time = ?
         WHERE Role_ID = ? AND Part_ID = ?', {},
         $CT2, $Role_ID, $Part_ID1 );
    }
    $self->{dbh}->do(
      'DELETE FROM ACT_Cast WHERE Role_ID = ? AND Part_ID = ?',
       {}, $Role_ID, $Part_ID2 );
  }
  my @usersconf = $self->{dbh}->selectrow_array(
    "SELECT UA.Username, ACT_Users.Username
     FROM ACT_Users AS UA, ACT_Users
     WHERE UA.Part_ID = ? AND ACT_Users.Part_ID = ?", {},
    $Part_ID1, $Part_ID2 );

  if ( @usersconf == 2) {
    $self->{dbh}->do(
      "DELETE FROM ACT_Users WHERE Username = ?", {}, $usersconf[1] );
    $self->Log( "Deleted user $usersconf[1]" );
  }

  for my $table ( qw(ACT_Users ACT_AuthKey ACT_Cast ACT_Invoice ACT_Log ACT_Task_Audit ACT_Task_Note) ) {
    $self->{dbh}->do(
      "UPDATE $table SET Part_ID = ? WHERE Part_ID = ?", {},
      $Part_ID1, $Part_ID2 );
  }
  my $Old_Fam_ID = $self->{dbh}->selectrow_array(
    'SELECT Fam_ID FROM ACT_Participant WHERE Part_ID = ?',
    {}, $Part_ID2 );
  $self->{dbh}->do(
    'DELETE FROM ACT_Participant WHERE Part_ID = ?', {}, $Part_ID2 );
  my $New_Fam_ID = $self->{dbh}->selectrow_array(
    'SELECT Fam_ID FROM ACT_Participant WHERE Part_ID = ?',
    {}, $Part_ID1 );
  $self->Log(" Merged Part_ID $Part_ID2 into $Part_ID1" );
  if ( $New_Fam_ID != $Old_Fam_ID ) {
    $self->purge_fam( $Old_Fam_ID, $New_Fam_ID );
    $self->Fixup_Parents( $New_Fam_ID );
  }

  $self->{dbh}->do('UNLOCK TABLES');
  return 0 if @{$self->{errors}};
  $self->Return( _submitted => 1, _submit => "Search" );
}

sub InitFields {
}

1;
