package CGI::NForm::ACT::Gifts;
use strict;
use CGI;
use CGI::FormBuilder;

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    { '' =>   'SELECT'
    };
}

# Support for the $params argument is optional, but necessary
# if you want to allow this form to be redirected.
# With FormBuilder 3.0, this functionality can be moved back
# to NForm and get_form() will be responsible only for specifying
# the fields and the template.
sub get_form {
  my ( $self, $op, $params )  = @_;
  $self->Connect();
  my $q = $self->{q} = ref($params) eq 'CGI' ? $params : new CGI($params);
  my $form = CGI::FormBuilder->new(
    action => "$ENV{SCRIPT_NAME}$self->{path_info}",
    method => 'POST',
    fields => [ qw(nxtPI nxtPA Fam_ID) ],
    params => $q,
    template => "$self->{tmpldir}/Gifts.html",
    title => "Gift History",
    submit => [ "New Gift", "Back" ],
    stylesheet => 1
  );
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'Fam_ID', type => 'hidden' );
  my $Fam_ID = $form->field('Fam_ID') ||
    $self->fatal( "No Fam_ID specified" );
  my ( $FamilyName ) =
    $self->{dbh}->selectrow_array(
      "SELECT FamilyName FROM ACT_Family WHERE Fam_ID = ?",
      {}, $Fam_ID );
  $form->title( "$FamilyName Family Gift History" );
  return $form;
}

sub authorize {
  my ( $self, $op ) = @_;
  if ( $self->check_key(EU=>1) ) {
    if ( $self->has_role( 1, 'Treasurer' ) ||
	 $self->has_role( 1, 'DBadmin' ) ) {
      $self->{tmp}->{is_admin} = 1;
      return 1;
    } elsif ( $self->has_role( 1, 'Board' ) ) {
      return 1;
    }
  }
  $self->fatal( "Access restricted to ACT Board" );
}

sub INSERT {}

sub UPDATE {}

sub DELETE {}

sub SELECT {
  my $self = shift;
  my $form = $self->{form};
  $self->Return if $form->submitted eq 'Back';
  my $Fam_ID = $form->field('Fam_ID');
  if ( $form->submitted eq 'New Gift' ) {
    $self->Call( "/Gift/new", Fam_ID => $Fam_ID );
  }
  my ( $submit ) = grep m/^[A-Z][A-Z]\d+$/, $form->cgi_param();
  my $cmd = 'XX';
  my $id = 0;
  if ( $submit && $submit =~ m/^([A-Z][A-Z])(\d+)$/ ) {
    $cmd= $1;
    $id = $2;
  }
  if ( $cmd eq 'ED' ) {
    $self->Call( "/Gift/edit", Gift_ID => $id, Fam_ID => $Fam_ID );
  } elsif ( $cmd eq 'CP' ) {
    $self->Call( "/Gift/new", Gift_ID => $id, Fam_ID => $Fam_ID );
  } elsif ( $cmd eq 'DE' ) {
    $self->Call( "/Gift/rm", Gift_ID => $id, Fam_ID => $Fam_ID );
  }
  $self->fatal( "Unknown command: '$cmd'" );
}

sub InitFields {
}

sub InitParams {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  my $Fam_ID = $form->field('Fam_ID');
  my @kws = qw(Gift_ID Gift_Attribution Gift_Amount Gift_Date
	      Gift_ExpDate Gift_Memo);
  my $vals = $self->{dbh}->selectall_arrayref(
    "SELECT " . join(', ', @kws) .
    ($self->{tmp}->{is_admin} ? ", 1" : ", 0") .
    " FROM ACT_Gift WHERE Fam_ID = ?
    ORDER BY Gift_Date DESC", {}, $Fam_ID );
  push( @kws, "is_admin" );
  $form->tmpl_param( Gifts => $self->map_cols( \@kws, $vals ) );
}
1;
