package CGI::NForm::ACT::Chars;
use strict;
use CGI;
use CGI::FormBuilder;

# Chars.pm provides Create/Edit/Delete for individual Chars.
# Linked from Production.pm

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    {
      '' => 'SELECT',
      'edit' => 'INSERT'
    };
}

# Support for the $params argument is optional, but necessary
# if you want to allow this form to be redirected.
# With FormBuilder 3.0, this functionality can be moved back
# to NForm and get_form() will be responsible only for specifying
# the fields and the template.
sub get_form {
  my ( $self, $op, $params )  = @_;
  $self->Connect();
  my $q = $self->{q} = ref($params) eq 'CGI' ? $params : new CGI($params);
  my $form = CGI::FormBuilder->new(
    action => "$ENV{SCRIPT_NAME}$self->{path_info}",
    method => 'POST',
    fields => [ qw(nxtPI nxtPA Proj_ID) ],
    params => $q,
    template => "$self->{tmpldir}/Chars.html",
    title => "Character Definitions",
    submit => [ "Back" ],
    reset => 0
  );
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'Proj_ID', type => 'hidden' );
  if ( $op eq 'INSERT' ) {
    $form->field( name => 'New_Chars', type => 'textarea', cols => 40, rows => 3 );
  }

  my $Proj_ID = $form->field('Proj_ID') ||
    $self->fatal("No Proj_ID Specified");
  my $Proj_Name = $self->{dbh}->selectrow_array(
    'SELECT Proj_Name FROM ACT_Project WHERE Proj_ID = ?', {}, $Proj_ID );
  $form->title( "Character Definitions for $Proj_Name" );
  

  return $form;
}

# Must be a Project Registrar
sub authorize {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  return 1 if $op eq 'SELECT'; # Characters can be seen by anyone
  my $Proj_ID = $form->field('Proj_ID');
  if ( $self->check_key(EU=>1) ) {
    $self->GoTo( '/Chars', Proj_ID => $Proj_ID )
      unless $self->Has_Privilege( $Proj_ID, 'Cast', 2 );
    return 1;
  }
  return 0;
}

sub INSERT {
  my $self = shift @_;
  my $form = $self->{form};
  if ( $form->submitted eq 'Back' ) {
    $self->Return;
  } elsif ( $form->submitted eq 'Add Character(s)' ) {
    my $New_Chars = $form->field( 'New_Chars' );
    my @New_Chars = grep /./, map {
        s/^\s+//;
        s/\s+$//;
        $_;
      } split( /\r?\n/, $New_Chars );
    my $Proj_ID = $form->field('Proj_ID');
    my $saw_err = 0;
    my $Reg_ID = $self->{dbh}->selectrow_array(
      'SELECT Role_ID FROM ACT_Role WHERE Proj_ID = ? AND Role_Name = ?',
      {}, $Proj_ID, 'Registration' );
    return $self->error('Unable to determine Registration ID') unless $Reg_ID;
    my $All_ID = $self->{dbh}->selectrow_array(
      "SELECT Grp_ID FROM ACT_Group
       WHERE Proj_ID = ?
       AND Grp_Name = 'All'
       AND Grp_Type = 'Normal'", {}, $Proj_ID );
    $self->error("No 'All' scene defined") unless $All_ID;
    $self->{dbh}->do( 'LOCK TABLES ACT_Role WRITE, ACT_Grp_Mem WRITE' );
    for my $Role_Name ( @New_Chars ) {
      my ( $old_ID ) = $self->{dbh}->selectrow_array(
        'SELECT Role_ID FROM ACT_Role
         WHERE Proj_ID = ? AND Role_Name = ?
         AND Role_Type = ?', {}, $Proj_ID, $Role_Name, 'Character' );
      if ( $old_ID ) {
        $self->{dbh}->do( 'UNLOCK TABLES' );
        $self->error( "Name '$Role_Name' already in use" );
        $saw_err = 1;
      }
    }
    if ( $saw_err ) {
      $self->{dbh}->do( 'UNLOCK TABLES' );
      return 0;
    }

    my $st = $self->{dbh}->prepare(
      'INSERT IGNORE INTO ACT_Role ( Role_Name, Proj_ID, Role_Type, Job_Committee, Role_Status )
       VALUES ( ?, ?, ?, ?, ? )' );
    my $st2 = $self->{dbh}->prepare(
      'INSERT IGNORE INTO ACT_Grp_Mem ( Grp_ID, Role_ID )
       VALUES ( ?, ? )' );

    for my $Role_Name ( @New_Chars ) {
      if ($st->execute( $Role_Name, $Proj_ID, 'Character', $Reg_ID, 'Closed' ) != 0 ) {
        my $Role_ID = $self->{dbh}->{mysql_insertid};
        $st2->execute( $All_ID, $Role_ID );
      }
    }
    
    $self->{dbh}->do('UNLOCK TABLES');
    $self->GoTo( '/Chars/edit', Proj_ID => $Proj_ID );
  } else {
    my ( $submit ) = grep m/^[A-Z][A-Z]\d+$/, $form->cgi_param();
    if ( $submit && $submit =~ m/^([A-Z][A-Z])(\d+)$/ ) {
      my $cmd= $1;
      my $id = $2;
      my $Proj_ID = $form->field('Proj_ID');
      if ( $cmd eq 'EC' ) {
	$self->Call( "/Char/edit", Proj_ID => $Proj_ID, Role_ID => $id );
      } elsif ( $cmd eq 'DC' ) {
	$self->Call( "/Char/delete", Proj_ID => $Proj_ID, Role_ID => $id );
      } else {
	return $self->error("'Unrecognized command: '$cmd'");
      }
    }
  }
  return $self->error('Validation error');
}

sub UPDATE {}

sub DELETE {}

sub SELECT {
  my $self = shift @_;
  $self->Return;
}

sub InitFields {}

sub InitParams {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  my $Proj_ID = $form->field('Proj_ID');
  my $Roles = $self->{dbh}->selectall_arrayref(
   'SELECT R1.Role_ID, R1.Role_Name, R2.Role_Name, ACT_Is_Also.Cast_ID, Cast_Name
    FROM ACT_Role AS R1 NATURAL LEFT JOIN ACT_Is_Also
    LEFT JOIN ACT_Role AS R2 ON Is_Also_ID = R2.Role_ID
    LEFT JOIN ACT_Casts ON ( ACT_Is_Also.Cast_ID IS NULL AND ACT_Casts.Cast_ID = 3 )
      OR ( ACT_Is_Also.Cast_ID IS NOT NULL AND ACT_Is_Also.Cast_ID = ACT_Casts.Cast_ID )
    WHERE R1.Proj_ID = ? AND R1.Role_Type = ?
    ORDER BY R1.Role_Name, R1.Role_ID', {},
      $Proj_ID, 'Character' );
  my $max_is = 0;
  my $cur_ID = 0;
  my $cur_rec;
  my $roles = [];
  my $cur_is = 0;
  for my $role ( @$Roles ) {
    my ( $Role_ID, $Role_Name, $Is_Also, $Cast_ID, $Cast_Name ) = @$role;
    if ( $Role_ID != $cur_ID ) {
      $cur_rec = { Role_ID => $Role_ID, Role_Name => $Role_Name, Is_Also => [], Edit => $op eq 'INSERT' };
      $cur_ID = $role->[0];
      $cur_is = 0;
      push @$roles, $cur_rec;
    }
    if ( $Is_Also ) {
      if ( ++$cur_is > $max_is ) {
	$max_is = $cur_is;
      }
      my $ia = { Name => $Is_Also };
      $ia->{Cast} = " ($Cast_Name)" if $Cast_ID;
      push @{$cur_rec->{Is_Also}}, $ia;
    }
  }
  for my $role ( @$roles ) {
    while ( @{$role->{Is_Also}} < $max_is ) {
      push @{$role->{Is_Also}}, { Name => '' };
    }
  }
  $form->tmpl_param( Roles => $roles );
  my $hdrs = [];
  push @$hdrs, { hdr => 'Also plays' } if $max_is > 0;
  while ( --$max_is > 0 ) {
    push @$hdrs, { hdr => 'and also' };
  }
  $form->tmpl_param( hdrs => $hdrs );
  $form->tmpl_param( Edit => 1 ) if $op eq 'INSERT';
}

1;
