package CGI::NForm::ACT::NewFamily;
use strict;
use CGI;
use CGI::FormBuilder;
use Mail::Sendmail;

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    { 
      '' =>   'INSERT'
    };
}

# Support for the $params argument is optional, but necessary
# if you want to allow this form to be redirected.
# With FormBuilder 3.0, this functionality can be moved back
# to NForm and get_form() will be responsible only for specifying
# the fields and the template.
sub get_form {
  my ( $self, $op, $params )  = @_;
  $self->Connect();
  my $q = $self->{q} = ref($params) eq 'CGI' ? $params : new CGI($params);
  my $form = CGI::FormBuilder->new(
    action => "$ENV{SCRIPT_NAME}$self->{path_info}",
    method => 'POST',
    fields => [ qw(nxtPI nxtPA FamilyName
		    LastName FirstName HomePhone Email) ],
    params => $q,
    template => "$self->{tmpldir}/NewFamily.html",
    title => "New ACT Family",
    submit => [ "Create a New Family Record" ]
  );
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'HomePhone', required => 1 );
  return $form;
}

sub authorize {
  return 1;
}

sub INSERT {
  my $self = shift;
  my $form = $self->{form};
  if ( $form->validate ) {
    my @KWs = qw(FamilyName LastName FirstName HomePhone Email);
    my %vals = map( ( $_ => $form->field($_) || '' ), @KWs );
    $self->{dbh}->do(
      'INSERT INTO ACT_Family ( FamilyName ) VALUES ( ? );',
      {}, $vals{FamilyName} );
    my $Fam_ID = $self->{dbh}->{mysql_insertid};
    $self->{dbh}->do(
      'INSERT INTO ACT_Address ( Fam_ID, HomePhone )
       VALUES ( ?, ? )', {}, $Fam_ID, $vals{HomePhone} );
    my $Addr_ID = $self->{dbh}->{mysql_insertid};
    $self->{dbh}->do(
      'UPDATE ACT_Family SET Addr_ID = ? WHERE Fam_ID = ?',
      {}, $Addr_ID, $Fam_ID );
    $self->{dbh}->do(
      'INSERT INTO ACT_Participant ( Fam_ID, Addr_ID, FullName,
       LastName, FirstName, Email ) VALUES ( ?, ?, ?, ?, ?, ? )', {},
       $Fam_ID, $Addr_ID, "$vals{FirstName} $vals{LastName}",
       map $vals{$_}, qw(LastName FirstName Email) );
    my $Part_ID = $self->{dbh}->{mysql_insertid};
    unless ( $form->sessionid ) {
      my $key = $self->create_ticket( $Part_ID, $ENV{REMOTE_ADDR},
			time()+600 );
      $form->sessionid($key);
    }
    $self->GoTo( "/User", Part_ID => $Part_ID );
  } else {
    $self->error("Validation errors");
  }
  return 0;
}

sub UPDATE {}

sub DELETE {}

sub SELECT {}

sub InitFields {
  my $self = shift;
  my $form = $self->{form};
  my $LastName = $form->field('LastName');
  $form->field(name=>'FamilyName',value=>$LastName,force=>1);
}

1;
