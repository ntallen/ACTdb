package CGI::NForm::ACT::Scenes;
use strict;
use CGI;
use CGI::FormBuilder;

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    {
      '' => 'SELECT',
      'edit' => 'UPDATE'
    };
}

# Support for the $params argument is optional, but necessary
# if you want to allow this form to be redirected.
# With FormBuilder 3.0, this functionality can be moved back
# to NForm and get_form() will be responsible only for specifying
# the fields and the template.
sub get_form {
  my ( $self, $op, $params )  = @_;
  $self->Connect();
  my $q = $self->{q} = ref($params) eq 'CGI' ? $params : new CGI($params);
  my $form = CGI::FormBuilder->new(
    action => "$ENV{SCRIPT_NAME}$self->{path_info}",
    method => 'POST',
    fields => [ qw(nxtPI nxtPA Proj_ID) ],
    params => $q,
    template => "$self->{tmpldir}/Scenes.html",
    title => "Scene Definitions",
    submit => [ $op eq 'UPDATE' ? "Submit Changes" : (), "Back"  ],
    reset => $op eq 'UPDATE' ? 'Reset' : 0
  );
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'Proj_ID', type => 'hidden' );
  
  my $Proj_ID = $form->field('Proj_ID') ||
    $self->fatal("No Proj_ID Specified");
  my $Proj_Name = $self->{dbh}->selectrow_array(
    'SELECT Proj_Name FROM ACT_Project WHERE Proj_ID = ?', {}, $Proj_ID );
  $form->title( "Scene Definitions for $Proj_Name" );
  
  my $char_ids = $self->{dbh}->selectall_arrayref(
    'SELECT Role_ID, Role_Name FROM ACT_Role
     WHERE Proj_ID = ? AND Role_Type = ?
     ORDER BY Role_Name ASC', {},
     $Proj_ID, 'Character' );
  my $scene_ids = $self->{dbh}->selectall_arrayref(
    'SELECT Grp_ID, Grp_Name FROM ACT_Group
     WHERE Proj_ID = ? AND Grp_Type = ?
     ORDER BY Grp_Order, Grp_Name', {},
     $Proj_ID, 'Normal' );
  
  # For each character, we'll create a field called Char_<id>
  # It's options will be the sc_role combination
  # It's values will be those that are selected currently
  for my $char_id ( map $_->[0], @$char_ids ) {
    my $fname = "Char_$char_id";
    $form->field( name => $fname, type => 'checkbox',
      options => $scene_ids );
  }
  $self->{tmp}->{char_ids} = $char_ids;
  $self->{tmp}->{sc_ids} = $scene_ids;
  return $form;
}

# Authorize updates for Registrar
# SELECT authorized for all
sub authorize {
  my ( $self, $op ) = @_;
  return 1 if $op eq 'SELECT';
  if ( $self->check_key ) {
    my $form = $self->{form};
    my $Proj_ID = $form->field('Proj_ID');
    return 1 if $self->Has_Privilege( $Proj_ID, 'Schedule', 1 );
    # return 1 if $self->has_role($Proj_ID, 'Registrar') ||
    #   $self->has_role( 1, 'Registrar' );
    $self->GoTo( "/Scenes", Proj_ID => $Proj_ID );
  }
  return 0;
}

sub INSERT {}

sub UPDATE {
  my $self = shift;
  my $form = $self->{form};
  $self->Return if $form->submitted eq 'Back';
  my $Proj_ID = $form->field('Proj_ID');
  my @char_ids = map $_->[0], @{$self->{tmp}->{char_ids}};
  $self->{dbh}->do( 'LOCK TABLES ACT_Grp_Mem WRITE' );
  my $insert = $self->{dbh}->prepare('INSERT INTO ACT_Grp_Mem ( Grp_ID, Role_ID ) VALUES ( ?, ? )');
  my $delete = $self->{dbh}->prepare('DELETE FROM ACT_Grp_Mem WHERE Grp_ID = ? AND Role_ID = ?');
  
  for my $char_id ( @char_ids ) {
    my $fname = "Char_$char_id";
    
    # get the old values from the database
    my $crnt_scs = $self->{dbh}->selectcol_arrayref(
      'SELECT ACT_Grp_Mem.Grp_ID FROM ACT_Grp_Mem
       WHERE Role_ID = ?', {}, $char_id );
    my %crnt_scs = map { ( $_ => 1 ) } @$crnt_scs;
    
    # get the new values from the form
    my @form_scs = $form->field( $fname );
    my %form_scs = map { ( $_ => 1 ) } @form_scs;
    my @new_scs = grep !$crnt_scs{$_}, @form_scs;
    my @old_scs = grep !$form_scs{$_}, @$crnt_scs;
    for my $Grp_ID ( @new_scs ) {
      $insert->execute( $Grp_ID, $char_id );
    }
    for my $Grp_ID ( @old_scs ) {
      $delete->execute( $Grp_ID, $char_id );
    }
  }
  $self->{dbh}->do( 'UNLOCK TABLES');
  return 0;
}

sub DELETE {}

sub SELECT {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  if ( $form->submitted eq 'Back') {
    $self->Return;
  }
}

sub InitFields {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  # This is where I would query the ACT_Grp_Mem to determine existing values
  my $Proj_ID = $form->field('Proj_ID');
  my @char_ids = map $_->[0], @{$self->{tmp}->{char_ids}};
  for my $char_id ( @char_ids ) {
    my $sc_ids = $self->{dbh}->selectcol_arrayref(
      'SELECT ACT_Grp_Mem.Grp_ID FROM ACT_Grp_Mem NATURAL JOIN ACT_Group
       WHERE Role_ID = ?
       ORDER BY Grp_Order, Grp_Name', {}, $char_id );
    if ( @$sc_ids ) {
      $form->field( name => "Char_$char_id", value => $sc_ids );
    }
  }
}

sub InitParams {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  my $Proj_ID = $form->field('Proj_ID');
  
	my $Proj_URL = $self->{dbh}->selectrow_array(
		'SELECT Proj_URL FROM ACT_Project WHERE Proj_ID = ?',
		{}, $Proj_ID);
	$form->tmpl_param( Proj_URL => $Proj_URL) if $Proj_URL;
		
  # chars = [ char scene_ids [ sc_role ]]
  # scenes = [ scene ]
  my $chars = $self->{tmp}->{char_ids};
  my @scenes = map $_->[1], @{$self->{tmp}->{sc_ids}};
  my %fields = map { ( $_->name => $_ ) } $form->field;
  my @sc_id_params;
  for my $chr ( @$chars ) {
    my $fname = "Char_$chr->[0]";
    my @elts = map m,^(<.*/>), ? $1 : (), split '\n', $fields{$fname}->render;
    @elts = map( m,checked, ? 'X' : '', @elts ) if $op eq 'SELECT';
    push @sc_id_params, { char => $chr->[1], scene_ids => [ map { { sc_role => $_ } } @elts ] };
  }
  
  $form->tmpl_param( scenes => [ map { { scene => $_ } } @scenes ] );
  $form->tmpl_param( n_scenes => scalar(@scenes) );
  $form->tmpl_param( chars => \@sc_id_params );
}

1;
