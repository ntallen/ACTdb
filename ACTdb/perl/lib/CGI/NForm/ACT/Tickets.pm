package CGI::NForm::ACT::Tickets;
use strict;
use CGI;
use CGI::FormBuilder;
use CGI::Carp;
use Spreadsheet::WriteExcel;
use IO::Socket::SSL;
use Mozilla::CA;
use LWP::UserAgent;
use XML::LibXML;
use Encode;

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    {
      '' => 'SELECT'
    };
}

sub InitForm {
  my ( $self, $form, $op ) = @_;
  $self->Connect();
  $form->field(name => 'Proj_ID', type => 'hidden' );
  $form->field(name => 'Ref_ID', type => 'hidden');
  $form->field(name => 'View_ID', type => 'hidden');
  $form->field(name => 'View_Type', type => 'hidden');
  
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->template("$self->{tmpldir}/Tickets.html");
  # $form->reset(0);
  $form->submit( ['Back'] );

  my $Proj_ID = $form->field('Proj_ID');
  $self->fatal("No Proj_ID Specified")
    unless defined($Proj_ID) && $Proj_ID =~ m/^\d+$/;
  my $Proj_Name = $self->{dbh}->selectrow_array(
    'SELECT Proj_Name FROM ACT_Project WHERE Proj_ID = ?', {}, $Proj_ID );
  $form->title( "$Proj_ID: Online Ticket Reports for '$Proj_Name'" );
  my $jsfunc = <<'EOJS';
    if (form._submit.value == 'Back') {
      return true;
    }
EOJS
    $form->jsfunc($jsfunc);
  return $form;
}

sub authorize {
  my ( $self, $op ) = @_;
  if ($self->check_key(EU => 1)) {
    my $form = $self->{form};
    my $Proj_ID = $form->field('Proj_ID');
    my $rolepriv = $self->Get_Privilege_Value( $Proj_ID, 'Tickets' );
    $self->{tmp}->{rolepriv} = $rolepriv;
    if ($rolepriv >= 3) {
      $form->field(name => 'BPT_ID', type => 'text', required => 1);
      $form->field(name => 'BPT_account', type => 'text', maxlength => 60, required => 1);
    }
    return 1;
  }
  return 0;
}

sub InitBPTparams {
  my ($self) = @_;
  my $form = $self->{form};
  my $Proj_ID = $form->field('Proj_ID');
  my ($BPT_ID, $BPT_account) = $self->{dbh}->selectrow_array(
    'SELECT BPT_ID, BPT_account FROM ACT_BPT_Event WHERE Proj_ID = ?',
    {}, $Proj_ID);
  $form->field(name => 'BPT_ID', value => $BPT_ID, force => 1) if $BPT_ID;
  $BPT_account = $CGI::NForm::ACT::Config{bpt_default_account} unless $BPT_account;
  $form->field(name => 'BPT_account', value => $BPT_account, force => 1);
}

# Called when form is first displayed (prior to submission)
sub InitFields {
  my ( $self, $op ) = @_;
  $self->InitBPTparams;
  # my $today = strftime('%Y-%m-%d', localtime);
  # $form->field( name => 'StartDate', value => $today, force => 1 );
}

sub INSERT {}

sub UPDATE {}

sub DELETE {}

sub SELECT {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  my $Proj_ID;
  if ($form->field('Proj_ID') =~ m/^(\d+)$/) {
    $Proj_ID = $1;
  } else {
    return $self->error("Invalid Proj_ID");
  }
  my ($Proj_Name, $Proj_mnc) = $self->{dbh}->selectrow_array(
    "SELECT Proj_Name, Proj_abbr FROM ACT_Project WHERE Proj_ID = ?",
    {}, $Proj_ID);
  $Proj_mnc = "P$Proj_ID" unless $Proj_mnc;
  if ( $form->submitted eq 'Back' ) {
    $self->Return;
  } elsif ( $form->validate ) {
    if ( $form->submitted eq 'Submit' ) {
      return $self->error("Not authorized to modify BPT configuration")
        if $self->{tmp}->{rolepriv} < 3;
      my $BPT_ID = $form->field('BPT_ID');
      my $BPT_account = $form->field('BPT_account');
      $self->{dbh}->do('INSERT INTO ACT_BPT_Event (Proj_ID, BPT_ID, BPT_account)
                        VALUES (?, ?, ?)
                        ON DUPLICATE KEY UPDATE
                          BPT_ID = VALUES(BPT_ID),
                          BPT_account = VALUES(BPT_account)', {},
                        $Proj_ID, $BPT_ID, $BPT_account );
      $self->GoTo("/Tickets", Proj_ID => $Proj_ID, BPT_account => $BPT_account);
    } elsif ($form->submitted == 0) {
      my $View_Type = $form->field('View_Type');
      my $View_ID = $form->field('View_ID');
      my $Ref_ID = $form->field('Ref_ID') || 0;
      $self->InitBPTparams;
      if ($View_Type !~ m/^xls$/) {
        return $self->error("Invalid View_Type: '$View_Type'");
      }
      if ($View_ID == 0 && $self->{tmp}->{rolepriv} < 2) {
        return $self->error("Not authorized to create current reports");
      }
      my $bpt_dev_id = $CGI::NForm::ACT::Config{bpt_dev_id};
      my $BPT_ID = $form->field('BPT_ID');
      my $BPT_account = $form->field('BPT_account');
      # 5/13/2018: BPT apparently installed a new cert within
      # the last few months, which broke this script's access
      # to their API. The new cert used a wildcard in the CN
      # and also in the subject alternate name. As it happens,
      # the default SSL_verifycn_scheme ('www') will expand
      # wildcards in the CN, but not if names are listed in
      # the subject alternate, and won't expand wildcards in
      # the subject alternate. The 'smtp' scheme will, in fact
      # expand wildcards in the subject alternate, so that
      # is the scheme we need to use.
      IO::Socket::SSL::set_defaults(
        SSL_ca_file => Mozilla::CA::SSL_ca_file(),
        SSL_verify_mode => 'SSL_VERIFY_PEER',
        SSL_verifycn_scheme => 'smtp');
      my $ua = LWP::UserAgent->new;
      if ($View_ID == 0) {
        my $response = $self->BPT_api($ua, 'orderlist', id => $bpt_dev_id,
            account => $BPT_account, event_id => $BPT_ID);
        return 0 unless $response;
        $self->{dbh}->do('INSERT INTO ACT_BPT_Report (Proj_ID, Part_ID, Report_Date, Report_XML)
          VALUES (?, ?, NOW(), ?)', {},
          $Proj_ID, $self->{auth}->{Part_ID}, $response);
        $View_ID = $self->{dbh}->{mysql_insertid};
      }
      my ( $crnt_sales, $ref_sales, $diff );
      my %dates;
      my %prices;
      my %pricenames;
      my %ucname;
      { my $response = $self->BPT_api($ua, 'datelist', id => $bpt_dev_id, event_id => $BPT_ID);
        return 0 unless $response;
        my $dom = XML::LibXML->load_xml(string => $response);
        my @dates = $dom->getElementsByTagName('date');
        foreach my $date (@dates) {
          my $date_id = getText($date, 'date_id');
          $dates{$date_id} = { map { $_ => getText($date, $_) } qw(datestart timestart live) };
        }
      }

      for my $date_id (keys %dates) {
        my $response = $self->BPT_api($ua, 'pricelist', id => $bpt_dev_id, event_id => $BPT_ID, date_id => $date_id);
        return 0 unless $response;
        my $dom = eval { XML::LibXML->load_xml(string => $response) };
        return $self->error("Error from load_xml: $@\n") unless defined $dom;
        my @prices = $dom->getElementsByTagName('price');
        foreach my $price (@prices) {
          my $price_id = getText($price, 'price_id');
          $prices{$price_id} = { map { $_ => getText($price, $_) } qw(name value) };
          $pricenames{$prices{$price_id}->{name}} = 1;
        }
      }
      { my ($As_Of, $response) =
          $self->{dbh}->selectrow_array(
            "SELECT Report_Date, Report_XML
             FROM ACT_BPT_Report
             WHERE Proj_ID = ? AND BPT_report_ID = ?", {},
            $Proj_ID, $View_ID);
        return $self->error("Invalid View_ID") unless $response;
        $crnt_sales = parse_sales($response, $As_Of, \%dates, \%prices, \%pricenames, \%ucname);
      }
      if ($Ref_ID && $Ref_ID != $View_ID) {
        my ($As_Of, $response) =
          $self->{dbh}->selectrow_array(
            "SELECT Report_Date, Report_XML
             FROM ACT_BPT_Report
             WHERE Proj_ID = ? AND BPT_report_ID = ?", {},
            $Proj_ID, $Ref_ID);
        return $self->error("Invalid Ref_ID") unless $response;
        $ref_sales = parse_sales($response, $As_Of, \%dates, \%prices, \%pricenames, \%ucname);
        $diff = {};
        do_diff( $diff, $ref_sales, $crnt_sales, 1 );
        do_diff( $diff, $crnt_sales, $ref_sales, 0 );
      }
      if ($View_Type eq 'xls') {
        my $spreadsheet = "$CGI::NForm::ACT::Config{tmpdir}/actwebmaster_${Proj_ID}_tickets.xls";
        my $wb = Spreadsheet::WriteExcel->new($spreadsheet);
        my %fmt;
        $fmt{bold_left} = $wb->add_format( bold => 1, align => 'left' );
        $fmt{bold_center} = $wb->add_format( bold => 1, align => 'center' );
        $fmt{grey_bold} = $wb->add_format( bg_color => 'silver', bold => 1 );
        $fmt{bottom_bold} = $wb->add_format( bold => 1, bottom => 1 );
        $fmt{white} = $wb->add_format();
        $fmt{grey_center} = $wb->add_format( bg_color => 'silver', align => 'center', border => 1 );
        $fmt{white_center} = $wb->add_format( align => 'center', border => 1 );
        $fmt{outline} = $wb->add_format( border => 2 );
        my $ws = $wb->add_worksheet("Tickets");
        $ws->hide_gridlines();
        $ws->center_horizontally();
        $ws->write_string( 0, 0, decode("utf8", "Ticket Sales for $Proj_Name"), $fmt{bold_left} );
        $ws->write_string( 1, 1, "as of $crnt_sales->{As_Of}", $fmt{bold_left} );
        $ws->write_string( 3, 3, "Names listed are for Attendee, not Purchaser, when different" );
        $ws->set_column( 0, 2, 2 );
        $ws->set_column( 3, 3, 50 );
        $ws->set_column( 4, 4, 3 );
        my $row = 4;
      
        $row = report_sales_xls($ws, $row, $diff,  "Updates since $ref_sales->{As_Of} UTC", \%ucname, \%fmt)
          if $diff;
        report_sales_xls($ws, $row, $crnt_sales, "Current Totals as of $crnt_sales->{As_Of} UTC:", \%ucname, \%fmt);
        $wb->close();
        my $filesize = -s $spreadsheet;
        open( XLS, "<$spreadsheet" ) ||
          return $self->error("Unable to read spreadsheet");
        my $contents;
        my $rb = read XLS, $contents, $filesize;
        return $self->error("Short read: size: $filesize read: $rb")
          if $rb != $filesize;
        close XLS;
        unlink($spreadsheet);
        my $q = $self->{q};
        print
          $q->header( -type => 'application/vnd.ms-excel', -Content_length => $filesize,
              -attachment => "${Proj_ID}_${Proj_mnc}_tickets.xls" ),
          $contents;
        exit(0);
      } else {
        return $self->error("View_Type '$View_Type' is not implemented");
      }
    }
  } else {
    return $self->error( "Validation failed" );
  }
  return $self->error("Invalid submission code: '" . $form->submitted . "'");
}

sub InitParams {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  my $BPT_ID = $form->field('BPT_ID');
  my $Proj_ID = $form->field('Proj_ID');
  $form->tmpl_param(event_defined => 1) if $BPT_ID;
  $form->tmpl_param(authorized => 1) if $self->{tmp}->{rolepriv} > 0;
  $form->tmpl_param(can_write => 1) if $self->{tmp}->{rolepriv} > 1;
  $form->tmpl_param(is_admin => 1) if $self->{tmp}->{rolepriv} > 2;
  my $reports = $self->{dbh}->selectall_arrayref(
    "SELECT BPT_report_ID, Report_Date,
     CONCAT(FirstName, ' ', LastName) AS Name
     FROM ACT_BPT_Report NATURAL JOIN ACT_Participant
     WHERE Proj_ID = ? ORDER BY Report_Date ASC", {}, $Proj_ID);
  $reports = $self->map_cols(
    [ qw(BPT_report_ID Report_Date Name) ],
    $reports );
  if (@$reports) {
    my $last = pop @$reports;
    $last->{checked} = 1;
    push(@$reports, $last);
  }
  $form->tmpl_param('reports', $reports);
}

sub BPT_api {
  my ($self, $ua, $feed, %opts) = @_;
  my $api = 'https://www.brownpapertickets.com/api2';
  my $url = "$api/$feed?" . join('&', map "$_=$opts{$_}", keys %opts);
  my $response = $ua->get($url);
  if (! ($response->is_success)) {
    $self->error("BPT_api($url) failed\n");
    return '';
  }
  return $response->content;
}

sub parse_sales {
  my ($response, $As_Of, $dates, $prices, $pricenames, $ucname) = @_;
  my @Items;
  { my $dom = eval { XML::LibXML->load_xml(string => $response) };
    my @items = $dom->getElementsByTagName('item');
    foreach my $item (@items) {
      push @Items, {
        map { $_ => getText($item, $_) }
		  qw(fname lname date_id price_id shipping_method quantity) };
    }
  }

  # Now group the tickets by date_id, shipping_method, fullname, price
  my $sales = { As_Of => $As_Of };
  my $multiprice = keys(%$pricenames) > 1;
  foreach my $item (@Items) {
    my $category = $item->{shipping_method};
    my $name = "$item->{fname} $item->{lname}";
    my $showref = $dates->{$item->{date_id}};
    my $show = "$showref->{datestart} $showref->{timestart}";
    $name .= "/$prices->{$item->{price_id}}->{name}" if $multiprice;
    $ucname->{lc($name)} ||= $name;
    $sales->{$category}->{$show}->{lc($name)} += $item->{quantity};
    $sales->{Totals}->{$show} += $item->{quantity};
  }
  return $sales;
}

sub getText {
  my ($node, $tag) = @_;
  my $text = join "", map $_->textContent(), $node->getElementsByTagName($tag);
  return $text;
}
# do_diff( $diff, $a, $b, $dir );
# $diff is a hash ref
# $a and $b are hash refs from parse_sales
# $dir is non-zero if $b is the newer sales, zero if $a is
sub do_diff {
  my ( $diff, $a, $b, $dir ) = @_;
  for my $cat ( reverse sort keys %$b ) {
    next if $cat =~ m/^(?:Totals|As_Of|Show)$/;
    for my $show ( sort keys %{$b->{$cat}} ) {
      for my $buyer ( sort keys %{$b->{$cat}->{$show}} ) {
        my $bval = $b->{$cat}->{$show}->{$buyer};
        my $aval = $a->{$cat}->{$show}->{$buyer} || 0;
        if ( $aval != $bval ) {
          my $text;
          if ( $dir ) { # forward
            if ( $aval == 0 ) {
              $text = $bval;
            } elsif ( $bval > $aval ) {
              my $newval = $bval - $aval;
              $text = "$newval additional: Now $bval";
            } else {
              my $newval = $aval - $bval;
              $text = "$newval CANCELLED: Now $bval";
            }
          } elsif ( $aval == 0 ) { # backward: only concerned with total cancellations
            $text = "$bval CANCELLED: Now $aval";
          }
          $diff->{$cat}->{$show}->{$buyer} = $text if defined $text;
        }
      }
    }
  }
}

sub report_sales_xls {
  my ( $ws, $row, $sales, $title, $ucname, $fmt ) = @_;
  ++$row;
  $ws->merge_range( $row, 0, $row, 4, $title, $fmt->{grey_bold} );
  ++$row;
  
  # my @cats = grep $sales->{$_}, "Will Call Tickets", "Physical Tickets",
  #   "Print-at-Home Tickets", "Open Ticket Network Tickets", "Mobile Tickets", "Totals";
  my @cats = sort grep !m/Totals|As_Of|Show/, keys %$sales;
  push(@cats, 'Totals') if @cats;
  for my $cat (@cats) {
    ++$row;
    if ( $cat eq 'Totals') {
      $ws->merge_range( $row, 1, $row, 4, "$cat:", $fmt->{grey_bold} );
      ++$row;
    } else {
      $ws->merge_range($row, 1, $row, 4, "$cat:", $fmt->{bottom_bold});
      ++$row;
    }
    my @shows = sort keys %{$sales->{$cat}};
    for my $show ( @shows ) {
      if ( $cat eq 'Totals' ) {
        $ws->write( ++$row, 2, "$show:", $fmt->{bold_left} );
        $ws->write( $row++, 4, $sales->{$cat}->{$show} );
      } else {
        ++$row;
        $ws->write( $row++, 2, "$show:", $fmt->{bold_left} );
      	my @buyers = sort keys %{$sales->{$cat}->{$show}};
      	for my $buyer ( @buyers ) {
          $ws->write( $row++, 3, [ $ucname->{$buyer}, $sales->{$cat}->{$show}->{$buyer} ] );
        }
      }
    }
  }
  return $row;
}

1;
