package CGI::NForm::ACT::RegConfirm;
use strict;
use CGI;
use CGI::FormBuilder;

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    {
      '' => 'SELECT'
    };
}

# Support for the $params argument is optional, but necessary
# if you want to allow this form to be redirected.
# With FormBuilder 3.0, this functionality can be moved back
# to NForm and get_form() will be responsible only for specifying
# the fields and the template.
sub get_form {
  my ( $self, $op, $params )  = @_;
  $self->Connect();
  my $q = $self->{q} = ref($params) eq 'CGI' ? $params : new CGI($params);
  my $form = CGI::FormBuilder->new(
    action => "$ENV{SCRIPT_NAME}$self->{path_info}",
    method => 'POST',
    fields => [ qw(nxtPI nxtPA Part_ID Role_ID Cast_ID Fam_ID EConfirm Reg_errmsg) ],
    params => $q,
    template => "$self->{tmpldir}/RegConfirm.html",
    title => "ACT Registration",
    submit => [ "Register another child", "My Home Page" ]
  );
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'Role_ID', type => 'hidden' );
  $form->field( name => 'Part_ID', type => 'hidden' );
  $form->field( name => 'Cast_ID', type => 'hidden' );
  $form->field( name => 'Fam_ID', type => 'hidden' );
  return $form;
}

# If Fam_ID is specified, it had better be your family unless you're
# on the board. If it isn't specified, set it to your family.
# If Part_ID is specified, it must be in Fam_ID.
sub authorize {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  my $Role_ID = $form->field('Role_ID');
  $self->fatal("No Project Specified for Registration") unless $Role_ID;
  my $Part_ID = $form->field('Part_ID');
  $self->fatal("No Participant specified for confirmation") unless $Part_ID;
  my $Cast_ID = $form->field('Cast_ID') || 0;
  my ( $submit ) = grep m/^Role_ID\d+$/, $form->cgi_param();
  if ( $submit && $submit =~ m/^Role_ID(\d+)$/ ) {
    $self->GoTo( "/Register", Part_ID => $Part_ID, Role_ID => $1,
      _submitted => 1, _submit => 'Register' );
  }
  if ( $self->check_key( EU => 1 ) ) {
    my $Reg_errmsg = $form->field('Reg_errmsg');
    $self->error( $Reg_errmsg ) if $Reg_errmsg;
    my $My_Part_ID = $self->{auth}->{Part_ID};
    my ( $My_Fam_ID, $My_Status, $My_Email ) = $self->{dbh}->selectrow_array(
      'SELECT Fam_ID, Status, Email FROM ACT_Participant where Part_ID = ?',
      {}, $My_Part_ID );
    unless ( defined $My_Fam_ID && defined $My_Status ) {
      $self->fatal("No Fam_ID or Status defined for user Part_ID = $My_Part_ID");
    }
    my ( $Part_Fam_ID, $FullName, $Part_Email ) = $self->{dbh}->selectrow_array(
      'SELECT Fam_ID, FullName, Email FROM ACT_Participant where Part_ID = ?',
      {}, $Part_ID );
    unless ( defined $Part_Fam_ID ) {
      $self->fatal("No Fam_ID defined for user Part_ID = $Part_ID");
    }
    if ( $Part_Fam_ID != $My_Fam_ID && ! $self->has_role( 1, 'Board' ) ) {
      $self->fatal("You are not authorized to view this confirmation data" );
    }
    $form->field( name => 'Fam_ID', value => $Part_Fam_ID, force => 1 );

    # Now initialize template parameters
    $form->tmpl_param( FullName => $FullName );
    
    if ( $form->field('EConfirm') ) {
      $form->tmpl_param( EConfirm =>
	"An E-mail Confirmation has been sent to " .
	join( " and ", grep $_, $My_Email, $Part_Email ) . "." );
    }

    my $vals = $self->RegConfirm( $Role_ID, $Part_ID, $Cast_ID );
    for ( keys %$vals ) {
      $vals->{$_} = '' unless $vals->{$_};
      $form->tmpl_param( $_ => $vals->{$_} );
    }
    
    if ( $vals->{auditions} && @{$vals->{auditions}} ) {
      $form->tmpl_param( has_auditions => 1 );
      $form->tmpl_param( auditions => [
	  map {
	    my ( $Role_ID, $Role_Status, $Event_Date, $Event_Start, $Event_End, $Nreg, $Nlim ) = @$_;
	    my $filled = ( $Role_Status eq 'Closed' ) || (defined($Nlim) && $Nreg >= $Nlim);
	    my $date = "$Event_Date, $Event_Start&ndash;$Event_End";
	    $date =~ s/12:00 PM/Noon/;
	    $date =~ s/ PM(.*)PM/${1}PM/;
	    $date =~ s/ AM(.*)AM/${1}AM/;
	    { Role_ID => $Role_ID, filled => $filled,
	      date => $date };
	  } @{$vals->{auditions}}
	] );
    }
    
    return 1;
  }
  return 0;
}

sub INSERT {}

sub UPDATE {}

sub DELETE {}

sub SELECT {
  my $self = shift;
  my $form = $self->{form};
  if ( $form->submitted eq 'View our account' ) {
  } elsif ( $form->submitted eq 'Register another child' ) {
    $self->GoTo( "/Register", Role_ID => $form->field('Role_ID'),
      Fam_ID => $form->field('Fam_ID') );
  } elsif ( $form->submitted eq 'My Home Page' ) {
    $self->redirect( "/Home", _sessionid => $form->sessionid );
  } else {
    $self->error( "Unrecognized option" );
  }
  return 0;
}

sub InitFields {}

1;
