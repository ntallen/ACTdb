package CGI::NForm::ACT::Cast;
use strict;
use CGI;
use CGI::FormBuilder;

# Cast.pm provides Casting control
# Linked from Production.pm

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    {
      '' => 'UPDATE'
    };
}

# Support for the $params argument is optional, but necessary
# if you want to allow this form to be redirected.
# With FormBuilder 3.0, this functionality can be moved back
# to NForm and get_form() will be responsible only for specifying
# the fields and the template.
sub get_form {
  my ( $self, $op, $params )  = @_;
  $self->Connect();
  my $q = $self->{q} = ref($params) eq 'CGI' ? $params : new CGI($params);
  my $form = CGI::FormBuilder->new(
    action => "$ENV{SCRIPT_NAME}$self->{path_info}",
    method => 'POST',
    params => $q,
    template => "$self->{tmpldir}/Cast.html",
    title => "Character Definitions",
    reset => 0
  );
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'Proj_ID', type => 'hidden' );
  $form->field( name => 'Part_ID', type => 'select' );
  $form->field( name => 'Role_ID', type => 'select' );
  $form->field( name => 'Cast_ID', type => 'select' );
  $form->field( name => 'AllActors', type => 'radio',
    options => [ [ 1, 'list only uncast ACTors' ],
                 [ 2, 'list all ACTors' ] ],
    linebreaks => 1, value => 1 );
  $form->field( name => 'PrimaryRoles', type => 'radio',
    options => [ [ 2, 'list only primary roles' ],
                 [ 1, 'list all roles' ] ],
    linebreaks => 1, value => 1 );
  $form->field( name => 'ShowDelete', options => [ 'Show Delete' ] );
  $form->field( name => 'ShowCast', options => [ 'Hide Cast' ] );

  my $Proj_ID = $form->field('Proj_ID') ||
    $self->fatal("No Proj_ID Specified");
  my $Proj_Name = $self->{dbh}->selectrow_array(
    'SELECT Proj_Name FROM ACT_Project WHERE Proj_ID = ?', {}, $Proj_ID );
  $form->title( "Cast for $Proj_Name" );

  my $n_casts = $self->{dbh}->selectrow_array(
    'SELECT N_Casts FROM ACT_Project WHERE Proj_ID = ?', {}, $Proj_ID );
  my $doublecast = $n_casts > 1;
  $self->{tmp}->{doublecast} = $doublecast;
  
  return $form;
}

# Must be a Project Registrar
sub authorize {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  my $Proj_ID = $form->field('Proj_ID');
  if ( $self->check_key(EU=>1) ) {
    my $cast_priv =
      $self->Get_Privilege_Value( $Proj_ID, 'Cast' );
    $self->{tmp}->{can_write} = $cast_priv >= 2;
    return 1 if $cast_priv >= 1;
  }
  $self->error('Not authorized to access cast data for this project');
  return 0;
}

sub INSERT {}

sub UPDATE {
  my $self = shift @_;
  my $form = $self->{form};
  $self->Return if $form->submitted eq 'Back';
  my $Proj_ID = $form->field('Proj_ID');
  $self->Reload if $form->submitted eq 'Update';
  return $self->error("Not authorized to do that")
    unless $self->{tmp}->{can_write};
  if ( $form->submitted eq 'Submit' ) {
    my $Cast_ID;
    my $Role_ID = $form->field('Role_ID');
    my $Part_ID = $form->field('Part_ID');
    if ( $self->{tmp}->{doublecast} ) {
      $Cast_ID = $form->field('Cast_ID');
      if ( defined $Cast_ID && $Cast_ID ne '' ) {
        $self->error( "Invalid Cast_ID" ) unless $Cast_ID =~ m/^[012]$/;
      } else {
        $self->error( "Must select a cast" );
      }
    } else {
      $Cast_ID = 0;
    }
    $self->error( "Must select a Role" ) unless $Role_ID;
    $self->error( "Must select an actor" ) unless $Part_ID;
    return 0 if @{$self->{errors}};
    
    # ideally we should double-check that Role_ID is part of Proj_ID
    # and is a Character role, and that Part_ID is an accepted registrant in this or a parent project
    $self->{dbh}->do( 'LOCK TABLES ACT_Project READ, ACT_Role READ, ACT_Cast WRITE, ACT_Subproject READ' );
    my $role_chk = $self->{dbh}->selectrow_array(
      'SELECT 1 FROM ACT_Role WHERE Role_ID = ? AND Proj_ID = ? AND Role_TYPE = ?',
      {}, $Role_ID, $Proj_ID, 'Character' );
    $self->error( "Invalid Role" ) unless $role_chk;
    my $part_chk = $self->{dbh}->selectrow_array(
      "SELECT 1
      FROM ACT_Role NATURAL JOIN ACT_Cast
      WHERE Proj_ID IN (
             SELECT ? AS Proj_ID
           UNION
             SELECT Parent_Proj
             FROM ACT_Subproject
             WHERE Proj_ID = ?)
        AND Role_Name = 'Registration'
        AND Cast_Status = 'Accepted'
        AND Part_ID = ?",
      {}, $Proj_ID, $Proj_ID, $Part_ID );
    $self->error( "Invalid actor ID" ) unless $part_chk;
    if ( @{$self->{errors}} ) {
      $self->{dbh}->do( 'UNLOCK TABLES' );
      return 0;
    }
    eval {
      $self->{dbh}->do(
        'INSERT INTO ACT_Cast ( Role_ID, Cast_ID, Part_ID, Cast_Status )
        VALUES ( ?, ?, ?, ? )', {}, $Role_ID, $Cast_ID, $Part_ID, 'Accepted' );
    };
    $self->{dbh}->do( 'UNLOCK TABLES' );
    if ( $@ ) {
      if ( $@ =~ m/Duplicate entry/ ) {
        return $self->error( "That casting has already been made" );
      } else {
        return $self->error( "Error inserting: $@" );
      }
    }
    $self->Reload;
  } else {
    #  D<Part_ID>_<Cast_ID>_<Role_ID>
    my ( $submit ) = grep m/^D\d+_\d+_\d+$/, $form->cgi_param();
    if ( $submit && $submit =~ m/^D(\d+)_(\d+)_(\d+)$/ ) {
      my ( $Part_ID, $Cast_ID, $Role_ID ) = ( $1, $2, $3 );
      $self->{dbh}->do(
        'DELETE FROM ACT_Cast
        WHERE Part_ID = ? AND Cast_ID = ? AND Role_ID = ?', {},
        $Part_ID, $Cast_ID, $Role_ID );
      $self->Reload;
    }
  }
  return $self->error('Not implemented');
}

sub DELETE {}

sub SELECT {}

sub InitFields {}

sub InitParams {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  my $Proj_ID = $form->field('Proj_ID');
  my $can_write = $self->{tmp}->{can_write};
  $form->tmpl_param( can_write => $can_write );
  my $doublecast = $self->{tmp}->{doublecast};
  $form->tmpl_param( doublecast => $doublecast );

  # init cast = [ name ]


  # Need to init Part_ID, Role_ID and Cast_ID
  
  if ( $doublecast ) {
    my $casts = $self->{dbh}->selectall_arrayref(
      'SELECT Cast_ID, Cast_Name FROM ACT_Casts ORDER BY Cast_ID' );
    for ( @$casts ) {
      $_->[1] = $_->[0] ? "the $_->[1] cast" : "both casts";
    }
    $form->field( name => 'Cast_ID', options => $casts, type => 'select', value => 0,
          cleanopts => 0 );
  }
  
  if ( $can_write ) {
    # Part_ID depends on AllActors
    my $AllActors = $form->field('AllActors');
    my $actors = $AllActors == 2 ?
      $self->{dbh}->selectall_arrayref(
        "SELECT ACT_Participant.Part_ID, CONCAT(LastName, ', ', FirstName)
        FROM ACT_Role AS R
          NATURAL JOIN ACT_Cast
          NATURAL JOIN ACT_Participant
        WHERE R.Proj_ID IN (
             SELECT ? AS Proj_ID
           UNION
             SELECT Parent_Proj
             FROM ACT_Subproject
             WHERE Proj_ID = ?)
          AND Role_Name = 'Registration'
          AND Cast_Status = 'Accepted'
        ORDER BY LastName, FirstName",
        {}, $Proj_ID, $Proj_ID ) :
      $self->{dbh}->selectall_arrayref(
        "SELECT P.Part_ID, CONCAT(LastName, ', ', FirstName) AS Name
        FROM ACT_Role AS R1
          NATURAL JOIN ACT_Cast AS C1
          NATURAL JOIN ACT_Participant AS P
          LEFT JOIN (
            SELECT Part_ID
            FROM ACT_Role AS R
              NATURAL JOIN ACT_Cast
            WHERE R.Proj_ID IN (
                  SELECT ?
                UNION
                  # Parent
                  SELECT Parent_Proj
                  FROM ACT_Subproject
                  WHERE Proj_ID = ?
                UNION
                  # Siblings
                  SELECT SP2.Proj_ID
                  FROM ACT_Subproject AS SP1 JOIN ACT_Subproject AS SP2
                    ON SP1.Parent_Proj = SP2.Parent_Proj
                  WHERE SP1.Proj_ID = ? AND SP1.Proj_ID != SP2.Proj_ID
                UNION
                  # Subprojects
                  SELECT Proj_ID
                  FROM ACT_Subproject
                  WHERE Parent_Proj = ?
              )
              AND R.Role_Type = 'Character'
              AND Cast_Status = 'Accepted'
            ) AS Cast ON P.Part_ID = Cast.Part_ID
        WHERE R1.Proj_ID IN (
              SELECT ?
            UNION
              # Parent
              SELECT Parent_Proj
              FROM ACT_Subproject
              WHERE Proj_ID = ? )
          AND R1.Role_Name = 'Registration'
          AND C1.Cast_Status = 'Accepted'
        GROUP BY P.Part_ID
        HAVING COUNT(Cast.Part_ID) = 0
        ORDER BY LastName, FirstName", {},
        $Proj_ID, $Proj_ID, $Proj_ID, $Proj_ID, $Proj_ID, $Proj_ID );
    $form->field( name => 'Part_ID', options => $actors );
    $form->tmpl_param( AllCast => 1 ) unless @$actors;
  
    # Role_ID depends on PrimaryRoles
    my $PrimaryRoles = $form->field( 'PrimaryRoles' );
    my $roles = $PrimaryRoles == 2 ?
      $self->{dbh}->selectall_arrayref(
        "SELECT R.Role_ID, Role_Name
        FROM ACT_Role AS R LEFT JOIN ACT_Is_Also AS I ON R.Role_ID = I.Is_Also_ID
        WHERE Proj_ID = ? AND Role_Type = ? AND I.Is_Also_ID IS NULL
        ORDER BY Role_Name",
        {}, $Proj_ID, 'Character' ) :
      $self->{dbh}->selectall_arrayref(
        "SELECT R.Role_ID, Role_Name
        FROM ACT_Role AS R
        WHERE Proj_ID = ? AND Role_Type = ?
        ORDER BY Role_Name",
        {}, $Proj_ID, 'Character' );
    $form->field( name => 'Role_ID', options => $roles );
  }

  if ( $doublecast ) {
    my $casts = $self->{dbh}->selectcol_arrayref(
      'SELECT Cast_Name FROM ACT_Casts
       WHERE Cast_ID > 0
       ORDER BY Cast_ID' );
    $form->tmpl_param( cast => [ map { { name => $_ } } @$casts ] );
  } else {
    $form->tmpl_param( cast => [ { name => "Actors" } ] );
  }

  my $ShowCast = $can_write ? ! $form->field('ShowCast') : 1;
  if ( $ShowCast ) {
    $form->tmpl_param( ShowCast => $ShowCast );
    my $ShowDelete = $self->{tmp}->{can_write} && $form->field( 'ShowDelete' );

    # R1 here is the 'primary role'
    # R2 is either the primary role or an 'IsAlso' role
    # We now assume there is a single (0,0,0) entry in
    # ACT_Is_Also so we can avoid LEFT JOINs
    my $chars = $self->{dbh}->selectall_arrayref(
      "SELECT R2.Role_ID, R2.Role_Name,
       C.Cast_ID, Part_ID, CONCAT( FirstName, ' ', LastName )
      FROM ACT_Role AS R2 
        LEFT JOIN (
          SELECT IF(I.Role_ID = 0,C.Role_ID,I.Is_Also_ID) AS Role_ID,
              C.Cast_ID, C.Part_ID, FirstName, LastName
            FROM ACT_Role NATURAL JOIN ACT_Cast AS C
              NATURAL JOIN ACT_Participant,
              ACT_Is_Also AS I
            WHERE Proj_ID = ? AND Role_Type = ?
              AND (I.Role_ID = C.Role_ID OR I.Role_ID = 0)
              AND (I.Cast_ID = 0 OR C.Cast_ID = 0 OR I.Cast_ID = C.Cast_ID)
          ) AS C ON R2.Role_ID = C.Role_ID
      WHERE R2.Proj_ID = ? AND R2.Role_Type = ?
      GROUP BY R2.Role_ID, C.Cast_ID, Part_ID
      ORDER BY R2.Role_Name, C.Cast_ID, LastName, FirstName", {},
      $Proj_ID, 'Character', $Proj_ID, 'Character' );

    my @chars;
    my $curchar;
    # init chars = [ char => Role_Name, cast = [ actors = [ name, Cast_ID, Role_ID, Part_ID ] ] ]
    for ( @$chars ) {
      my ( $Role_ID, $Role_Name, $Cast_ID, $Part_ID, $name ) = @$_;
      if ( ! $curchar || $curchar->{char} ne $Role_Name ) {
        $curchar = { char => $Role_Name, cast => $doublecast ? 
           [ { actors => [] }, { actors => [] } ] : [ { actors => [] } ] };
        push @chars, $curchar;
      }
      my $actrec = $ShowDelete ?
        { name => $name, Cast_ID => $Cast_ID, Role_ID => $Role_ID, Part_ID => $Part_ID } :
        { name => $name };
      if ( $doublecast ) {
        if ( defined($Cast_ID) ) {
          if ( $Cast_ID == 1 || $Cast_ID == 2 ) {
            push @{$curchar->{cast}->[$Cast_ID-1]->{actors}}, $actrec;
          } elsif ( $Cast_ID == 0 ) {
            push @{$curchar->{cast}->[0]->{actors}}, $actrec;
            push @{$curchar->{cast}->[1]->{actors}}, $actrec;
          } else {
            warn "Invalid Cast_ID '$Cast_ID'\n";
          }
        }
      } else {
        push @{$curchar->{cast}->[0]->{actors}}, $actrec;
      }
    }
    $form->tmpl_param( chars => \@chars );
  }
}

sub Reload {
  my $self = shift @_;
  my $form = $self->{form};
  my @params;
  for my $param ( qw(Proj_ID AllActors PrimaryRoles ShowCast ShowDelete Cast_ID) ) {
    my $val = scalar($form->field($param)) || '';
    push @params, $param, $val;
  }
  $self->GoTo( "/Cast", @params );
}

1;
