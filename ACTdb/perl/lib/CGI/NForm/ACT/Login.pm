package CGI::NForm::ACT::Login;
use strict;
use CGI;
use CGI::FormBuilder;

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    { # rm  =>  'DELETE',
      # edit => 'UPDATE',
      '' =>   'SELECT'
    };
}

# Support for the $params argument is optional, but necessary
# if you want to allow this form to be redirected.
# With FormBuilder 3.0, this functionality can be moved back
# to NForm and get_form() will be responsible only for specifying
# the fields and the template.
sub get_form {
  my ( $self, $op, $params )  = @_;
  $self->Connect();
  my $q = $self->{q} = ref($params) eq 'CGI' ? $params : new CGI($params);
  my $form = CGI::FormBuilder->new(
    action => "$ENV{SCRIPT_NAME}$self->{path_info}",
    method => 'POST',
    fields => [ 'Username', 'Password', 'Key', 'FirstName',
		'LastName', 'Email', 'nxtPI', 'nxtPA', 'EU' ],
    params => $q,
    template => "$self->{tmpldir}/Login.html",
    title => "ACT Login"
  );
  $form->field( name => 'Password', type => 'password' );
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'EU', type => 'hidden' );
  return $form;
}

sub authorize {
  return 1;
}

sub INSERT {
  return 0;
}

sub UPDATE {
}

sub DELETE {
}

sub SELECT {
  my $self = shift;
  my $form = $self->{form};
  if ( $form->submitted eq 'Login' ) {
    my $username = $form->field('Username');
    my $password = $form->field('Password');
    $self->Return if $self->check_passwd( $username, $password );
    return 0;
  } elsif ( $form->submitted eq 'Enter Key' ) {
    my $key = $form->field('Key') || '';
    $key =~ s/^(?:.*\D)?(\d+_[a-zA-Z0-9]{15}).*$/$1/;
    $form->sessionid($key);
    if ( $self->check_key( 'NOFWD' ) ) {
      my $Part_ID = $self->{auth}->{Part_ID};
      my $Username = $self->{dbh}->selectrow_array(
        'SELECT Username from ACT_Users
	 WHERE Part_ID = ?;', {}, $Part_ID );
      my $PI = $Username ? "/User/edit" : "/User/new";
      $self->GoTo( $PI );
    } else {
      push @{$self->{errors}}, "Invalid Key";
      my $rawkey = $form->field('Key');
      $self->Log("Invalid Key: '$rawkey'");
      return 0;
    }
  } else {
    my $LastName = $form->field('LastName') || '';
    my $FirstName = $form->field('FirstName') || '';
    my $Email = $form->field('Email') || '';
    if ( ! ( $LastName && $FirstName ) ) {
      push @{$self->{errors}},
	"Must specify Last Name and First Name";
      return 0;
    }
    my $Part_ID = 0;
    my $res = $self->{dbh}->selectall_hashref(
      'SELECT P.Email, P.Part_ID, P.Fam_ID, P.Status, U.Username
       FROM ACT_Participant AS P
       LEFT OUTER JOIN ACT_Users as U
       ON P.Part_ID = U.Part_ID
       WHERE P.LastName = ? AND P.FirstName = ?', 'Part_ID',
       {}, $LastName, $FirstName );
    die "Expected hashref" unless $res && ref($res);
    my @ematch = grep lc($res->{$_}->{Email}) eq lc($Email), keys %$res;
    if ( @ematch ) {
      warn "Duplicate Last/First/Email on $LastName/$FirstName/$Email\n"
	if @ematch > 1;
      $Part_ID = shift @ematch;
    } elsif ( $Email ) {
      PICK_PART:
      for my $PID ( keys %$res ) {
	if ( ! $res->{$PID}->{Username} ) {
	  my $fam = $self->{dbh}->selectall_hashref(
	    "SELECT P.Part_ID, U.Username FROM ACT_Participant AS P
	     LEFT OUTER JOIN ACT_Users as U
	     ON P.Part_ID = U.Part_ID
	     WHERE P.Fam_ID = ? AND P.Email = ?
	      AND P.Status = 'Adult';",
	     'Part_ID', {}, $res->{$PID}->{Fam_ID}, $Email );
	  die "Expected arrayref" unless $fam && ref($fam);
	  for my $fPID ( keys %$fam ) {
	    # The e-mail address matches someone in this family
	    if ( $res->{$PID}->{Status} eq 'Adult' ) {
	      if ( ! $res->{$PID}->{Email} ) {
		# I'm not currently a user, my record does not
		# include an e-mail address, but the e-mail address
		# submitted matches an adult in my family, so I'll
		# update my record's e-mail to match what I
		# submitted.
		$self->{dbh}->do(
		  'UPDATE ACT_Participant
		   SET Email = ? WHERE Part_ID = ?;',
		  {}, $Email, $PID );
		$Part_ID = $PID;
		$self->Log("E-mail copied from spouse: Part_ID=$Part_ID" );
		last PICK_PART;
	      } elsif ( ! $fam->{$fPID}->{Username} ) {
		# I'm not currently a user, my record includes
		# an e-mail address that differs from that
		# submitted, but the submitted address matches
		# that of an adult in my family who is also
		# not yet a user. This is most likely because
		# we have the addresses swapped, so swap 'em.
		$self->{dbh}->do(
		  'UPDATE ACT_Participant
		   SET Email = ? WHERE Part_ID = ?;',
		   {}, $Email, $PID );
		$self->{dbh}->do(
		  'UPDATE ACT_Participant
		   SET Email = ? WHERE Part_ID = ?;',
		   {}, $res->{$PID}->{Email}, $fPID );
		$Part_ID = $PID;
		$self->Log( "E-mail swapped: Part_ID = $Part_ID" );
		last PICK_PART;
	      } else {
		# We clearly have data on this family, but
		# something isn't quite right.
	      }
	    } else {
	      push @{$self->{errors}},
		"For the purposes of sign on, please use the name
		 of an adult in the family. Adults can then
		 update the records of their children.";
	      return 0;
	    }
	  }
	}
      }
    }
    if ( $Part_ID && $Email ) {
      my $key = $self->create_ticket( $Part_ID, '', time()+(72*3600) );
    unless ( $CGI::NForm::ACT::Config{smtp_server} ) {
	$self->error( "The key '$key' has been mailed to you. Enter it" );
	return 0;
      } else {
	$self->mail_key( $key, $res->{$Part_ID}->{Username} );
	$self->GoTo( "/LoginKey" );
      }
    }
    # Advance to full /SignUp
    if ( $form->field('EU') ) {
      $self->error(
        'The values you entered do not match our records. Please ' .
	'try again or contact the ' .
	'<a href="mailto:webmaster@act.arlington.ma.us">webmaster</a> ' .
	'for assistance.' );
    } else {
      $self->GoTo( "/NewFamily", LastName => $LastName, FirstName => $FirstName,
		    Email => $Email );
    }
    return 0;
  }
}

sub InitFields {}

1;
