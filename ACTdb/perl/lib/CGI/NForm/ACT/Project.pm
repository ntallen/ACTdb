package CGI::NForm::ACT::Project;
use strict;
use CGI;
use CGI::FormBuilder;

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    {
      new => 'INSERT',
      edit => 'UPDATE',
      '' => 'SELECT'
    };
}

# Support for the $params argument is optional, but necessary
# if you want to allow this form to be redirected.
# With FormBuilder 3.0, this functionality can be moved back
# to NForm and get_form() will be responsible only for specifying
# the fields and the template.
sub get_form {
  my ( $self, $op, $params )  = @_;
  $self->Connect();
  my $q = $self->{q} = ref($params) eq 'CGI' ? $params : new CGI($params);
  my $form = CGI::FormBuilder->new(
    action => "$ENV{SCRIPT_NAME}$self->{path_info}",
    method => 'POST',
    fields => [ qw(nxtPI nxtPA Proj_ID Proj_Type Proj_Status
                Proj_Name Proj_Ages Proj_Desc Proj_Meetings
                Proj_Rehearsals Proj_Performances Proj_Start Proj_End
                Cancel_100 Cancel_75 Role_Status Role_Fee Role_Limit
                Wait_Limit Board_Reserve Role_Due Proj_URL
                Proj_Leader Proj_LdrTitle Role_BPT_ID Proj_Abbr) ],
    params => $q,
    template => "$self->{tmpldir}/Project.html",
    title => "Create or Update an ACT Project",
  );
  my $static = ( $op eq 'SELECT' );
  $form->submit( $static ? [ "Back" ] : [ "Submit", "Back" ] );
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'Proj_ID', type => 'hidden' );
  $form->field( name => 'Proj_Type', options =>
              [ 'Show', 'Workshop', 'FamilyEvent', 'Group' ],
          required => 1, static => $static );
  $form->field( name => 'Proj_Status', options =>
              [ 'Preview', 'Active', 'Archive' ], value => 'Preview',
          static => $static );
  $form->field( name => 'Role_Status', options =>
              [ 'None', 'Open', 'Closed', 'Limited', 'Moderated' ],
              required => 1, static => $static );
  $form->field( name => 'Proj_Name', size => 50, static => $static );
  $form->field( name => 'Proj_URL', size => 50, static => $static );
  $form->field( name => 'Role_Limit', size => 3, static => $static );
  $form->field( name => 'Role_Min', size => 3, static => $static );
  $form->field( name => 'Wait_Limit', size => 3, static => $static );
  $form->field( name => 'Board_Reserve', size => 3, static => $static );
  # $form->field( name => 'Role_BPT_ID', size => 6, static => $static );
  $form->field( name => 'Proj_Abbr', size => 20, static => $static );
  
  $form->field( name => 'Role_Fee', static => $static );
  $form->field( name => 'N_Casts', options => [ 1, 2 ], value => 1,
    static => $static );
  $form->field( name => 'RegAdults', options => [ 'Adults' ], value => 0,
    static => $static );
  for ( qw(Proj_Start Proj_End Cancel_100 Cancel_75) ) {
    $form->field( name => $_, validate => 'DATE', size => 12,
      static => $static );
  }
  
  for ( qw(Proj_Desc Proj_Meetings Proj_Rehearsals
            Proj_Performances) ) {
    $form->field( name => $_, type => 'textarea', cols => 50,
      rows => 4, wrap => 'physical', static => $static );
  }
  for ( qw(Proj_Ages Proj_Leader Proj_LdrTitle Role_Due ) ) {
    $form->field( name => $_, static => $static );
  }
  return $form;
}

sub authorize {
  my ( $self, $op ) = @_;
  if ( $self->check_key ) {
    # return 0 unless $self->has_role( 1, 'Board' );
    if ( $op eq 'INSERT' ) {
      return 0
        unless $self->Has_Privilege( 0, 'ProjectAdmin', 2 );
    } else {
      my $form = $self->{form};
      my $Proj_ID = $form->field('Proj_ID');
      $self->fatal("No Project specified (auth)") unless $Proj_ID;
      my $rolepriv =
        $self->Get_Privilege_Value( $Proj_ID, 'ProjectAdmin' );
      return 0 unless $rolepriv >= 1;
      return 0 if $op eq 'UPDATE' && $rolepriv < 2;
    }
    return 1;
  }
  return 0;
}

sub INSERT {
  my $self = shift;
  my $form = $self->{form};
  if ( $form->submitted eq 'Back' ) {
    $self->Return;
  }
  if ( $form->validate ) {
    my @PKWs = qw(Proj_Type Proj_Status
                Proj_Name Proj_Ages Proj_Desc Proj_Meetings
                Proj_Rehearsals Proj_Performances Proj_URL N_Casts
                Proj_Leader Proj_LdrTitle Proj_Abbr);
    my @DKWs = qw(Proj_Start Proj_End Cancel_100 Cancel_75);
    my @RKWs = qw(Role_Status Role_Due);
    my @RoptKWs = qw(Role_Limit Wait_Limit Board_Reserve Role_Min Role_Fee);
    my @Pvals = map $form->field($_) || '', @PKWs;
    push( @PKWs, 'Proj_Schedule' );
    push( @Pvals, '' );
    my @Dvals = map {
          my $val = $form->field($_) || '00/00/0000';
          if ( $val =~ m|^(\d+)/(\d+)/(\d\d\d\d)$| ) {
            $val = "$3-$1-$2";
          } else {
            $self->error(
              "Must specify date as mm/dd/yyyy for field $_" );
          }
          $val;
        } @DKWs;
    return 0 if @{$self->{errors}};
    my @Rvals = map $form->field($_) || '', @RKWs;
    for my $KW ( @RoptKWs ) {
      my $val = $form->field($KW) || '';
      if ( $val ne '' ) {
        push( @RKWs, $KW );
        push( @Rvals, $val );
      }
    }
    { my $val = $form->field('RegAdults') || '';
      $val = ($val eq '') ? 0 : 1;
      push(@RKWs, 'RegAdults');
      push(@Rvals, $val);
    }
    my $icmd = join ' ', 'INSERT INTO ACT_Project (',
      join( ', ', @PKWs, @DKWs ), ') VALUES (',
      join( ', ', map '?', @PKWs, @DKWs ), ')';
    eval { $self->{dbh}->do( $icmd, {}, @Pvals, @Dvals ); };
    if ( $@ ) {
      if ( $@ =~ m/Duplicate entry/ ) {
        $self->error(
          "A Project of that name already exists. You must select a new name.");
        return 0;
      }
      $self->fatal( "Error creating project: $@",
        "Query was: $icmd",
        "Values were: " . join ", ", map "'$_'", @Pvals, @Dvals
      );
    }
    my $Proj_ID = $self->{dbh}->{mysql_insertid};
    if ( $Rvals[0] ne 'None' ) {
      my $rcmd = join ' ', 'INSERT INTO ACT_Role',
        '( Proj_ID, Role_Name, Role_Type,',
        join( ', ', @RKWs ), ') VALUES ( ?, ?, ?,',
        join( ', ', map '?', @RKWs ), ')';
      eval {
        $self->{dbh}->do( $rcmd, {}, $Proj_ID, 'Registration', 'Category', @Rvals );
      };
      if ($@) {
        $self->fatal( "Error creating Registration Role: $@", "Query was:" . $rcmd,
           "Values were: $Proj_ID, 'Registration', 'Category', " .
           join ", ", map "'$_'", @Rvals );
      }
      my $Role_ID = $self->{dbh}->{mysql_insertid};
      eval {
        $self->{dbh}->do(
          'INSERT IGNORE INTO ACT_Group
          ( Proj_ID, Grp_Name, Grp_Type, Grp_Desc, Grp_Order )
          VALUES ( ?, ?, ?, ?, ? )', {},
          $Proj_ID, 'All', 'Normal', 'All', 5 );
        my $Grp_ID = $self->{dbh}->{mysql_insertid};
        $self->{dbh}->do(
          'INSERT IGNORE INTO ACT_Grp_Mem (Grp_ID, Role_ID)
          VALUES ( ?, ? )', {}, $Grp_ID, $Role_ID );
      };
      if ( $@ ) {
        $self->fatal( "Error creating 'All' scene: $@" );
      }
      eval {
        $self->{dbh}->do(
          'INSERT IGNORE INTO ACT_Role ( Proj_ID, Role_Name, Role_Type, Role_Status )
           VALUES ( ?, ?, ?, ? );', {},
          $Proj_ID, 'Parent', 'Category', 'Closed' );
      };
      if ($@) {
        $self->fatal( "Error creating Parent Role: $@" );
      }
    }
    
    my $Proj_Name = $form->field('Proj_Name');
    $self->Log( $Proj_Name, "/Project/edit?Proj_ID=$Proj_ID" );
    $self->GoTo( "/Production", Proj_ID => $Proj_ID );
  } else {
    $self->error("Some field(s) contained invalid data" );
  }
  return 0;
}

sub UPDATE {
  my $self = shift;
  my $form = $self->{form};
  if ( $form->submitted eq 'Back') {
    $self->Return;
  }
  if ( $form->validate ) {
    my $Proj_ID = $form->field('Proj_ID');
    my @PKWs = qw(Proj_Type Proj_Status
                Proj_Name Proj_Ages Proj_Desc Proj_Meetings
                Proj_Rehearsals Proj_Performances Proj_URL N_Casts
                Proj_Leader Proj_LdrTitle Proj_Abbr);
    my @DKWs = qw(Proj_Start Proj_End Cancel_100 Cancel_75);
    my @RKWs = qw(Role_Status Role_Due);
    my @RoptKWs = qw(Role_Limit Wait_Limit Board_Reserve Role_Min Role_Fee);
    my @Pvals = map $form->field($_) || '', @PKWs;
    push( @PKWs, 'Proj_Schedule' );
    push( @Pvals, '' );
    my @Dvals = map {
          my $val = $form->field($_) || '00/00/0000';
          if ( $val =~ m|^(\d+)/(\d+)/(\d\d\d\d)$| ) {
            $val = "$3-$1-$2";
          } else {
            $self->error(
              "Must specify date as mm/dd/yyyy for field $_" );
          }
          $val;
        } @DKWs;
    return 0 if @{$self->{errors}};
    my @Rvals = map $form->field($_) || '', @RKWs;
    for my $KW ( @RoptKWs ) {
      my $val = $form->field($KW) || '';
      $val = '0' if $val eq '';
      push( @RKWs, $KW );
      push( @Rvals, $val );
    }
    { my $val = $form->field('RegAdults') || '';
      $val = ($val eq '') ? 0 : 1;
      push(@RKWs, 'RegAdults');
      push(@Rvals, $val);
    }
    my $Role_ID = $self->{dbh}->selectrow_array(
      'SELECT Role_ID from ACT_Role WHERE Proj_ID = ? AND Role_Name = ?',
      {}, $Proj_ID, 'Registration' );
    if ( $Role_ID && $form->field('Role_Status') eq 'None' ) {
      $self->error( "Cannot delete Roles from here" );
      return 0;
    }
    my $ucmd = join ' ', 'UPDATE ACT_Project SET',
      join( ', ', map( "$_ = ?", @PKWs, @DKWs ) ),
      'WHERE Proj_ID = ?';
    eval { $self->{dbh}->do( $ucmd, {}, @Pvals, @Dvals, $Proj_ID ); };
    if ( $@ ) {
      if ( $@ =~ m/Duplicate entry/ ) {
        $self->error(
          "A Project of that name already exists.");
        return 0;
      }
      $self->fatal( "Error updating project: $@" );
    }
    if ( $form->field('Role_Status') ne 'None' ) {
      if ( $Role_ID ) {
        $self->{dbh}->do(
          join( ' ',
            'UPDATE ACT_Role SET',
            join( ', ', map "$_ = ?", @RKWs ),
            'WHERE Role_ID = ?' ),
          {}, @Rvals, $Role_ID );
      } else {
        my $rcmd = join ' ', 'INSERT INTO ACT_Role',
          '( Proj_ID, Role_Name, Role_Type,',
          join( ', ', @RKWs ), ') VALUES ( ?, ?, ?,',
          join( ', ', map '?', @RKWs ), ')';
        eval {
          $self->{dbh}->do( $rcmd, {}, $Proj_ID, 'Registration', 'Category', @Rvals );
        };
        if ($@) {
          $self->fatal( "Error creating Registration Role: $@" );
        }
      }
    }
    $self->Return;
    # $self->GoTo( "/Register2", Role_ID => $Role_ID );
  }
  $self->error( "Some fields have incorrect values:" );
  return 0;
}

sub DELETE {}

sub SELECT {
  my $self = shift;
  $self->Return;
}

sub InitFields {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  my $Proj_ID = $form->field('Proj_ID');
  if ( $Proj_ID ) {
    my @KWs = qw(Proj_Type Proj_Status
                Proj_Name Proj_Ages Proj_Desc Proj_Meetings
                Proj_Rehearsals Proj_Performances Role_Status
                Role_Fee Role_Limit Role_Due
                Wait_Limit Board_Reserve Proj_URL N_Casts
                Proj_Leader Proj_LdrTitle Role_Min
                Proj_Abbr RegAdults);
    my @DKWs = qw(Proj_Start Proj_End Cancel_100 Cancel_75);
    my $cmd = join ' ', 'SELECT',
      join( ', ', @KWs, @DKWs ), 'FROM ACT_Project LEFT OUTER JOIN ACT_Role',
      'ON ACT_Project.Proj_ID = ACT_Role.Proj_ID AND',
      'ACT_Role.Role_Name = ?',
      'WHERE ACT_Project.Proj_ID = ?';
    my @vals = $self->{dbh}->selectrow_array( $cmd, {},
                            'Registration', $Proj_ID );
    if (@vals != @KWs + @DKWs) {
      warn "Project::init_fields vals returned " . scalar(@vals) . " values, expected " .
        (scalar(@KWs) + scalar(@DKWs)) . "\n";
    }
    # warn "Project::init_fields query was '$cmd'\n";
    # warn "Project::init_fields vals returned were: " .
    #   join(', ', map "'$_'", @vals), "\n";
    for my $field ( @KWs ) {
      my $val = shift(@vals);
      $val = ($val == 0) ? '' : 'Adults' if ($field eq 'RegAdults');
      warn "Project::init_fields Role_Fee is '$val'\n" if ($field eq 'Role_Fee');
      $form->field( name => $field, value => $val || '',
              force => 1 );
    }
    for my $field ( @DKWs ) {
      my $val = shift(@vals) || '0000-00-00';
      if ( $val eq '0000-00-00' ) {
        $val = '';
      } elsif ( $val =~ m/^(\d\d\d\d)-(\d\d)-(\d\d)$/ ) {
        $val = "$2/$3/$1";
      } else {
        $self->fatal("Unexpected data value from mysql: '$val'");
      }
      $form->field( name => $field, value => $val, force => 1 );
    }
    unless ( $form->field('Role_Status') ) {
      $form->field( name => 'Role_Status', value => 'None', force =>
      1 );
    }
  } elsif ( $op ne 'INSERT' ) {
    $self->fatal("No Project Specified");
  }
}

1;
