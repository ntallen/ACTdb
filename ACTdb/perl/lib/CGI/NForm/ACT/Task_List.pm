package CGI::NForm::ACT::Task_List;
use strict;
use CGI;
use CGI::FormBuilder;

# Created 2/19/2009

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    {
      '' => 'SELECT'
    };
}

# 
sub InitForm {
  my ( $self, $form, $op )  = @_;
  $self->Connect();
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'Proj_ID', type => 'hidden' );
  $form->field( name => 'Task_ID', type => 'hidden' );
  my $Proj_ID = $form->field('Proj_ID');
  $self->fatal("Invalid or missing Proj_ID") if $Proj_ID !~ /^\d+$/;
  my $Proj_Name = $self->{dbh}->selectrow_array(
    "SELECT Proj_Name FROM ACT_Project WHERE Proj_ID = ?", {}, $Proj_ID );
  $self->fatal("Invalid Proj_ID") unless $Proj_Name;
  $form->title("Task List for $Proj_Name"); # This should be qualified by project
  $form->template( "$self->{tmpldir}/Task_List.html" );
  my $Task_IDs = $self->{dbh}->selectall_arrayref(
    "SELECT Task_ID FROM ACT_Task
     WHERE Proj_ID = ?", {}, $Proj_ID );
  $form->field( name => 'Done_ID', options => $Task_IDs, multiple => 1,
    type => 'checkbox', linebreaks => 1 );
}

sub authorize {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  if ( $self->check_key( EU => 1 )) {
    my $Proj_ID = $form->field('Proj_ID');
    my $TaskPriv = $self->Get_Privilege_Value( $Proj_ID, 'TaskAdmin' );
    $self->fatal("Not authorized to access tasks for this project") unless $TaskPriv;
    $self->{tmp}->{TaskPriv} = $TaskPriv;
    my $submit = ['Back'];
    push(@$submit, 'Mark Selected as Done') if $TaskPriv > 1;
    push(@$submit, 'Admin') if $TaskPriv >= 4;
    $form->submit($submit);
    return 1;
  }
  return 0;
}

sub INSERT {}

sub UPDATE {}

sub SELECT {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  my $Proj_ID = $form->field('Proj_ID');
  $self->Return if $form->submitted eq 'Back';
  if ( $form->submitted eq 'Admin' ) {
    $self->Call( '/Task_Admin', Proj_ID => $Proj_ID );
  } elsif ( $form->submitted eq 'Mark Selected as Done' ) {
    my @Task_IDs = $form->field('Done_ID');
    my $sth1 = $self->{dbh}->prepare(
      "UPDATE ACT_Task Set Task_Status = 'Done', Task_Date_Completed = CURRENT_DATE
       WHERE Proj_ID = ? AND Task_ID = ?");
    my $sth2 = $self->{dbh}->prepare(
      "INSERT INTO ACT_Task_Audit ( Proj_ID, Task_ID, Task_Status, TA_Date, Part_ID, TA_Desc )
       VALUES ( ?, ?, 'Done', CURRENT_DATE, ?, 'Multiple' )" );
    my $failure = 0;
    for my $Task_ID ( @Task_IDs ) {
      unless ( $sth1->execute( $Proj_ID, $Task_ID ) &&
               $sth2->execute( $Proj_ID, $Task_ID, $self->{auth}->{Part_ID} )) {
        $self->error( "Update of Proj_ID $Proj_ID Task_ID $Task_ID failed" );
        $failure = 1;
      }
    }
    return $failure;
  } else {
    my $Task_ID = $form->field('Task_ID');
    $self->Call( $self->{tmp}->{TaskPriv} > 1 ? "/Task_Detail/edit" : "/Task_Detail",
                  Proj_ID => $Proj_ID, Task_ID => $Task_ID );
  }
}

sub DELETE {}

# Called before form has been submitted
sub InitFields {
  # my ( $self, $op ) = @_;
  # my $form = $self->{form};
}

# Called after INSERT, UPDATE, SELECT, DELETE or InitFields
sub InitParams {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  my $Proj_ID = $form->field('Proj_ID');
  my $update = $self->{tmp}->{TaskPriv};
  my $tasks = $self->{dbh}->selectall_arrayref(
    "SELECT Tasks.Task_ID,
      IF(Task_Status = 'Done' OR ? < 2, 0, IF(? = 2,COALESCE(CanUpdate.Updt,0),1)),
      COALESCE(CanUpdate.Updt,0),
      IF(Task_Status = '',
         IF(Task_Date < CURRENT_DATE(),
            'Late',
            IF(Task_Date < DATE_ADD(CURRENT_DATE(),INTERVAL 7 DAY),
               'Soon',
               '')),
         Task_Status) AS Task_Status,
      Task_Date, Task_Action, Task_Category, Who
    FROM (
      SELECT ACT_Task.Task_ID, Task_Status,
        IF(Task_Status = 'Ongoing', 'Ongoing',
            DATE_ADD(Milestone_Date, INTERVAL Milestone_Delta DAY)) AS Task_Date,
        Task_Action, Task_Date_Completed, Task_Category,
        GROUP_CONCAT(Delegate ORDER BY Task_Del_Role SEPARATOR ' ') AS Who
      FROM ACT_Task NATURAL LEFT JOIN ACT_Task_Milestone
      LEFT JOIN (
        SELECT Task_ID, Task_Del_Role, CONCAT('<b>', Task_Del_Role, ':</b>', GROUP_CONCAT(Role_Abbr) ) AS Delegate
        FROM ACT_Task_Delegate JOIN ACT_Task_Abbr ON ACT_Task_Delegate.Abbr_ID = ACT_Task_Abbr.Abbr_ID
        WHERE ACT_Task_Delegate.Proj_ID = ?
        GROUP BY Task_ID, Task_Del_Role ) AS Who ON ACT_Task.Task_ID = Who.Task_ID
      WHERE ACT_Task.Proj_ID = ?
      GROUP BY Task_ID
    ) AS Tasks LEFT JOIN (
      SELECT T.Task_ID, 1 AS Updt
      FROM ACT_Task AS T
        JOIN ACT_Task_Delegate AS D ON T.Proj_ID = D.Proj_ID AND T.Task_ID = D.Task_ID
        JOIN ACT_Task_Abbr AS A ON D.Abbr_ID = A.Abbr_ID
        JOIN ACT_Cast AS C ON A.Role_ID = C.Role_ID
      WHERE T.Proj_ID = ? AND C.Part_ID = ?
        AND ( A.Cast_Note = '' OR A.Cast_Note = C.Cast_Note)
      GROUP BY T.Task_ID
    ) CanUpdate ON Tasks.Task_ID = CanUpdate.Task_ID
    ORDER BY Task_Date, Task_ID", {}, $update, $update, $Proj_ID, $Proj_ID, $Proj_ID, $self->{auth}->{Part_ID} );
  $tasks = $self->map_cols(
    [ qw(Task_ID update mine Task_Status Task_Date Task_Action Task_Category Who) ],
    $tasks );
  for my $task ( @$tasks ) {
    $task->{Task_Stat} = $task->{Task_Status} eq "N/A" ? "NA" : $task->{Task_Status};
  }
  $form->tmpl_param('Tasks', $tasks);
  
  my $Abbrs = $self->{dbh}->selectall_arrayref(
    "SELECT Role_Abbr,
      IF(ACT_Task_Abbr.Cast_Note = '',
         Role_Name,
         CONCAT(Role_Name, ':', ACT_Task_Abbr.Cast_Note)
      ) AS Role,
      CONCAT(FirstName, ' ', LastName) AS Name,
      ACT_Cast.Part_ID
    FROM ACT_Task_Abbr
      JOIN ACT_Role ON ACT_Task_Abbr.Role_ID = ACT_Role.Role_ID
      LEFT JOIN ACT_Cast ON ACT_Role.Role_ID = ACT_Cast.Role_ID
        AND ( ACT_Task_Abbr.Cast_Note = '' OR
              ACT_Task_Abbr.Cast_Note = ACT_Cast.Cast_Note )
      LEFT JOIN ACT_Participant ON ACT_Cast.Part_ID = ACT_Participant.Part_ID
    WHERE ACT_Task_Abbr.Proj_ID = ?
    ORDER BY Role_Abbr, LastName, FirstName", {}, $Proj_ID );
  my $ABL = [];
  my $current_abbr = '';
  my $current_abl;
  for my $Abbr ( @$Abbrs ) {
    my ( $Role_Abbr, $Role, $Name, $Part_ID ) = @$Abbr;
    if ( $Role_Abbr ne $current_abbr ) {
      $current_abbr = $Role_Abbr;
      $current_abl = [];
      push( @$ABL, { Role_Abbr => $Role_Abbr, Role => $Role, Names => $current_abl } );
    }
    push( @$current_abl, { Role_Abbr => $Role_Abbr, Name => $Name, Part_ID => $Part_ID } )
      if $Name;
  }
  $form->tmpl_param(Abbreviations => $ABL );
  
  my $Milestones = $self->{dbh}->selectall_arrayref(
    "SELECT Milestone, Milestone_Date, Milestone_Desc
      FROM ACT_Task_Milestone
      WHERE Proj_ID = ?
      ORDER BY Milestone_Date", {}, $Proj_ID );
  $Milestones = $self->map_cols( [ qw(Milestone Milestone_Date Milestone_Desc) ],
                  $Milestones);
  $form->tmpl_param(Milestones => $Milestones);
  $form->tmpl_param( update => 1 ) if $self->{tmp}->{TaskPriv} > 1;
}

1;
