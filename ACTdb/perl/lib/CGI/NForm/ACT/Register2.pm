package CGI::NForm::ACT::Register2;
use strict;
use CGI;
use CGI::FormBuilder;
use Mail::Sendmail;
use Text::Wrap;

# NOTES:
# -If role is closed, say so immediately. Allow continuation
#  if registrar.
# -Make sure Role_ID is valid (Role_Name = 'Registration')

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    {
      '' => 'INSERT'
    };
}

# Support for the $params argument is optional, but necessary
# if you want to allow this form to be redirected.
# With FormBuilder 3.0, this functionality can be moved back
# to NForm and get_form() will be responsible only for specifying
# the fields and the template.
sub InitForm {
  my ( $self, $form, $op )  = @_;
  $self->Connect();
  $form->title('ACT Registration');
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'Role_ID', type => 'hidden' );
  $form->field( name => 'Fam_ID', type => 'hidden' );
  $form->field( name => 'Mail_ID', type => 'hidden',
    multiple => 1 );
  $form->field( name => 'Part_ID', type => 'text' );
  $form->field( name => 'login', type => 'hidden' );
  # Part_ID will be redefined as either a select field or hidden later
  $form->field( name => 'Step', type => 'hidden', value => 0 );
  for my $field ( qw(FirstName LastName FamilyName Email HomePhone) ) {
    $form->field( name => $field, type => 'text' );
  }
  $form->field( name => 'HomePhone', validate => '/^\d\d\d[ -.]?\d\d\d[ -.]?\d\d\d\d$/',
    comment => '###-###-####', required => 0 );
  $form->field( name => 'Username', type => 'text' );
  $form->field( name => 'Password', type => 'password' );
  $form->template( "$self->{tmpldir}/Register2.html" );
  $form->action( "$ENV{SCRIPT_NAME}$self->{path_info}#CurStep" );
  return $form;
}

# If Fam_ID is specified, it had better be your family unless you're
# on the board. If it isn't specified, set it to your family.
# If Part_ID is specified, it must be in Fam_ID.
sub authorize {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  my $Role_ID = $form->field('Role_ID');
  $self->fatal("No Project Specified for Registration") unless $Role_ID;
  return 1 if $form->submitted eq 'Cancel';
  my ( $Role_Status, $Job_Committee, $Role_Name, $Proj_Name,
        $Proj_Type, $Proj_ID, $RegAdults ) =
    $self->{dbh}->selectrow_array(
    'SELECT Role_Status, Job_Committee, Role_Name, Proj_Name,
       Proj_Type, ACT_Role.Proj_ID, RegAdults
     FROM ACT_Role NATURAL JOIN ACT_Project
     WHERE Role_ID = ?',
    {}, $Role_ID );
  $self->fatal("Project undefined") unless $Role_Status;
  $self->fatal("Invalid Role_ID")
    unless $Role_Name eq 'Registration';
  $self->{tmp} = {};
  $self->{tmp}->{reg_ok} = 1;
  $self->{tmp}->{RegAdults} = $RegAdults;
  if ( $Role_Status eq 'Closed' ) {
    my $qual = $Role_Name eq 'Registration' ? '' : 'Registration for ';
    $self->error("$qual$Role_Name for $Proj_Name is closed");
    $self->{tmp}->{reg_ok} = 0;
    # Can't check privileges here because we haven't gone through
    # check_key yet.
  }
  if ( $self->check_key( 'NOFWD' ) ) {
    # Now we're logged in, so beyond Step 1
    my $Step = $form->field('Step') || 0;
    $form->field(name=>'Step', value => 2, force => 1)
      if $Step < 2;
    my $My_Part_ID = $self->{auth}->{Part_ID};
    $self->fatal("No auth Part_ID defined") unless $My_Part_ID;
    my ( $My_Fam_ID, $My_Status, $My_Name ) =
      $self->{dbh}->selectrow_array(
        "SELECT Fam_ID, Status, CONCAT(FirstName,' ',LastName)
         FROM ACT_Participant where Part_ID = ?",
        {}, $My_Part_ID );
    unless ( defined $My_Fam_ID && defined $My_Status ) {
      $self->fatal("No Fam_ID or Status defined for user Part_ID = $My_Part_ID");
    }
    $form->tmpl_param( yourname => $My_Name );
    my $Fam_ID = $form->field('Fam_ID');
    my $Part_ID = $form->field('Part_ID');
    if ( defined($Part_ID) && $Part_ID ne '' ) {
      unless ( $Part_ID =~ m/^\d+$/ && $Part_ID ) {
        $self->fatal("Invalid Part_ID specified");
      }
      my $Part_Fam_ID = $self->{dbh}->selectrow_array(
        'SELECT Fam_ID FROM ACT_Participant where Part_ID = ?',
        {}, $Part_ID );
      unless ( defined $Part_Fam_ID ) {
        $self->fatal("No Fam_ID defined for user Part_ID = $Part_ID");
      }
      $Fam_ID = $Part_Fam_ID unless $Fam_ID;
      if ( $Part_Fam_ID != $Fam_ID ) {
        $self->fatal("Form Fam_ID ($Fam_ID) does not match Participant's Fam_ID ($Part_Fam_ID)" );
      }
    }

    if ( ! $self->{tmp}->{reg_ok} &&
         $self->Has_Privilege( $Proj_ID, 'OverrideClosed', 1) ) {
      $self->{tmp}->{reg_ok} = 1;
    }
    
    # my $is_admin = $self->has_role2( $Role_ID, 'Registrar' );
    #if ( $Role_Status eq 'Closed' && ( ! $is_admin || ! $form->submitted ) ) {
    #  my $qual = $Role_Name eq 'Registration' ? '' : 'Registration for ';
    #  $self->error("$qual$Role_Name for $Proj_Name is closed");
    #}
    # $self->{tmp}->{reg_ok} = 1 if $Role_Status ne 'Closed' || $is_admin;
    
    $Fam_ID = $My_Fam_ID unless $Fam_ID;
    if ( $Fam_ID != $My_Fam_ID &&
         ! $self->Has_Privilege( $Proj_ID, 'RegisterOthers', 1 ) ) {
      $self->fatal("You are not authorized to register for another family");
    }
    $form->field( name => 'Fam_ID', value => $Fam_ID, force => 1 );
    # if ( $Fam_ID == $My_Fam_ID ) {
    #   $form->field( name => 'Mail_ID', value => $My_Part_ID, force => 1 );
    # }
    $self->{tmp}->{My_Fam_ID} = $My_Fam_ID;
    $self->{tmp}->{My_Part_ID} = $My_Part_ID;

    ### This should key off of Role_Type, not Proj_Type
    ### But actually, Event registration should probably take place
    ### in an entirely separate module
    if ( $Proj_Type eq 'FamilyEvent' ) {
      $self->fatal("FamilyEvents should be handled separately!");
    }
  }
  return 1;
}

# hidden param Step is set to current step number
# tmpl_params step1-5 => set if step is completed
# step1 is login
#   if step1, then yourname => defined if logged in
# step2 is verify family info
#   set if family info has been checked
# step3 is select a child and register
# step4 is choose e-mail for confirmation
# step5 is confirmation

sub InitParams {
  my $self = shift;
  my $My_Fam_ID = $self->{tmp}->{My_Fam_ID};
  my $My_Part_ID = $self->{tmp}->{My_Part_ID};
  my $form = $self->{form};
  my $Part_ID = $form->field('Part_ID');
  my $Role_ID = $form->field('Role_ID');
  my $Fam_ID = $form->field('Fam_ID');
  my $Step = $form->field('Step') || 1;
  my $login = $form->field('login') || 0;
  
  my @TKWs = qw( Role_Name Proj_Type Proj_Name Role_Fee Cancel_100
      Cancel_75 Proj_Ages Proj_Desc Proj_Meetings Proj_Rehearsals
      Proj_Performances Role_Due);
  my $cmd = join ' ', 'SELECT',
    join( ', ', @TKWs ), 'FROM ACT_Role, ACT_Project',
    'WHERE ACT_Project.Proj_ID = ACT_Role.Proj_ID AND',
    'Role_ID = ?';
  my $vals = $self->{dbh}->selectrow_hashref( $cmd, {},
                          $Role_ID );
  if ( $vals->{Proj_Ages} ) {
    my $ages = $vals->{Proj_Ages};
    $ages =~ s/^\s+//;
    $ages =~ s/\s+$//;
    $ages =~ s/\s+to\s+/-/;
    $ages =~ s/\s*-\s*/&ndash;/;
    $vals->{Proj_Ages} = $ages;
  }
  $vals->{Role_Due} =~ s/^\s*due\s+by\s+// if $vals->{Role_Due};
  $vals->{Cancel_100} =~ s/0000-00-00//;
  $vals->{Cancel_75} =~ s/0000-00-00//;
  $vals->{Refund} = 1 if $vals->{Cancel_100} || $vals->{Cancel_75};
  $vals->{NrowsFee} = 1 +
    ($vals->{Cancel_100} ? 1 : 0) +
    ($vals->{Cancel_75} ? 1 : 0) +
    ($vals->{Refund} ? 1 : 0);
  for my $kw ( keys %$vals ) {
    my $val = $vals->{$kw};
    if ( defined $val ) {
      $form->tmpl_param( $kw => $val );
    }
  }

  if ( $login == 1 ) {
    $form->tmpl_param( forgot => 1 );
  } elsif ( $login == 2 ) {
    $form->tmpl_param( newfamily => 1 );
    $form->field( name => 'FamilyName', required => 1,
      jsmessage => "Participant's last name is required" );
    $form->field( name => 'FirstName', required => 1,
      jsmessage => "Parent's first name is required" );
    $form->field( name => 'HomePhone', required => 1 );
  }

  if ( $Step == 3 ) {
    my $kids = $self->{tmp}->{RegAdults} ?
    $self->{dbh}->selectall_arrayref(
      'SELECT Part_ID, FirstName FROM ACT_Participant
       WHERE Fam_ID = ?', {}, $Fam_ID ) :
    $self->{dbh}->selectall_arrayref(
      'SELECT Part_ID, FirstName FROM ACT_Participant
       WHERE Fam_ID = ? AND Status = ?', {}, $Fam_ID, 'Child' );
    # $form->tmpl_param( child => 'child' ); # lingering hook from Event reg.
    $form->field( name => 'Part_ID', options => $kids, type => 'select' );
  } else {
    $form->field( name => 'Part_ID', type => 'hidden' );
  }
  
  if ( $Step > 3 ) {
    my $RegName = $self->{dbh}->selectrow_array(
      "SELECT CONCAT(FirstName, ' ', LastName) AS Name
       FROM ACT_Participant
       WHERE Part_ID = ?", {}, $Part_ID );
    $form->tmpl_param( RegName => $RegName );
  }

  if ( $Step == 4 ) {
    my $MIDs = $self->{dbh}->selectall_arrayref(
      "SELECT Part_ID, FullName, Email
       FROM ACT_Participant
       WHERE ( Fam_ID = ? AND Email != '' AND
              ( Status = 'Adult' OR Part_ID = ? ) )
         OR Part_ID = ?",
       {}, $Fam_ID, $Part_ID, $My_Part_ID );
    $MIDs = $self->map_cols( [ 'Part_ID', 'FullName', 'Email' ], $MIDs );
    my @MIDs = grep m/^\d+$/, $form->field('Mail_ID');
    my %MIDs;
    if ( @MIDs ) {
      %MIDs = map( ( $_ => 1 ), @MIDs );
    } else {
      %MIDs = map( ( $_->{Part_ID} => 1 ), @$MIDs );
    }
    for ( @$MIDs ) {
      # $_->{Sel} = ' checked="checked"';
      $_->{Sel} = $MIDs{$_->{Part_ID}} ? ' checked="checked"' : '';
    }
    $form->tmpl_param( Mail_IDS => $MIDs );
    $form->tmpl_param( adminreg => 1 );
  }

  $form->tmpl_param( reg_ok => 1 ) if $self->{tmp}->{reg_ok};
  
  for ( my $i = 1; $i < $Step; ++$i ) {
    $form->tmpl_param( "step$i" => 1 );
  }
  $form->tmpl_param( "Step_$Step" => 1 );
  $form->tmpl_param( "child" =>
    $self->{tmp}->{RegAdults} ? 'participant' : 'child' );
  $form->tmpl_param( "cchild" =>
    $self->{tmp}->{RegAdults} ? 'Participant' : 'Child' );
  $form->tmpl_param( "FamilyMember" =>
    $self->{tmp}->{RegAdults} ? 'Family Member' : 'Child' );
}

# Issues:
#   I want to eliminate auto-closing of Limited roles. This will
#   simplify reopening of auditions if people change their choices.
#   Instead, a count will be made at registration time to determine
#   if all the positions have been taken or not. One alternative
#   implementation I've considered is to switch from 'Limited' to
#   'Filled'.
#
#   Without a Filled state, the following sequence:
#     All spots and wait list fills
#     applicant gets auto-rejected due to being filled
#     Someone withdraws
#     another applicant gets added to the waiting list
#   Or worse:
#     All spots and wait list fills
#     applicant gets auto-rejected due to being filled
#     Enough people withdraw so total drops below Role_limit
#     another applicant gets accepted before those on the waiting list
#
#   In reality, neither scenario is significant. The second can be
#   avoided by accepting waitlistees before withdrawing an applicant.
#   In either case, the size of the waiting list should be selected
#   to be larger than is every likely to be needed.
#
# With a Filled state, we would go to Filled whenever all the
# spots and waitlist are filled and no subsequent applications
# would be allowed. The only reasonable case for automatically
# removing the Filled state would be if a) the total drops
# below Role_limit and b) there is no one on the waitlist.
#
# Audition enrollment is the main place where this is likely to
# be useful. There we have no waitlist, so any withdrawal would
# reopen the slot, but this is true for the implementation
# above as well.
#
# Since we aren't using Limited in really competetive cases,
# this is probably not a high priority to change.

sub INSERT {
  my $self = shift;
  my $form = $self->{form};
  my $Step = $form->field('Step') || 1;
  my $Role_ID = $form->field('Role_ID');
  if ( $form->submitted eq 'Login' ) {
    my $Username = $form->field('Username');
    my $Password = $form->field('Password');
    if ( $self->check_passwd( $Username, $Password ) ) {
      # Use GoTo here to redo the authorization step that
      # pulls in Fam_ID, etc.
      $self->GoTo( '/Register2', Role_ID => $Role_ID, Step => 2 );
    }
    return 0;
  } elsif ( $form->submitted eq 'Forgot Username/Password' ) {
    $form->field( name => 'Step', value => 2, force => 1 );
    $self->Call( '/Register2', login => 1, Role_ID => $Role_ID );
    # $form->tmpl_param( forgot => 1 );
    # return 0;
  } elsif ( $form->submitted eq 'Find Username' ) {
    my $FirstName = $form->field('FirstName');
    my $LastName = $form->field('LastName');
    my $Email = $form->field('Email');
    if ( $self->forgot_password( $LastName, $FirstName, $Email ) ) {
      if ( $self->{tmp}->{key} ) {
        # special case for testing when key is not really mailed
        $self->GoTo( "/LoginKey", Key => $self->{tmp}->{key} );
      } else {
        $self->GoTo( "/LoginKey" );
      }
    } else {
      $form->tmpl_param( forgot => 1 );
      $form->tmpl_param( forgot_error =>
        "We were unable to locate a match in our records.
         You may try again or " );
    }
    return 0;
  } elsif ( $form->submitted eq 'Create a New Family Record' ) {
    $form->field( name => 'Step', value => 2, force => 1 );
    $self->Call( '/Register2', login => 2, Role_ID => $Role_ID );
    # $form->tmpl_param( newfamily => 1 );
    # return 0;
  } elsif ( $form->submitted eq 'Submit New Family Record' ) {
    if ( $form->validate ) {
      ### Copied verbatim from NewFamily.pm
      $self->FixPhoneEntry( 'HomePhone' );
      my @KWs = qw(FamilyName LastName FirstName HomePhone Email);
      my %vals = map( ( $_ => $form->field($_) || '' ), @KWs );
      $vals{LastName} ||= $vals{FamilyName};
      $self->{dbh}->do(
        'INSERT INTO ACT_Family ( FamilyName ) VALUES ( ? );',
        {}, $vals{FamilyName} );
      my $Fam_ID = $self->{dbh}->{mysql_insertid};
      $self->{dbh}->do(
        'INSERT INTO ACT_Address ( Fam_ID, HomePhone )
         VALUES ( ?, ? )', {}, $Fam_ID, $vals{HomePhone} );
      my $Addr_ID = $self->{dbh}->{mysql_insertid};
      $self->{dbh}->do(
        'UPDATE ACT_Family SET Addr_ID = ? WHERE Fam_ID = ?',
        {}, $Addr_ID, $Fam_ID );
      $self->{dbh}->do(
        "INSERT INTO ACT_Participant ( Fam_ID, Addr_ID, FullName,
         LastName, FirstName, Email )
         VALUES ( ?, ?, ?, ?, ?, ? )", {},
         $Fam_ID, $Addr_ID, "$vals{FirstName} $vals{LastName}",
         map $vals{$_}, qw(LastName FirstName Email) );
      my $Part_ID = $self->{dbh}->{mysql_insertid};
      unless ( $form->sessionid ) {
        my $key = $self->create_ticket( $Part_ID, $ENV{REMOTE_ADDR},
                          time()+600 );
        $form->sessionid($key);
      }
      $self->GoTo( "/User", Part_ID => $Part_ID );
    }
    # return 0;
  } elsif ( $form->submitted =~ m/^Add a (?:Child|Family Member)/i ) {
    $self->Call( "/Person/new", Fam_ID => $form->field('Fam_ID'), Status => 'Child' );
  } elsif ( ($form->submitted eq 'Continue' && $Step == 2) ||
              $form->submitted eq 'Update my Family Information' ) {
    $form->field( name => 'Step', value => 3, force => 1 );
    $self->Call( "/Family/edit", Fam_ID => $form->field('Fam_ID') );
  } elsif ( $form->submitted eq 'Continue' && $Step == 3 ) {
    my $Part_ID = $form->field('Part_ID');
    if ( $Part_ID ) {
      my ( $Age, $ClassYear, $Status ) = $self->{dbh}->selectrow_array(
        "SELECT
             IF(Birthdate>0,
                YEAR(NOW())-YEAR(Birthdate)-(MID(NOW(),6,5)<MID(Birthdate,6,5)),
                -1) AS Age, ClassYear, Status
         FROM ACT_Participant WHERE Part_ID = ?", {}, $Part_ID );
      if ( $Status eq 'Child' && ( $Age < 3 || $ClassYear < 2004 ) ) {
        $self->error( "You must specify the child's age and grade.",
          "Please select 'Update my Family Information' below" );
        return 0;
      }
      $form->field( name => 'Step', value => 4, force => 1 );
    } else {
      $self->error( "You must select a participant" );
    }
    return 0;
  } elsif ( $form->submitted eq 'Continue' && $Step == 4 ) {
    $form->field( name => 'Step', value => 5, force => 1 );
    return 0;
  } elsif ( $form->submitted =~ m/^Register Another /i ) {
    $form->field( name => 'Step', value => 3, force => 1 );
    return 0;
  } elsif ( $form->submitted eq 'Backup' ) {
    $form->field( name => 'Step', value => $Step-1, force => 1 );
    return 0;
  } elsif ( $form->submitted =~ m/^Back|Cancel|Done$/ ) {
    my $nxtPI = $form->field('nxtPI');
    $self->Return if $nxtPI;
    my ( $Proj_Type ) = $self->{dbh}->selectrow_array(
      'SELECT Proj_Type FROM ACT_Role NATURAL JOIN ACT_Project
      WHERE Role_ID = ?', {}, $Role_ID );
    my $uri;
    if ($ENV{SERVER_NAME} && $ENV{SERVER_NAME} =~ m/^reg\./) {
      $uri = "https://act.arlington.ma.us/" .
          (($Proj_Type eq 'Show') ?
            'shows/' :
            'programs/');
    } else {
      $uri = ($Proj_Type eq 'Show') ?
        '/auditions.html' :
        '/workshops.html';
    }
    print "Status: 302 Redirect\cM\cJLocation: $uri\cM\cJ\cM\cJ";
    exit(0);
  } elsif ( $form->submitted eq 'Register' && $form->validate ) {
    return 0 if @{$self->{errors}};
    my $Part_ID = $form->field('Part_ID');
    unless ( $Part_ID ) {
      $self->error( "You must select a participant" );
      return 0;
    }
    my $Fam_ID = $form->field('Fam_ID');
    my ( $Age, $ClassYear, $Status ) = $self->{dbh}->selectrow_array(
      "SELECT
           IF(Birthdate>0,
              YEAR(NOW())-YEAR(Birthdate)-(MID(NOW(),6,5)<MID(Birthdate,6,5)),
              -1) AS Age, ClassYear, Status
       FROM ACT_Participant WHERE Part_ID = ?", {}, $Part_ID );
    if ( $Status eq 'Child' && ( $Age < 3 || $ClassYear < 2004 ) ) {
      $self->error( "You must specify the child's age and grade.",
        "Please select 'Backup' below" );
      return 0;
    }

    my $Brd_Fam = $self->{dbh}->selectrow_array(
      'SELECT 1 FROM ACT_Role, ACT_Cast, ACT_Participant
       WHERE ACT_Role.Proj_ID = ? AND
             ACT_Role.Role_ID = ACT_Cast.Role_ID AND
             ACT_Cast.Part_ID = ACT_Participant.Part_ID AND
             ACT_Role.Role_Name = ? AND Fam_ID = ?', {},
      1, 'Board', $Fam_ID );
    $self->{dbh}->do(
     'LOCK TABLES ACT_Project READ, ACT_Cast WRITE,
                  ACT_Role WRITE, ACT_Invoice WRITE, ACT_Log WRITE,
                  ACT_Participant READ, ACT_Participant AS P2 READ,
                  ACT_Role AS R2 READ' );
    my ( $Role_Status, $Board_Reserve, $Role_Limit, $Wait_Limit, $Role_Fee,
         $Role_Name, $Proj_Name ) =
      $self->{dbh}->selectrow_array(
        'SELECT Role_Status, Board_Reserve, Role_Limit, Wait_Limit, Role_Fee,
         Role_Name, Proj_Name
         FROM ACT_Role, ACT_Project
         WHERE Role_ID = ? AND ACT_Role.Proj_ID = ACT_Project.Proj_ID',
        {}, $Role_ID );
    $self->fatal("Error accessing Project registrations") unless defined $Role_Limit;
    my $Cast_Status = 'Applied';
    my $Used_reserve = 0;
    my $Count;
    if ( $Role_Status eq 'Closed' && ! $self->{tmp}->{reg_ok} ) {
      $self->Log("Closed: $Role_Name for $Proj_Name", "Part_ID=$Part_ID");
      $self->{dbh}->do( 'UNLOCK TABLES');
      $self->fatal( "Registration for this offering is closed." );
    } elsif ( $Role_Status eq 'Limited' ) {
      $Count = $self->{dbh}->selectrow_array(
        "SELECT COUNT(Part_ID) FROM ACT_Cast
         WHERE Role_ID = ? AND Cast_Status != 'Withdrawn'
         GROUP BY Role_ID", {}, $Role_ID );
      $Wait_Limit = $Board_Reserve if $Wait_Limit < $Board_Reserve;
      if ( $Brd_Fam && $Board_Reserve ) {
        $Used_reserve = 1;
        $Board_Reserve--;
        $Cast_Status = 'Accepted';
      } elsif ( $Count + $Board_Reserve < $Role_Limit ) {
        # add as accepted
        $Cast_Status = 'Accepted';
      } elsif ( $Count + $Board_Reserve < $Role_Limit + $Wait_Limit ) {
        $Cast_Status = 'Waitlisted';
      } else {
        # $self->{dbh}->do(
        #   'UPDATE ACT_Role SET Role_Status = ? WHERE Role_ID = ?',
        #   {}, 'Closed', $Role_ID );
        $self->Log("Filled: $Role_Name for $Proj_Name", "Part_ID=$Part_ID");
        $self->{dbh}->do( 'UNLOCK TABLES');
        
        ### Handle this more gracefully through error message,
        ### template
        $self->fatal(
          $Wait_Limit ?
          "All available spots for this offering and on the waiting list
           have been filled, so we cannot accept any more registrations
           at this time." :
          "All available spots for this offering have been filled,
           so we cannot accept any more registrations at this time." );
      }
    } elsif ( $Role_Status eq 'Open' ) {
      $Cast_Status = 'Accepted';
    } # else for Closed and Moderated, they're added as Applied
    
    # Probably want to add Cast_ID, Cast_Note, Cast_Order when generalizing
    eval {
      $self->{dbh}->do(
        "INSERT INTO ACT_Cast ( Role_ID, Part_ID, Cast_Status, Invoice_ID, Cast_Time )
         VALUES ( ?, ?, ?, ?, IF(?,'0000-00-00 00:00:00',NOW()) )", {},
         $Role_ID, $Part_ID, $Cast_Status, 0, $Brd_Fam );
    };
    if ( $@ ) {
      if ( $@ =~ m/Duplicate entry/ ) {
        ### Use error message, don't go to RegConfirm
        $self->error("Participant has already registered for this project");
        $self->{dbh}->do('UNLOCK TABLES');
        #$self->GoTo( "/RegConfirm", Role_ID => $Role_ID, Part_ID =>
        #             $Part_ID, Cast_ID => 0,
        #             Reg_errmsg => "Participant has already registered for this project" );
        return 0;
      } else {
        $self->error("Insert Failed: $@");
        $self->{dbh}->do('UNLOCK TABLES');
        return 0;
      }
    }
    $self->Fixup_Parents( $Fam_ID );
    $self->Log("$Role_Name for $Proj_Name", "Part_ID=$Part_ID");
    my $Invoice_ID = $self->check_invoice( $Cast_Status, $Role_ID, $Part_ID,
                      $Role_Fee, "$Role_Name for $Proj_Name" );
    if ( $Role_Status eq 'Limited' ) {
      if ( $Used_reserve ) {
        $self->{dbh}->do(
          'UPDATE ACT_Role SET Board_Reserve = ? WHERE Role_ID = ?',
          {}, $Board_Reserve, $Role_ID );
      }
    }
    $self->{dbh}->do('UNLOCK TABLES');
    my $EConfirm = 0;
    
    # Now send an e-mail confirmation
    my $emails;
    my $vals = $self->RegConfirm( $Role_ID, $Part_ID, 0 );
    if ( 1 ) { # $CGI::NForm::ACT::Config{smtp_server} ) {
      my @MIDs = grep m/^\d+$/, $form->field('Mail_ID');
      my %msg;
      if ( @MIDs ) {
        my $cmd =
          "SELECT
           CONCAT(FirstName, ' ', LastName, ' <', Email, '>') AS Email
           FROM ACT_Participant WHERE Part_ID IN ( " .
          join( ', ', @MIDs ) .  " ) AND Email <> ''";
        eval {
          $emails = $self->{dbh}->selectcol_arrayref( $cmd );
        };
        if ( $@ ) {
          warn( "Error from mySQL: $@\n" );
          warn( "SQL: $cmd\n" );
          $msg{To} = 'webmaster@act.arlington.ma.us';
          $self->error(
            'An error occurred reading your e-mail
             selections. You will not receive an
             e-mail confirmation immediately, but
             your registration has been completed.
             Please contact webmaster@act.arlington.ma.us' );
        } else {
          $msg{To} = join(', ', @$emails);
          $msg{CC} = 'webmaster@act.arlington.ma.us';
        }
      } else {
        $msg{To} = 'webmaster@act.arlington.ma.us';
      }
      $msg{From} = 'webmaster@act.arlington.ma.us';
      $msg{Subject} = "ACT Registration Confirmation: $vals->{Proj_Name}";
      $msg{smtp} = $CGI::NForm::ACT::Config{smtp_server};
      $msg{Message} = $vals->{Message};
      if ( $self->sendmail_wrap( %msg ) ) {
        $EConfirm = 1;
        $self->Log( "Email Sent Role_ID=$Role_ID Part_ID=$Part_ID");
      }
    }

    $form->field( name => 'Step', value => 6, force => 1 );
    $form->tmpl_param( Emails =>
       [ map { $_ =~ s/</&lt;/;
               $_ =~ s/>/&gt;/;
               { Email => $_ }; } @$emails ] ); 

    # my $vals = $self->RegConfirm( $Role_ID, $Part_ID, 0 );
    for ( keys %$vals ) {
      $vals->{$_} = '' unless defined $vals->{$_};
      my $param = $_ eq 'Role_Due' ? 'Role_Due2' : $_;
      $form->tmpl_param( $param => $vals->{$_} );
    }
    return 0;
    # $self->GoTo( "/RegConfirm", Role_ID => $Role_ID, Part_ID =>
    #             $Part_ID, Cast_ID => 0, EConfirm => $EConfirm );
  }
  return 0;
}

sub UPDATE {}

sub DELETE {}

sub SELECT {}

sub InitFields {}

1;
