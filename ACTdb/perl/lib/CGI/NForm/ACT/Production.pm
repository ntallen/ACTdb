package CGI::NForm::ACT::Production;
use strict;
use CGI;
use CGI::FormBuilder;

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    {
      '' => 'SELECT'
    };
}

# Support for the $params argument is optional, but necessary
# if you want to allow this form to be redirected.
# With FormBuilder 3.0, this functionality can be moved back
# to NForm and get_form() will be responsible only for specifying
# the fields and the template.
sub InitForm {
  my ( $self, $form, $op ) = @_;
  $self->Connect();
  $form->field( name => 'Proj_ID', type => 'hidden' );

  my $Proj_ID = $form->field('Proj_ID');
  $self->fatal("No Proj_ID Specified")
    unless defined($Proj_ID) && $Proj_ID =~ m/^\d+$/;
  my ( $Proj_Name, $Proj_Leader, $Proj_LdrTitle, $Proj_Start, $Proj_End,
        $Proj_Status, $Date_Status, $Cancel_100, $Cancel_75,
        $Proj_Ages, $Proj_Desc, $Proj_Meetings, $Proj_Rehearsals,
        $Proj_Performances ) =
   $self->{dbh}->selectrow_array(
    'SELECT Proj_Name, Proj_Leader, Proj_LdrTitle,
     DATE_FORMAT(Proj_Start,"%c/%e/%Y"), DATE_FORMAT(Proj_End,"%c/%e/%Y"),
     Proj_Status,
     IF(Proj_Start = "0000-00-00","",IF(CURDATE()<Proj_Start,"Upcoming",IF(CURDATE()<Proj_End,"Current","Ended")))
     AS Date_Status, DATE_FORMAT(Cancel_100,"%c/%e/%Y"),
     DATE_FORMAT(Cancel_75,"%c/%e/%Y"), Proj_Ages,
     Proj_Desc, Proj_Meetings, Proj_Rehearsals, Proj_Performances
     FROM ACT_Project WHERE Proj_ID = ?', {}, $Proj_ID );
  $Proj_Start = '' if $Proj_Start eq '0/0/0000';
  $Proj_End = '' if $Proj_End eq '0/0/0000';
  $Cancel_100 = '' if $Cancel_100 eq '0/0/0000';
  $Cancel_75 = '' if $Cancel_75 eq '0/0/0000';
  $form->title( "$Proj_ID: DB Production Page for $Proj_Name" );
  $form->tmpl_param( Proj_Name => $Proj_Name );
  $form->tmpl_param( Proj_ID => $Proj_ID );
  # Eliminate bio links with transition to new website. 8/3/19
  # $Proj_Leader =~ s|<([^>]*) #([^>]*)>|<a href="http://act.arlington.ma.us/workshops/leaders.html#$2">$1 $2</a>|g;
  $Proj_Leader =~ s|<([^>]*) #([^>]*)>|$1 $2|g;
  $form->tmpl_param( Proj_Leader => $Proj_Leader );
  $form->tmpl_param( Proj_LdrTitle => $Proj_LdrTitle );
  $form->tmpl_param( Proj_Status => $Proj_Status );
  $form->tmpl_param( Date_Status => $Date_Status );
  $form->tmpl_param( Proj_Start => $Proj_Start );
  $form->tmpl_param( Proj_End => $Proj_End );
  $form->tmpl_param( Cancel_100 => $Cancel_100 );
  $form->tmpl_param( Cancel_75 => $Cancel_75 );
  $form->tmpl_param( Proj_Ages => $Proj_Ages );
  $form->tmpl_param( Proj_Desc => $Proj_Desc );
  $form->tmpl_param( Proj_Meetings => $Proj_Meetings );
  $form->tmpl_param( Proj_Rehearsals => $Proj_Rehearsals );
  $form->tmpl_param( Proj_Performances => $Proj_Performances );

  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->template("$self->{tmpldir}/Production.html");
  $form->reset(0);
  $form->submit( [ 'Project', 'Copy Project', 'Contact List', 'Email Lists', 'Registration',
       'Auditions', 'Schedule', 'My Schedule', 'Scene Grid',
       'Scene List', 'Characters', 'Casting', 'Roles', 'Back',
       'Task List', 'Sign In' ] );
  return $form;
}

sub authorize {
  my ( $self, $op ) = @_;
  if ( $self->check_key ) {
    my $form = $self->{form};
    my $Proj_ID = $form->field('Proj_ID');
    my $rolepriv = $self->Get_Privilege_Value( $Proj_ID, 'ProjectAdmin' );
    $self->{tmp}->{rolepriv} = $rolepriv;
    return 1 if $rolepriv >= 1;
  }
  return 0;
}

# Called when form is first displayed (prior to submission)
sub InitFields {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  my $Proj_ID = $form->field('Proj_ID');
  my ( $Role_Status, $Role_ID, $Proj_URL, $Role_Fee, $Role_Limit ) =
   $self->{dbh}->selectrow_array(
    "SELECT Role_Status, Role_ID, Proj_URL, Role_Fee, Role_Limit
     FROM ACT_Project LEFT JOIN ACT_Role ON ACT_Project.Proj_ID = ACT_Role.Proj_ID
       AND Role_Name = 'Registration'
     WHERE ACT_Project.Proj_ID = ?", {}, $Proj_ID );
  $form->tmpl_param( Role_Status => $Role_Status || '' );
  $form->tmpl_param( Role_ID => $Role_ID );
  $form->tmpl_param( Proj_URL => $Proj_URL );
  $form->tmpl_param( Role_Fee => $Role_Fee || '' );
  $form->tmpl_param( Role_Limit => $Role_Limit || '' );
  my $committees = $self->{dbh}->selectall_arrayref(
    "SELECT Role_Name, Cast_Note, CONCAT(FirstName, ' ', LastName) AS Name
     FROM ACT_Role NATURAL JOIN ACT_Cast NATURAL JOIN ACT_Participant
     WHERE Proj_ID = ? AND Role_Type = 'Committee'
     AND Cast_Status = 'Accepted'
     ORDER BY ACT_Role.Role_ID ASC, Cast_Order DESC, LastName, FirstName",
     {}, $Proj_ID );
  my %comms;
  my $curnote;
  for my $member ( @$committees ) {
    if ( ! defined($comms{$member->[0]}) ) {
      $comms{$member->[0]} = [];
      undef $curnote;
    }
    my $note = $member->[1] ? "$member->[1]:" : '';
    my $memberref = { Cast_Note => $note, notecount => 0, Name => $member->[2] };
    if ( !defined($curnote) || ( $curnote->{Cast_Note} ne $memberref->{Cast_Note} ) ) {
      $curnote = $memberref;
    }
    push @{$comms{$member->[0]}}, $memberref;
    $curnote->{notecount}++;
  }
  my $comms = [];
  for my $comm ( 'Production Team', sort keys %comms ) {
    if ( defined($comms{$comm}) ) {
      push @$comms, { committee => $comm, Cast_Notes => $comms{$comm} };
      delete $comms{$comm};
    }
  }
  $form->tmpl_param( Committees => $comms );
  my $auditions = $self->{dbh}->selectcol_arrayref(
    "SELECT CONCAT(DATE_FORMAT(Event_Date,'%a %b %e'),' ',
       TIME_FORMAT(MIN(Event_Start),'%l:%i %p-'),
       TIME_FORMAT(MAX(Event_End),'%l:%i %p')) AS Time
    FROM ACT_Role JOIN ACT_Event ON Role_ID = GR_ID
    WHERE ACT_Role.Proj_ID = ? AND Role_Type = 'Audition'
      AND ACT_Role.Proj_ID = ACT_Event.Proj_ID
      AND GR_TYPE = 'Role'
    GROUP BY Event_Date", {}, $Proj_ID
  );
  my @auditions;
  for my $audition ( @$auditions ) {
    $audition =~ s/12:00 PM/Noon/g;
    $audition =~ s/12:00 AM/Midnight/g;
    $audition =~ s/:00//g;
    $audition =~ s/( [AP]M)(-.*\1)/$2/;
    push @auditions, $audition;
  }
  $form->tmpl_param( Auditions => join("<br>\n",@auditions) );
  my $venues = $self->{dbh}->selectall_arrayref(
    "SELECT Venue_Short, ACT_Event.Venue_ID, COUNT(Event_ID) AS N
    FROM ACT_Event NATURAL JOIN ACT_Venue
    WHERE Proj_ID = ?
    GROUP BY ACT_Event.Venue_ID
    ORDER BY N DESC", {}, $Proj_ID );
  my $vflds = $self->map_cols( [ qw( Venue_Short Venue_ID N ) ], $venues );
  $form->tmpl_param( Venues => $vflds );
}

sub INSERT {}

sub UPDATE {}

sub DELETE {}

sub SELECT {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  my $Proj_ID = $form->field('Proj_ID');
  my %delegate = (
    Project => $self->{tmp}->{rolepriv} >= 2 ?
      '/Project/edit' : '/Project',
    'Copy Project' => '/Project/new',
    'Contact List' => '/Contact',
    'Task List' => '/Task_List',
    'Email Lists' => '/Email',
    Registration => '/Registration',
    Auditions => '/Audition',
    Schedule => '/Schedule',
    'My Schedule' => '/MySchedule',
    'Scene Grid' => '/Scenes',
    'Scene List' => '/Groups/edit',
    'Sign In' => '/SignIn',
    'Characters' => '/Chars/edit',
    'Casting' => '/Cast',
    'Roles' => '/Roles',
    'Tickets' => '/Tickets'
  );
  if ( $form->submitted eq 'Back') {
    $self->Return;
  } elsif ( $form->submitted eq 'Copy Project' ) {
    $self->GoTo( $delegate{$form->submitted}, Proj_ID => $Proj_ID );
  } elsif ( $delegate{$form->submitted} ) {
    $self->Call( $delegate{$form->submitted}, Proj_ID => $Proj_ID );
  }
}

sub InitParams {}
1;
