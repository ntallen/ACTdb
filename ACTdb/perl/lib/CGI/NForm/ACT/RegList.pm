package CGI::NForm::ACT::RegList;
use strict;
use CGI;
use CGI::FormBuilder;

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    {
      '' => 'SELECT'
    };
}

# Support for the $params argument is optional, but necessary
# if you want to allow this form to be redirected.
# With FormBuilder 3.0, this functionality can be moved back
# to NForm and get_form() will be responsible only for specifying
# the fields and the template.
sub get_form {
  my ( $self, $op, $params )  = @_;
  $self->Connect();
  my $q = $self->{q} = ref($params) eq 'CGI' ? $params : new CGI($params);
  my $form = CGI::FormBuilder->new(
    action => "$ENV{SCRIPT_NAME}$self->{path_info}",
    method => 'POST',
    fields => [ qw(nxtPI nxtPA Part_ID) ],
    params => $q,
    template => "$self->{tmpldir}/RegList.html",
    title => "ACT Registration"
  );
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'Part_ID', type => 'hidden' );
  return $form;
}

sub authorize {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  if ( $self->check_key( EU => 1 ) ) {
    my $shows = $self->{dbh}->selectall_arrayref(
      "SELECT P.Proj_Name, R.Role_ID, IF(R.Role_Status='Closed',1,0) AS Closed
         FROM ACT_Role AS R NATURAL JOIN ACT_Project P,
           ACT_Cast AS C NATURAL JOIN ACT_Role AS myR,
           ACT_Priv_Roles AS PR, ACT_PRVals AS PV
           NATURAL JOIN ACT_Privileges
         WHERE R.Role_Name = 'Registration'
          AND P.Proj_Status != 'Archive'
          AND C.Part_ID = ?
          AND ( myR.Proj_ID = P.Proj_ID OR myR.Proj_ID = 0 )
          AND ( PR.Role_ID = myR.Role_ID OR ( PR.Role_ID = 0 AND PR.Role_Name = myR.Role_Name ))
          AND PR.RP_ID = PV.RP_ID
          AND PV.PV_Value >= 1
          AND Priv_Name = 'RegisterOthers'
          AND P.Proj_Type = 'Show'
        GROUP BY R.Role_ID
        ORDER BY P.Proj_Type, P.Proj_Start;",
      {}, $self->{auth}->{Part_ID}
    );
    $shows = map_cols( [ 'Show', 'Role_ID', 'Closed' ], $shows );
    $form->tmpl_param( shows => $shows ) if @$shows;
    my $workshops = $self->{dbh}->selectall_arrayref(
      "SELECT P.Proj_Name, R.Role_ID, IF(R.Role_Status='Closed',1,0) AS Closed
         FROM ACT_Role AS R NATURAL JOIN ACT_Project P,
           ACT_Cast AS C NATURAL JOIN ACT_Role AS myR,
           ACT_Priv_Roles AS PR, ACT_PRVals AS PV
           NATURAL JOIN ACT_Privileges
         WHERE R.Role_Name = 'Registration'
          AND P.Proj_Status != 'Archive'
          AND C.Part_ID = ?
          AND ( myR.Proj_ID = P.Proj_ID OR myR.Proj_ID = 0 )
          AND ( PR.Role_ID = myR.Role_ID OR ( PR.Role_ID = 0 AND PR.Role_Name = myR.Role_Name ))
          AND PR.RP_ID = PV.RP_ID
          AND PV.PV_Value >= 1
          AND Priv_Name = 'RegisterOthers'
          AND P.Proj_Type = 'Workshop'
        GROUP BY R.Role_ID
        ORDER BY P.Proj_Type, P.Proj_Start;",
      {}, $self->{auth}->{Part_ID}
    );
    $workshops = map_cols( [ 'Workshop', 'Role_ID', 'Closed' ], $workshops );
    $form->tmpl_param( workshops => $workshops ) if @$workshops;
  }
  return 1;
}

sub INSERT {}

sub UPDATE {}

sub DELETE {}

sub SELECT {
  my $self = shift;
  my $form = $self->{form};
  my ( $submit ) = grep m/^Role_ID\d+$/, $form->cgi_param();
  if ( $submit && $submit =~ m/^Role_ID(\d+)$/ ) {
    my $Part_ID = $form->field('Part_ID');
    $self->GoTo( "/Register2", Role_ID => $1,
      Part_ID => $Part_ID, Step => 4 );
  } else {
    $self->fatal( "No Role_ID specified" );
  }
}

sub InitFields {}

sub map_cols {
  my ( $labels, $aa ) = @_;
  my $ah = [ map {
      my $row = $_;
      my $ref = { map( ( $_ => shift @$row ), @$labels ) };
    } @$aa ];
}

1;
