package CGI::NForm::ACT::Audition;
use strict;
use CGI;
use CGI::FormBuilder;
use Spreadsheet::WriteExcel;
use Digest::MD5;

#--------------------------------------------------------------
# Audition.pm is the form for creating, viewing and modifying
# auditions as well as seeing who is signed up or not. It
# includes links to Auditions.pm to sign kids up or change
# their audition times.
#--------------------------------------------------------------

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    {
      '' => 'INSERT',
      edit => 'UPDATE',
      delete => 'DELETE'
    };
}
my %ops = ( INSERT => 'Add',
  UPDATE => 'Update',
  DELETE => 'Delete' );

# Support for the $params argument is optional, but necessary
# if you want to allow this form to be redirected.
# With FormBuilder 3.0, this functionality can be moved back
# to NForm and get_form() will be responsible only for specifying
# the fields and the template.
sub get_form {
  my ( $self, $op, $params )  = @_;
  $self->Connect();
  my $q = $self->{q} = ref($params) eq 'CGI' ? $params : new CGI($params);
  my $form = CGI::FormBuilder->new(
    action => "$ENV{SCRIPT_NAME}$self->{path_info}",
    method => 'POST',
    params => $q,
    template => "$self->{tmpldir}/Audition.html",
    title => "$ops{$op} Auditions",
    submit => [ "Back" ],
    jsfunc => <<'EOJS'
      if (form._submit.value == 'Back' || form._submit.value == 'Download Spreadsheet') {
            return true;
      }
EOJS
  );
  my $static = $op eq 'DELETE';
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'Proj_ID', type => 'hidden' );
  for my $field( qw(Event_Date Event_Start Role_Limit) ) {
    $form->field( name => $field, type => 'text', static => $static );
  }
  if ( $op eq 'INSERT' ) {
    $form->field( name => 'RepeatCt', size => 2, value => 4 );
    $form->field( name => 'RepeatTime', size => 2, value => 30 );
  } else {
    $form->field( name => 'Event_End', type => 'text', static => $static );
    $form->field( name => 'Role_ID', type => 'hidden' );
  }
  
  my $Venues = $self->{dbh}->selectall_arrayref(
    "SELECT Venue_ID, Venue_Short FROM ACT_Venue
     ORDER BY Venue_Short", {} );
  $form->field( name => 'Venue_ID', options => $Venues, static => $static );
  return $form;
}

### Creating or editing auditions should be restricted
### to Registrar, but board and production team should
### be able to view them. Currently, anyone who can
### see them can change them.
sub authorize {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  my $Proj_ID = $form->field('Proj_ID');
  $self->fatal("No Project Specified for Registration") unless $Proj_ID;
  my ( $Proj_Type, $Proj_Name ) = $self->{dbh}->selectrow_array(
    'SELECT Proj_Type, Proj_Name FROM ACT_Project WHERE
     Proj_ID = ?', {}, $Proj_ID );
  $self->fatal("Invalid Project ID") unless $Proj_Type && $Proj_Name;
  $form->tmpl_param( Proj_Type => $Proj_Type );
  $form->tmpl_param( Proj_Name => $Proj_Name );
  if ( $self->check_key ) {
    # Need Registration/Read to see actors' names
    # Need Registration/Write to signup or change signups
    # Need Schedule/Write to create auditions
    my $RegPriv = $self->Get_Privilege_Value( $Proj_ID, 'Registration' );
    my $SchedPriv = $self->Get_Privilege_Value( $Proj_ID, 'Schedule' );

    $self->fatal("Not authorized to view registration")
      unless $RegPriv;

    # We use INSERT for SELECT functionality as well, so we allow
    # access for read, but we need to check later in INSERT to
    # see if we have SchedPriv
    $self->fatal("Not authorized to alter schedule")
      unless $SchedPriv || $op eq 'INSERT';
    my @subs = "Back";
    unshift( @subs, "Download Spreadsheet" ) if $op eq 'INSERT';
    if ( $SchedPriv ) {
      $form->tmpl_param( can_sched => 1 );
      unshift(@subs, $ops{$op} );
    }
    $form->submit( \@subs );
    $self->{tmp}->{RegPriv} = $RegPriv;
    $self->{tmp}->{SchedPriv} = $SchedPriv;
    return 1;
  }
  return 0;
}

sub fix_time {
  my $input = shift;
  if ( $input =~ m,^(\d\d?)(?::(\d\d))?(?: *([AaPp][Mm]))?$, ) {
    my ( $h, $m, $suff ) = ( $1, $2, $3 );
    $m = '00' unless defined $m;
    if ( $suff && $suff =~ m,pm,i && $h < 12 ) {
      $h += 12;
    }
    $input = "$h:$m:00";
  }
  return $input;
}

sub addminutes {
  use integer;
  my ( $t1, $m2 ) = @_;
  my ( $h, $m, $s ) = $t1 =~ m/^(\d+):(\d+):(\d+)$/;
  return undef unless defined($s);
  $m += $m2;
  if ( $s >= 60 ) {
    $m += $s/60;
    $s = $s % 60;
  }
  if ( $m >= 60 ) {
    $h += $m/60;
    $m = $m % 60;
  }
  return undef if $h >= 24;
  return sprintf( "%02d:%02d:%02d", $h, $m, $s );
}

sub INSERT {
  my $self = shift;
  my $form = $self->{form};
  
  my %vals;
  # And find the Registration Role
  $vals{Proj_ID} = $form->field('Proj_ID');
  my $Reg_Role = $self->{dbh}->selectrow_array(
    'SELECT Role_ID FROM ACT_Role
     WHERE Proj_ID = ? AND Role_Name = ?', {},
    $vals{Proj_ID}, 'Registration' );
  $self->fatal("No registration role found") unless $Reg_Role;
  
  if ( $form->submitted eq 'Test' ) {
    $self->GoTo("/Register2", Role_ID => $Reg_Role );
  } elsif ( $form->submitted eq 'Back' ) {
    $self->Return;
  } elsif ( $form->submitted eq 'Download Spreadsheet' ) {
    return $self->error("Invalid Proj_ID") unless $vals{Proj_ID} =~ m/^(\d+)$/;
    $self->generate_spreadsheet( $1 );
  } elsif ( $form->submitted eq 'New Venue' ) {
    $self->Call( '/Venue/new' );
  } elsif ( $form->submitted eq 'Edit Venue' ) {
    my $Venue_ID = $form->field('Venue_ID');
    return $self->error('Must select a venue to edit') unless $Venue_ID;
    $self->Call( '/Venue/edit', Venue_ID => $Venue_ID );
  } elsif ( $form->submitted eq 'Add' ) {
    $self->fatal("Not authorized to alter schedule")
      unless $self->{tmp}->{SchedPriv};
    if ( $self->{form}->validate ) {
      my @KWs = qw(Proj_ID Event_Date Event_Start Role_Limit
        Venue_ID RepeatCt RepeatTime);
      for ( @KWs ) {
        $vals{$_} = $form->field($_) || $self->error("Must specify $_");
      }
      return 0 if @{$self->{errors}};
    
      # Figure out what name to use
      my $auds = $self->{dbh}->selectall_arrayref(
        'SELECT Role_Name FROM ACT_Role
         WHERE Proj_ID = ? AND Role_Type = ?', {},
        $vals{Proj_ID}, 'Audition' );
      my @audnums = sort { $a <=> $b } map $_->[0] =~ m/^Audition (\d+)$/ && $1, @$auds;
      my $audnum = pop(@audnums) || 0;
    
      my $RepeatCt = $vals{RepeatCt};
      my $stime = fix_time($vals{Event_Start});
      while ( $RepeatCt-- > 0 ) {
        my $etime = addminutes( $stime, $vals{RepeatTime} );
        ++$audnum;
        $self->{dbh}->do(
          'INSERT INTO ACT_Role ( Proj_ID, Role_Name, Role_Type, Job_Committee,
           Role_Status, Role_Limit ) VALUES ( ?, ?, ?, ?, ?, ? )', {},
           $vals{Proj_ID}, "Audition $audnum", 'Audition', $Reg_Role,
           'Limited', $vals{Role_Limit} );
        my $Role_ID = $self->{dbh}->{mysql_insertid};
        $self->{dbh}->do(
          'INSERT INTO ACT_Event ( Proj_ID, GR_ID, GR_Type, Event_Desc, Event_Date,
           Venue_ID, Event_Start, Event_End )
           VALUES ( ?, ?, ?, ?, ?, ?, ?, ? )', {},
           $vals{Proj_ID}, $Role_ID, 'Role', "Audition $audnum",
           map( $vals{$_}, qw(Event_Date Venue_ID) ),
           $stime, $etime);
        $stime = $etime;
      }
      $self->GoTo( "/Audition",
        Proj_ID => $vals{Proj_ID},
        Event_Date => $vals{Event_Date},
        Event_Start => $vals{Event_Start},
        Role_Limit => $vals{Role_Limit},
        RepeatCt => $vals{RepeatCt},
        RepeatTime => $vals{RepeatTime},
        Venue_ID => $vals{Venue_ID} );
    }
  } else {
    my ( $submit ) = grep m/^[A-Z][A-Z]\d+$/, $form->cgi_param();
    if ( $submit && $submit =~ m/^([A-Z][A-Z])(\d+)$/ ) {
      my $cmd= $1;
      my $ID = $2;
      if ( $cmd eq 'CT' ) {
        $self->Call( '/Home', Part_ID => $ID );
      } elsif ( $cmd eq 'CH' ) {
        $self->Call( '/Auditions/change', Part_ID => $ID,
            Proj_ID => $vals{Proj_ID} );
      } elsif ( $cmd eq 'ED' ) {
        $self->Call( '/Audition/edit', Role_ID => $ID,
          Proj_ID => $vals{Proj_ID} );
      } elsif ( $cmd eq 'DE' ) {
        $self->Call( '/Audition/delete', Role_ID => $ID,
          Proj_ID => $vals{Proj_ID} );
      }
    }
    $self->error( "No command found" );
  }
  return 0;
}

sub UPDATE {
  my $self = shift;
  my $form = $self->{form};
  if ( $form->submitted eq 'Back' ) {
    $self->Return;
  } elsif ( $form->submitted eq 'New Venue' ) {
    $self->Call( '/Venue/new' );
  } elsif ( $form->submitted eq 'Edit Venue' ) {
    my $Venue_ID = $form->field('Venue_ID');
    return $self->error('Must select a venue to edit') unless $Venue_ID;
    $self->Call( '/Venue/edit', Venue_ID => $Venue_ID );
  } elsif ( $form->validate ) {
    my %vals;
    my @KWs = qw(Proj_ID Role_ID Event_Date Event_Start Event_End
      Role_Limit Venue_ID);
    my $Event_End = $form->field('Event_End');
    warn("Event_End = '$Event_End'\n");
    for ( @KWs ) {
      $vals{$_} = $form->field($_) || $self->error("Must specify $_");
    }
    return 0 if @{$self->{errors}};
    $vals{Event_Start} = fix_time($vals{Event_Start});
    $vals{Event_End} = fix_time($vals{Event_End});
    $self->{dbh}->do(
      "UPDATE ACT_Role SET Role_Limit = ? WHERE Role_ID = ?",
      {}, $vals{Role_Limit}, $vals{Role_ID} );
    $self->{dbh}->do(
      "UPDATE ACT_Event SET Event_Date = ?, Event_Start = ?,
       Event_End = ?, Venue_ID = ? WHERE GR_Type = 'Role'
       AND GR_ID = ?", {},
       $vals{Event_Date}, $vals{Event_Start}, $vals{Event_End},
       $vals{Venue_ID}, $vals{Role_ID} );
    $self->Return;
  }
  $self->error("I don't know what to do" );
  return 0;
}

sub DELETE {
  my $self = shift;
  my $form = $self->{form};
  if ( $form->submitted eq 'Back' ) {
    $self->Return;
  }
  my $Role_ID = $form->field('Role_ID');
  eval {
    $self->{dbh}->do(
      'DELETE from ACT_Cast WHERE Role_ID = ?', {}, $Role_ID );
    $self->{dbh}->do(
      'DELETE from ACT_Role WHERE Role_ID = ?', {}, $Role_ID );
    $self->{dbh}->do(
      'DELETE from ACT_Event WHERE GR_Type = ? AND GR_ID = ?',
      {}, 'Role', $Role_ID );
  };
  $self->fatal( "Error deleting audition: $@" ) if $@;
  $self->Return;
}

sub SELECT {}

sub InitFields {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  if ( $op ne 'INSERT' ) {
    my $Proj_ID = $form->field('Proj_ID');
    my $Role_ID = $form->field('Role_ID');
    warn( "Role_ID = $Role_ID\n" );
    my @vals =  $self->{dbh}->selectrow_array(
      "SELECT Role_Name, Role_Limit, " .
       ($op eq 'DELETE' ? "DATE_FORMAT( Event_Date, '%a %b %e' ),"
         : "Event_Date," ) .
      "TIME_FORMAT( Event_Start, '%l:%i %p' ),
       TIME_FORMAT( Event_End, '%l:%i %p' ),
       Venue_ID FROM ACT_Role, ACT_Event
       WHERE Role_ID = ? AND GR_Type = 'Role' AND
         GR_ID = Role_ID", {}, $Role_ID );
    my $vals = $self->map_cols(
       [ qw(Role_Name Role_Limit Event_Date Event_Start Event_End Venue_ID) ],
       [ \@vals ] );
    $vals = $vals->[0];
    warn( "vals is now a ", ref($vals), "\n" );
    for my $var ( keys %$vals ) {
      warn( "Initialize $var => $vals->{$var}\n" );
      $form->field( name => $var, value => $vals->{$var},
        force => 1 );
    }
  }
}

sub InitParams {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  $form->tmpl_param( myop => $ops{$op} );

  my $Proj_ID = $form->field('Proj_ID');
  if ( $op eq 'INSERT' ) {
    # Initialize list of auditions
    my $can_sched = $self->{tmp}->{SchedPriv};
    my $can_reg = $self->{tmp}->{RegPriv} > 1 ? '1' : '0';
    my $auds = $self->{dbh}->selectall_arrayref(
      "SELECT Role_ID, Role_Name, DATE_FORMAT( Event_Date, '%a %b %e' ),
       TIME_FORMAT( Event_Start, '%l:%i %p' ),
       TIME_FORMAT( Event_End, '%l:%i %p' ),
       ACT_Venue.Venue_ID, Venue_Short, $can_sched
       FROM ACT_Event, ACT_Role, ACT_Venue
       WHERE ACT_Event.Proj_ID = ?
       AND GR_Type = ?
       AND GR_ID = ACT_Role.Role_ID
       AND ACT_Role.Role_Type = ?
       AND ACT_Event.Venue_ID = ACT_Venue.Venue_ID
       ORDER BY Event_Date, Event_Start", {},
      $Proj_ID, 'Role', 'Audition'
    );
    my $audees = $self->{dbh}->selectall_arrayref(
      'SELECT ACT_Role.Role_ID, FirstName, LastName, ACT_Cast.Part_ID
       FROM ACT_Role NATURAL JOIN ACT_Cast NATURAL JOIN ACT_Participant
       WHERE Proj_ID = ? AND Role_Type = ? AND Cast_Status != ?
       ORDER BY LastName, FirstName', {}, $Proj_ID, 'Audition', 'Withdrawn' );
    $auds =
      $self->map_cols( [ qw(Role_ID Role_Name Event_Date
      Event_Start Event_End Venue_ID Venue_Short can_sched) ], $auds );
    my %aud;
    for my $aud ( @$auds ) {
      $aud->{auditionees} = $aud{$aud->{Role_ID}} = [];
    }
    for my $audee ( @$audees ) {
      push @{$aud{$audee->[0]}}, { FirstName => $audee->[1],
      LastName => $audee->[2],
      Part_ID => $audee->[3], Role_ID => $audee->[0],
      can_reg => $can_reg };
    }
    $form->tmpl_param( auditions => $auds);

    my $notaud = $self->{dbh}->selectall_arrayref(
      "SELECT CONCAT(FirstName, ' ', LastName),
      ACT_Participant.Part_ID, $can_reg
      FROM ACT_Role AS R1 NATURAL JOIN ACT_Cast AS C1
        NATURAL JOIN ACT_Participant
        JOIN ACT_Role AS R2
          ON R2.Proj_ID = R1.Proj_ID
             AND R2.Role_Type = 'Audition'
        LEFT JOIN ACT_Cast AS C2
          ON C2.Part_ID = ACT_Participant.Part_ID
             AND C2.Role_ID = R2.Role_ID
      WHERE R1.Proj_ID = ?
        AND R1.Role_Name = 'Registration'
        AND C1.Cast_Status = 'Accepted'
      GROUP BY ACT_Participant.Part_ID
        HAVING COUNT(C2.Role_ID) = 0
      ORDER BY LastName, FirstName", {}, $Proj_ID );
    $notaud = $self->map_cols( [ 'name', 'Part_ID', 'can_reg' ], $notaud );
    $form->tmpl_param( notaud => $notaud );
  }
  if ( $op ne 'DELETE' ) {
    $form->tmpl_param( pick_venue =>
      '<input type="submit" name="_submit" value="Edit Venue">
       <input type="submit" name="_submit" value="New Venue">' );
  }
  $form->tmpl_param( editaudition => 1 ) unless $op eq 'INSERT';
}

sub generate_spreadsheet {
  my ( $self, $Proj_ID ) = @_;
  my ( $Proj_Name, $Proj_mnc ) =
   $self->{dbh}->selectrow_array(
    "SELECT P.Proj_Name, P.Proj_Abbr
     FROM ACT_Project AS P WHERE P.Proj_ID = ?",
    {}, $Proj_ID );
  $Proj_mnc = "P$Proj_ID" unless $Proj_mnc;

  die "Unknown Proj_ID: $Proj_ID\n" unless defined $Proj_Name;

  my $spreadsheet = "/tmp/actwebmaster_" . Digest::MD5::md5_hex("${Proj_ID}_Auditions.xls");
  my $wb = Spreadsheet::WriteExcel->new($spreadsheet);
  my $header_fmt = $wb->add_format( size => 12, bold => 1, align => 'center' );
  my $subtitle_fmt = $wb->add_format( align => 'center', bold => 1 );
  my $slot_fmt = $wb->add_format( italic => 1, bg_color => 'silver', border => 1 );
  my $hcol_fmt = $wb->add_format( bold => 1, border => 1 );
  my $vcol_fmt = $wb->add_format( bold => 1, border => 1, rotation => 90, align => 'center' );
  my $chk_fmt  = $wb->add_format( border => 1 );
  my $cfhdr_fmt = $wb->add_format( size => 14, align => 'center' );
  my $cf_fmt = $wb->add_format( border => 1, valign => 'vcenter' );

  my %worksheets;
  my $wshdrtext = "&C&\"Arial,Bold\"&12$Proj_Name Auditions&R&14Page &P";
  my $ws = $wb->add_worksheet('Checklist');
  # Header: <Proj_Name> Auditions, centered, bold Arial 12
  $ws->set_header($wshdrtext);
  $ws->hide_gridlines();
  $ws->center_horizontally();

  # Row 0: "Sign In Checklist" columns B-M merged, centered bold
  $ws->merge_range( 0, 1, 0, 12, "Sign In Checklist", $subtitle_fmt );

  # Row 2: Column headings columns B-M
  $ws->write( 2, 1, [  "Name", "Pyt\nNum", "Pyt\nAmt" ], $hcol_fmt );
  $ws->write( 2, 4, [ "Liability", "Medical", "Behavior", "Audition", "May Leave",
    "Conflicts", "Special" ], $vcol_fmt );
  $ws->write( 2, 11, [ "Photo #", "T-Shirt\nSize" ],$hcol_fmt );
  $ws->repeat_rows(0,2);

  $ws->set_column( 0, 0, undef, undef, 1 ); # Hide column A
  $ws->set_column( 1, 1, 24 );
  $ws->set_column( 2, 3, 8 );
  $ws->set_column( 4, 10, 3 );
  $ws->set_column( 11, 12, 8 );

  my $auds = $self->{dbh}->selectall_arrayref(
    "SELECT Role_ID, CONCAT(Role_Name, ' ',
       DATE_FORMAT( Event_Date, '%a %b %e' ), ' ',
       TIME_FORMAT( Event_Start, '%l:%i %p' ), ' - ',
       TIME_FORMAT( Event_End, '%l:%i %p' )) AS Event,
     Role_Limit
     FROM ACT_Event, ACT_Role
     WHERE ACT_Event.Proj_ID = ?
     AND GR_Type = 'Role'
     AND GR_ID = ACT_Role.Role_ID
     AND ACT_Role.Role_Type = 'Audition'
     ORDER BY Event_Date, Event_Start", {},
    $Proj_ID
  );
  my $audees = $self->{dbh}->selectall_arrayref(
    "SELECT ACT_Role.Role_ID,
     CONCAT(FirstName, ' ', LastName) AS Name
     FROM ACT_Role NATURAL JOIN ACT_Cast NATURAL JOIN ACT_Participant
     WHERE Proj_ID = ? AND Role_Type = 'Audition' AND Cast_Status != 'Withdrawn'
     ORDER BY LastName, FirstName", {}, $Proj_ID );
  $auds =
    $self->map_cols( [ qw(Role_ID Event Role_Limit) ], $auds );
  my %aud;
  for my $aud ( @$auds ) {
    $aud->{auditionees} = $aud{$aud->{Role_ID}} = [];
  }
  for my $audee ( @$audees ) {
    push @{$aud{$audee->[0]}}, { Name => $audee->[1] }
  }
  my $notaud = $self->{dbh}->selectall_arrayref(
    "SELECT CONCAT(FirstName, ' ', LastName)
    FROM ACT_Role AS R1 NATURAL JOIN ACT_Cast AS C1
      NATURAL JOIN ACT_Participant
      JOIN ACT_Role AS R2
        ON R2.Proj_ID = R1.Proj_ID
        AND R2.Role_Type = 'Audition'
      LEFT JOIN ACT_Cast AS C2
        ON C2.Part_ID = ACT_Participant.Part_ID
           AND C2.Role_ID = R2.Role_ID
    WHERE R1.Proj_ID = ?
      AND R1.Role_Name = 'Registration'
      AND C1.Cast_Status = 'Accepted'
    GROUP BY ACT_Participant.Part_ID
      HAVING COUNT(C2.Role_ID) = 0
    ORDER BY LastName, FirstName", {}, $Proj_ID );
  $notaud = $self->map_cols( [ 'Name' ], $notaud );

  my $row = 3;
  # spaces for each of the empty fields (C-M)
  my @space = ( '', '', '', '', '', '', '', '', '', '', '' );
  my $pagerows = 44;
  my $audnum = 1;
  my $conflict_sheets = 0;
  for my $aud ( @$auds ) {
    my $nslots = $aud->{Role_Limit};
    my $nauds = @{$aud->{auditionees}};
    $nslots = $nauds if $nauds > $nslots;
    my $nblanks = $nslots - $nauds;
    if ( $pagerows <= $nslots ) {
      $ws->set_h_pagebreaks($row);
      $pagerows = 44;
    } else {
      $pagerows -= $nslots+1;
    }
    
    my $cf;
    if ( $conflict_sheets ) {
      # Add a conflicts sheet
      $cf = $wb->add_worksheet("Conflicts$audnum");
      ++$audnum;
      $cf->set_header($wshdrtext);
      $cf->merge_range( 0, 0, 0, 1, "Conflicts", $cfhdr_fmt );
      $cf->merge_range( 2, 0, 2, 1, $aud->{Event}, $slot_fmt );
      $cf->set_column( 0, 0, 24 );
      $cf->set_column( 1, 1, 60 );
      $cf->hide_gridlines();
      $cf->center_horizontally();
    }
    my $cfrowht = 60;
    my $cfrow = 3;

    $ws->merge_range( $row, 0, $row, 12, $aud->{Event}, $slot_fmt );
    ++$row;
    for my $audee ( @{$aud->{auditionees}} ) {
      $ws->write( $row++, 1, [ $audee->{Name}, @space ],  $chk_fmt);
      if ( $conflict_sheets ) {
        $cf->write( $cfrow, 0, [ $audee->{Name}, ''], $cf_fmt);
        $cf->set_row( $cfrow, $cfrowht );
        ++$cfrow;
      }
    }
    while ($nblanks-- > 0) {
      $ws->write( $row++, 1, [ '', @space ],  $chk_fmt);
      if ( $conflict_sheets ) {
        $cf->write( $cfrow, 0, [ '', ''], $cf_fmt);
        $cf->set_row( $cfrow, $cfrowht );
        ++$cfrow;
      }
    }
  }

  $ws->set_h_pagebreaks($row) if $pagerows <= @$notaud;
  $ws->merge_range( $row, 0, $row, 12, "Not Signed Up", $slot_fmt );
  ++$row;
  for my $audee ( @$notaud ) {
    $ws->write( $row++, 1, [ $audee->{Name}, @space ],  $chk_fmt);
  }
  $wb->close();
  my $filesize = -s $spreadsheet;
  open( XLS, "<$spreadsheet" ) ||
    return $self->error("Unable to read spreadsheet");
  my $contents;
  my $rb = read XLS, $contents, $filesize;
  return $self->error("Short read: size: $filesize read: $rb")
    if $rb != $filesize;
  close XLS;
  unlink($spreadsheet);
  my $q = $self->{q};
  print
    $q->header( -type => 'application/vnd.ms-excel', -Content_length => $filesize,
        -attachment => "${Proj_ID}_${Proj_mnc}_Auditions.xls" ),
    $contents;
  exit(0);

}

1;
