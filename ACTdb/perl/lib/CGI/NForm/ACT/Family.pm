package CGI::NForm::ACT::Family;
use strict;
use CGI;
use CGI::FormBuilder;

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    { # rm  =>  'DELETE',
      edit => 'UPDATE',
      '' =>   'SELECT'
    };
}

# Support for the $params argument is optional, but necessary
# if you want to allow this form to be redirected.
# With FormBuilder 3.0, this functionality can be moved back
# to NForm and get_form() will be responsible only for specifying
# the fields and the template.
sub get_form {
  my ( $self, $op, $params )  = @_;
  $self->Connect();
  my $q = $self->{q} = ref($params) eq 'CGI' ? $params : new CGI($params);
  my $static = $op eq 'SELECT';
  my $form = CGI::FormBuilder->new(
    action => "$ENV{SCRIPT_NAME}$self->{path_info}#CurStep",
    method => 'POST',
    params => $q,
    template => "$self->{tmpldir}/Family.html",
    title => "Your Family Information",
    submit => $op eq 'UPDATE' ? [ "Discard Changes", "Continue" ]
	  : [ "Back" ]
  );
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'Fam_ID', type => 'hidden' );
  $form->field( name => 'HomePhone', validate => '/^\d\d\d[ -.]?\d\d\d[ -.]?\d\d\d\d$/',
    comment => '###-###-####', required => 0, static => $static );
  $form->field( name => 'EmergencyContact1', size => 40,
    static => $static );
  $form->field( name => 'EmergencyContact2', size => 40,
    static => $static );
  $form->field( name => 'EmergencyPhone1',
    validate => '/^\d\d\d[ -.]?\d\d\d[ -.]?\d\d\d\d(x\d{1,4})?$/',
    comment => '###-###-####', required => 0, static => $static );
  $form->field( name => 'EmergencyPhone2',
    validate => '/^\d\d\d[ -.]?\d\d\d[ -.]?\d\d\d\d(x\d{1,4})?$/',
    comment => '###-###-####', required => 0, static => $static );
  for my $field ( qw(FamilyName Address1 Address2 City State Zip) ) {
    $form->field( name => $field, type => 'text',
      static => $static );
  }
  return $form;
}

sub authorize {
  my ( $self, $op ) = @_;
  if ( $self->check_key ) {
    my $form = $self->{form};
    my $Fam_ID = $form->field('Fam_ID');
    my $Part_ID = $self->{auth}->{Part_ID};
    my ( $DBFam_ID, $DBStatus, $Age ) = $self->{dbh}->selectrow_array(
      'SELECT Fam_ID, Status,
       IF(Birthdate>0,
        YEAR(CURDATE())-YEAR(Birthdate)-(MID(CURDATE(),6,5)<MID(Birthdate,6,5)),
        -1) AS Age
       FROM ACT_Participant where Part_ID = ?',
      {}, $Part_ID );
    $self->fatal("No Fam_ID or Status defined for Part_ID = $Part_ID")
      unless defined $DBFam_ID && defined $DBStatus;
    $self->fatal( "User must be an adult to update family information: '$DBStatus'" )
      if $DBStatus ne 'Adult' && $Age < 18;
    if ( $Fam_ID ) {
      if ( $Fam_ID != $DBFam_ID ) {
        my $CI_Priv =
          $self->Get_Privilege_Value( -1, "ContactInfo" );
        if ( $op eq 'UPDATE' && $CI_Priv == 1 ) {
          $self->GoTo( '/Family', Fam_ID => $Fam_ID );
        }
        return 0 if $CI_Priv < ($op eq 'SELECT' ? 1 : 2 );
      }
    } else {
      $form->field(name => 'Fam_ID', value => $DBFam_ID, force => 1 );
      $Fam_ID = $DBFam_ID;
    }
    my $members = $self->{dbh}->selectall_arrayref(
      "SELECT Part_ID, STRCMP(Status,'Child') AS Adult, FullName, WorkPhone, CellPhone,
       IF(Birthdate>0,YEAR(NOW())-YEAR(Birthdate)-(MID(NOW(),6,5)<MID(Birthdate,6,5)),
       '?') AS Age, ClassYear, YEAR(CURDATE()), MONTH(CURDATE()), Email
       FROM ACT_Participant
       WHERE Fam_ID = ?
       ORDER BY Adult, Birthdate", {}, $Fam_ID );
    $members =
      $self->map_cols( [ qw(Part_ID Adult Name Work Cell Age
         ClassYear CurYear CurMonth Email) ], $members );
    for ( @$members ) {
      $_->{Grade} = $self->Grade( $_->{ClassYear}, $_->{CurYear}, $_->{CurMonth} );
    }
    $form->tmpl_param( Members => $members );
    $form->tmpl_param( UPDATE => 1 ) if $op eq 'UPDATE';
    return 1;
  }
  return 0;
}

sub INSERT {
}

sub UPDATE {
  my $self = shift;
  my $form = $self->{form};
  $self->Return if $form->submitted eq "Discard Changes";
  my @FKWs = qw(FamilyName EmergencyContact1 EmergencyPhone1
       EmergencyContact2 EmergencyPhone2);
  my @AKWs = qw(Address1 Address2 City State Zip HomePhone);
  if ( $form->validate ) {
    $self->FixPhoneEntry( qw(EmergencyPhone1 EmergencyPhone2
        HomePhone) );
    my $Fam_ID = $form->field('Fam_ID');
    my $Addr_ID = $self->{dbh}->selectrow_array(
      'SELECT Addr_ID from ACT_Family WHERE Fam_ID = ?',
      {}, $Fam_ID );
    my $cmd = join ' ',
      'UPDATE ACT_Family SET',
      join( ', ', map "$_=?", @FKWs ),
      'WHERE Fam_ID = ?';
    my @vals = map $form->field($_), @FKWs;
    $self->Log( "$vals[0] Family", "/Family?Fam_ID=$Fam_ID" );
    $self->{dbh}->do( $cmd, {}, @vals, $Fam_ID );
    my @avals = map $form->field($_), @AKWs;
    if ( $Addr_ID ) {
      $cmd = join ' ',
	'UPDATE ACT_Address SET',
	join( ', ', map "$_=?", @AKWs ),
	'WHERE Addr_ID = ?';
      $self->{dbh}->do( $cmd, {}, @avals, $Addr_ID );
    } else {
      $cmd = join ' ',
	'INSERT INTO ACT_Address (',
	join( ', ', @AKWs ),
	') VALUES (',
	join( ', ', map "?", @AKWs ),
	')';
      $self->{dbh}->do( $cmd, {}, @avals );
      $Addr_ID = $self->{dbh}->{mysql_insertid};
      $self->{dbh}->do(
	'UPDATE ACT_Family SET Addr_ID = ? WHERE Fam_ID = ?',
	{}, $Addr_ID, $Fam_ID );
      $self->{dbh}->do(
	'UPDATE ACT_Participant SET Addr_ID = ?
	 WHERE Fam_ID = ? AND Addr_ID = 0', {}, $Addr_ID, $Fam_ID );
    }
  } else {
    $self->error( "Validation errors" );
  }
  return 0 if @{$self->{errors}};
  my ( $submit ) = grep m/^[A-Z][A-Z]\d+$/, $form->cgi_param();
  if ( $submit && $submit =~ m/^([A-Z][A-Z])(\d+)$/ ) {
    my $cmd= $1;
    my $id = $2;
    if ( $cmd eq 'EP' ) {
      $self->Call( "/Person/edit", Part_ID => $id );
    }
  }
  if ( $form->submitted eq "Submit Changes" ) {
    $self->Return;
  } elsif ( $form->submitted eq 'Continue' ) {
    $self->Return;
  } elsif ( $form->submitted eq 'Add Another Family Member' ) {
    $self->Call( "/Person/new", Fam_ID => $form->field('Fam_ID') );
  } else {
    push @{$self->{errors}}, "Unrecognized request";
  }
  return 0;
}

sub DELETE {
}

sub SELECT {
  my $self = shift;
  my $form = $self->{form};
  $self->Return if $form->submitted eq 'Back';
  $self->error("Unexpected response");
  return 1;
}

sub InitFields {
  my $self = shift;
  my $form = $self->{form};
  my $Fam_ID = $form->field('Fam_ID');
  my @KWs = qw(FamilyName EmergencyContact1 EmergencyPhone1
       EmergencyContact2 EmergencyPhone2 Address1 Address2
       City State Zip HomePhone);
  my $cmd = join ' ', 
    'SELECT',
    join( ', ', @KWs ),
    'FROM ACT_Family AS F, ACT_Address AS A
	 WHERE F.Addr_ID = A.Addr_ID AND F.Fam_ID = ?';
  my @vals =
    $self->{dbh}->selectrow_array( $cmd, {}, $Fam_ID );
  for my $KW ( @KWs ) {
    my $dbval = shift(@vals);
    if ( ! defined $form->field($KW) ) {
      $form->field( name => $KW, value => $dbval, force => 1 );
    }
  }
}

1;
