package CGI::NForm::ACT::Email;
use strict;
use CGI;
use CGI::FormBuilder;

our @ISA = ( 'CGI::NForm::ACT' );

sub op_def {
  return
    {
      '' => 'SELECT'
    };
}

# Support for the $params argument is optional, but necessary
# if you want to allow this form to be redirected.
# With FormBuilder 3.0, this functionality can be moved back
# to NForm and get_form() will be responsible only for specifying
# the fields and the template.
sub get_form {
  my ( $self, $op, $params )  = @_;
  $self->Connect();
  my $q = $self->{q} = ref($params) eq 'CGI' ? $params : new CGI($params);
  my $form = CGI::FormBuilder->new(
    action => "$ENV{SCRIPT_NAME}$self->{path_info}",
    method => 'POST',
    fields => [ qw(nxtPI nxtPA Proj_ID) ],
    params => $q,
    template => "$self->{tmpldir}/Email.html",
    title => "Email Lists",
    submit => [ 'Submit', 'Back' ],
    reset => 0
  );
  $form->field( name => 'nxtPI', type => 'hidden' );
  $form->field( name => 'nxtPA', type => 'hidden' );
  $form->field( name => 'Proj_ID', type => 'hidden' );
  $form->field( name => 'Registration', type => 'checkbox', options => 'Registration' );
  $form->field( name => 'Board', type => 'checkbox', options => 'Board' );
  $form->field( name => 'RegStatus', options => [ qw(Applied Accepted Waitlisted Withdrawn) ] );

  my $Proj_ID = $form->field('Proj_ID') ||
    $self->fatal("No Proj_ID Specified");
  my $Proj_Name = $self->{dbh}->selectrow_array(
    'SELECT Proj_Name FROM ACT_Project WHERE Proj_ID = ?', {}, $Proj_ID );
  $form->title( "DB Email Lists for $Proj_Name" );
  
  my $Roles = $self->{dbh}->selectall_arrayref(
    "SELECT Role_ID, CONCAT( Role_Name, ' (', Role_Type, ')' ) FROM ACT_Role
    WHERE Proj_ID = ?
      AND Role_Type IN ( 'Committee', 'Job' )",
    {}, $Proj_ID );

  if ( @$Roles ) {
    $form->field( name => "Roles", type => 'checkbox', options => $Roles, linebreaks => 1 );
	$self->{tmp}->{has_roles} = 1;
  }
  
  return $form;
}

sub authorize {
  my ( $self, $op ) = @_;
  if ( $self->check_key ) {
    my $form = $self->{form};
    my $Proj_ID = $form->field('Proj_ID');
    return 1 if
      $self->Has_Privilege( $Proj_ID, 'ContactInfo', 1 );
    # return 1 if $self->has_role( $Proj_ID, 'Production Team' ) ||
    #  $self->has_role( 1, 'Board');
  }
  return 0;
}

# Called when form is first displayed (prior to submission)
sub InitFields {
}

sub INSERT {}

sub UPDATE {}

sub DELETE {}

sub SELECT {
  my ( $self, $op ) = @_;
  my $form = $self->{form};
  $self->Return if $form->submitted eq 'Back';
  my $Proj_ID = $form->field('Proj_ID');
  my $Registration = $form->field('Registration');
  my $RegStatus = $form->field('RegStatus');
  my $Board = $form->field('Board');
  my @Roles = $self->{tmp}->{has_roles} ? $form->field('Roles') : ();
  push @Roles, 1 if $Board;
  if ( $Registration && ! $RegStatus ) {
    return $self->error( "Must select registration status if Registration is selected" );
  }
  my @where;
  if ( $Registration ) {
    my $Reg_ID = $self->{dbh}->selectrow_array(
      "SELECT Role_ID FROM ACT_Role WHERE Proj_ID = ? AND Role_Name = ?",
      {}, $Proj_ID, 'Registration' );
    push @where, "( Role_ID = $Reg_ID AND Cast_Status = '$RegStatus' )";
  }
  if ( @Roles ) {
    push @where, join '', "Role_ID IN ( ",
      join( ", ", @Roles ), " )";
  }
  my $where;
  if ( @where == 2 ) {
    $where = join '', "( ", join( " OR ", @where ), " )";
  } elsif ( @where ==1 ) {
    $where = shift @where;
  } else {
    return $self->error( "Must select some criteria" );
  }
  my $query = 
    "SELECT P2.Email FROM ACT_Cast NATURAL JOIN ACT_Participant AS P1,
       ACT_Participant AS P2
     WHERE $where
     AND P1.Fam_ID = P2.Fam_ID
     AND P2.Email != ''
     AND ( P1.Part_ID = P2.Part_ID
           OR ( P1.Status = 'Child' AND P2.Status = 'Adult' ) )
     GROUP BY P2.Email
     ORDER BY P2.Email";
  my $email = $self->{dbh}->selectcol_arrayref( $query );
  my @maillist = (
    map( { Email => $_ }, @$email )
  );
  $form->tmpl_param( maillist => \@maillist );
  return 0;
}

sub InitParams {
  my $self = shift;
  my $form = $self->{form};
  $form->tmpl_param( has_roles => $self->{tmp}->{has_roles} ? 1 : 0 );
}
1;
