package CGI::NForm::ACT;
use strict;
our %Config;
# This file needs to be edited for the local environment and copied into the
# lib/CGI/NForm directory.

$Config{dbhost} = '127.0.0.1';
$Config{dbname} = 'act';
$Config{dbuser} = 'act_app';
$Config{dbpasswd} = 'password';
$Config{smtp_server} = 'smtp.comcast.net';
$Config{tmpldir} = '/var/www/htdocs/cgi-bin';
1;
