# This script should be copied and edited to match the database name
# and the user names and passwords for the 'architect' (administrative) user
# and the 'application' user.
# The passwords, at least, should be modified.
# This information needs to be propagated to .my.app.cnf, .my.arch.cnf and ACT_local.pm

#-------------------------------
# Permissions for the architect:
#-------------------------------
# Privileges *not* granted to act_arch:	  
#            Grant_priv: N
#       References_priv: N
# Create_tmp_table_priv: N
#      Lock_tables_priv: N
#-------------------------------
GRANT Select, Insert, Update, Delete, Create, Drop, Index, Alter, Lock Tables
ON ACT.*
TO act_arch@localhost identified by '<arch passwd>';


#---------------------------------
# Permissions for the application:
#---------------------------------
GRANT Select, Insert, Update, Delete, Lock Tables,
  Create Temporary Tables
ON ACT.*
TO act_app@localhost identified by '<app passwd>';

CREATE DATABASE IF NOT EXISTS ACT CHARACTER SET UTF8;
