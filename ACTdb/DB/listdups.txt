SELECT LastName, FirstName, COUNT(Part_ID)
FROM ACT_Participant 
GROUP BY LastName, FirstName HAVING COUNT(Part_ID) > 1 ORDER BY 3 DESC;
