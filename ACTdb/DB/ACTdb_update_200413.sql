Update ACT_Text_Text SET
  Text_Text =
"Checks can be made payable to:

  Arlington Children's Theatre
  115 Massachusetts Avenue
  Arlington MA, 02474.
  
You can also pay online here:

  https://act.arlington.ma.us/participants/make_payment/

"
  WHERE Text_ID = 6 AND Proj_ID = 0;

# Sample query implemented in GetProjText() in ACT.pm
# SELECT IF(ISNULL(T1.Text_Text),IF(ISNULL(T2.Text_Text),'',T2.Text_Text),T1.Text_Text) AS Text_Text
# FROM ACT_Text_Keys LEFT JOIN ACT_Text_Text AS T1
#   ON ACT_Text_Keys.Text_ID = T1.Text_ID AND T1.Proj_ID = 545
#   LEFT JOIN ACT_Text_Text AS T2
#   ON ACT_Text_Keys.Text_ID = T2.Text_ID AND T2.Proj_ID = 0;
