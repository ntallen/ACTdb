#! perl -w
use CGI::NForm::ACT;
use CGI::FormBuilder;
use Image::Size;
use strict;

# contact4show <Proj_ID> <ofilename>
# This script is modified from contactform. contactform was
# intended as a general purpose contact form for intake, so
# it leaves empty spaces for information to be filled in.

my $act = CGI::NForm::ACT->Connect;
my $dbh = $act->{dbh};
my $Proj_ID = shift( @ARGV ) || die "Must specify Proj_ID as first argument\n";
my ( $Proj_mnc, $Par_ID, $Par_mnc ) =  $dbh->selectrow_array(
  "SELECT P.Proj_Abbr, Parent_Proj, PP.Proj_Abbr AS Par_mnc
   FROM ACT_Project AS P
      NATURAL LEFT JOIN ACT_Subproject
      LEFT JOIN ACT_Project AS PP ON Parent_Proj = PP.Proj_ID
   WHERE P.Proj_ID = ?",
  {}, $Proj_ID );
die "Must specify project mnemonic in database\n" unless $Proj_mnc;
my $ofilename = shift(@ARGV) ||
  die "Must specify output filename as second argument\n";

my $exinfo = {};

# filevars can include:
#   Name: template for making the name, defaults to '${First} ${Last}'
#   Photo: template for making photo filename
my %filevars = ( Name => '${First} ${Last}' );
my %hdrs;
my $nflds = 0;
my $dpi = 300;
my $min_height = 1; # 1 inch

my @conf;
push @conf, "${Proj_ID}_${Proj_mnc}_info.txt";
if ( defined($Par_ID) && defined($Par_mnc) ) {
  push @conf, "${Par_ID}_${Par_mnc}_info.txt";
}
for my $fname ( @conf ) {
  if ( open( IF, "<$fname" ) ) {
    print STDERR "Reading configuration file '$fname'\n";
    while ( <IF> ) {
      my @flds = map { s/\s*$//;
                       s/^\s*//;
                       s/^"(.*)"$/$1/;
                       s/(^| )""/$1\`\`/g;
                       s/""( |$)/\'\'$1/g;
                       s/""/"/g; $_; } split /\t/, $_;
      if ( $flds[0] =~ m/^(\w+):$/ ) {
        $filevars{$1} = $flds[1];
      } elsif ( !$nflds ) {
        while ( @flds ) {
          my $fld = shift @flds;
          $hdrs{$fld} = $nflds++;
        }
      } else {
        my %flds;
        for my $hdr ( keys %hdrs ) {
          $flds{$hdr} = $flds[$hdrs{$hdr}];
        }
        my $name = resolve_template($filevars{Name}, \%flds);
        $exinfo->{$name} ||= {};
        my $rec = $exinfo->{$name};
        if ( $flds{MayLeave} ) {
          $rec->{MayLeave} = 1;
          if ( $flds{MayLeave} ne '1' ) {
            $rec->{MayLeaveTxt} = " $flds{MayLeave}";
          }
        }
        $rec->{Special} = $flds{Special} if $flds{Special};
        $rec->{Excused} = $flds{Excused} if $flds{Excused};
        if ( $filevars{Photo} ) {
          my $photo = resolve_template($filevars{Photo}, \%flds);
          if ( -f $photo ) {
            my ($w, $h) = imgsize($photo);
            if ( defined $w ) {
              my $h_in = $h/$dpi;
              my $w_in = $w/$dpi;
              if ( $h_in < $min_height ) {
                my $ndpi = $h/$min_height;
                $h_in = $h/$ndpi; # $min_height, right?
                $w_in = $w/$ndpi;
              }
              $rec->{Photo} = $photo;
              $rec->{Photo_w} = sprintf "%.2fin", $w_in;
              $rec->{Photo_h} = sprintf "%.2fin", $h_in;
            }
          } else {
            warn "Unable to locate photo '$photo'\n";
          }
        }
      }
    }
    close IF || warn "Error closing $fname\n";
  }
}

my $form = $act->{form} = CGI::FormBuilder->new(
  fields => [ qw(Proj_ID) ],
  params => new CGI( '' ),
  template => "contact4show.tex",
  title => "Contact Information List",
);
$form->field( name => 'Proj_ID', value => $Proj_ID, force => 1 );
  #$act->ProjSearch('prod');
$form->tmpl_param( 'Today' => scalar(localtime) );
  #$form->tmpl_param( 'ProdTeam' => texify($form->tmpl_param('Families')) );
  #$form->tmpl_param( 'has_prodteam' => 1 );
$act->ProjSearch('', $exinfo);
$form->tmpl_param( 'Families' => texify($form->tmpl_param('Families')) );
open( OF, ">$ofilename" ) ||
  die "Unable to write to output file: '$ofilename'\n";
print OF $form->render;
close OF || warn "Error closing output file\n";
for my $name ( keys %$exinfo ) {
  unless ( $exinfo->{$name}->{matched} ) {
    warn "Name '$name' from exinfo not matched to DB\n";
  }
}
print "${Proj_ID}_$Proj_mnc.pdf\n";

# get rid of special characters recursively
sub texify {
  my $f = shift @_;
  if ( ref($f) ) {
    if ( ref($f) eq 'HASH' ) {
      for my $key ( keys %$f ) {
        $f->{$key} = texify($f->{$key}) unless $key eq 'Photo';
      }
    } elsif ( ref($f) eq 'ARRAY' ) {
      my $newf = [];
      while ( @$f ) {
        push( @$newf, texify(shift @$f) );
      }
      $f = $newf;
    } else {
      die "Unexpected ref type: " . ref($f) . "\n";
    }
  } elsif ( defined($f) ) {
    # This line is a kluge because it assumes certain HTML
    # formatting applied by ContactParams() and also assumes
    # how contact4show.tex then handles that element. This
    # is very fragile. Fix is to create a true loop in
    # ContactParams() and then handle it in contact4show.tex
    #$f =~ s/<br>\n/}\n    \\inline{/g;
    
    $f =~ s/#/\\#/g;
    $f =~ s/_/\\_/g;
    $f =~ s/&/\\&/g;
    $f =~ s/\@/\\\@/g;
  }
  return $f;
}

sub resolve_template {
  my ( $template, $flds ) = @_;
  while ( $template =~ m/\$\{(\w+)\}/ ) {
    my $var = $1;
    my $val = $flds->{$var} || '';
    $template =~ s/\$\{$var\}/$val/g;
  }
  return $template;
}
