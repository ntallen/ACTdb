#! /usr/bin/perl -w
use lib '/home/actwebmaster/local/lib';
use CGI::NForm::ACT;
use CGI::FormBuilder;
use Mail::Sendmail;
use POSIX qw(strftime);

my $logfile = '/home/actwebmaster/logs/cron_log';
# my $dbserver = 'localhost';
# $dbserver = '127.0.0.1';

open(STDERR, '>', '/dev/null' );
open(STDOUT, '>>', $logfile);
# $ENV{SERVER_ADDR} = $dbserver;
my $act = CGI::NForm::ACT->Connect;
my $dbh = $act->{dbh};
my $msg = '';

sub query {
  my ($title, $qual ) = @_;
  my $counts = $dbh->selectall_arrayref(
	"SELECT AP, AC, WL, WD, AC+WL+AP-Min AS TG, P2.Proj_Start,
	  ID, P2.Proj_Name
	FROM ( SELECT
		SUM(IF(Cast_Status = 'Applied',1,0)) AS AP,
		SUM(IF(Cast_Status = 'Accepted',1,0)) AS AC,
		SUM(IF(Cast_Status = 'Waitlisted',1,0)) AS WL,
		SUM(IF(Cast_Status = 'Withdrawn',1,0)) AS WD,
		P1.Proj_ID AS ID, Role_Min AS Min
	  FROM ACT_Project AS P1 NATURAL JOIN ACT_Role NATURAL LEFT JOIN ACT_Cast
		WHERE P1.Proj_Status = 'Active'
		  AND Role_Name = 'Registration'
		  AND $qual
	  GROUP BY P1.Proj_Name ) AS T1, ACT_Project AS P2
	WHERE ID = P2.Proj_ID
	ORDER BY P2.Proj_Start;" );

  if (@$counts) {
	$msg .=
	  "\n\n$title:\n" .
	  "  AP  AC  WL  WD  CO  Start Date  Project\n" .
	  "-----------------------------------------------------------\n" .
	  join( '', map( sprintf( "%4d%4d%4d%4d%4d  %s  %3d: %s\n", @$_ ), @$counts ) );
  }
}

$msg .=
  "Registration report as of " . scalar(localtime);

query( "Upcoming Projects", 'P1.Proj_Start > CURDATE()' );
query( "Current Projects", 'Proj_Start <= CURDATE() AND Proj_End >= CURDATE()' );
query( "Projects that have ended", 'Proj_End < CURDATE()' );

$msg .= <<EOF;

Legend:
  AP: Applied
  AC: Accepted
  WL: Waitlisted
  WD: Withdrawn
  CO: Cutoff--number of registrants above minimum
      AP+AC+WL-Min. Negative numbers mean we are
	  below the cutoff.
EOF

my @addrs = (
 'Database Report <DBreport@act.arlington.ma.us>' );

# @addrs = ( 'WebDude Only <allen@huarp.harvard.edu>' );

my %msg = (
  From => 'ACTdb <webmaster@act.arlington.ma.us>',
  To => join( ', ', @addrs ),
  Subject => 'ACT Registration Report ' . strftime('%D',localtime),
  Message => $msg
  #smtp => $smtpserver
  #smtp => 'localhost'
);

if ( sendmail(%msg) ) {
  print $Mail::Sendmail::log, "\n\n";
} else {
  print $Mail::Sendmail::log,
    $Mail::Sendmail::error, "\n\n";
}

