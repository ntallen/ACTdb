SELECT FullName, Birthdate
FROM ACT_Project NATURAL JOIN ACT_Role NATURAL JOIN ACT_Cast NATURAL JOIN ACT_Participant
WHERE Proj_Name LIKE 'Damn%' AND Role_Name = 'Registration' AND Cast_Status = 'Accepted'
ORDER BY RIGHT(Birthdate,5);
