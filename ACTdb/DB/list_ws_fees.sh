#! /bin/bash
./interact >list_ws_fees.csv <<EOF
select act_project.Proj_ID, Proj_Name, Proj_end, Role_Fee, Hours FROM act_project
  JOIN act_role ON act_project.Proj_ID = act_role.Proj_ID AND Role_Name = 'Registration'
  JOIN (
    SELECT act_event.Proj_ID, SUM(TIME_TO_SEC(TIMEDIFF(Event_End,Event_Start)))/3600 AS Hours
      FROM act_event
    GROUP BY Proj_ID
  ) AS DT ON act_project.Proj_ID = DT.Proj_ID
  WHERE Proj_Type = 'Workshop' and
        not Proj_Name LIKE '%cancelled' and
        ( Proj_Name LIKE '%After School%' OR
          ( not Proj_Name LIKE '%Program%' and
            not Proj_Name LIKE '%Vacation%' )
        );
EOF
