#! /bin/perl -w

my %ws;
my %ages;
#--------------------------------------------
# ids.txt is a colon-delimited text file with
# Role_ID:Ages:Title
#--------------------------------------------
open( IDS, "<ids.txt" ) || die "Cannot open ids.txt\n";
while (<IDS>) {
  chomp $_; chomp $_;
  if ( m/^(\d+):([^:]+):(.+)$/ ) {
    $ws{$3} = $1;
    $ages{$2} = [] unless $ages{$2};
    push(@{$ages{$2}}, $3 );
    #warn "name: '$3'\n";
  } else {
    warn "Line did not match: '$_'\n";
  }
}
close IDS || warn "Error closing ids.txt\n";

open(HTML, "</Desktop/workshops.html" ) || die "Cannot open /Desktop/workshops.html";
while (<HTML>) {
  if ( m|<dt>([^<]+)</dt>| ) {
    my $name = $1;
    my $id = $ws{$name} || 0;
    if ( $id ) {
      s|<dt>([^<]+)</dt>|<dt><a name="ID$id"></a>$1</dt>| || die;
      print $_;
      while (<HTML>) {
	if ( s|(</dd>.*)$|| ) {
	  my $tail = $1;
	  print $_, '<form action="http://act.arlington.ma.us/cgi-bin/ACTdb/Register">', "\n",
	    '<input type="hidden" name="Role_ID" value="', $id, '">', "\n",
	    '<input type="submit" name="_submit" value="Register Now">', "\n",
	    "</form>\n", $tail;
	  last;
	} else {
	  print $_;
	}
      }
    } else {
      warn "Workshop name did not match: '$name'\n";
    }
  } else {
    print $_;
  }
}

my @ages = map $_->[2],
  sort { $a->[0] <=> $b->[0] || $a->[1] <=> $b->[1] }
  map { m/^(\d+)-(\d+)/ ? [ $1, $2, $_ ] : () } keys %ages;

print "ages:\n", map "  $_\n", @ages;

for my $age ( @ages ) {
  my $htmlage = $age;
  $htmlage =~ s/-/&ndash;/;
  print "<tr><th class=\"Top\">$htmlage</th></tr>\n";
  my @names = sort { $ws{$a} <=> $ws{$b} } @{$ages{$age}};
  for my $name ( @names ) {
    my $id = $ws{$name};
    print "<tr><td><a href=\"#ID$id\">$name</a></td></tr>\n";
  }
}
