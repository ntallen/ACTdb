#! /bin/bash
./interact >maillist.txt <<EOF
# Selects families that have been active within the last two years.
SELECT CONCAT(FamilyName, ' Family') AS Name, Address1, Address2,
  CONCAT( City, ', ', State, ' ', Zip ) AS Address3, MAX(Proj_End) AS Last
FROM ACT_Project NATURAL JOIN ACT_Role NATURAL JOIN ACT_Cast NATURAL JOIN ACT_Participant
NATURAL JOIN ACT_Family NATURAL JOIN ACT_Address
WHERE
  (Proj_End = '0000-00-00' OR Date_Add(Proj_End, INTERVAL 5 YEAR) > CURDATE() )
  AND Address1 <> '' AND City <> '' AND State <> '' AND Zip <> ''
  AND Cast_Status = 'Accepted'
GROUP BY ACT_Family.Fam_ID
ORDER BY State, City, Address1, FamilyName;
EOF
