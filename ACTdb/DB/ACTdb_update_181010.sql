DROP TABLE IF EXISTS `ACT_Text_Keys`;
CREATE TABLE `ACT_Text_Keys` (
  `Text_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Text_Name` char(50) NOT NULL DEFAULT '',
  `Text_Description` TEXT NOT NULL,
  PRIMARY KEY (`Text_ID`),
  UNIQUE KEY `Text_Name` (`Text_Name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `ACT_Text_Text`;
CREATE TABLE `ACT_Text_Text` (
  `Text_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Proj_ID` int(11) NOT NULL,
  `Text_Text` TEXT NOT NULL,
  PRIMARY KEY (`Text_ID`,`Proj_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO ACT_Text_Keys (Text_ID, Text_Name, Text_Description) VALUES
  (1, 'Waitlist_Email','Text after "{Name} has been placed on the waiting list for {Event}." in confirmation email'),
  (2, 'Moderated_Email','Text after "{Name} has applied for {Event}." in confirmation email'),
  (3, 'Offstage_Email', 'Text to display after {Waitlist_Email} for shows'),
  (4, 'Withdrawn_Email', 'Text to display after "{Name} has withdrawn from {Event}". No default.'),
  (5, 'Custom_Payment_Text', 'Text describing the fee: Default is "The fee is ${fee}, due {fee_when}".'),
  (6, 'Pay_Instructions', 'Instructions on how to pay. Complete paragraph(s) ending with newline'),
  (7, 'Custom_Refund_Text', 'Instructions overriding Cancel_100 etc. Should end with newline'),
  (8, 'Audition_Text', 'Instructions for signing up for auditions. End with double newline.'),
  (9, 'Annual_Fund_Text', 'Annual fund solicitation. End with double newline.'),
  (10, 'Opening_Email', 'The first paragraph of all registration emails. End with double newline.'),
  (11, 'Closing_Email', 'The last paragraph of all registration emails. End with newline.');

INSERT INTO ACT_Text_Text (Text_ID, Proj_ID, Text_Text) VALUES
  (1, 0, 'All currently available spaces have been filled. We will let you know if a space becomes available.'),
  (2, 0, 'Available spaces are limited. We will select the cast at the end of the registration period, and we will let you know as soon as possible after that whether you are accepted into the show or placed on the waiting list.'),
  (3, 0, "ACT offers many opportunities to participate in the creation of each show in addition to acting. These are described on our website. If a space in the show does not open up, and you would be interested in being a part of the show by helping out offstage, please contact us."),
  (6, 0, "Checks can be made payable to:

  Arlington Children's Theatre
  115 Massachusetts Avenue
  Arlington MA, 02474.
  
You can also pay online here:

  https://act.arlington.ma.us/Payments.html

"),
  (8, 0, "Please use the link below to go to our web site and select an audition time. You will need to log in, either using a username and password or using your name and e-mail address.

  https://act.arlington.ma.us/cgi-bin/ACTdb/Auditions

"),
  (9, 0, "The mission of ACT is to provide a quality theater program to as many children as possible. We strive to keep our registration fees low even though many parents have told us that they would be willing to pay a higher fee. At the same time, you should be forewarned that there may be additional fees associated with this production. As with all ACT workshops and shows, ACT is willing to help out anyone who cannot afford any portion of our fees. If you agree with and support our mission to be accessible and inclusive of all families, please consider contributing to our annual fund and send in your contribution along with your registration fee. Remember that your financial support allows other families to participate and also ensures that ACT will be able to continue providing these valuable programs.

" ),
  (10, 0, "Thank you for your ACT registration.

"),
  (11, 0, "If you have any questions, please feel free to contact us at info@act.arlington.ma.us or (781)316-8090.
" ),


  (2, 545, '

Registration will close November 9th and we will be in touch with a rehearsal schedule and to organize an audition time by November 14th.  An informational Meeting will be held November 15th from 6:30-7:30 at ACT/TRINITY to answer any questions you might have about the production, process or auditions.

Auditions will take place November 19th, 20th 6:00-9:00 and November 24th 2:00-6:00. Please prepare a one-minute comedic monologue and 32 bars (1.2-2 mins) of a contemporary musical theatre or pop song. Auditionees may be asked to read from the script. Callbacks will take place November 25th 5:30-8:00 (IF NECESSARY). The cast will be notified by November 26th and the first rehearsal will take place on November 27th.

A registration fee of $175.00 ($30.00 with a valid Student ID) will be due at the first rehearsal.

If you have any questions or need any assistance, please do not hesitate to contact director Joseph C. Walsh at joewalsh@act.arlington.ma.us.'),
  (5, 545, "The fee is $175 or $30 with a valid student ID, and will be due at the first rehearsal."),
  (11, 545, "You may also send questions to ACT at info@act.arlington.ma.us or (781)316-8090.
" )
;

# Sample query implemented in GetProjText() in ACT.pm
# SELECT IF(ISNULL(T1.Text_Text),IF(ISNULL(T2.Text_Text),'',T2.Text_Text),T1.Text_Text) AS Text_Text
# FROM ACT_Text_Keys LEFT JOIN ACT_Text_Text AS T1
#   ON ACT_Text_Keys.Text_ID = T1.Text_ID AND T1.Proj_ID = 545
#   LEFT JOIN ACT_Text_Text AS T2
#   ON ACT_Text_Keys.Text_ID = T2.Text_ID AND T2.Proj_ID = 0;
