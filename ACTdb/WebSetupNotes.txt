April 27, 2013
  Established Account
    Discarded passwords for a2hosting account
    Brp7^8fblU
    z0u8W2fptb
  VPS Host: server.actarlingtonma.us
    IP: 75.98.169.104
    SSH Port: 7822
  
  [x] Create OS user(s)
        webmaster: will run daily backup and cron reports
          Set password (lastpass)
            Might want to revert to memorable pw if I need to sudo
          Setup shared key from nortt400
  [x] MySQL Configuration
        Root password in /root/.my.cnf
        Look at DB/admin.txt for GRANT rules
        Need to figure out how to limit access to MySQL
        
        /etc/my.cnf
          [mysqld]
          skip-networking (else could set bind-address)
          # skip-innodb (innodb is the new default, and I've used it, but
          # I am not actually using tranactions, and it does take more
          # memory)
  [x] Web checkout
      [x] svn checkout --username=ntallen http://act.arlington.ma.us/svn/actweb/trunk/public public
          note: ntallen, not ntallen-act
          note: act.arlington.ma.us, not bellevue, which will present problems when we move.
      [x] Copy and unpack the non-svn part of the site
  [x] Apache Configuration
      [x] DocumentRoot
      [ ] cgi-bin
      [x] SSL Stuff
          [x] yum install mod_ssl
      [ ] Rewrite to force https
  [x] Database Initialization
      [x] mkdir build; cd build
      [x] svn checkout --username=ntallen http://act.arlington.ma.us/svn/actdb/trunk/ACTdb ACTdb
          note: ntallen, not ntallen-act
          note: act.arlington.ma.us, not bellevue, which will present problems when we move
      [x] Create database users (create new passwords):
            cp grants.sql newgrants.sql
            vi newgrants.sql
            # Update passwords, database name, database users as necessary
            # Propagate that info to .my.app.cnf, my.arch.cnf and ACT_local.pm
            sudo mysql <newgrants.sql
  [x] Perl Configuration
      [x] As root install
          .yum install perl-CPAN
          .yum install libxml2-devel
          cpan>install YAML
          cpan>install CGI
          cpan>upgrade LWP::UserAgent
      [x] Install prereqs, etc.
          .export PERL5LIB=/home/webmaster/local/lib
          .cpan
            PREFIX=/home/webmaster/local LIB=/home/webmaster/local/lib
          .cpan>install HTML::Template
          #cpan>install Text::Wrap (Already there, but installed anyway)
          .cpan>install IO::Socket::SSL
          .cpan>install Mozilla::CA
          .cpan>install OLE::Storage-Lite
          .cpan>install Spreadsheet::WriteExcel (include Parse::RecDescent (or something))
          .cpan>install XML::Lib_XML
          .cpan>install CGI::FormBuilder
          .install CGI::NForm (from svn)
          .install ACTdb (from svn)
            Setup ACT_Local.pm:
              Database = ACT
              DBuser = act_app
              DBpasswd = .....
              Set permissions on tmpdir so apache can write
            make install
            copy cgi/ACTdb to ~/cgi-bin and edit, adding "use lib";
  [x] Add cgi-bin to SSL config
  [x] Add RewriteRule to http config
  [x] Setup Daily Backup, registration report
      Primary site should rsync backups to secondary
  [ ] Transfer subversion repositories
  [ ] Come up with a strategy for backing up subversion
      How about switch to git? Then just clone.

Contacts and Accounts
  DNS
    act.arlington.ma
      Joe Miksis, miksis@town.arlington.ma.us Town of Arlington is listed as
      contact according to WhoIs. He refers requests to Rich Brennan of
      Connactivity.com, who hosts DNS for the town. Our nameserver (NS)
      records live in the arlington.ma.us DNS and currently point to:
        @        IN NS    dns1.easydns.com.
        @        IN NS    dns2.easydns.net.
        @        IN NS    dns3.easydns.ca.
      
    EasyDNS https://web.easydns.com
      Username: ntallen
      Password: lastpass
  Textdrive: http://textdrive.com
    Host: bellevue.textdrive.us
    User: actwebmaster
    Password: lastpass
  A2 Hosting:
    Host: 75.98.169.104
    Username: root, webmaster
    Passwords: lastpass secure note
  PayPal
  Brown Paper Tickets
  