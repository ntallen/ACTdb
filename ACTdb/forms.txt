Forms.txt defines which forms (or views) will be needed.
This will mirror the Scenarios in db.txt

Authentication:

login
  username
  password
  Alternatives

Registration
  In all cases, carry along what it is we want to
  register for.
  Cases:
    Have a username/password
	  Enter username/password and proceed to appropriate page

	Forgot username/password
	  FirstName
	  LastName
	  e-mail address
	  
	    If e-mail address matches, create a random authkey and
		mail it. If not, inform admins, who can check it out
		and follow up.

	Have been in an ACT production
	Totally new
      LastName
	  FirstName
	  e-mail
	  phone
	  new username
	  new password
	  confirm password
	  Have you previously been involved in ACT?
	    (If so, admin should clean up the DB)
        [Create Family using LastName and this one participant]
	
  Family Page:
    The <FamilyName> Family:
	  <Address>
	  <HomePhone>

	  Adults:
	    <FirstName> <LastName> <Address> <HomePhone> <WorkPhone> <Email>
		[Edit/Delete]
		[Add Adult]

      Children:
		<FirstName> <LastName> <Address> <HomePhone> <WorkPhone> <Email>
		[Edit/Delete]
		[Add Child]

    Emergency Contact Info
	  [Not required for registration, but must be filled
	   in or confirmed by first meeting]