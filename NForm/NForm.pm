package CGI::NForm;

use 5.00503;
use strict;
use vars qw($VERSION @ISA);
@ISA = qw();

$VERSION = '0.04';

use CGI;
use CGI::Carp;

# _get_def($pi,$def) takes a ref to a string and
# a hashref. The string path information, so it begins
# with a '/'. _get_def tries to find the first node in
# the refrenced hash, and returns the associated value.
# It also strips the first level from $$pi. Could this
# also handle wildcards?
sub _get_def {
  my ( $pi, $def ) = @_;
  my $key = '';
  if ( $$pi =~ s|^/(\w+)((/.*)?)$|$2| ) {
	$key = $1;
  }
  if ( ref($def) ) {
	return $def->{$key} if $def->{$key};
	return "$def->{'*'}$key" if $def->{'*'};
	return $def->{''}; # may be undef
  } else {
	return $def;
  }
}

# sub _get_def {
#   my ( $pi, $def ) = @_;
#   my $val;
#   if ( ref($def) ) {
#     my @keys = grep /\w+/, keys %$def;
#     my $pat = join '',
#       '^/(',
#       join( '|', @keys ),
#       ')((?:/.*)?)$';
#     if ( $$pi =~ s/$pat/$2/ ) {
#       $val = $def->{$1};
#     } else {
#       $val = $def->{''}; # may be undef
#     }
#   } else {
#     $val = $def;
#   }
#   $val;
# }  

sub new {
  my $this = shift;
  my $class = ref($this) || $this;
  my $self = bless { errors => [], confirm => [], @_ }, $class;
  return $self;
}

# CGI::NForm::ACT->Process( $path_info, $params );
# $path_info and $params can be passed if the default info
# from the environment is not desired (such as if moving
# to a new form after processing the current one).
# $path_info is a string and $params is an argument
# suitable for passing to new CGI. These are passed
# sight unseen to CGI::NForm->PickForm.
sub Process {
  my ( $this, $path_info, $params, @xtranew ) = @_;
  my $self = $this->new( @xtranew );
  $self->PickForm( $self->AppDef(), $path_info, $params );
  exit(0);
}

# PickForm switched to object-invocation. If invoked on an
# existing object, a new object will not be created.
sub PickForm {
  my ( $self, $formdef, $path, $params ) = @_;
  $self = $self->new unless ref($self);
  my $pi = $self->{path_info} =
    defined $path ? $path : ( $ENV{PATH_INFO} || '' );
  my $formpkg = _get_def( \$pi, $formdef );
  $self->fatal("No Form Package default in CGI::NForm::PickForm")
    unless $formpkg;
  eval "require $formpkg";
  $self->fatal( $@ ) if $@;
  bless $self, $formpkg; # rebless into designated package
  my $op = _get_def( \$pi, $self->op_def ) || '';
  $self->fatal( "Invalid Request:",
      "PATH_INFO = '$self->{path_info}'",
      "formpkg = '$formpkg'",
      "pi = '$pi'",
      "op = '$op'" )
    if ( $pi || ! $op );
  $self->fatal( "Misconfiguration in CGI::NForm::PickForm:",
        "op = '$op'" )
    if $op !~ m/^INSERT|UPDATE|DELETE|SELECT$/;
  $self->{op} = $op;
  my $form = $self->{form} = $self->get_form( $op, $params );
  $self->authorize( $op ) ||
	$self->fatal( "Not authorized for this operation" );
  my $q = new CGI('');
  if ( $form->submitted ) {
    if ( $self->$op ) {
      print
	$q->header,
	$q->start_html( -title => 'NForm Confirmation' ),
        $form->confirm,
	$q->end_html;
      exit 0;
    }
  } else {
    $self->InitFields($op);
  }
  $self->InitParams($op);
  # if ( $form->{opt}->{template} ) {
  if ( $form->template ) {
    $form->tmpl_param( "NForm-errors", $self->pack_errors );
    $form->tmpl_param( "NForm-confirm", $self->pack_confirm );
    print
      $q->header,
      $form->render;
  } else {
    print
      $q->header,
      $q->start_html( -title => 'NForm page', -style => {
           -code => '.NForm_error { background-color: red }'
         }
       ),
      $self->pack_errors,
      $self->pack_confirm,
      $form->render,
      $q->end_html, "\n";
      # "<table>",
	# "<tr><th>Var</th><th>Val</th></tr>\n",
	# map( "<tr><th bgcolor=\"gray\" align=\"left\">$_</th><td>$ENV{$_}</td></tr>\n",
	#   sort keys %ENV ),
      # "</table>\n",
  }
}

sub get_form {
  my ( $self, $op, $params )  = @_;
  my $q = $self->{q} = ref($params) eq 'CGI' ? $params : new CGI($params);
  my $form = $self->{form} = CGI::FormBuilder->new(
    action => "$ENV{SCRIPT_NAME}$self->{path_info}",
    method => 'POST',
    params => $q );
  $self->InitForm( $form, $op );
  return $form;
}

sub pack_errors {
  my $self = shift @_;
  if ( @{$self->{errors}} ) {
    return join '',
      '<table class="NForm_error"><tr><th>', "\n",
      "Errors Observered:",
      "</th></tr>\n",
      "<tr><td>\n",
      "<ul>\n",
      map( "  <li>$_</li>\n", @{$self->{errors}} ),
      "</ul>\n",
      "</td></tr></table>\n";
  } else {
    return '';
  }
}

sub pack_confirm {
  my $self = shift @_;
  if ( $self->{confirm} && @{$self->{confirm}} ) {
    return join '',
      '<table class="NForm_confirm"><tr><th>', "\n",
      "Confirmation:",
      "</th></tr>\n",
      "<tr><td>\n",
      "<ul>\n",
      map( "  <li>$_</li>\n", @{$self->{confirm}} ),
      "</ul>\n",
      "</td></tr></table>\n";
  } else {
    return '';
  }
}

sub fatal {
  my ( $self, @errors ) = @_;
  $self = $self->new unless ref($self);
  warn( "In fatal: ", @errors, "\n");
  my $q = new CGI('');
  unshift( @{$self->{errors}}, "Non-fatal errors included:" )
    if @{$self->{errors}};
  unshift( @{$self->{errors}}, @errors );
  print
    $q->header,
    $q->start_html( -title => 'NForm page', -style => {
         -code => '.NForm_error { background-color: red }'
       }
     ),
    "<h1>Fatal Error</h1>\n",
    $self->pack_errors,
    $q->end_html, "\n";
  exit(0);
}


# $self->update_query( $uri_encoded_query_string, @replacement_attrs );
# $self->update_query( { attr => value }, @replacement_attrs );
# $self->update_query( [ attr => value ], @replacement_attrs );
# returns a new uri-encoded query string (e.g.
#   'foo=bar&fud=a+short+phrase' )
sub update_query {
  use URI::Escape;
  
  my ( $self, $attrs, @newattrs ) = @_;
  my @oldattrs;
  if ( ref $attrs eq 'HASH' ) {
    @oldattrs = ( %$attrs );
  } elsif ( ref $attrs eq 'ARRAY' ) {
    @oldattrs = @$attrs;
  } elsif ( ref $attrs eq '' ) {
    @oldattrs = (
      map { s/\+/ /g; uri_unescape($_); }
	map split( '=', $_ ),
	  split( '&', $attrs ) );
  } else {
    die "Invalid attr arg in update_query";
  }
  die "Odd args in update_query"
    if @newattrs % 2;
  push @newattrs, _sessionid => $self->{form}->sessionid
    if $self->{form} && $self->{form}->sessionid;
  my %newattrs = @newattrs;
  my @keep_idx =
    grep ! defined $newattrs{$oldattrs[$_]},
      map $_*2, (0..(@oldattrs/2-1));
  return $self->build_query(
    map( @oldattrs[$_,$_+1], @keep_idx ), @newattrs
  );
}

sub form_params {
  my $self = shift;
  my $form = $self->{form};
  my @form_params =
    map { my $fld = $_->name;
	  ( $_->static ? () :
	    map( ($fld => $_),
		 $form->field($fld) ) );
	} $form->field;
}

# Call( $PI, @params )
# Call( $PI, [ PA_params ], @params );
# [ PA_params ] are values to add to this view when 'Return' is
# called.
sub Call {
  my ( $self, $PI, @params ) = @_;
  my @PA_params;
  if ( @params && ref($params[0]) eq 'ARRAY' ) {
    my $p_ref = shift @params;
    @PA_params = @$p_ref;
  }
  #$self->redirect( $PI,
  #  nxtPI => $self->{path_info},
  #  nxtPA => scalar($self->build_query( $self->form_params, @PA_params )),
  #  _sessionid => $self->{form}->sessionid,
  #  @params );
  $self->Process( $PI,
    { nxtPI => $self->{path_info},
      nxtPA => scalar($self->build_query( $self->form_params, @PA_params )),
      _sessionid => $self->{form}->sessionid,
      @params } );
}

sub Return {
  my $self = shift;
  my $form = $self->{form};
  my $nxtPI = $form->field('nxtPI') || '';
  my $nxtPA = $form->field('nxtPA') || '';
  # $self->redirect( $nxtPI,
  #   $self->update_query( $nxtPA, @_ ) );
  $self->Process( $nxtPI,
    $self->update_query( $nxtPA, @_ ) );
}

sub GoTo {
  my ( $self, $PI, @params ) = @_;
  my $form = $self->{form};
  my $nxtPI = $form->field('nxtPI') || '';
  my $nxtPA = $form->field('nxtPA') || '';
  # $self->redirect( $PI, nxtPI => $nxtPI, nxtPA => $nxtPA,
  #	_sessionid => $form->sessionid, @params );
  $self->Process( $PI, { nxtPI => $nxtPI, nxtPA => $nxtPA,
	_sessionid => $form->sessionid, @params } );
}

# $self->redirect( $PI, @params )
# redirects to this script with the specified PATH_INFO (If m,^/,)
# or to another script ( if m,^\w+:, )
# and @params
sub redirect {
  my ( $self, $PI, @params ) = @_;
  my $script_uri;
  if ( $PI =~ m/^\w+:/ ) {
    $script_uri = $PI;
  } else {
    $script_uri = $self->script_uri . $PI;
  }
  my $uri = $script_uri;
  my $query = $self->build_query( @params );
  $uri .= "?$query" if defined $query && $query ne '';
  print "Status: 302 Redirect\cM\cJLocation: $uri\cM\cJ\cM\cJ";
  exit(0);
}

# build_query takes an array of parameter/value pairs. It omits any
# parameters with empty values, and it supports multi-valued parameters.
# In list context, it will return and empty array if there are no
# params.
sub build_query {
  use URI::Escape;
  my $self = shift;
  return wantarray ? @_ : shift if @_ < 2;
  my @attrs = map {
      my $val = uri_escape(defined($_) ? $_ : '', "^A-Za-z0-9\-_.!~*'() ");
      $val =~ s/ /+/g;
      $val; } @_;
  return
    join '&',
      map "$attrs[$_]=$attrs[$_+1]",
	grep defined $attrs[$_+1] && $attrs[$_+1] ne '',
	  map $_*2, (0..(@attrs/2-1));
}

sub script_uri {
  my $self = shift;
  my $method = ( $ENV{HTTPS} && $ENV{HTTPS} eq 'on' ) ?
    'https' : 'http';
  my $port = ( $ENV{SERVER_PORT} && $ENV{SERVER_PORT} != 80 ) ?
	":$ENV{SERVER_PORT}" : '';
  my $host = $ENV{HTTP_HOST} || 'localhost';
  my $script = $ENV{SCRIPT_NAME} || '/cgi/safety';
  return "$method://$host$port$script";
}

sub error {
  my $self = shift;
  push @{$self->{errors}}, @_;
  return 0;
}

sub confirm {
  my $self = shift;
  push @{$self->{confirm}}, @_;
  return 0;
}

1;
__END__

=head1 NAME

CGI::NForm - Simple Database Framework

=head1 SYNOPSIS

    package CGI::NForm::MyApp;
    use CGI::NForm;
    our @ISA = qw(CGI::NForm);
    
    sub Process  {
      my $nform = CGI::NForm::MyApp->new(
	tmpldir => '/var/www/htdocs/cgi' );
      $nform->PickForm(
	{ form1 => 'MyApp::form1',
	  form2 => 'MyApp::form2' });
    }
    
    sub fatal { ... }
    
    package CGI::NForm::MyApp::form1;
    our @ISA = qw(CGI::NForm::MyApp);
    
    $nform->Call( $PI, @params );
    $nform->Return( @params );
    $nform->fatal( "Not authorized to view page" );
    $nform->pack_errors;
    $nform->form_params;
    $nform->update_query( $PA, newparam => 'newval' );
    $nform->redirect( $PI, $PA, @params );

=head1 ABSTRACT

  CGI::NForm provides a simple framework for a multi-form database
  application using CGI::FormBuilder and HTML::Template. The
  choice of form and operation is determined using
  $ENV{PATH_INFO}, which can be queried prior to invoking CGI.

=head1 DESCRIPTION

  CGI::NForm recognizes the fact that most database applications need
  to follow the same basic operating scenario.

    Check authorization
    Define the Form
    If Submitted {
      If Validated && Update_Succeeded {
	print confirmation and/or next screen
	exit
      }
    } else {
      initialize_fields_from_database
    }
    print Form

  CGI::NForm provides this structure, leaving it to the application
  to fill in the particular methods.

=head2 ATTRIBUTES

  $nform->{errors} is an array-ref of error strings.
  $nform->{path_info} holds the PATH_INFO which determines which
                      form to use
  $nform->{form} holds the CGI::FormBuilder object
  
  Attributes of the form _NF_* are reserved for internal use by
  CGI::NForm. Classes inheriting from CGI::NForm can define
  additonal attributes.

=head2 CLASS METHODS

=item new

  $nform = CGI::NForm->new( %attrs );
  $newnform = $nform->new( %attrs );

  Only attributes not reserved by CGI::NForm can be set.
  
=item PickForm

  CGI::NForm->PickForm( $pkg_def, $path_info, $params );
  $nform->PickForm( $pkg_def, $path_info, $params );
  $nform->PickForm(
    { form1 => 'CGI::NForm::MyApp::form1',
      form2 => 'CGI::NForm::MyApp::form2' });

  PickForm() is the top-level method that performs the
  entire operation. It takes two arguments that define how to
  interpret the $ENV{PATH_INFO}. The assumption is that PATH_INFO
  is of the form /form/op.
  
  The first argument, $pkg_def, can be either a package name or
  a hash ref mapping words to packages. In the latter case, the
  matching word is stripped from the beginning of PATH_INFO
  before the /op portion is interpreted. If your
  application has only a single form, you can specify its package
  directly. The package name
  
  Once the appropriate package has been selected,
  CGI::NForm::PickForm() creates an object, $self, of that package, and
  uses it to invoke appropriate methods during the operation.
  Methods it will call are listed below.

=item $self->fatal( 'A bad thing happened' )

  Produces a complete web page displaying the error message(s).
  A simple stub is provided by CGI::NForm, but this should be
  customized to use a suitable template. (Maybe all that needs
  to be specified is a suitable template, and pack_errors could
  do the rest?) This can be invoked either as a class method or
  as an object method. In the former case, an object is created
  as a convenience.

=head2 Methods to be defined in the form package

=item $self->op_def

  Returns either a operation string or a hash ref defining the
  mapping from strings in the PATH_INFO to operation strings.
  The operation strings must be one of INSERT, DELETE, UPDATE or
  SELECT, corresponding roughly to the SQL operations.

=item $self->get_form($op)

  get_form() returns a CGI::FormBuilder object. This is assigned to
  $self->{form} and can be used by all subsequent methods except
  fatal().

=item $self->authorize($op)

  authorize( $op ) should return non-zero (true) if the user is
  authorized to perform the specified operation. If it returns
  zero, $self->fatal() will be called with an appropriately
  generic error message. If you want to take some other action,
  you can do so within authorize() and exit without returning.

=item $self->INSERT, $self->UPDATE, $self->DELETE, $self->SELECT

  These methods are charged with validating the inputs and if
  successful, carrying out the specified operation on the
  database, returning TRUE if the operation succeeds. The
  validation process can call the CGI::FormBuilder validate
  function and/or perform higher-level checks. Errors should
  accumulate in $self->{errors}, and will be packed up and
  added to an appropriate tmpl_var before the form is rendered.
  Field-specific validation errors can stay with their fields,
  but should probably produce a "See errors within the form" or
  similar text at the top.

=item $self->InitFields()

  InitFields() sets values for the fields of the form based on
  the current contents of the database. It can use values of key
  fields as appropriate to the application. Any errors
  encountered should be pushed onto @{$self->{errors}}

=item $self->InitParams()

  Called after INSERT, UPDATE, SELECT, DELETE or InitFields is
  called. Always called before the form is rendered.

=item $self->pack_errors

  This method collects @{$self->{errors}} and returns a single
  string of encoded HTML for displaying the errors. It will only
  be called if there is at least one error. The resulting HTML
  will be packed into an appropiate tmpl_var for rendering.
  A default version is available as CGI::NForm::pack_errors which
  could be called from within your method, or you could inherit
  from CGI::NForm.

=head1 SEE ALSO

CGI::FormBuilder, HTML::Template, CGI

=head1 AUTHOR

Norton Allen, E<lt>allen@huarp.harvard.edu<gt>

=head1 COPYRIGHT AND LICENSE

Copyright 2003 by Norton Allen

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself. 

=cut
